package com.shujia.phoenixcode;

import java.sql.*;

public class PhoenixJDBC {
    public static void main(String[] args) throws SQLException {
        // 1.给定Phoenix驱动
//        DriverManager.registerDriver("com.");
        // Phoenix中的驱动并不需要手动指定

        // 2.创建连接
        Connection connection = DriverManager.getConnection("jdbc:phoenix:master:2181");

        // 3.获取操作对象
        // 4.执行SQL
//        connection.createStatement()
        PreparedStatement preparedStatement = connection.prepareStatement("select x,y,county from dianxin where x=? and y =?");

        preparedStatement.setDouble(1,117.288);
        preparedStatement.setDouble(2,31.822);

        ResultSet resultSet = preparedStatement.executeQuery();

        // 5.遍历结果
        while (resultSet.next()){
            double x = resultSet.getDouble(1);
            double y = resultSet.getDouble(2);
            String county = resultSet.getString(3);
            System.out.println("x:"+x+"\ty:"+y+"\tcounty:"+ county);
        }


        // 6.关闭
        preparedStatement.close();
        connection.close();
    }
}
