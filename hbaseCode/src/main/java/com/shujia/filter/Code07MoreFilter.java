package com.shujia.filter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


//  对于文科班 取出学生编号以15001008开头，学生成绩大于450分
public class Code07MoreFilter {
    public static void main(String[] args) throws IOException {
        Configuration configuration = new Configuration();
        configuration.set("hbase.zookeeper.quorum","node1,node2,master");

        Connection hbaseCon = ConnectionFactory.createConnection(configuration);

        // 对命名空间进行操作属于对于元数据信息操作，那么API中元数据操作的类对象为Admin

        Admin admin = hbaseCon.getAdmin();

        // 对具体的数据进行操作，需要获取Table对象
        TableName tableName = TableName.valueOf("api:student");

        if(admin.tableExists(tableName)) {
            Table table = hbaseCon.getTable(tableName);

            Scan scan = new Scan();

//            ArrayList<Filter> filters = new ArrayList<>();

            FilterList filterList = new FilterList();

            // 文科班 取出学生编号以15001008开头，学生成绩大于450分

            // 文科班需要取出class 列 对列值进行判断过滤

            /**
             * 由于需要对列值进行模糊匹配，所以需要选择使用如下方式构建对象
             * public SingleColumnValueFilter(final byte [] family, final byte [] qualifier,
             *       final CompareOp compareOp, final ByteArrayComparable comparator)
             */
            SingleColumnValueFilter clazzFilter = new SingleColumnValueFilter(
                    Bytes.toBytes("info"),
                    Bytes.toBytes("clazz"),
                    CompareFilter.CompareOp.EQUAL,
                    new RegexStringComparator("^文科")
            );
            filterList.addFilter(clazzFilter);


            // 学生编号以15001008开头
            PrefixFilter prefixFilter = new PrefixFilter(Bytes.toBytes("15001008"));
            filterList.addFilter(prefixFilter);

            // 学生成绩大于450分
            /**
             * public SingleColumnValueFilter(final byte [] family, final byte [] qualifier,
             *       final CompareOp compareOp, final byte[] value) {
             */
            SingleColumnValueFilter scoreFilter = new SingleColumnValueFilter(
                    Bytes.toBytes("score"),
                    Bytes.toBytes("total_score"),
                    CompareFilter.CompareOp.GREATER,
                    Bytes.toBytes(450)
            );

            filterList.addFilter(scoreFilter);


            // 一般函数取名时，对于单个的值进行设置 叫做set  对于多个值进行添加时 叫做add
            scan.setFilter(filterList);

            ResultScanner scanner = table.getScanner(scan);

            for (Result result : scanner) {
                printRes(result);
            }

            table.close();
            admin.close();
            hbaseCon.close();

        }
    }
    public static void printRes(Result result){
        List<Cell> cells = result.listCells();

        for (Cell cell : cells) {
            String family = Bytes.toString(CellUtil.cloneFamily(cell));
            String column = Bytes.toString(CellUtil.cloneQualifier(cell));
            String rowKey = Bytes.toString(CellUtil.cloneRow(cell));
            if (column.equals("age") || column.equals("total_score") ){
                int value = Bytes.toInt(CellUtil.cloneValue(cell));
                System.out.println("rowKey:"+rowKey+" family:"+family+" column:"+column+" value:"+value);
            }else{
                String value = Bytes.toString(CellUtil.cloneValue(cell));
                System.out.println("rowKey:"+rowKey+" family:"+family+" column:"+column+" value:"+value);
            }
        }

    }
}
