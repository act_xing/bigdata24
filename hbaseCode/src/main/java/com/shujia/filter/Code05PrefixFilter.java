package com.shujia.filter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.PrefixFilter;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.List;

public class Code05PrefixFilter {
    public static void main(String[] args) throws IOException {
        Configuration configuration = new Configuration();
        configuration.set("hbase.zookeeper.quorum","node1,node2,master");

        Connection hbaseCon = ConnectionFactory.createConnection(configuration);

        // 对命名空间进行操作属于对于元数据信息操作，那么API中元数据操作的类对象为Admin

        Admin admin = hbaseCon.getAdmin();

        // 对具体的数据进行操作，需要获取Table对象
        TableName tableName = TableName.valueOf("api:student");

        if(admin.tableExists(tableName)) {
            Table table = hbaseCon.getTable(tableName);

            Scan scan = new Scan();

            /**
             * public RowFilter(final CompareOp rowCompareOp,
             *       final ByteArrayComparable rowComparator)
             */

            // 以RowKey前缀的方式获取15001009为开头的学生编号
            PrefixFilter prefixFilter = new PrefixFilter(Bytes.toBytes("15001009"));
            // 一般来说，对于RowKey进行做数据过滤时，其效率较高，因为Hbase中的数据是按照ROWKey进行排序，对有序集合进行取值速度快
            scan.setFilter(prefixFilter);

            ResultScanner scanner = table.getScanner(scan);

            for (Result result : scanner) {
                printRes(result);
            }

            table.close();
            admin.close();
            hbaseCon.close();
        }
    }
    public static void printRes(Result result){
        List<Cell> cells = result.listCells();

        for (Cell cell : cells) {
            String family = Bytes.toString(CellUtil.cloneFamily(cell));
            String column = Bytes.toString(CellUtil.cloneQualifier(cell));
            String rowKey = Bytes.toString(CellUtil.cloneRow(cell));
            if (column.equals("age") || column.equals("total_score") ){
                int value = Bytes.toInt(CellUtil.cloneValue(cell));
                System.out.println("rowKey:"+rowKey+" family:"+family+" column:"+column+" value:"+value);
            }else{
                String value = Bytes.toString(CellUtil.cloneValue(cell));
                System.out.println("rowKey:"+rowKey+" family:"+family+" column:"+column+" value:"+value);
            }
        }

    }
}
