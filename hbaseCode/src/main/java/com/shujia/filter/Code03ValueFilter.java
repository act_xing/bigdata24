package com.shujia.filter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueExcludeFilter;
import org.apache.hadoop.hbase.filter.ValueFilter;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.List;

// 需求：取出所有值等于23的数据
public class Code03ValueFilter {
    public static void main(String[] args) throws IOException {
        Configuration configuration = new Configuration();
        configuration.set("hbase.zookeeper.quorum","node1,node2,master");

        Connection hbaseCon = ConnectionFactory.createConnection(configuration);

        // 对命名空间进行操作属于对于元数据信息操作，那么API中元数据操作的类对象为Admin

        Admin admin = hbaseCon.getAdmin();

        // 对具体的数据进行操作，需要获取Table对象
        TableName tableName = TableName.valueOf("api:student");

        if(admin.tableExists(tableName)) {
            Table table = hbaseCon.getTable(tableName);

            Scan scan = new Scan();


            /**
             * public ValueFilter(final CompareOp valueCompareOp,
             *       final ByteArrayComparable valueComparator) {
             */

            // 该比较器是从所有的Cell中取出对应的数据进行比较，一般来说通常使用等于或者不等于进行判断
            //    通过该方式进行判断后，可以取出其ROWKEY信息，之后再去通过GET获取ROWKEY对应的列信息
            ValueFilter valueFilter = new ValueFilter(
                    CompareFilter.CompareOp.GREATER,
                    new BinaryComparator(Bytes.toBytes(23))
            );

            scan.setFilter(valueFilter);
            ResultScanner scanner = table.getScanner(scan);
            for (Result result : scanner) {
                printRes(result);
            }
            table.close();
        }
        admin.close();
        hbaseCon.close();

    }

    public static void printRes(Result result){
        List<Cell> cells = result.listCells();

        for (Cell cell : cells) {
            String family = Bytes.toString(CellUtil.cloneFamily(cell));
            String column = Bytes.toString(CellUtil.cloneQualifier(cell));
            String rowKey = Bytes.toString(CellUtil.cloneRow(cell));
            if (column.equals("age") || column.equals("total_score") ){
                int value = Bytes.toInt(CellUtil.cloneValue(cell));
                System.out.println("rowKey:"+rowKey+" family:"+family+" column:"+column+" value:"+value);
            }else{
                String value = Bytes.toString(CellUtil.cloneValue(cell));
                System.out.println("rowKey:"+rowKey+" family:"+family+" column:"+column+" value:"+value);
            }
        }

    }
}
