package com.shujia.mr;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Mutation;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;


import java.io.IOException;

/**
 * 需求： 要求统计所有学生中各年龄段的人数
 *
 *
 *
 */
public class Hbase2Hbase {


    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        Configuration conf = new Configuration();
        conf.set("hbase.zookeeper.quorum","node1,node2,master");

        Job job = Job.getInstance(conf);
        job.setJarByClass(Hbase2Hbase.class);
        job.setJobName("Hbase2Hbase");

        // 设置Mapper 和 Reducer
        Scan scan = new Scan();
        TableMapReduceUtil.initTableMapperJob(
                Bytes.toBytes("api:student"),
                scan,
                Hbase2HbaseMapper.class,
                IntWritable.class,
                IntWritable.class,
                job
        );


        /**
         * public static void initTableReducerJob(String table,
         *     Class<? extends TableReducer> reducer, Job job)
         *
         * 创建结果表：
         *      create 'api:ageres','res'
         */
        TableMapReduceUtil.initTableReducerJob(
            "api:ageres",
                Hbase2HbaseReducer.class,
                job
        );


        job.waitForCompletion(true);


    }


    // 从需求中可以看出，Mapper端输出的数据应该为 age作为Key  个数作为Value
    public static class Hbase2HbaseMapper extends TableMapper<IntWritable,IntWritable>{
        @Override
        protected void map(ImmutableBytesWritable key, Result value, Mapper<ImmutableBytesWritable, Result, IntWritable, IntWritable>.Context context) throws IOException, InterruptedException {
            // Result中保存了一行数据 ，从一行数据中取出每一列对应的值
            for (Cell cell : value.listCells()) {
                // 如果取到的值对应的列名称为age 那么再取出其对应的Value
                if (Bytes.toString(CellUtil.cloneQualifier(cell)).equals("age")) {
                    // 通过cloneValue对其进行取值
                    int age = Bytes.toInt(CellUtil.cloneValue(cell));
                    // 每一个rowkey执行一次map函数 每个rowKey代表一个学生
                    context.write(new IntWritable(age),new IntWritable(1));
                }
            }
        }
    }



    // 需要将结果数据写出到HBase中，所以需要继承 TableReducer 而 其需要给定三个泛型 <KEYIN, VALUEIN, KEYOUT>
    //  其中 KEYIN, VALUEIN 是Mapper端输出的泛型，所以可以直接复制粘贴
    //  由于是直接将数据写出到HBase中，所以需要给定HBase的数据类型，在HBase中一般来说 Key表示Rowkey,既然为RowKey 那么可以直接取ImmutableBytesWritable
    //  TableReducer的父类为Reducer类，其ValueOut为Mutation 它为一个抽象类，其子类中包含有Put 而之前通过HBase的基础API
    //        可以知道 Put对象可以包含写出的数据
    //  最终的结果需要以age作为rowkey 个数作为列 写出到HBase中
    public static class Hbase2HbaseReducer extends TableReducer<IntWritable,IntWritable,ImmutableBytesWritable>{
        @Override
        protected void reduce(IntWritable key, Iterable<IntWritable> values, Reducer<IntWritable, IntWritable, ImmutableBytesWritable, Mutation>.Context context) throws IOException, InterruptedException {
            int num =0 ;
            for (IntWritable value : values) {
                num += value.get();
            }
            int age = key.get();

            ImmutableBytesWritable rowKey = new ImmutableBytesWritable(Bytes.toBytes(age));

            Put put = new Put(Bytes.toBytes(age));

            /**
             * public KeyValue(final byte [] row, final byte [] family,
             *       final byte [] qualifier, final byte [] value)
             */
            KeyValue keyValue = new KeyValue(
                    Bytes.toBytes(age),
                    Bytes.toBytes("res"),
                    Bytes.toBytes("age_num"),
                    Bytes.toBytes(num)
            );
            put.add(keyValue);

            context.write(rowKey,put);
        }
    }




}
