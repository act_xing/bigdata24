package com.shujia.mr;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.filter.SubstringComparator;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;
import java.util.List;


/**
 *  需求：从Hbase中读取数据，计算所有班级中的人数，并且将结果写入至HDFS中
 *      所有文科班的学生
 *
 *
 *  注意： hadoop jar hbaseCode-1.0-SNAPSHOT-jar-with-dependencies.jar  com.shujia.mr.Hbase2HDFS
 *      当直接将jar包提交至HADOOP中执行时，其Hadoop的lib下并没有HBase相关的依赖，所以需要在本地对Jar包打包时，需要添加对应的依赖
 *
 */
public class Hbase2HDFS {
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {

        Configuration conf = new Configuration();
        conf.set("hbase.zookeeper.quorum","node1,node2,master");

        Job job = Job.getInstance(conf);
        job.setJarByClass(Hbase2HDFS.class);
        job.setJobName("Hbase2HDFS");

        // 设置Mapper和Reducer类

        /**
         * public static void initTableMapperJob(byte[] table, Scan scan,
         *       Class<? extends TableMapper> mapper,
         *       Class<?> outputKeyClass,
         *       Class<?> outputValueClass, Job job)
         */
        Scan scan = new Scan();

        // 对所有文科班的学生进行过滤
        /**
         * public SingleColumnValueFilter(final byte [] family, final byte [] qualifier,
         *       final CompareOp compareOp, final ByteArrayComparable comparator)
         */
        SingleColumnValueFilter singleColumnValueFilter = new SingleColumnValueFilter(
                Bytes.toBytes("info"),
                Bytes.toBytes("clazz"),
                CompareFilter.CompareOp.EQUAL,
                // 包含有文科字符串的班级取出其值
                new SubstringComparator("文科")
        );
        scan.setFilter(singleColumnValueFilter);

        TableMapReduceUtil.initTableMapperJob(
                Bytes.toBytes("api:student"),
                scan,
                Hbase2HDFSMapper.class,
                Text.class,
                IntWritable.class,
                job
        );

        // 设置Reducer类 由于自定义的Reduce类继承reducer所以使用Mr阶段写法

        job.setReducerClass(Hbase2HDFSReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);


        // 设置输入输出路径


//        TextOutputFormat.setOutputPath(job,new Path("output/Hbase2HDFS"));
        TextOutputFormat.setOutputPath(job,new Path("/output/Hbase2HDFS"));

        job.waitForCompletion(true);

    }

    /**
     * TableMapper是从HBase中读取数据，所以其KeyIn 和 ValueIn是固定格式的
     *          ImmutableBytesWritable是ROWkey格式
     *          Result 一个RowKey对应的一行数据
     * public abstract class TableMapper<KEYOUT, VALUEOUT>
     * extends Mapper<ImmutableBytesWritable, Result, KEYOUT, VALUEOUT>
     */

    /**
     * Mapper端：
     *      ①需要读取HBase中的数据
     *      ② 将读取到的数据进行取值，取出班级信息
     *      ③ 将班级作为Key  1 作为Value写出至Reduce端
     */

    public static class Hbase2HDFSMapper extends TableMapper<Text, IntWritable>{
        @Override
        protected void map(ImmutableBytesWritable key, Result value, Mapper<ImmutableBytesWritable, Result, Text, IntWritable>.Context context) throws IOException, InterruptedException {
            System.out.println("rowKey:"+ Bytes.toString(key.get()));
            List<Cell> cells = value.listCells();
            // 遍历每个Cell 从Cell中取出班级列的Value值，将数据写出到Reduce端，再进行计算
            for (Cell cell : cells) {
                String column = Bytes.toString(CellUtil.cloneQualifier(cell));
                if(column.equals("clazz")){
                    String clazz = Bytes.toString(CellUtil.cloneValue(cell));
                    context.write(new Text(clazz),new IntWritable(1));
                }
            }
        }
    }

    /**
     *  Reduce端：
     *      ① 读取Mapper端传入的数据
     *      ② 将相同班级中的1进行累加得到人数结果
     *      ③ 将结果写出至HDFS中
     */
    public static class Hbase2HDFSReducer extends Reducer<Text, IntWritable,Text, IntWritable>{
        @Override
        protected void reduce(Text key, Iterable<IntWritable> values, Reducer<Text, IntWritable, Text, IntWritable>.Context context) throws IOException, InterruptedException {

            int num = 0;
            for (IntWritable value : values) {
                num += value.get();
            }
            context.write(key,new IntWritable(num));

        }
    }
}
