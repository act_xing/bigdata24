package com.shujia.mr;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2;
import org.apache.hadoop.hbase.mapreduce.KeyValueSortReducer;
import org.apache.hadoop.hbase.mapreduce.LoadIncrementalHFiles;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;

import java.io.IOException;

public class BLockLoading {
    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        conf.set("hbase.zookeeper.quorum","node1,node2,master");

        Job job = Job.getInstance(conf);
        job.setJarByClass(BLockLoading.class);
        job.setJobName("BLockLoading");

        // 设置Mapper类

        job.setMapperClass(BlockLoadingMapper.class);
        job.setMapOutputKeyClass(ImmutableBytesWritable.class);
        job.setMapOutputValueClass(KeyValue.class);

        job.setOutputKeyClass(ImmutableBytesWritable.class);
        job.setOutputValueClass(KeyValue.class);

        // 对KeyValue进行排序
        job.setReducerClass(KeyValueSortReducer.class);


        // 设置输入路径
        TextInputFormat.addInputPath(job,new Path("/input/BLockLoading"));


        Connection connection = ConnectionFactory.createConnection(conf);

        TableName tableName = TableName.valueOf("api:block_student");
        Table table = connection.getTable(tableName);

        // RegionLocator 表示Region的位置信息对象
        RegionLocator regionLocator = connection.getRegionLocator(tableName);


        // 配置HFile的元数据信息
        // public static void configureIncrementalLoad(Job job, Table table, RegionLocator regionLocator)
        //
        HFileOutputFormat2.configureIncrementalLoad(
                job,
                table,
                regionLocator
        );

        Path hfileOutPath = new Path("/output/BLockLoading");
        // 设置输出路径
        HFileOutputFormat2.setOutputPath(job,hfileOutPath);

        // 提交JOB进行执行，会在/output/BLockLoading路径中生成HFile的结果文件
        job.waitForCompletion(true);

        LoadIncrementalHFiles loadIncrementalHFiles = new LoadIncrementalHFiles(conf);

        /**
         * public void doBulkLoad(Path hfofDir, Admin admin, Table table, RegionLocator regionLocator)
         */
        Admin admin = connection.getAdmin();
        loadIncrementalHFiles.doBulkLoad(
                hfileOutPath,
                admin,
                table,
                regionLocator
        );
    }

    /**
     * Mapper阶段：
     *      读取HDFS中的数据，再将数据转换成HBase中的数据格式
     *
     *      KEYIN, VALUEIN, : 从文本中读取数据 LongWritable Text
     *      KEYOUT, VALUEOUT : 由于数据要转换成HBase中数据格式，所以KeyOut是RowKey信息 对应格式为 ImmutableBytesWritable
     *              VALUEOUT: Mutation 它为一个抽象类，其子类中包含有Put 但是在BLockLoading中不使用该方式，Put是HBase写数据操作对象
     *                      当前是通过MR将数据转出HBase的存储格式，所以直接指定其KV数据格式  KeyValue
      */

    // 对于每个KeyValue数据写出一次
    public static class BlockLoadingMapper extends Mapper<LongWritable, Text, ImmutableBytesWritable, KeyValue>{
//        1500100001	施笑槐,22,女,文科六班,406


        @Override
        protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, ImmutableBytesWritable, KeyValue>.Context context) throws IOException, InterruptedException {
            String[] split = value.toString().split("\t");
            String id = split[0];
            String[] columns = split[1].split(",");

            ImmutableBytesWritable rowKey = new ImmutableBytesWritable(Bytes.toBytes(id));

            KeyValue nameKv = new KeyValue(
                    Bytes.toBytes(id),
                    Bytes.toBytes("info"),
                    Bytes.toBytes("name"),
                    Bytes.toBytes(columns[0])
            );
            context.write(rowKey,nameKv);

            KeyValue ageKv = new KeyValue(
                    Bytes.toBytes(id),
                    Bytes.toBytes("info"),
                    Bytes.toBytes("age"),
                    Bytes.toBytes(Integer.parseInt(columns[1]))
            );
            context.write(rowKey,ageKv);

            KeyValue genderKv = new KeyValue(
                    Bytes.toBytes(id),
                    Bytes.toBytes("info"),
                    Bytes.toBytes("gender"),
                    Bytes.toBytes(columns[2])
            );
            context.write(rowKey,genderKv);

            KeyValue clazzKv = new KeyValue(
                    Bytes.toBytes(id),
                    Bytes.toBytes("info"),
                    Bytes.toBytes("clazz"),
                    Bytes.toBytes(columns[3])
            );
            context.write(rowKey,clazzKv);

            KeyValue scoreKv = new KeyValue(
                    Bytes.toBytes(id),
                    Bytes.toBytes("score"),
                    Bytes.toBytes("total_score"),
                    Bytes.toBytes(Integer.parseInt(columns[4]))
            );

            context.write(rowKey,scoreKv);

        }
    }




    /**
     * Reducer阶段：
     *      对于RowKey来说，需要对RowKey进行做字典序排序，需要在Reduce阶段完成 可以调用HADOOP自带的操作
     *
     *      该过程不需要程序员自己编写逻辑
     */


}
