package com.shujia.mr;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.List;

public class ScanResData {
    public static void main(String[] args) throws IOException {

        Configuration configuration = new Configuration();
        configuration.set("hbase.zookeeper.quorum", "node1,node2,master");

        Connection hbaseCon = ConnectionFactory.createConnection(configuration);

        // 对命名空间进行操作属于对于元数据信息操作，那么API中元数据操作的类对象为Admin

        Admin admin = hbaseCon.getAdmin();

        // 对具体的数据进行操作，需要获取Table对象
        TableName tableName = TableName.valueOf("api:ageres");

        if (admin.tableExists(tableName)) {
            Table table = hbaseCon.getTable(tableName);

            Scan scan = new Scan();

            ResultScanner scanner = table.getScanner(scan);

            // 遍历结果
            for (Result result : scanner) {
                printRes(result);
            }

            table.close();
        }
        admin.close();
        hbaseCon.close();
    }

    public static void printRes(Result result) {
        List<Cell> cells = result.listCells();

        for (Cell cell : cells) {
            String family = Bytes.toString(CellUtil.cloneFamily(cell));
            String column = Bytes.toString(CellUtil.cloneQualifier(cell));
            int rowKey = Bytes.toInt(CellUtil.cloneRow(cell));
            int value = Bytes.toInt(CellUtil.cloneValue(cell));
            System.out.println("rowKey:" + rowKey + " family:" + family + " column:" + column + " value:" + value);

        }

    }
}
