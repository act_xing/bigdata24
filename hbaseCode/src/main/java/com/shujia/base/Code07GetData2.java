package com.shujia.base;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.List;

public class Code07GetData2 {
    public static void main(String[] args) throws IOException {
        Configuration configuration = new Configuration();
        configuration.set("hbase.zookeeper.quorum","node1,node2,master");

        Connection hbaseCon = ConnectionFactory.createConnection(configuration);

        // 对命名空间进行操作属于对于元数据信息操作，那么API中元数据操作的类对象为Admin

        Admin admin = hbaseCon.getAdmin();

        // 对具体的数据进行操作，需要获取Table对象
        TableName tableName = TableName.valueOf("api:student");

        if(admin.tableExists(tableName)) {
            Table table = hbaseCon.getTable(tableName);
            Get get = new Get(Bytes.toBytes("15001"));
            Result result = table.get(get);

            // Cell : 行列交叉的一个结果值  KV形式存在： Key: RowKey + 列族 + 列 + 时间戳   Value: 值
            List<Cell> cells = result.listCells();
            for (Cell cell : cells) {
                String family = Bytes.toString(CellUtil.cloneFamily(cell));
                String column = Bytes.toString(CellUtil.cloneQualifier(cell));
                String rowKey = Bytes.toString(CellUtil.cloneRow(cell));
                if (column.equals("age") || column.equals("total_score") ){
                    int value = Bytes.toInt(CellUtil.cloneValue(cell));
                    System.out.println(value);
                }else{
                    String value = Bytes.toString(CellUtil.cloneValue(cell));
                    System.out.println(value);
                }
                System.out.println(family);
                System.out.println(column);
                System.out.println(rowKey);


            }
            table.close();
        }
        admin.close();
        hbaseCon.close();
    }
}
