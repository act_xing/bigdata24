package com.shujia.base;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class Code06GetData {
    public static void main(String[] args) throws IOException {

        Configuration configuration = new Configuration();
        configuration.set("hbase.zookeeper.quorum","node1,node2,master");

        Connection hbaseCon = ConnectionFactory.createConnection(configuration);

        // 对命名空间进行操作属于对于元数据信息操作，那么API中元数据操作的类对象为Admin

        Admin admin = hbaseCon.getAdmin();

        // 对具体的数据进行操作，需要获取Table对象
        TableName tableName = TableName.valueOf("api:student");

        if(admin.tableExists(tableName)){
            Table table = hbaseCon.getTable(tableName);

            // 传入一个RowKey获取对象
            Get get = new Get(Bytes.toBytes("15001"));
            // Result 用于存放结果数据
            Result result = table.get(get);

            // 通过列族和列获取对应Value的数据
            String name = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("name")));
            int age = Bytes.toInt(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("age")));
            String gender = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("gender")));
            int score = Bytes.toInt(result.getValue(Bytes.toBytes("score"), Bytes.toBytes("total_score")));

            System.out.println(name);
            System.out.println(age);
            System.out.println(gender);
            System.out.println(score);

        }


    }
}
