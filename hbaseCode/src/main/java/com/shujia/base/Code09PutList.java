package com.shujia.base;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Code09PutList {
    public static void main(String[] args) throws IOException {
        Configuration configuration = new Configuration();
        configuration.set("hbase.zookeeper.quorum", "node1,node2,master");

        Connection hbaseCon = ConnectionFactory.createConnection(configuration);

        // 对命名空间进行操作属于对于元数据信息操作，那么API中元数据操作的类对象为Admin

        Admin admin = hbaseCon.getAdmin();

        // 对具体的数据进行操作，需要获取Table对象
        TableName tableName = TableName.valueOf("api:student_split");

        Table table = hbaseCon.getTable(tableName);

        FileReader fileReader = new FileReader("data/part-r-00000");

        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;


        // 当读取的文件中的数据量较大时，那么所有的数据都会存放至ArrayList中，那么会导致内存溢出
        //     所以可以对当前的代码进行优化
        ArrayList<Put> puts = new ArrayList<>();


        // 读取到每一行数据后，对其进行切分处理
        while ((line = bufferedReader.readLine()) != null) {
            String[] split = line.split("\t");
            String id = split[0];
            String[] columns = split[1].split(",");

            // 传入一个Rowkey
            Put put = new Put(Bytes.toBytes(id));

            // 施笑槐,22,女,文科六班,406
            put.add( new KeyValue(Bytes.toBytes(id)
                            , Bytes.toBytes("info")
                            , Bytes.toBytes("name")
                            , Bytes.toBytes(columns[0])
                    )
            );

            put.add( new KeyValue(Bytes.toBytes(id)
                            , Bytes.toBytes("info")
                            , Bytes.toBytes("age")
                            , Bytes.toBytes(Integer.parseInt(columns[1]))
                    )
            );

            put.add( new KeyValue(Bytes.toBytes(id)
                            , Bytes.toBytes("info")
                            , Bytes.toBytes("gender")
                            , Bytes.toBytes(columns[2])
                    )
            );

            put.add( new KeyValue(Bytes.toBytes(id)
                            , Bytes.toBytes("info")
                            , Bytes.toBytes("clazz")
                            , Bytes.toBytes(columns[3])
                    )
            );

            put.add( new KeyValue(Bytes.toBytes(id)
                            , Bytes.toBytes("score")
                            , Bytes.toBytes("total_score")
                            , Bytes.toBytes(Integer.parseInt(columns[4]))
                    )
            );

            puts.add(put);

            // 当ArrayList中数据达到100时，那么将该部分数据提交至Hbase中进行执行，之后再对列表中的数据进行清空操作
            if(puts.size() >= 100){
                table.put(puts);
                // 清空
                puts.clear();
            }
        }


        table.close();
        admin.close();
        bufferedReader.close();
        hbaseCon.close();


    }



}
