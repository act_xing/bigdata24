package com.shujia.base;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;

import java.io.IOException;

public class Code01HbaseCon {
    public static void main(String[] args) throws IOException {

        Configuration configuration = new Configuration();
        configuration.set("hbase.zookeeper.quorum","node1,node2,master");

        Connection hbaseCon = ConnectionFactory.createConnection(configuration);
        System.out.println("子实现类："+hbaseCon.getClass().getName());

        hbaseCon.close();
    }
}
