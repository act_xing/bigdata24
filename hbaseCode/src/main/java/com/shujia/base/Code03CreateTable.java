package com.shujia.base;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;

import java.io.IOException;

public class Code03CreateTable {
    public static void main(String[] args) throws IOException {
        Configuration configuration = new Configuration();
        configuration.set("hbase.zookeeper.quorum","node1,node2,master");

        Connection hbaseCon = ConnectionFactory.createConnection(configuration);

        Admin admin = hbaseCon.getAdmin();

        // 由于HTableDescriptor创建对象时需要TableName对象，所以查看TableName的类可以获取一个static 的ValueOf方法
        //    该方法可以返回一个 HTableDescriptor 对象，并且需要传入一个String类型的表名称
        TableName tableName = TableName.valueOf("api:student");
        HTableDescriptor hTableDescriptor = new HTableDescriptor(tableName);

        //  HColumnDescriptor family

        hTableDescriptor.addFamily(new HColumnDescriptor("info"));
        hTableDescriptor.addFamily(new HColumnDescriptor("score"));


        // 判断表是否存在，如果不存在，那么就创建

        if (!admin.tableExists(tableName)) {
            // Table should have at least one column family.  表示创建表时，必须至少添加一个列族
            admin.createTable(hTableDescriptor);
        }else{
            System.out.println("该表已经存在....");
        }
        hbaseCon.close();
    }
}
