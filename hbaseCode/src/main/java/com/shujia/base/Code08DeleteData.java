package com.shujia.base;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class Code08DeleteData {
    public static void main(String[] args) throws IOException {
        Configuration configuration = new Configuration();
        configuration.set("hbase.zookeeper.quorum","node1,node2,master");

        Connection hbaseCon = ConnectionFactory.createConnection(configuration);

        // 对命名空间进行操作属于对于元数据信息操作，那么API中元数据操作的类对象为Admin

        Admin admin = hbaseCon.getAdmin();

        // 对具体的数据进行操作，需要获取Table对象
        TableName tableName = TableName.valueOf("api:student");

        if(admin.tableExists(tableName)) {
            Table table = hbaseCon.getTable(tableName);

            Delete delete = new Delete(Bytes.toBytes("15001"));

            table.delete(delete);
            table.close();
        }

        admin.close();
        hbaseCon.close();
    }
}
