package com.shujia.base;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.NamespaceDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class Code05PutData {
    public static void main(String[] args) throws IOException {
        Configuration configuration = new Configuration();
        configuration.set("hbase.zookeeper.quorum","node1,node2,master");

        Connection hbaseCon = ConnectionFactory.createConnection(configuration);

        // 对命名空间进行操作属于对于元数据信息操作，那么API中元数据操作的类对象为Admin

        Admin admin = hbaseCon.getAdmin();

        // 对具体的数据进行操作，需要获取Table对象
        TableName tableName = TableName.valueOf("api:student");

        Table table = hbaseCon.getTable(tableName);

        Put put = new Put(Bytes.toBytes("15001"));

        /**
         * public KeyValue(final byte [] row, final byte [] family,
         *       final byte [] qualifier, final byte [] value)
         */
        KeyValue nameKv = new KeyValue(Bytes.toBytes("15001")
                , Bytes.toBytes("info")
                , Bytes.toBytes("name")
                , Bytes.toBytes("zhangsan")
        );
        put.add(nameKv);

        KeyValue ageKv = new KeyValue(Bytes.toBytes("15001")
                , Bytes.toBytes("info")
                , Bytes.toBytes("age")
                , Bytes.toBytes(23)
        );
        put.add(ageKv);

        KeyValue genderKv = new KeyValue(Bytes.toBytes("15001")
                , Bytes.toBytes("info")
                , Bytes.toBytes("gender")
                , Bytes.toBytes("nv")
        );
        put.add(genderKv);


        KeyValue scoreKv = new KeyValue(Bytes.toBytes("15001")
                , Bytes.toBytes("score")
                , Bytes.toBytes("total_score")
                , Bytes.toBytes(530)
        );
        put.add(scoreKv);


        table.put(put);


        hbaseCon.close();
    }
}
