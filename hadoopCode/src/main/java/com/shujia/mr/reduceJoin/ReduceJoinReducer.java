package com.shujia.mr.reduceJoin;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class ReduceJoinReducer extends Reducer<Text, Text, Text, Text> {
    @Override
    protected void reduce(Text key, Iterable<Text> reduce, Reducer<Text, Text, Text, Text>.Context context) throws IOException, InterruptedException {

        String otherInfo = "";
        String score = "";
        for (Text text : reduce) {
            String oneMessage = text.toString();
            if (oneMessage.contains(",")) {
                otherInfo = oneMessage;
            } else {
                score = oneMessage;
            }
        }
        context.write(key,new Text(otherInfo+","+score));
    }
}
