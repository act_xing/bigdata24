package com.shujia.mr.reduceJoin;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class ReduceJoinMapper extends Mapper<LongWritable, Text, Text, Text> {
    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, Text>.Context context) throws IOException, InterruptedException {
        // 1500100001,施笑槐,22,女,文科六班    1500100001	406
        String oneLine = value.toString();

        // 1500100001,施笑槐,22,女,文科六班
        if (oneLine.contains(",")) {
            String[] split = oneLine.split(",");
            String id = split[0];
            String otherInfo  = split[1]+","+split[2]+","+split[3]+","+split[4];
            context.write(new Text(id),new Text(otherInfo));
        }else{
            String[] split = oneLine.split("\t");
            String id = split[0];
            String score = split[1];
            context.write(new Text(id),new Text(score));
        }
    }
}
