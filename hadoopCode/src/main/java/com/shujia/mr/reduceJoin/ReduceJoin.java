package com.shujia.mr.reduceJoin;

import com.shujia.mr.wordcount.WordCount;
import com.shujia.mr.wordcount.WordCountMapper;
import com.shujia.mr.wordcount.WordCountReducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * 需求：
 *      读取学生的基本信息和学生的总分数据，在Reduce端对相同学生的数据进行拼接 形成Join效果
 *
 * Mapper阶段：
 *      ① 读取传入的每一行数据    1500100001,施笑槐,22,女,文科六班    1500100001	406
 *      ② 对读取的数据进行判断，根据不同种类的数据分开进行处理
 *      ③  以学生的ID作为Key 其他信息作为VALUE写出到Reduce端
 *
 * Reduce阶段：
 *      ① 接受相同学生ID的数据  施笑槐,22,女,文科六班    406
 *      ② 对获取到的信息进行判断 并做拼接
 *      ③ 将结果数据写出 学生ID作为Key  1500100001  其他信息作为VALUE写出 施笑槐,22,女,文科六班,406
 *
 *
 */
public class ReduceJoin {
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        // 1.创建Job的操作对象

        // 2.需要创建配置类对象
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        // 3.配置Job
        job.setJobName("ReduceJoin");
        job.setJarByClass(ReduceJoin.class);

        // 4.设置Mapper 及其输出类型
        job.setMapperClass(ReduceJoinMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);

        // 5.设置Reduce及其输出类型
        job.setReducerClass(ReduceJoinReducer.class);

        // 6.设置最终输出的数据类型
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        // 7.设置输入输出路径(输入路径既可以指定文件，也可以指定目录)

        //当需要添加多份数据时，第一种方式需要将所有的数据放入同一个目录中，第二种方式将每个文件多次添加到路径中
        FileInputFormat.addInputPath(job,new Path("data/students.txt"));
        FileInputFormat.addInputPath(job,new Path("output/scoreCount"));
        FileOutputFormat.setOutputPath(job,new Path("output/reduceJoin"));


        // 8.提交执行当前Job
        job.waitForCompletion(true);
    }
}
