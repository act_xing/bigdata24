package com.shujia.mr.filter1;


import com.shujia.mr.wordcount.WordCount;
import com.shujia.mr.wordcount.WordCountMapper;
import com.shujia.mr.wordcount.WordCountReducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * 需求1：过滤出总分大于450分的同学
 *
 * 分析：
 *      Mapper阶段：
 *          ① 读取ReduceJoin的结果数据
 *          ② 对结果数据进行切分，并将成绩信息截取出来
 *          ③ 对成绩大于450分的学生进行写出
 *
 *      Reducer阶段：
 *          由于处理过程中，不需要对数据进行做汇总处理，所以可以不适用该部分的代码
 *
 */
public class Filter01 {
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        // 1.创建Job的操作对象

        // 2.需要创建配置类对象
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        // 3.配置Job
        job.setJobName("Filter01");
        job.setJarByClass(Filter01.class);

        // 4.设置Mapper 及其输出类型
        job.setMapperClass(Filter01Mapper.class);
        job.setMapOutputKeyClass(NullWritable.class);
        job.setMapOutputValueClass(Text.class);

        // 6.设置最终输出的数据类型
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(Text.class);

        // 7.设置输入输出路径(输入路径既可以指定文件，也可以指定目录)

        //本地执行的路径
        FileInputFormat.addInputPath(job, new Path("output/reduceJoin"));
        FileOutputFormat.setOutputPath(job, new Path("output/filter01"));


        // 8.提交执行当前Job
        job.waitForCompletion(true);
    }
}
