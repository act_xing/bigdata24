package com.shujia.mr.filter1;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * 对于Mapper中的KeyOut可以使用Null 所有的一行数据可以作为VALUE写出 所以对应的数据类型 NullWritable,Text
 */
public class Filter01Mapper extends Mapper<LongWritable, Text, NullWritable,Text> {

    /**
     *
     * @param key
     * @param value  1500100033	桑昆峰,24,男,理科三班,532
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, NullWritable, Text>.Context context) throws IOException, InterruptedException {
        // 取出Score数据
        String[] split = value.toString().split(",");
        int score = Integer.parseInt(split[4]);

        // 如果大于450分就写出该行数据
        if(score > 450){
            context.write(NullWritable.get(),value);
        }
    }
}
