package com.shujia.mr.mapJoin;

import com.shujia.mr.filter1.Filter01;
import com.shujia.mr.filter1.Filter01Mapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * MapJoin:
 *      将数据关联的过程发生在Map端进行处理
 *      那么Map函数是一行处理一次，那么如果要做关联，关联逻辑： 按行读取一个行时，需要再另外一部分数据中根据Key进行遍历匹配
 *      所以需要加载两部分数据
 *
 *
 * 加载两部分数据
 *
 *
 */
public class MapJoinDriver {

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {

        // 2.需要创建配置类对象
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        // 3.配置Job
        job.setJobName("MapJoinDriver");
        job.setJarByClass(MapJoinDriver.class);

        // 4.设置Mapper 及其输出类型
        job.setMapperClass(MapJoinMapper.class);
        job.setMapOutputKeyClass(NullWritable.class);
        job.setMapOutputValueClass(MapJoinStudent.class);

        // 6.设置最终输出的数据类型
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(MapJoinStudent.class);

        // 添加学生成绩的缓存路径
        job.addCacheFile(new Path("output/scoreCount/part-r-00000").toUri());

        // 7.设置输入输出路径(输入路径既可以指定文件，也可以指定目录)

        //本地执行的路径
        FileInputFormat.addInputPath(job, new Path("hadoopCode/data/students.txt"));
        FileOutputFormat.setOutputPath(job, new Path("output/mapJoin"));


        // 8.提交执行当前Job
        job.waitForCompletion(true);
    }
}
