package com.shujia.mr.mapJoin;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;


public class MapJoinMapper  extends Mapper<LongWritable, Text, NullWritable,MapJoinStudent> {

    HashMap<String,String> studentScore = new HashMap<String,String>();

    /**
     * setup函数是在Map函数执行之前运行了一次 ，根据这个特性，我们可以在setup函数中加载学生成绩数据到内存中进行保存 ，
     *      之后在Map函数中读取每一行的学生基本信息数据，再根据学生ID去 内存中进行数据的匹配关联
     * @param context 上下文对象
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void setup(Mapper<LongWritable, Text, NullWritable, MapJoinStudent>.Context context) throws IOException, InterruptedException {
        // 加载学生成绩数据，并保存成一个HashMap，并且以学生ID作为Key 成绩作为VALUE

        // context中保存有缓存的路径
        URI[] cacheFiles = context.getCacheFiles();
        URI scoreFile = cacheFiles[0];

        // 创建FileSystem 由于上下文对象中保存了其配置类的对象，所以可以直接获取
        Configuration configuration = context.getConfiguration();
        FileSystem fileSystem = FileSystem.get(configuration);

        // 创建输入流，并按行读取Score中的数据
        FSDataInputStream fsDataInputStream = fileSystem.open(new Path(scoreFile));

        InputStreamReader inputStreamReader = new InputStreamReader(fsDataInputStream);

        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        // 1500100001	406
        //1500100002	440
        //1500100003	359
        //1500100004	421
        String line;
        while ((line = bufferedReader.readLine())!=null){
            String[] split = line.split("\t");

            // 将读取到的数据添加至内存中
            studentScore.put(split[0],split[1]);
        }

        System.out.println("当前函数被执行...");
    }


    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, NullWritable, MapJoinStudent>.Context context) throws IOException, InterruptedException {

        String student = value.toString();

        String[] splitRes = student.split(",");

        String id = splitRes[0];


        int score = Integer.parseInt(studentScore.get(id));

        // public MapJoinStudent(String id, String name, String age, String gender, String clazz, int score) {
        MapJoinStudent oneStudent = new MapJoinStudent(id, splitRes[1], splitRes[2], splitRes[3], splitRes[4], score);

        context.write(NullWritable.get(),oneStudent);


//        System.out.println(student);
    }
}
