package com.shujia.mr.output;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class OutputMapper extends Mapper<LongWritable, Text, NullWritable,OutputStudent> {
    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, NullWritable, OutputStudent>.Context context) throws IOException, InterruptedException {
        String[] split = value.toString().split("\t");
        String id = split[0];
        String[] otherInfo = split[1].split(",");

        OutputStudent outputStudent = new OutputStudent(id, otherInfo[0], otherInfo[1], otherInfo[2], otherInfo[3], Integer.parseInt(otherInfo[4]));

        context.write(NullWritable.get(),outputStudent);

    }
}
