package com.shujia.mr.output;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * 需求：读取ReduceJoin的结果数据，并对结果进行包装，之后再将大于450分的学生写入一个结果文件，小于450分的写入另外一个文件中
 *
 *   对于使用之前所学内容分析：
 *      在Mapper端需要对数据进行做切分包装处理，之后需要将大于450分的学生数据写入一个Reduce  小于450分的写入另外一个Reduce
 *      可以使用分区将大于450分的学生给定一个编号，小于450分的给定另外一个编号 在Reduce函数中对数据不做任何处理，直接写出
 *
 *   通过该方式缺点：
 *      在该过程中使用了Reduce过程，会产生大量的网络IO
 *
 *   优化：
 *      可以避免使用Reduce，可以通过自定义输出类操作：
 *          在自定义输入类中，对于传入的数据进行判断，如果数据大于450分创建一个数据的输出流写出到一个文件中，否则写入另外一个文件
 *
 *
 *
 *
 */
public class OutputDriver {
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        // 2.需要创建配置类对象
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        // 3.配置Job
        job.setJobName("OutputDriver");
        job.setJarByClass(OutputDriver.class);

        // 4.设置Mapper 及其输出类型
        job.setMapperClass(OutputMapper.class);
        job.setMapOutputKeyClass(NullWritable.class);
        job.setMapOutputValueClass(OutputStudent.class);

        // 6.设置最终输出的数据类型
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(OutputStudent.class);


        /**
         * 自定义输出类:
         *  setOutputFormatClass函数中要求传入一个outputFormat子类，该类的子类有很多，并且outputFormat中有三个抽象函数，
         *          需要实现，由于需求中只要求定义写出数据的逻辑，对于其他并无要求，所以可以直接去实现其 getRecordWriter方法即可
         *          对于其他的抽象方法，可以从子类中继承，所以自定义的输出类可以继承 FileOutputFormat
         *
         */

        //

        job.setOutputFormatClass(MyOutputFormat.class);



        //本地执行的路径
        FileInputFormat.addInputPath(job, new Path("output/reduceJoin"));
        FileOutputFormat.setOutputPath(job, new Path("output/output"));


        // 8.提交执行当前Job
        job.waitForCompletion(true);
    }
}
