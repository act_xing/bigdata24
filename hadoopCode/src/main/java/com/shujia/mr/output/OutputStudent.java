package com.shujia.mr.output;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;


// 注意由于当前对象是作为Mapper端的VALUE写出，所以需要对其进行做序列化操作
public class OutputStudent implements Writable {
    String id;
    String name;
    String age;
    String gender;
    String clazz;
    int score;

    public OutputStudent() {
    }

    public OutputStudent(String id, String name, String age, String gender, String clazz, int score) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.clazz = clazz;
        this.score = score;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", gender='" + gender + '\'' +
                ", clazz='" + clazz + '\'' +
                ", score=" + score +
                '}'+"\n"
                ;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    /**
     * 通过该函数对当前对象进行做序列化操作
     * @param out <code>DataOuput</code> to serialize this object into.
     * @throws IOException
     */
    @Override
    public void write(DataOutput out) throws IOException {
        /**
         *     String id;
         *     String name;
         *     String age;
         *     String gender;
         *     String clazz;
         *     int score;
         */
        out.writeUTF(this.id);
        out.writeUTF(this.name);
        out.writeUTF(this.age);
        out.writeUTF(this.gender);
        out.writeUTF(this.clazz);
        out.writeInt(this.score);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.id = in.readUTF();
        this.name = in.readUTF();
        this.age = in.readUTF();
        this.gender = in.readUTF();
        this.clazz = in.readUTF();
        this.score = in.readInt();
    }
}
