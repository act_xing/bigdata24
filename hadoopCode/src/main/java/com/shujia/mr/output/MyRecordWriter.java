package com.shujia.mr.output;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.IOException;

public class MyRecordWriter extends RecordWriter<NullWritable,OutputStudent> {

    FileSystem fileSystem;
    FSDataOutputStream more450scoreStream;
    FSDataOutputStream less450scoreStream;

    public MyRecordWriter(Configuration configuration) throws IOException {
        fileSystem = FileSystem.get(configuration);
        more450scoreStream = fileSystem.create(new Path("output/output/more450score.txt"));
        less450scoreStream = fileSystem.create(new Path("output/output/less450score.txt"));
    }

    @Override
    public void write(NullWritable key, OutputStudent student) throws IOException, InterruptedException {
        /**
         * 在write中通过输出流写出数据
         *    输出流在哪里定义？ 需要哪些条件
         *      需要有FileSystem 创建FileSystem需要哪些条件  所以需要在构造函数中对所需的对象进行传入
         *
         */
        if(student.getScore() > 450){
            more450scoreStream.writeUTF(student.toString());
            more450scoreStream.flush();
        }else{
            less450scoreStream.writeUTF(student.toString());
            less450scoreStream.flush();
        }

    }

    @Override
    public void close(TaskAttemptContext context) throws IOException, InterruptedException {
        // 关闭输出流
        more450scoreStream.close();
        less450scoreStream.close();
        fileSystem.close();
    }
}
