package com.shujia.mr.output;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class MyOutputFormat extends FileOutputFormat{
    /**
     *
     * 该方法中需要返回一个RecordWriter的子类对象，并且该子类对象中包含了写出数据的逻辑
     *
     * RecordWriter 中包含有两个抽象方法：
     *      write: 该函数可以接受 Map端或者Reduce端写出的KV数据 并且在该函数中可以定义写出数据的逻辑
     *      close: 可以用来关闭输出流
     */
    @Override
    public RecordWriter getRecordWriter(TaskAttemptContext job) throws IOException, InterruptedException {
        Configuration configuration = job.getConfiguration();

        return new MyRecordWriter(configuration);
    }
}
