package com.shujia.mr.sort;


import com.shujia.mr.filter03.Student;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

// 对于Map端写出的数据，进行做排序操作，该排序需要我们自定义。并且不需要Reduce过程
public class SortMapper extends Mapper<LongWritable, Text,SortStudent , NullWritable> {
    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, SortStudent, NullWritable>.Context context) throws IOException, InterruptedException {

        String oneLine = value.toString();

        String[] split = oneLine.split("\t");
        String id = split[0];
        // 桑昆峰,24,男,理科三班,532
        String[] otherInfo = split[1].split(",");

        // public Student(String id, String name, String age, String gender, String clazz, int score) {
        SortStudent student = new SortStudent(id, otherInfo[0], otherInfo[1], otherInfo[2], otherInfo[3], Integer.parseInt(otherInfo[4]));

        context.write(student,NullWritable.get());

    }
}
