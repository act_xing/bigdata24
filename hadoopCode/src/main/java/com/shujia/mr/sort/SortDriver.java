package com.shujia.mr.sort;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

// 需求：将所有学生的数据按分数进行排序，并写出
//   注意:MapReduce 在做数据排序时，默认是按照Key进行排序，并写出，所以对于实现了compareTo的函数需要作为Key
public class SortDriver {

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        // 2.需要创建配置类对象
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        // 3.配置Job
        job.setJobName("SortDriver");
        job.setJarByClass(SortDriver.class);

        // 4.设置Mapper 及其输出类型
        job.setMapperClass(SortMapper.class);
        job.setMapOutputKeyClass(SortStudent.class);
        job.setMapOutputValueClass(NullWritable.class);

        // 6.设置最终输出的数据类型
        job.setOutputKeyClass(SortStudent.class);
        job.setOutputValueClass(NullWritable.class);



        //本地执行的路径
        FileInputFormat.addInputPath(job, new Path("output/reduceJoin"));
        FileOutputFormat.setOutputPath(job, new Path("output/sort"));


        // 8.提交执行当前Job
        job.waitForCompletion(true);
    }

}
