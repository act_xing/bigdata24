package com.shujia.mr.fiter2;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * 需求2：对于各班级中的学生总分进行排序，要求取出各班级中总分前三名学生
 * 分析：
 *      需要读取ReduceJoin的结果数据，之后再对数据进行处理，将相同班级的学生数据汇集到一个Reduce中进行处理，
 *         需要取出对应学生的信息，并包装成一个学生对象，再将所有的学生对象存储在List集合中，对于List集合中的数据再做数据的排序，对应取出下标的数据
 *
 * Mapper端：
 *      ①读取数据，并切分，取出班级信息
 *      ②对班级信息作为Key，其他信息作为VALUE写出到Reduce
 *
 * Reducer端：
 *      将相同班级的学生数据汇集到一个Reduce中进行处理，
 *      需要取出对应学生的信息，并包装成一个学生对象，再将所有的学生对象存储在List集合中，对于List集合中的数据再做数据的排序，对应取出下标的数据
 *
 */
public class Filter02 {
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        // 1.创建Job的操作对象

        // 2.需要创建配置类对象
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        // 3.配置Job
        job.setJobName("Filter02");
        job.setJarByClass(Filter02.class);

        // 4.设置Mapper 及其输出类型
        job.setMapperClass(Filter02Mapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);

        // 5.设置Reduce及其输出类型
        job.setReducerClass(Filter02Reducer.class);

        // 6.设置最终输出的数据类型
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(Student.class);

        // 7.设置输入输出路径(输入路径既可以指定文件，也可以指定目录)

        //本地执行的路径
        FileInputFormat.addInputPath(job, new Path("output/reduceJoin"));
        FileOutputFormat.setOutputPath(job, new Path("output/filter02"));

        // 8.提交执行当前Job
        job.waitForCompletion(true);
    }

}
