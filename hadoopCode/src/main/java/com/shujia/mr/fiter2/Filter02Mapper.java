package com.shujia.mr.fiter2;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class Filter02Mapper extends Mapper<LongWritable, Text,Text,Text> {
    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, Text>.Context context) throws IOException, InterruptedException {
        // 1500100033	桑昆峰,24,男,理科三班,532
        String oneLine = value.toString();
        String[] split = oneLine.split(",");
        String clazz = split[3];
        context.write(new Text(clazz),value);
    }
}
