package com.shujia.mr.wordcount;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * 实现Reduce阶段的数据处理逻辑
 *      需要继承 Reducer<KEYIN,VALUEIN,KEYOUT,VALUEOUT>
 *          KEYIN,VALUEIN：表示reduce阶段数据输入的类型，数据是由Mapper端输出的，所以Reduce端输入数据类型就是Mapper端输出类型
 *          KEYOUT,VALUEOUT: 表示最终的输出结果，Key: 单词   Value：统计得到的结果 单词为一个字符串所以可以使用Text 而数据统计结果是一个数值可以使用 IntWritable
 */
public class WordCountReducer extends Reducer<Text, IntWritable,Text, IntWritable> {

    /**
     * 在该函数中对相同Key的数据进行做数据的汇总
     *
     * @param key 单词
     * @param values 单词对应的每一个标记数据 1
     * @param context 上下文对象，可以连接Map和Reduce
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Reducer<Text, IntWritable, Text, IntWritable>.Context context) throws IOException, InterruptedException {

        int res = 0;

        // 对标记值进行取值，并做累加
        for (IntWritable value : values) {
            int one = value.get();
            res += one;
        }

        // 得到结果后，对结果进行写出操作,注意需要对VALUE包装成 IntWritable
        context.write(key,new IntWritable(res));

    }
}
