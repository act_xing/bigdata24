package com.shujia.mr.CombineFile;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.CombineTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * 表示整个运行程序Job的入口类，在该类的main函数中可以定义当前Job的一些配置信息
 *      自定义Mapper类 Reducer类 输入数据路径 和 输出数据路径
 *
 *
 * 注意： 一般遇到如下错误表示当前类的路径给错了
 * 2023-05-30 14:34:41,780 WARN org.apache.hadoop.mapred.MapTask: Unable to initialize MapOutputCollector org.apache.hadoop.mapred.MapTask$MapOutputBuffer
 * java.lang.ClassCastException: interface javax.xml.soap.Text
 *
 *
 * 在HADOOP集群中执行的命令：
 *      hadoop jar hadoopCode-1.0-SNAPSHOT.jar com.shujia.mr.wordcount.CombineFile
 *
 */
public class CombineFile {
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {

        // 1.创建Job的操作对象

        // 2.需要创建配置类对象
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        // 3.配置Job
        job.setJobName("CombineFile");
        job.setJarByClass(CombineFile.class);

        // 4.设置Mapper 及其输出类型
        job.setMapperClass(CombineFileMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        // 5.设置Reduce及其输出类型
        job.setReducerClass(CombineFileReducer.class);

        // 6.设置最终输出的数据类型
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);


        // 设置多个ReduceTask
        job.setNumReduceTasks(2);


        // 该类中可以实现对小文件的合并操作
        job.setInputFormatClass(CombineTextInputFormat.class);

        // 设置切片大小
        CombineTextInputFormat.setMaxInputSplitSize(job,4194304L);



        // 7.设置输入输出路径(输入路径既可以指定文件，也可以指定目录)

        //本地执行的路径

        FileInputFormat.addInputPath(job,new Path("data/words"));
        FileOutputFormat.setOutputPath(job,new Path("output/combineFile"));

        // Hadoop集群执行的路径
//        FileInputFormat.addInputPath(job,new Path("/input"));
//        FileOutputFormat.setOutputPath(job,new Path("/output/wordcount2"));

        // 8.提交执行当前Job
        job.waitForCompletion(true);

    }
}
