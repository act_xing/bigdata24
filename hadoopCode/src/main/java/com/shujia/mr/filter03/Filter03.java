package com.shujia.mr.filter03;

import com.shujia.mr.fiter2.Filter02Mapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * 如果以student类作为Mapper的输出VALUE类型，那么会报以下错误：
 *
 * 2023-06-01 09:47:52,263 WARN org.apache.hadoop.mapred.MapTask: Unable to initialize MapOutputCollector org.apache.hadoop.mapred.MapTask$MapOutputBuffer
 * java.lang.NullPointerException
 * 	at org.apache.hadoop.mapred.MapTask$MapOutputBuffer.init(MapTask.java:1011)
 * 	at java.lang.Thread.run(Thread.java:750)
 * 2023-06-01 09:47:52,264 INFO org.apache.hadoop.mapred.LocalJobRunner: map task executor complete.
 * 2023-06-01 09:47:52,267 WARN org.apache.hadoop.mapred.LocalJobRunner: job_local1903328928_0001
 * java.lang.Exception: java.io.IOException: Initialization of all the collectors failed. Error in last collector was :null
 * 	at org.apache.hadoop.mapred.LocalJobRunner$Job.runTasks(LocalJobRunner.java:462)
 * 	at org.apache.hadoop.mapred.LocalJobRunner$Job.run(LocalJobRunner.java:522)
 * Caused by: java.io.IOException: Initialization of all the collectors failed. Error in last collector was :null
 *
 * 错误原因是因为Student没有做实例化相关的操作
 *
 */
public class Filter03 {
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        // 1.创建Job的操作对象

        // 2.需要创建配置类对象
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        // 3.配置Job
        job.setJobName("Filter03");
        job.setJarByClass(Filter03.class);

        // 4.设置Mapper 及其输出类型
        job.setMapperClass(Filter03Mapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Student.class);

        // 5.设置Reduce及其输出类型
        job.setReducerClass(Filter03Reducer.class);

        // 6.设置最终输出的数据类型
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(Student.class);

        // 7.设置输入输出路径(输入路径既可以指定文件，也可以指定目录)

        //本地执行的路径
        FileInputFormat.addInputPath(job, new Path("output/reduceJoin"));
        FileOutputFormat.setOutputPath(job, new Path("output/filter03"));

        // 8.提交执行当前Job
        job.waitForCompletion(true);
    }

}
