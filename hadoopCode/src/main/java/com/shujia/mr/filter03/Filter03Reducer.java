package com.shujia.mr.filter03;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Filter03Reducer extends Reducer<Text,Student, NullWritable, Student> {
    @Override
    protected void reduce(Text key, Iterable<Student> values, Reducer<Text, Student, NullWritable, Student>.Context context) throws IOException, InterruptedException {

        // 创建列表，并对列表中的学生对象进行排序
        ArrayList<Student> allStu = new ArrayList<>();

        // 相同班级中所有学生数据
        for (Student student : values) {
//            allStu.add(student);
        //每次添加数据时，都是对对象的引用添加到列表中，所当当前对象数据发生变化时，而allStu中所有的引用都指向一个对象
            // public Student(String id, String name, String age, String gender, String clazz, int score) {
            Student newStudent = new Student(student.getId(), student.getName(), student.getAge(), student.getGender(), student.getClazz(), student.getScore());
            allStu.add(newStudent);
        }



        // 对ArrayList中的数据进行排序
        // Collections是JAVA的集合工具类，在该类中存在有一些函数可以对集合进行排序
        Collections.sort(allStu, new Comparator<Student>() {
            /**
             * 提供的Comparator中需要自定义compare函数，在函数中给定比较的逻辑
             * @param o1 the first object to be compared.
             * @param o2 the second object to be compared.
             * @return
             */
            @Override
            public int compare(Student o1, Student o2) {
                int thisScore = o1.getScore();
                int thatScore = o2.getScore();
                // (thisValue<thatValue ? -1 : (thisValue==thatValue ? 0 : 1));
                // 该方式为升序的方式,如果要倒序排序，直接在() 之前进行取反
                return -(thisScore < thatScore ? -1:(thisScore == thatScore ? 0 : 1));
            }
        });

        Student student01 = allStu.get(0);
        context.write(NullWritable.get(),student01);
        Student student02 = allStu.get(1);
        context.write(NullWritable.get(),student02);
        Student student03 = allStu.get(2);
        context.write(NullWritable.get(),student03);


    }
}
