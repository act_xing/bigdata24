package com.shujia.mr.filter03;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class Filter03Mapper extends Mapper<LongWritable, Text,Text,Student> {
    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, Student>.Context context) throws IOException, InterruptedException {
        // 1500100033	桑昆峰,24,男,理科三班,532
        String oneLine = value.toString();

        String[] split = oneLine.split("\t");
        String id = split[0];
        // 桑昆峰,24,男,理科三班,532
        String[] otherInfo = split[1].split(",");

        // public Student(String id, String name, String age, String gender, String clazz, int score) {
        Student student = new Student(id, otherInfo[0], otherInfo[1], otherInfo[2], otherInfo[3], Integer.parseInt(otherInfo[4]));
        context.write(new Text(student.getClazz()),student);
    }
}
