package com.shujia.mr.combine;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * 1.在自定义Mapper类中需要实现Map阶段的数据处理逻辑,并需要继承 .mapreduce.Mapper 类
 *    而在 Mapper<KEYIN, VALUEIN, KEYOUT, VALUEOUT> 父类中，存在有四个泛型，需要手动指定其类型
 *      KEYIN, VALUEIN => 表示输入数据的Key以及Value对应的类型，从之前分析的WordCount流程图中可以看到输入数据的Key是偏移量（数据读取的位置一般来说是Long类型）
 *                          输入数据的Value 是文本文件中的一行数据 （一行数据是用字符串来表示）
 *          由于MapReduce有自己的数据结构，不能直接使用JAVA中的数据结构，在继承的泛型中不能使用JAVA中的数据类型，但是在函数中可以使用
 *          根据MapReduce的类型映射表 可以得到：
 *              KEYIN：LongWritable
 *              VALUEIN:Text
 *
 *      KEYOUT, VALUEOUT => 根据WordCount分析得到最终输出的结果是  Key: 单词   Value：标记值 1
 *                              对于单词对应JAVA的字符串 -> MapReduce中的Text   对应标记值 1 是一个数值类型 int  -> IntWritable
 *
 *
 * 2.需要重新父类中的map函数，并在Map函数中实现自己的数据处理逻辑
 *
 *
 */
public class CombineMapper extends Mapper<LongWritable, Text,Text, IntWritable> {

    /**
     *
     * @param key 表示输入数据的Key -> 偏移量
     * @param value 表示输入数据的 VALUE -> 一行数据
     * @param context 上下文对象，连接Map和Reduce的一个主线
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, IntWritable>.Context context) throws IOException, InterruptedException {
        // hello python
        String[] words = value.toString().split(" ");

        for (String word : words) {

            // 根据输出数据的类型要求，需要将类型进行转换
            Text outKey = new Text(word);
            IntWritable outValue = new IntWritable(1);
            // write函数是将数据写出至磁盘，之后提供给Reduce阶段进行做数据的拉取
            context.write(outKey,outValue);
        }
    }
}
