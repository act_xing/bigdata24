package com.shujia.mr.hw.emp;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 *
 * 在HADOOP集群中执行的命令：
 *      hadoop jar hadoopCode-1.0-SNAPSHOT.jar com.shujia.mr.hw.emp.HWEmp
 *
 */
public class HWEmp {
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {

        // 1.创建Job的操作对象

        // 2.需要创建配置类对象
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        // 3.配置Job
        job.setJobName("HWEmp");
        job.setJarByClass(HWEmp.class);

        // 4.设置Mapper 及其输出类型
        job.setMapperClass(HWEmpMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        // 5.设置Reduce及其输出类型
        job.setReducerClass(HWEmpReducer.class);

        // 6.设置最终输出的数据类型
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        // 7.设置输入输出路径(输入路径既可以指定文件，也可以指定目录)

        //本地执行的路径
//        FileInputFormat.addInputPath(job,new Path("data/words2.txt"));
        //        FileOutputFormat.setOutputPath(job,new Path("output/wordcount2"));

        // Hadoop集群执行的路径
        FileInputFormat.addInputPath(job,new Path("/api/emp.txt"));
        FileOutputFormat.setOutputPath(job,new Path("/output/hwemp"));

        // 8.提交执行当前Job
        job.waitForCompletion(true);

    }
}
