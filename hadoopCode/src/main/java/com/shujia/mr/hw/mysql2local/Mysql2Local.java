package com.shujia.mr.hw.mysql2local;

import com.shujia.mr.filter1.Filter01;
import com.shujia.mr.filter1.Filter01Mapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class Mysql2Local {
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        // 3.配置Job
        job.setJobName("Mysql2Local");
        job.setJarByClass(Mysql2Local.class);

        // 4.设置Mapper 及其输出类型
        job.setMapperClass(Mysql2LocalMapper.class);
        job.setMapOutputKeyClass(NullWritable.class);
        job.setMapOutputValueClass(Text.class);

        // 6.设置最终输出的数据类型
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(Text.class);

        // 7.设置输入输出路径(输入路径既可以指定文件，也可以指定目录)

        //本地执行的路径
//        FileInputFormat.addInputPath(job, new Path("data/emp.txt"));
        FileInputFormat.addInputPath(job, new Path("/api/emp.txt"));
        FileOutputFormat.setOutputPath(job, new Path("/output/mysql2local"));


        // 8.提交执行当前Job
        job.waitForCompletion(true);
    }
}
