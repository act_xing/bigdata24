package com.shujia.mr.hw.mysql2local;


import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.sql.*;
import java.util.HashMap;

public class Mysql2LocalMapper extends Mapper<LongWritable, Text, NullWritable,Text> {

    HashMap<Integer,String> dept = new HashMap();

    // 当前函数中链接MySQL读取维度表数据（小表）
    @Override
    protected void setup(Mapper<LongWritable, Text, NullWritable, Text>.Context context) throws IOException, InterruptedException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection mysqlCon = DriverManager.getConnection("jdbc:mysql://master:3306/bigdata22?useSSL=false&useUnicode=true&characterEncoding=utf8"
                    ,"root","123456"
            );
            Statement statement = mysqlCon.createStatement();

            ResultSet resultSet = statement.executeQuery("select * from dept");

            while (resultSet.next()){
                int deptno = resultSet.getInt(1);
                String deptName = resultSet.getString(2);
                dept.put(deptno,deptName);
            }
            statement.close();
            mysqlCon.close();
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, NullWritable, Text>.Context context) throws IOException, InterruptedException {
        String[] split = value.toString().split(",");
        String deptNo = split[6];

        String deptName = dept.get(Integer.parseInt(deptNo));

        String res = value.toString() + ","+deptName ;
        context.write(NullWritable.get(),new Text(res));
    }
}
