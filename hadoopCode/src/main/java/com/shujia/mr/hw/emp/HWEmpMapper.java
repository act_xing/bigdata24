package com.shujia.mr.hw.emp;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * 针对HDFS中保存的emp表中的数据（HDFS阶段从MySQL中读取写入），做MR数据计算，统计各部门的年度人力资源成本
 *
 */
public class HWEmpMapper extends Mapper<LongWritable, Text,Text, IntWritable> {

    /**
     *
     * @param key 表示输入数据的Key -> 偏移量
     * @param value 表示输入数据的 VALUE -> 一行数据
     * @param context 上下文对象，连接Map和Reduce的一个主线
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, IntWritable>.Context context) throws IOException, InterruptedException {
        // 7934,MILLER,CLERK,7782,1300,0,10
        String[] columns = value.toString().split(",");

        String deptno = columns[6];
        int sum_sal = Integer.parseInt(columns[4]) * 12 +Integer.parseInt(columns[5]);

        context.write(new Text(deptno),new IntWritable(sum_sal));
    }
}
