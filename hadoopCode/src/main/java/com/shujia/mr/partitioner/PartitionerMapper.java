package com.shujia.mr.partitioner;

import com.shujia.mr.filter03.Student;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class PartitionerMapper extends Mapper<LongWritable, Text,Text,PartitionerStudent> {
    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, PartitionerStudent>.Context context) throws IOException, InterruptedException {
        String[] split = value.toString().split("\t");
        String id = split[0];
        String[] otherInfo = split[1].split(",");

        PartitionerStudent student = new PartitionerStudent(id, otherInfo[0], otherInfo[1], otherInfo[2], otherInfo[3], Integer.parseInt(otherInfo[4]));
        context.write(new Text(student.getClazz()),student);

    }
}
