package com.shujia.mr.partitioner;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;


// 由于对年龄做了分区，那么相同分区编号的数据会发送到同一个ReduceTask中也表示同一个年龄的学生会在一个ReduceTask中处理
// 执行reduce函数时，同一个班级同一个年龄的数据会在一个Reduce函数中执行
public class PartitionerReducer extends Reducer<Text,PartitionerStudent, NullWritable,PartitionerStudent> {
    /**
     *
     * @param key 班级数据
     * @param values 学生对象，同一个年龄的学生，同一个班级的学生
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void reduce(Text key, Iterable<PartitionerStudent> values, Reducer<Text, PartitionerStudent, NullWritable, PartitionerStudent>.Context context) throws IOException, InterruptedException {
        for (PartitionerStudent stu : values) {
            context.write(NullWritable.get(),stu);
        }
    }
}
