package com.shujia.mr.partitioner;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;


// 注意当前Partitioner的类是在 org.apache.hadoop.mapreduce 下，并且
public class MyPartitioner extends Partitioner<Text, PartitionerStudent> {
    @Override
    public int getPartition(Text key, PartitionerStudent value, int numPartitions) {

        String age = value.getAge();
        if(age.equals("23")){
            return 0;
        }else if (age.equals("24")){
            return 1;
        }else if (age.equals("21")){
            return 2;
        }else{
            return 3;
        }
    }


}
