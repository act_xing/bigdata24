package com.shujia.mr.partitioner;

import com.shujia.mr.wordcount.WordCount;
import com.shujia.mr.wordcount.WordCountMapper;
import com.shujia.mr.wordcount.WordCountReducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * 需求：
 *      现在读取ReduceJoin的结果数据，将读取到的数据包装成学生对象，之后再将学生对象通过Reduce写出到多个文件中，
 *              区分的逻辑是按照年龄 每个年龄保存到一个文件中
 *       1500100013	逯君昊,24,男,文科二班,369
 *       1500100014	羿旭炎,23,男,理科五班,396
 */
public class PartitionDriver {
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf);
        // 3.配置Job
        job.setJobName("PartitionDriver");
        job.setJarByClass(PartitionDriver.class);

        // 4.设置Mapper 及其输出类型
        job.setMapperClass(PartitionerMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(PartitionerStudent.class);

        // 设置自定义分区
        job.setPartitionerClass(MyPartitioner.class);


        // 5.设置Reduce及其输出类型
        job.setReducerClass(PartitionerReducer.class);

        // 6.设置最终输出的数据类型
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(PartitionerStudent.class);


        // 设置多个ReduceTask
        job.setNumReduceTasks(4);

        // 7.设置输入输出路径(输入路径既可以指定文件，也可以指定目录)

        //本地执行的路径

        FileInputFormat.addInputPath(job,new Path("output/reduceJoin"));
        FileOutputFormat.setOutputPath(job,new Path("output/partitioner"));

        // 8.提交执行当前Job
        job.waitForCompletion(true);
    }
}
