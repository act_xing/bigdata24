package com.shujia.mr.count;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 *  Mapper<KEYIN, VALUEIN, KEYOUT, VALUEOUT>
 *       KEYIN, VALUEIN
 *       KEYOUT, VALUEOUT  => 学生ID作为Key 学生的成绩作为VALUE  => Text, IntWritable

 */


public class ScoreCountMapper extends Mapper<LongWritable, Text,Text, IntWritable> {
    /**
     *
     * @param key  偏移量
     * @param value 一行数据
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, IntWritable>.Context context) throws IOException, InterruptedException {
        String[] split = value.toString().split(",");
        String id = split[0];
        int score = Integer.parseInt(split[2]);

        // 写出数据
        context.write(new Text(id),new IntWritable(score));
    }
}
