package com.shujia.mr.count;


import com.shujia.mr.wordcount.WordCount;
import com.shujia.mr.wordcount.WordCountMapper;
import com.shujia.mr.wordcount.WordCountReducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * 需求：
 *      读取学生的成绩数据，并按学生ID进行分组对成绩进行求和
 *
 *  分析：
 *      1.Mapper阶段：
 *          （按行读取文本中的数据，并对当前行的数据进行做处理，并组合成KeyValue形式将数据写出至Reduce）
 *          ① 读取每一行数据  1500100001,1000001,98
 *          ② 需要对字符串进行做切分
 *          ③ 需要组合 Key VALUE 将数据写出，需要以学生ID作为Key 学生的成绩作为VALUE写出至Reduce端
 *                  因为需要按照学生进行分组，将相同学生的数据汇集到一起，再做成绩的累加
 *
 *
 *      2.Reducer阶段
 *          ① 接受Mapper端输出的数据，以相同学生ID的数据进行执行一次Reduce函数
 *          ② 对相同学生的所有成绩数据进行累计求和
 *          ③ 将累计的数据以学生ID作为Key 学生总分作为VALUE写出到HDFS或本地
 *
 */
public class ScoreCount {
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        // 1.创建Job的操作对象

        // 2.需要创建配置类对象
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        // 3.配置Job
        job.setJobName("ScoreCount");
        job.setJarByClass(ScoreCount.class);

        // 4.设置Mapper 及其输出类型
        job.setMapperClass(ScoreCountMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        // 5.设置Reduce及其输出类型
        job.setReducerClass(ScoreCountReducer.class);

        // 6.设置最终输出的数据类型
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        // 7.设置输入输出路径(输入路径既可以指定文件，也可以指定目录)

        //本地执行的路径
        FileInputFormat.addInputPath(job, new Path("data/score.txt"));
        FileOutputFormat.setOutputPath(job, new Path("output/scoreCount"));

        // Hadoop集群执行的路径
//        FileInputFormat.addInputPath(job,new Path("/input"));
//        FileOutputFormat.setOutputPath(job,new Path("/output/wordcount2"));

        // 8.提交执行当前Job
        job.waitForCompletion(true);
    }
}
