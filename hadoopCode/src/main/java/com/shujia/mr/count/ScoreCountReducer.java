package com.shujia.mr.count;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Reducer<KEYIN,VALUEIN,KEYOUT,VALUEOUT>
 *     KEYIN,VALUEIN:
 *
 */
public class ScoreCountReducer extends Reducer<Text, IntWritable,Text,IntWritable> {
    /**
     *
     * @param key 学生的ID
     * @param values 相同学生的所有成绩数据
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Reducer<Text, IntWritable, Text, IntWritable>.Context context) throws IOException, InterruptedException {

        int totalScore = 0;
        for (IntWritable value : values) {
            int score = value.get();
            totalScore += score;
        }
        context.write(key,new IntWritable(totalScore));

    }
}
