package com.shujia.zookeeper;


import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;
import java.util.List;

// 作业： 递归删除所有节点
public class Code07DeleteAll {
    public static void main(String[] args) throws IOException, InterruptedException, KeeperException {

        ZooKeeper zooKeeper = new ZooKeeper("192.168.49.92:2181", 20 * 1000, new Watcher() {

            // 对于接口对象创建时，需要实现其抽象函数
            @Override
            public void process(WatchedEvent event) {
                System.out.println("Zookeeper链接正在发生变化..执行process函数");
            }
        });

        String path = "/api";
        deleteAll(zooKeeper,path,path);

    }

    public static void deleteAll(ZooKeeper zooKeeper, String path,String startPath) throws InterruptedException, KeeperException {
        // path如果存在
        Stat stat = zooKeeper.exists(path, false);
        if (stat != null) {
            int numChildren = stat.getNumChildren();
            if (numChildren == 0) {
                zooKeeper.delete(path, -1);
            } else {
                List<String> children = zooKeeper.getChildren(path, false);
                for (String child : children) {
                    String childPath = path + "/" + child;
                    deleteAll(zooKeeper,childPath,startPath);
                    // 删除子节点后，往上回溯
                    if (startPath.equals(path)){
                        zooKeeper.delete(path,-1);
                    }else{
                        deleteAll(zooKeeper,path,startPath);
                    }
                }
            }

        }


    }

}
