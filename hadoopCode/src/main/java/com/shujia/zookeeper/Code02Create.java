package com.shujia.zookeeper;

import org.apache.hadoop.fs.Path;
import org.apache.zookeeper.*;

import java.io.IOException;

public class Code02Create {
    public static void main(String[] args) throws IOException, InterruptedException, KeeperException {
        ZooKeeper zooKeeper = new ZooKeeper("192.168.49.92:2181", 20 * 1000, new Watcher() {

            // 对于接口对象创建时，需要实现其抽象函数
            @Override
            public void process(WatchedEvent event) {
                System.out.println("Zookeeper链接正在发生变化..执行process函数");
            }
        });

        /**
         * public String create(final String path, byte data[], List<ACL> acl,
         *             CreateMode createMode)
         *
         *  OPEN_ACL_UNSAFE: 创建节点，并且赋予其读和写的权限
         *  CREATOR_ALL_ACL: 创建节点有写权限
         *  READ_ACL_UNSAFE: 表示有读权限
         *
         *
         *  CreateMode:
         *      PERSISTENT: 持久化节点，普通节点
         *      PERSISTENT_SEQUENTIAL: 持久化序列化节点
         *      EPHEMERAL: 临时节点
         *      EPHEMERAL_SEQUENTIAL：临时序列化节点
         *
         *
         */
//        zooKeeper.create("/api","apiData".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        zooKeeper.create("/api/eph", "apiData".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL);

        System.out.println("执行了..");
        Thread.sleep(100 * 1000);

        zooKeeper.close();
    }
}
