package com.shujia.zookeeper;

import com.google.common.primitives.Bytes;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;

public class Code03GetData {
    public static void main(String[] args) throws IOException, InterruptedException, KeeperException {
        ZooKeeper zooKeeper = new ZooKeeper("192.168.49.92:2181", 20 * 1000, new Watcher() {

            // 对于接口对象创建时，需要实现其抽象函数
            @Override
            public void process(WatchedEvent event) {
                System.out.println("Zookeeper链接正在发生变化..执行process函数");
            }
        });


        // public byte[] getData(final String path, Watcher watcher, Stat stat)
        String path = "/api";

        Stat exists = zooKeeper.exists(path, false);

        if (exists != null){
            byte[] data = zooKeeper.getData(path, false, exists);

            String resData = new String(data);

            System.out.println("结果："+resData);

        }




        zooKeeper.close();

//        zooKeeper.getData(
//                "/api",false,
//        )
    }
}
