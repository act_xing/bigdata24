package com.shujia.zookeeper;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

import java.io.IOException;
import java.util.List;

public class Code05GetChildChange {
    public static void main(String[] args) throws IOException, InterruptedException, KeeperException {
        ZooKeeper zooKeeper = new ZooKeeper("192.168.49.92:2181", 20 * 1000, new Watcher() {

            // 对于接口对象创建时，需要实现其抽象函数
            @Override
            public void process(WatchedEvent event) {
                System.out.println("Zookeeper链接正在发生变化..执行process函数");
            }
        });

        List<String> children = zooKeeper.getChildren("/api", new Watcher() {
            // 当子节点发生变化时，会异步触发 process 函数
            @Override
            public void process(WatchedEvent event) {
                System.out.println("监听被启动");
            }
        });
        Thread.sleep(Long.MAX_VALUE);
        zooKeeper.close();

    }
}
