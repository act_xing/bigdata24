package com.shujia.zookeeper;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

import java.io.IOException;

public class Code01ZookeeperCon {

    public static void main(String[] args) throws IOException, InterruptedException {

        // 创建连接
        // public ZooKeeper(String connectString, int sessionTimeout, Watcher watcher)
        // String connectString 表示Zookeeper的链接信息
        // int sessionTimeout 表示会话超时时间 毫秒
        // Watcher watcher 监控器的对象
        ZooKeeper zooKeeper = new ZooKeeper("master:2181,node1:2181,node2:2181", 10 * 1000, new Watcher() {

            // 对于接口对象创建时，需要实现其抽象函数
            @Override
            public void process(WatchedEvent event) {
                System.out.println("Zookeeper链接正在发生变化..执行process函数");
            }
        });

        System.out.println("代码正在执行...");

        zooKeeper.close();
    }
}
