package com.shujia.zookeeper;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;
import java.util.List;

public class Code04GetChild {
    public static void main(String[] args) throws IOException, InterruptedException, KeeperException {
        ZooKeeper zooKeeper = new ZooKeeper("192.168.49.92:2181", 20 * 1000, new Watcher() {

            // 对于接口对象创建时，需要实现其抽象函数
            @Override
            public void process(WatchedEvent event) {
                System.out.println("Zookeeper链接正在发生变化..执行process函数");
            }
        });

//        for (String child : zooKeeper.getChildren("/api", false)) {
//            System.out.println(child);
//        }
//
        String path = "/api";

        getAllChild(path,zooKeeper);

        zooKeeper.close();

    }


    public static void getAllChild(String path,ZooKeeper zooKeeper) throws InterruptedException, KeeperException {
        Stat stat = zooKeeper.exists(path, false);

        if(stat.getNumChildren() > 0){
            List<String> children = zooKeeper.getChildren(path, false);
            for (String child : children) {
                String childPath = path +"/"+child;
                System.out.println("获取到子节点："+childPath);
                getAllChild(childPath,zooKeeper);
            }
        }
    }


}
