package com.shujia.zookeeper;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

import java.io.IOException;

public class Code06DeleteNode {
    public static void main(String[] args) throws IOException, InterruptedException, KeeperException {
        ZooKeeper zooKeeper = new ZooKeeper("192.168.49.92:2181", 20 * 1000, new Watcher() {

            // 对于接口对象创建时，需要实现其抽象函数
            @Override
            public void process(WatchedEvent event) {
                System.out.println("Zookeeper链接正在发生变化..执行process函数");
            }
        });

        // Directory not empty for /api 无法删除一个不为空的节点

        zooKeeper.delete("/api",-1);


        zooKeeper.close();

        // 作业： 递归删除所有节点

    }
}
