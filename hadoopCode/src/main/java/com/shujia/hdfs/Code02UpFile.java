package com.shujia.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class Code02UpFile {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        Configuration configuration = new Configuration();
//        configuration.set("fs.defaultFS","hdfs://master:9000");
//
//        FileSystem fileSystem = FileSystem.get(configuration);

        // 方式一：上传数据，并设置用户名
//        FileSystem fileSystem = FileSystem.get(new URI("hdfs://master:9000"), configuration,"root");

        // 设置系统变量
//        System.setProperty("HADOOP_USER_NAME","root");
        FileSystem fileSystem = FileSystem.get(new URI("hdfs://node1:8020"), configuration);

        // 在系统环境变量中添加 "HADOOP_USER_NAME","root"

        // 在VM环境中添加 HADOOP_USER_NAME=root


        fileSystem.copyFromLocalFile(new Path("data/score.txt"),new Path("/api/"));


        fileSystem.close();
    }
}
