package com.shujia.hdfs.hw;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;

/**
 * 1.从MySQL中取出emp数据过滤部门编号是 10 和 20 的员工 取出其部门编号 员工编号，姓名，工作信息，
 * 并分别写入到HDFS的/api/emp目录中的两个文件中，每个文件保存一个部门的数据
 */
public class HW01Emp2HDFS {
    public static void main(String[] args) throws ClassNotFoundException, SQLException, URISyntaxException, IOException, InterruptedException {

        // 1.从MySQL中读取数据
        Class.forName("com.mysql.jdbc.Driver");
//        DriverManager.registerDriver(new Driver());

        // 2.创建MySQL链接对象
        Connection mysqlCon = DriverManager.getConnection("jdbc:mysql://master:3306/bigdata22?useSSL=false&useUnicode=true&characterEncoding=utf8"
                , "root", "123456"
        );


        // 3.SQL执行的操作对象
        Statement statement = mysqlCon.createStatement();

        // 4.执行查询SQL
        ResultSet resultSet = statement.executeQuery("SELECT DEPTNO,EMPNO,ENAME,JOB FROM emp WHERE deptno in (10,20)");

        // 6.创建HDFS的链接对象

        Configuration configuration = new Configuration();
        FileSystem fileSystem = FileSystem.get(new URI("hdfs://master:9000"), configuration, "root");


        // fsDataOutputStream 父类中存在有OutputStream
        FSDataOutputStream fsDataOutputStream10 = fileSystem.create(new Path("/api/emp/dept10.txt"));
        OutputStreamWriter outputStreamWriter10 = new OutputStreamWriter(fsDataOutputStream10);

        FSDataOutputStream fsDataOutputStream20 = fileSystem.create(new Path("/api/emp/dept20.txt"));
        OutputStreamWriter outputStreamWriter20 = new OutputStreamWriter(fsDataOutputStream20);


        // 5.遍历结果
        while (resultSet.next()) {
            int deptno = resultSet.getInt(1);
            int EMPNO = resultSet.getInt(2);
            String ENAME = resultSet.getString(3);
            String Job = resultSet.getString(4);

            if (deptno == 10) {
                outputStreamWriter10.write(deptno + "," + EMPNO + "," + ENAME + "," + Job);
                outputStreamWriter10.flush();
            }else{
                outputStreamWriter20.write(deptno + "," + EMPNO + "," + ENAME + "," + Job);
                outputStreamWriter20.flush();
            }

        }


        //7.关闭
        outputStreamWriter20.close();
        fsDataOutputStream20.close();
        outputStreamWriter10.close();
        fsDataOutputStream10.close();
        fileSystem.close();
        statement.close();
        mysqlCon.close();

    }
}
