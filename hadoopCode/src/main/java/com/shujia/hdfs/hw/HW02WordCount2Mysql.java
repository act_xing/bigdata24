package com.shujia.hdfs.hw;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

/**
 * 2.读取HDFS中的words.txt数据，并对单词进行统计，之后再将统计结果写入至MySQL中的wordcount表
 */
public class HW02WordCount2Mysql {
    public static void main(String[] args) throws ClassNotFoundException, SQLException, URISyntaxException, IOException, InterruptedException {
        Class.forName("com.mysql.jdbc.Driver");
//        DriverManager.registerDriver(new Driver());

        Connection mysqlCon = DriverManager.getConnection("jdbc:mysql://master:3306/bigdata22?useSSL=false&useUnicode=true&characterEncoding=utf8"
                ,"root","123456"
        );

        Statement statement = mysqlCon.createStatement();

        // 创建HDFS的链接对象

        Configuration configuration = new Configuration();
        FileSystem fileSystem = FileSystem.get(new URI("hdfs://master:9000"), configuration,"root");

        // 从HDFS读出数据
        FSDataInputStream open = fileSystem.open(new Path("/words.txt"));
        InputStreamReader inputStreamReader = new InputStreamReader(open);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String line;

        HashMap<String, Integer> wordCounts = new HashMap<>();

        while ((line = bufferedReader.readLine()) != null){
            // hello python
            String[] splitRes = line.split(" ");
            for (String word : splitRes) {
                // 1.先判断单词是否在HashMap中，如果存在，那么取出对应的Value，并对Value + 1 保存  ，如果不存在 直接以 word,1 保存
                if (wordCounts.containsKey(word)) {
                    Integer value = wordCounts.get(word);
                    wordCounts.put(word,value+1);
                }else{
                    wordCounts.put(word,1);
                }
            }
        }

        // 2.对单词进行遍历，将数据写入MySQL中

        for (String word : wordCounts.keySet()) {
            Integer value = wordCounts.get(word);

            String sql = String.format(
                    "insert into wordcount values('%s',%s)", word, value
            );

            statement.executeUpdate(sql);

        }

        // 关闭
        statement.close();
        mysqlCon.close();
        open.close();
        bufferedReader.close();
    }
}
