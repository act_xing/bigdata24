package com.shujia.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class Code06Mkdir {
    public static void main(String[] args) throws IOException, URISyntaxException {

        Configuration configuration = new Configuration();
//        configuration.set("fs.defaultFS","hdfs://master:9000");
//
//        FileSystem fileSystem = FileSystem.get(configuration);

        FileSystem fileSystem = FileSystem.get(new URI("hdfs://master:9000"), configuration);

        boolean mkdirs = fileSystem.mkdirs(new Path("/api/dir/"));

        fileSystem.close();
    }
}
