package com.shujia.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;


public class Code01ConnectFileSystem {
    public static void main(String[] args) throws IOException, URISyntaxException {

        Configuration configuration = new Configuration();
//        configuration.set("fs.defaultFS","hdfs://master:9000");
//
//        FileSystem fileSystem = FileSystem.get(configuration);

        FileSystem fileSystem = FileSystem.get(new URI("hdfs://master:9000"), configuration);

        // org.apache.hadoop.hdfs.DistributedFileSystem
        System.out.println("链接成功... FileSystem的自实现类："+fileSystem.getClass().getName());

        fileSystem.close();
    }
}
