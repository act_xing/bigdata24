package com.shujia.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class Code04DownloadData {
    public static void main(String[] args) throws URISyntaxException, IOException {
        Configuration configuration = new Configuration();

        FileSystem fileSystem = FileSystem.get(new URI("hdfs://master:9000"), configuration);

        // 第一个路径：HDFS
        fileSystem.copyToLocalFile(new Path("/api/emp.txt"),new Path("data/emp.txt"));

        /**
         * Exception in thread "main" java.io.IOException: (null) entry in command string: null chmod 0644 D:\workspace\hadoopCode\data\words.txt
         * 	at org.apache.hadoop.util.Shell$ShellCommandExecutor.execute(Shell.java:773)
         * 	at org.apache.hadoop.util.Shell.execCommand(Shell.java:869)
         * 	at org.apache.hadoop.util.Shell.execCommand(Shell.java:852)
         *
         * 	错误原因：没有本地HADOOP环境，需要配置
         * 	   1.找到对应HADOOP的bin包
         * 	   2.在win环境中配置对应的环境变量
         * 	   3.重新打开idea获取环境变量
         *     4.执行winutils命令，出现长串内容 则配置成功
         */

        fileSystem.close();
    }
}
