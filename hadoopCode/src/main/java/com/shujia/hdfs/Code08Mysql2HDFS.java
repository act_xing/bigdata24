package com.shujia.hdfs;

import com.mysql.jdbc.Driver;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;

public class Code08Mysql2HDFS {
    public static void main(String[] args) throws ClassNotFoundException, SQLException, URISyntaxException, IOException, InterruptedException {

        // 1.从MySQL中读取数据
        Class.forName("com.mysql.jdbc.Driver");
//        DriverManager.registerDriver(new Driver());

        Connection mysqlCon = DriverManager.getConnection("jdbc:mysql://master:3306/bigdata22?useSSL=false&useUnicode=true&characterEncoding=utf8"
            ,"root","123456"
        );

        // 创建HDFS的链接对象

        Configuration configuration = new Configuration();
        FileSystem fileSystem = FileSystem.get(new URI("hdfs://master:9000"), configuration,"root");


        // fsDataOutputStream 父类中存在有OutputStream
        FSDataOutputStream fsDataOutputStream = fileSystem.create(new Path("/api/emp.txt"));
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fsDataOutputStream);


        Statement statement = mysqlCon.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from emp");
        while (resultSet.next()){
            int empno = resultSet.getInt("EMPNO");
            String ename = resultSet.getString("ENAME");
            String job = resultSet.getString("JOB");
            String mgr = resultSet.getString("MGR");
            int sal = resultSet.getInt("SAL");
            int comm = resultSet.getInt("COMM");
            int deptno = resultSet.getInt("DEPTNO");
            outputStreamWriter.write(empno+","+ename+","+job+","+mgr+","+sal+","+comm+","+deptno+"\n");
            outputStreamWriter.flush();
        }

        // 关闭操作
        statement.close();
        mysqlCon.close();
        fsDataOutputStream.close();
        outputStreamWriter.close();

    }
}
