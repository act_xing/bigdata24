package com.shujia.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Code09HDFS2Mysql {
    public static void main(String[] args) throws ClassNotFoundException, SQLException, URISyntaxException, IOException, InterruptedException {
        // 1.从MySQL中读取数据
        Class.forName("com.mysql.jdbc.Driver");
//        DriverManager.registerDriver(new Driver());

        Connection mysqlCon = DriverManager.getConnection("jdbc:mysql://master:3306/bigdata22?useSSL=false&useUnicode=true&characterEncoding=utf8"
                ,"root","123456"
        );

        Statement statement = mysqlCon.createStatement();

        // 创建HDFS的链接对象

        Configuration configuration = new Configuration();
        FileSystem fileSystem = FileSystem.get(new URI("hdfs://master:9000"), configuration,"root");

        // 从HDFS读出数据
        FSDataInputStream open = fileSystem.open(new Path("/api/emp.txt"));
        InputStreamReader inputStreamReader = new InputStreamReader(open);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String line;

        while ((line = bufferedReader.readLine()) != null){
            // 7369,SMITH,CLERK,7902,20
            String[] splitRes = line.split(",");

            // 将其转换成SQL再插入MySQL中
            String sql = String.format(
                    "insert into hdfs2mysql values(%s,'%s','%s','%s',%s)", splitRes[0], splitRes[1], splitRes[2], splitRes[3], splitRes[4]
            );

            statement.executeUpdate(sql);
            System.out.println(sql);
        }


        // 关闭
        statement.close();
        mysqlCon.close();
        open.close();
        bufferedReader.close();
    }
}
