package com.shujia.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.BlockLocation;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

// 获取文件的状态信息
public class Code07GetFileStatus {
    public static void main(String[] args) throws IOException, URISyntaxException {

        Configuration configuration = new Configuration();
//        configuration.set("fs.defaultFS","hdfs://master:9000");
//
//        FileSystem fileSystem = FileSystem.get(configuration);

        FileSystem fileSystem = FileSystem.get(new URI("hdfs://master:9000"), configuration);

        Path path = new Path("/test/hadoop-mapreduce-examples-2.7.6.jar");

        if (fileSystem.exists(path)) {
            FileStatus fileStatus = fileSystem.getFileStatus(path);
            long blockSize = fileStatus.getBlockSize();
            long len = fileStatus.getLen();
            System.out.println("文件长度:"+len+ " blockSize:"+blockSize);

            // 获取文件的BLock块位置信息，同时可以指定任意长度的文件所在的BLock块
            BlockLocation[] fileBlockLocations = fileSystem.getFileBlockLocations(path, 0, len);

            for (BlockLocation fileBlockLocation : fileBlockLocations) {
                // 获取BLock块所在哪些节点上
                String[] hosts = fileBlockLocation.getHosts();
                // 获取长度信息
                long length = fileBlockLocation.getLength();
                for (String host : hosts) {
                    System.out.println("hosts:"+host+" len:"+length);
                }
            }

        }

        fileSystem.close();
    }
}
