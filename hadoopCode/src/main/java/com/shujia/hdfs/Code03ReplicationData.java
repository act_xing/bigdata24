package com.shujia.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;


// 配置信息的优先级：
//   HADOOP集群中的配置 < 项目中resources目录中的配置文件 < Code中的配置信息
public class Code03ReplicationData {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        // Configuration可以用于对当前的链接对象进行做配置，配置内容可以在Hadoop的配置文件中寻找
        Configuration configuration = new Configuration();

        // 设置副本数为2
        configuration.set("dfs.replication","2");

        FileSystem fileSystem = FileSystem.get(new URI("hdfs://master:9000"), configuration,"root");


        fileSystem.copyFromLocalFile(new Path("data/apiWords.txt"),new Path("/"));


        fileSystem.close();
    }
}
