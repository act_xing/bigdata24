insert overwrite table dws.dws_app_acd_mrjq
select
  sgfssj,

  dr_sgs,
  sum(dr_sgs) over(partition by year(sgfssj)) as jn_sgs,
  round(nvl(dr_sgs / lag(dr_sgs,1,0) over(order by sgfssj) - 1,1.0) * 100,2) as tb_sgs,

  dr_swsgs,
  sum(dr_swsgs) over(partition by year(sgfssj)) as  jn_swsgs,
  round(nvl(dr_swsgs / lag(dr_swsgs,1,0) over(order by sgfssj) - 1,1.0) * 100,2) as  tb_swsgs,

  dr_swrs,
  sum(dr_swrs) over(partition by year(sgfssj)) as jn_swrs,
  round(nvl(dr_swrs / lag(dr_swrs,1,0) over(order by sgfssj) - 1,1.0) * 100,2) as tb_swrs,

  dr_ssrs,
  sum(dr_ssrs) over(partition by year(sgfssj)) as jn_ssrs,
  round(nvl(dr_ssrs / lag(dr_ssrs,1,0) over(order by sgfssj) - 1,1.0) * 100,2) as tb_ssrs,

  dr_zjccss,
  sum(dr_zjccss) over(partition by year(sgfssj)) as jn_zjccss,
  round(nvl(dr_zjccss / lag(dr_zjccss,1,0) over(order by sgfssj) - 1,1.0) * 100,2) as tb_zjccss

from (
  select
    sgfssj,
    count(1) as dr_sgs,
    count(case when swrs !=0 then 1 else null end) as dr_swsgs,
    sum(swrs) as dr_swrs,
    sum(ssrs) as dr_ssrs,
    sum(zjccss) as dr_zjccss
  from (
    select
      sgbh,
      sgfssj,
      zjccss,
      ssrs,
      max(swrs) as swrs -- 死亡人数
    from (
        select
            sgbh,
            sgfssj,
            zjccss,
            ssrs,
            explode(array(swrs,swrs24,swrs3,swrs7,swrs30)) as swrs
        from (
            select
                sgbh,
                sgfssj,
                zjccss,
                swrs,swrs24,swrs3,swrs7,swrs30,
                max(ssrs) ssrs -- 取受伤人数
            from (
                select
                    sgbh ,
                    date_format(sgfssj,'yyyy-MM-dd') as sgfssj,
                    zjccss,
                    swrs,swrs24,swrs3,swrs7,swrs30,
                    explode(array(ssrs,ssrs24,ssrs3,ssrs7,ssrs30)) as ssrs
                from
                    dwd.dwd_base_acd_file
            ) as a
            group by sgbh,sgfssj,zjccss,swrs,swrs24,swrs3,swrs7,swrs30
        ) as b
    ) as c
    group by
      sgbh,
      sgfssj,
      zjccss,
      ssrs
  ) as d
  group by sgfssj
) as e
;