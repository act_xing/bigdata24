insert overwrite table dws.dws_app_vio_zqzf
select
    wfsj,

    dr_zd_wfs,
    sum(dr_zd_wfs) over(partition by year(wfsj))  as jn_zd_wfs,
    round(nvl(dr_zd_wfs / lag(dr_zd_wfs,1,0) over(order by wfsj) - 1,1.0) * 100,2) as zd_tb,

    dr_zjjs_wfs,
    sum(dr_zjjs_wfs) over(partition by year(wfsj))  as jn_zjjs_wfs,
    round(nvl(dr_zjjs_wfs / lag(dr_zjjs_wfs,1,0) over(order by wfsj) - 1,1.0) * 100,2) as zjjs_tb,

    dr_yjjs_wfs,
    sum(dr_yjjs_wfs) over(partition by year(wfsj)) as  jn_yjjs_wfs,
    round(nvl(dr_yjjs_wfs / lag(dr_yjjs_wfs,1,0) over(order by wfsj) - 1,1.0) * 100,2) as  yjjs_tb,

    dr_zdhp_wfs,
    sum(dr_zdhp_wfs) over(partition by year(wfsj)) as  jn_zdhp_wfs,
    round(nvl(dr_zdhp_wfs / lag(dr_zdhp_wfs,1,0) over(order by wfsj) - 1,1.0) * 100,2) as  zdhp_tb,

    dr_wzbz_wfs,
    sum(dr_wzbz_wfs) over(partition by year(wfsj)) as  jn_wzbz_wfs,
    round(nvl(dr_wzbz_wfs / lag(dr_wzbz_wfs,1,0) over(order by wfsj) - 1,1.0) * 100,2) as  wzbz_tb,

    dr_tpjp_wfs,
    sum(dr_tpjp_wfs) over(partition by year(wfsj)) as  jn_tpjp_wfs,
    round(nvl(dr_tpjp_wfs / lag(dr_tpjp_wfs,1,0) over(order by wfsj) - 1,1.0) * 100,2) as  tpjp_tb,

    dr_wdtk_wfs,
    sum(dr_wdtk_wfs) over(partition by year(wfsj)) as  jn_wdtk_wfs,
    round(nvl(dr_wdtk_wfs / lag(dr_wdtk_wfs,1,0) over(order by wfsj) - 1,1.0) * 100,2) as  wdtk_tb,

    dr_jspz_wfs,
    sum(dr_jspz_wfs) over(partition by year(wfsj)) as jn_jspz_wfs,
    round(nvl(dr_jspz_wfs / lag(dr_jspz_wfs,1,0) over(order by wfsj) - 1,1.0) * 100,2) as jspz_tb,

    dr_wxjs_wfs,
    sum(dr_wxjs_wfs) over(partition by year(wfsj)) as jn_wxjs_wfs,
    round(nvl(dr_wxjs_wfs / lag(dr_wxjs_wfs,1,0) over(order by wfsj) - 1,1.0) * 100,2) as wxjs_tb,

    dr_cy_wfs,
    sum(dr_cy_wfs) over(partition by year(wfsj)) as jn_cy_wfs,
    round(nvl(dr_cy_wfs / lag(dr_cy_wfs,1,0) over(order by wfsj) - 1,1.0) * 100,2) as cy_tb,

    dr_cz_wfs,
    sum(dr_cz_wfs) over(partition by year(wfsj)) as jn_cz_wfs,
    round(nvl(dr_cz_wfs / lag(dr_cz_wfs,1,0) over(order by wfsj) - 1,1.0) * 100,2) as cz_tb

from (
    select
        wfsj,
        count(distinct id) as dr_zd_wfs,
        count(case when wfxw in ('20011','20021','1702','1703','6022','5049','2010','20102','20010','20020','17021','17031','6032','6033','10043','A01') then 1 else null end) as dr_zjjs_wfs,
        count(case when wfxw in ('20061','20071','1711','1712','1713','6022','A17','20060','20070','17122','17121','1605','1604','17111','6034','6035','60351','S03','10043') then 1 else null end) as dr_yjjs_wfs,
        count(case when wfxw in ('20211','1614','1329','20210','1718','17181','1331','1616','17201','1720') then 1 else null end) as dr_zdhp_wfs,
        count(case when wfxw in ('75011','5702','5703','5003','5006','5007','5010','5011','5009','57031','57032','50101','50102','50041','50042','50051','50052','50012','50011','50062','50061','50072','50071','50112','50111','5001','50031','5004','5005','5012','57022','57021','50032','60021','57011','57012','5054','5706','5601','5701','5002','50021','5008','50022') then 1 else null end) as dr_wzbz_wfs,
        count(case when wfxw in ('57011','57012','5054','5706','5601','5701','5002','50021','5008','50022') then 1 else null end) as dr_tpjp_wfs,
        count(case when wfxw in ('3020','30201','1207','20301','12071','74021') then 1 else null end) as dr_wdtk_wfs,
        count(case when wfxw in ('6021','5051','10022','1002','20671') then 1 else null end) as dr_jspz_wfs,
        count(case when wfxw in ('6026','20111','10072','10062','5703','1010','5006','20032','10052','16103','10064','1015','10151','1610','1709','1009','10091','10061','10141','1014','57031','57032','1005','1006','10104','17091','10051','16102','16104','16101','50062','50061','1704','10821','1082','10101','10103','20110','5012','17094','17093','10054','10053','10063','10102','17092','60063','60061','60062','60021') then 1 else null end) as dr_wxjs_wfs,
        count(case when wfxw in ('1710','20081','20082','1601','17101','17103','1202','1621','16211','17102','17104','20231','20412','6038','1238','1348','13481','71171','1241','1341','1714','1623','1626','16261','1627','17161','6017','60171','1716','16271') then 1 else null end) as dr_cy_wfs,
        count(case when wfxw in ('13542','1342','1201','13532','1637','71181','13534','13544','13543','13541','16391','16393','16392','16394','12011','1602','13421','13422','12012','13533','13531','1353','20241','1639','16371','16373','16374','16372','1354','1346','1239','1608') then 1 else null end) as dr_cz_wfs
    from (
        select
            wfbh as id, -- 主键
            date_format(wfsj,'yyyy-MM-dd') as wfsj,
            regexp_replace(wfxw,'(.*)[A-Z]','$1') as wfxw
        from
            dwd.dwd_base_vio_surveil --- 非现场违法表

        union all

        select
            id,-- 主键
            wfsj,
            regexp_replace(wfxw,'(.*)[A-Z]','$1') as wfxw
        from (
            select
                xh as id,
                date_format(wfsj,'yyyy-MM-dd') as wfsj,
                explode(array(wfxw1,wfxw2,wfxw3,wfxw4,wfxw5)) as wfxw
            from
                dwd.dwd_base_vio_force -- 现场违法表
        ) as b
    ) as a
    where wfsj is not null
    and wfxw is not null
    group by wfsj
) as c;