
insert overwrite table dws.dws_vio_wfjltj_maxsb
select
    sbbh,
    num as sbwfs_day,
    q3 + 1.5*(q3-q1)  as zdgjz,
    wfsj as wfsj_day
from (
    select
        sbbh,
        wfsj,
        num,
        r,
        sum,
        q1_index,
        q3_index,
        sum(case when r=q1_index then num else 0 end) over(partition by sbbh) as q1,
        sum(case when r=q3_index then num else 0 end) over(partition by sbbh) as q3
    from
        (select
            sbbh,
            wfsj,
            num,
            row_number() over(partition by sbbh order by num) as r, -- 排名
            count(1) over(partition by sbbh)  as sum, -- 总数
            ceil(count(1) over(partition by sbbh)/ 4) as q1_index, -- q1的下标
            ceil(count(1) over(partition by sbbh)/ 4 * 3) as q3_index -- q3的下标
        from (
            select
                sbbh,
                date_format(wfsj,'yyyy-MM-dd') as wfsj,
                count(1) as num
            from
                dwd.dwd_base_vio_surveil
            where
                sbbh is not null
            and
                sbbh != ''
            group by
                sbbh,
                date_format(wfsj,'yyyy-MM-dd')
        ) as a
    ) as b
) as c
where num > q3 + 1.5*(q3-q1) and  q3 + 1.5*(q3-q1) > 1