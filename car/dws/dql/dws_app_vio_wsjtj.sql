insert into  dws.dws_app_vio_wsjtj
select
    sbbh,-- 设备编号
    diff as sjc, -- 无数据的天数
    wfsj as wfsj_day, -- 有数据的日期
    last_wfsj as after_day, -- 上一次有数据的日期
    q3 + 1.5*(q3 - q1) as max_value, -- 最大值
    round(diff/(q3 + 1.5*(q3 - q1)) - 1,2) as  more_zb -- 超过占比
 from (
    select
        sbbh,
        wfsj,
        last_wfsj,
        diff,
        r,
        num,
        q1_index,
        q3_index,
        sum(case when q1_index=r then diff else 0 end) over(partition by sbbh) as q1, -- q1
        sum(case when q3_index=r then diff else 0 end) over(partition by sbbh) as q3 -- q3
    from (
        select
            sbbh,
            wfsj,
            last_wfsj,
            diff,
            row_number() over(partition by sbbh order by  diff) r, -- 按照时间差排名
            count(1) over(partition by sbbh) as num, -- 计算总的数据行数
            ceil(count(1) over(partition by sbbh) / 4) as q1_index, -- q1的下标
            ceil(count(1) over(partition by sbbh) / 4 * 3) as q3_index -- q3的下标
        from (
            select
                sbbh,
                wfsj,
                last_wfsj,
                datediff(wfsj,last_wfsj) - 1 as diff -- 计算时间差
            from (
                select
                sbbh,
                wfsj,
                lag(wfsj,1) over(partition by sbbh order by wfsj) as last_wfsj -- 上一次有数据的时间
                from (
                    select
                    distinct
                    sbbh,
                    date_format(wfsj,'yyyy-MM-dd') as wfsj
                    from
                    dwd.dwd_base_vio_surveil
                ) as a
            ) as b
            where last_wfsj is not null
        ) as c
    ) as d
) as e
-- 取出异常数据
where diff > q3 + 1.5*(q3 - q1) and q3 + 1.5*(q3 - q1) !=0;