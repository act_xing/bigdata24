spark-sql \
--master yarn-client \
--num-executors 2 \
--executor-cores 2 \
--executor-memory 4G \
--conf spark.sql.shuffle.partitions=2 \
-f ../dql/dws_app_vio_wsjtj.sql