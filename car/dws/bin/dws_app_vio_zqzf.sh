
# spark 资源设置的依据

# 1、资源充足，看数据量
# 100太服务器，128G+64核
# 比如源表的数据量大概100G --> 800block ---> 800个分区  --> 800task

# 最理想的方式
# --num-executors 100 \
# --executor-cores 8 \
# --executor-memory 16G \

# 合理的方式
# --num-executors 100 \
# --executor-cores 4 \
# --executor-memory 8G \


# 2、资源不足看剩余资源
# 给到剩余资源的1/3d到1/2之间
# 5太服务器，128G+64核
# 比如源表的数据量大概100G --> 800block ---> 800个分区  --> 800task
# --num-executors 5 \
# --executor-cores 20 \
# --executor-memory 40G \

# spark.sql.shuffle.partitions shuffle之后的分区数
# 取决于任务产生的结果的数据量，保证每个分区的数量在128内



# hive -f ../dql/dws_app_vio_zqzf.sql


spark-sql \
--master yarn-client \
--num-executors 2 \
--executor-cores 2 \
--executor-memory 4G \
--conf spark.sql.shuffle.partitions=2 \
-f ../dql/dws_app_vio_zqzf.sql