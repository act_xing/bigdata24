package com.shujia.code;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDTF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.StructField;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * 现需要有一个自定义UDTF函数 该函数可以将JSON数据一行转多行
 *
 *
 *
 * {"movie": [{"movie_name": "肖申克的救赎", "MovieType": "犯罪" }, {"movie_name": "肖申克的救赎", "MovieType": "剧
 * 情" }]}
 *
 * 肖申克的救赎  犯罪
 * 肖申克的救赎 剧情
 *
 * create table movie(
 *  one_line STRING
 * );
 * load data local inpath '/usr/local/soft/data/UDTF.txt' into table movie;
 *
 *  SELECT  get_json_object(one_line,"$.movie[0].movie_name")   FROM movie
 *   get_json_object 可以解析JSON数据用法如下：
 *         $ 表示其根路径
 *         . 表示根路径下的子路径,需要跟Key搭配使用
 *         [] 表示取JSONArray下标值
 *
 *  {"Key":"value","key1":1,"key2":[{},{}]}
 *  {} 可以表示为一个对象，Key为对象的属性，VALUE是对象属性值
 *
 *  创建一个UDTF函数实现如下功能：
 *  SELECT get_movie_type(get_json_object(one_line,'$.movie')) FROM movie;
 *
 *  肖申克的救赎  犯罪
 *  肖申克的救赎 剧情
 *
 *   4.上传 jar 包到Linux中 并在 hive 中通过 add jar 命令添加包到hive的 class path 路径下
 *        add jar /usr/local/soft/jars/hiveCode-1.0-SNAPSHOT.jar;
 * // 5.注册函数
 * //   create temporary function get_movie_type as 'com.shujia.code.MyUDTF2';
 *
 */

public class MyUDTF2 extends GenericUDTF {


    public StructObjectInspector initialize(StructObjectInspector argOIs)
            throws UDFArgumentException {

        List<? extends StructField> fieldRefs = argOIs.getAllStructFieldRefs();

        if (fieldRefs.size() != 1) {
            throw new UDFArgumentException("当前函数需要传入一个参数..");
        }

        // 返回字段的名称以及类型
        ArrayList<String> fieldNames = new ArrayList<String>();
        // 设置结果返回的列名称为word
        fieldNames.add("movie_name");
        fieldNames.add("movie_type");

        ArrayList<ObjectInspector> fieldOIs = new ArrayList<ObjectInspector>();
        // 设置word列对应的类型是JAVA中的STRING类型
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);


        // 通过字段的名称以及类型 返回对应的对象
        return ObjectInspectorFactory.getStandardStructObjectInspector(fieldNames,
                fieldOIs);

    }


    @Override
    public void process(Object[] args) throws HiveException {
        // [{"movie_name":"肖申克的救赎","MovieType":"犯罪"},{"movie_name":"肖申克的救赎","MovieType":"剧情"}]
        String oneJsonArray = args[0].toString();

        // 将其转换成JSONArray对象 再去取出其 电影名 以及类型

        try {
            JSONArray jsonArray = new JSONArray(oneJsonArray);
            for (int i = 0; i < jsonArray.length(); i++) {
                // {"movie_name":"肖申克的救赎","MovieType":"犯罪"} 数据转换成一个JSON对象了
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String movieName = jsonObject.getString("movie_name");
                String movieType = jsonObject.getString("MovieType");

                // 保存一行数据
                ArrayList<String> outOneLine = new ArrayList<>();
                outOneLine.add(movieName);
                outOneLine.add(movieType);
                // 将一行数据发送出去
                forward(outOneLine);

            }

        } catch (JSONException e) {
            throw new RuntimeException(e);
        }


    }

    @Override
    public void close() throws HiveException {

    }
}
