package com.shujia.code;


import org.apache.hadoop.hive.ql.exec.UDF;


// 需求：写一个函数能够传入一个英文的字符串，并返回该字符串要求字符串首字母大写，其他的为小写
//     SELECT upstring("aBCd") => Abcd

// 1.创建项目，导入依赖，并构建自定义类，该类需要继承UDF类
// 2.需要重写evaluate函数，并在该函数中定义数据处理逻辑
// 3.对当前项目进行打包
// 4.上传 jar 包到Linux中 并在 hive 中通过 add jar 命令添加包到hive的 class path 路径下
//        add jar /usr/local/soft/jars/hiveCode-1.0-SNAPSHOT.jar;
// 5.注册函数
//   create temporary function upstring as 'com.shujia.code.MyUDF';
// 6.使用
//    SELECT upstring("aBCd")

// 注意:当在HIVE中已经注册过函数后，再重新注册，那么新的Jar包中的代码不会生效，
//     所以需要重启客户端，再做上述操作


public class MyUDF extends UDF {
    public String evaluate(String word) {
        String upperCase = word.substring(0, 1).toUpperCase();
        String lowerCase = word.substring(1).toLowerCase();
        return upperCase+lowerCase+"#";
    }
}
