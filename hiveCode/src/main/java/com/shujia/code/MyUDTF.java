package com.shujia.code;


import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDTF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.StructField;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;

import java.util.ArrayList;
import java.util.List;

// UDTF 是传入一行数据 返回多行数据
// UDAF 是传入多行数据 返回一行数据  => count sum avg

// HIVE中的 explode 函数是一个UDTF函数

//  SELECT exploed_split("hello world"," ")
//  =>
//      hello
//      world

// 4.上传 jar 包到Linux中 并在 hive 中通过 add jar 命令添加包到hive的 class path 路径下
//        add jar /usr/local/soft/jars/hiveCode-1.0-SNAPSHOT.jar;
// 5.注册函数
//   create temporary function exploed_split as 'com.shujia.code.MyUDTF';

public class MyUDTF extends GenericUDTF {

    // 需要重写initialize函数
    public StructObjectInspector initialize(StructObjectInspector argOIs)
            throws UDFArgumentException {

        List<? extends StructField> fieldRefs = argOIs.getAllStructFieldRefs();



        if (fieldRefs.size() != 2) {
            throw new UDFArgumentException("当前函数需要传入两个参数..");
        }

        // 返回字段的名称以及类型
        ArrayList<String> fieldNames = new ArrayList<String>();
        // 设置结果返回的列名称为word
        fieldNames.add("word");

        ArrayList<ObjectInspector> fieldOIs = new ArrayList<ObjectInspector>();
        // 设置word列对应的类型是JAVA中的STRING类型
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);


        // 通过字段的名称以及类型 返回对应的对象
        return ObjectInspectorFactory.getStandardStructObjectInspector(fieldNames,
                fieldOIs);
    }


    //

    /**
     * 定义用户自己的逻辑
     * @param args HIVE函数调用时，传入的参数列表
     *          object array of arguments
     * @throws HiveException
     */
    @Override
    public void process(Object[] args) throws HiveException {
        String lineWords = args[0].toString();
        String split = args[1].toString();
        String[] words = lineWords.split(split);

        // 返回切分后的单词，但是当前process函数并没有返回值

        // 用于返回一行数据
        for (String word : words) {
            ArrayList<String> outOneLine = new ArrayList<>();
            outOneLine.add(word);
            forward(outOneLine);
        }
    }

    @Override
    public void close() throws HiveException {
        // TODO: NOTHING TODO...
    }
}
