package com.shujia.code;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentLengthException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.io.Text;


// 传入一个字符串，可以判断字符串中的长度
// add jar /usr/local/soft/jars/hiveCode-1.0-SNAPSHOT.jar;
// create temporary function getlen as 'com.shujia.code.NewUDF';

// 添加函数的描述信息
@Description(
        name = "getlen",
        value = "get string len ..",
        extended = "select getlen('123456'); => 6"
)

// UDF 函数是传入一行数据，返回一行数据
public class NewUDF extends GenericUDF {


    // initialize 实例化 或者说是 初始化函数
    // arguments表示传入的参数,使用该对象可以对传入的参数进行判断

    /**
     * @param arguments 表示在HIVE中执行函数时，传入的参数
     * @return 在HIVE中执行函数时，对应返回值的类型
     * @throws UDFArgumentException
     */
    @Override
    public ObjectInspector initialize(ObjectInspector[] arguments) throws UDFArgumentException {
        if (arguments.length != 1) {
            throw new UDFArgumentLengthException("当前函数使用时，只能传递一个参数");
        }

        if (!arguments[0].getCategory().equals(ObjectInspector.Category.PRIMITIVE)) {
            throw new UDFArgumentException("只能传入一个基本数据类型");
        }

        // 确定返回值的类型为JAVA中的STRING
        return PrimitiveObjectInspectorFactory.javaIntObjectInspector;
    }

    //

    /**
     * evaluate 可以定义数据的处理逻辑
     *
     * @param arguments HIVE调用时，传入的参数，该类型是HADOOP中的序列化类型  HIVE中的STRING对应 HADOOP的Text
     * @return 计算的结果对象
     * @throws HiveException
     */
    @Override
    public Object evaluate(DeferredObject[] arguments) throws HiveException {
        // org.apache.hadoop.io.Text cannot be cast to java.lang.String
        String arg = ((Text) arguments[0].get()).toString();
        // 该返回值的类型和initialize函数中返回值的类型一致
        return arg.length();
    }

    @Override
    public String getDisplayString(String[] strings) {
        return "no thing todo...";
    }
}
