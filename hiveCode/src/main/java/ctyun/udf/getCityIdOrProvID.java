package ctyun.udf;

import ctyun.udf.util.SSXRelation;
import org.apache.hadoop.hive.ql.exec.UDF;

public class getCityIdOrProvID extends UDF {
    public String evaluate(String county_id, String param) {
        // 根据county_id获取 cityID or provinceID
        String id = "-1";
        if ("city".equals(param)) {
            id = SSXRelation.COUNTY_CITY.get(county_id);
        } else if ("province".equals(param)) {
            id = SSXRelation.COUNTY_PROVINCE.get(county_id);
        }
        return id;
    }
}
