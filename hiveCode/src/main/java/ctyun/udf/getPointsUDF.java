package ctyun.udf;

import ctyun.udf.grld.Grid;
import org.apache.hadoop.hive.ql.exec.UDF;

import java.awt.geom.Point2D;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class getPointsUDF extends UDF {
    public ArrayList<Double> evaluate(String grid_id) {
        // 根据网格id 获取经纬度
        ArrayList<Double> cols = new ArrayList<Double>();
        Point2D.Double points = Grid.getCenter(Long.valueOf(grid_id));
        cols.add(points.x);
        cols.add(points.y);
        return cols;
    }
}
