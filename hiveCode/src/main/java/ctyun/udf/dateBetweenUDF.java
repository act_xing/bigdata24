package ctyun.udf;

import ctyun.udf.util.DateUtil;
import org.apache.hadoop.hive.ql.exec.UDF;

public class dateBetweenUDF extends UDF {
    public int evaluate(String grid_first_time,String grid_last_time) {
        // 获取两个时间字符串的差 单位：分
        return Math.abs(DateUtil.betweenM(grid_first_time,grid_last_time));
    }
}
