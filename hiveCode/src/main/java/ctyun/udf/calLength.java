package ctyun.udf;

import ctyun.udf.util.Geography;
import org.apache.hadoop.hive.ql.exec.UDF;

public class calLength extends UDF {
    public String evaluate(String grid_id, String resi_grid_id) {
        // 根据grid_id 网格id, resi_grid_id 居住地网格id 计算距离
        double distance = Geography.calculateLength(Long.valueOf(grid_id), Long.valueOf(resi_grid_id));
        return String.valueOf(distance);
    }
}
