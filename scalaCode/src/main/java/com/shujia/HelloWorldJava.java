package com.shujia;

public class HelloWorldJava {

    public void noStaticPrint() {
        System.out.println("noStaticPrint function is running...");
    }


    public static void print() {
        System.out.println("print function is running...");
    }

    public static void main(String[] args) {
        System.out.println("hello world java");

        // 在Java中对于static修饰的类可以直接用 类名.函数名 直接调用
        // 在main函数中直接调用类.main 表示为迭代调用 会出现 java.lang.StackOverflowError 堆内存溢出
//        HelloWorldJava.main(args);

        HelloWorldJava.print();

        new HelloWorldJava().noStaticPrint();


//        final int i = 10;


    }
}
