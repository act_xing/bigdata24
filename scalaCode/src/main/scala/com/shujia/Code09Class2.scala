package com.shujia

object Code09Class2 {

  def main(args: Array[String]): Unit = {
    /**
     * 类的继承
     */
    val person = new Person()

    val student = new StudentClass()
    student._age = 18
    student._name = "王宇杰"
    student.skill()
    student.learnObj = "可乐炸弹"
    student.skill()

    val studentClass = new StudentClass("老邢")
    println(studentClass._name)


  }

}

// 注意 创建类的时候需要考虑其他地方时候以及创建过同名的类 在编译的过程中 会创建同类名的.class编译文件
class PersonClass()  extends MonkeyClass{
  // 该操作是对属性进行重写
//  override var _age: Int = 10
  println("PersonClass类的对象正在构造..")

  _age = 10;

  def this(age:Int){
    this()
    this._age = age
  }


  def running(): Unit ={
    println("遇到危险会跑路...")
  }
}

class MonkeyClass(){
  println("MonkeyClass类的对象正在构造..")
  var _age: Int = 0

  def skill(): Unit ={
    println("猴子能跑能跳...")
  }
}

// Scala 只支持单继承 因为多继承会存在有钻石问题
class StudentClass extends PersonClass {
  println("StudentClass类的对象正在构造..")

  def this(name: String){
    this()
    this._name =name
    this.learnObj = "大数据"
  }

  var _name: String = _
  var learnObj: String = _

  // 经过继承以后，父类中的skill会 传递给当前类 那么就出现有两个skill 那么就需要对函数进行标记为重写
  // 在函数前添加 override 表示重写
  override def skill(): Unit ={
    println(s"能够制造$learnObj")
  }
}

