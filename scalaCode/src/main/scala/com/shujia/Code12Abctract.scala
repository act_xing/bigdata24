package com.shujia

object Code12Abstract {
  def main(args: Array[String]): Unit = {
    val son = new SonClass
    son.name = "son"
    println(son.name)
    son.printMessage("son Message")
  }

}

abstract class TestClass{
  var name:String

  // 定义一个抽象的函数 其没有返回值
  def printMessage(message:String):Unit
}


class SonClass extends TestClass{
  // 对抽象的属性进行做重写操作
  override var name: String = _

  override def printMessage(message: String): Unit = {
    println(message)
  }
}
