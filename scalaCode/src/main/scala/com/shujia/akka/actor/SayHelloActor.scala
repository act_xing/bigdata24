package com.shujia.akka.actor

import akka.actor.{Actor, ActorRef, ActorSystem, Props}


/**
 * 需求： 自己给自己发送一条消息 根据消息的内容 选择做不同的操作
 */
class SayHelloActor extends Actor {
  // 通过继承Actor 定义一个Actor对象 需要实现其receive函数
  override def receive: Receive = {
    case "hello" => println("接收到Hello 回应Hello too")
    case "OK" => println("收到OK，回应OK too")
    case "_" => println("不知道说啥...")
  }
}


object SayHelloActor extends App {

  // 构建Actor的对象类 工厂类
  private val actorSystem: ActorSystem = ActorSystem("ActorFactory")

  // 通过actorOf函数可以获取 ActorRef 通过该对象可以发送消息
  // 通过该函数获取SayHelloActor的ActorRef
  // actorOf(props: Props, name: String)
  private val sayHelloActorRef: ActorRef = actorSystem.actorOf(Props[SayHelloActor], "SayHelloActor")

  // 通过ActorRef 可以发送数据给 mailBox 被 Receive接收并处理
  sayHelloActorRef ! ("hello")
}
