package com.shujia.akka.actors

import akka.actor.{ActorRef, ActorSystem, Props}


object ActorGame extends App {
  private val actorFactory: ActorSystem = ActorSystem("ActorFactory")

  private val wangYuJieRef: ActorRef = actorFactory.actorOf(Props[WangActor], "wangYuJie")
  private val liMaoLongRef: ActorRef = actorFactory.actorOf(Props(new LiActor(wangYuJieRef)), "liMaoLong")

  liMaoLongRef ! "开打"
}
