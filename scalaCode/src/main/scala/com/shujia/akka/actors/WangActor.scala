package com.shujia.akka.actors

import akka.actor.Actor


class WangActor extends Actor{
  override def receive: Receive = {
    case "闪电五连鞭" => {
      println("王宇姐:我躲,看我出招...")
      println("王宇姐:看我可乐炸弹...")
      // 获取发送方的位置信息
      sender() ! "可乐炸弹"
    }
  }
}
