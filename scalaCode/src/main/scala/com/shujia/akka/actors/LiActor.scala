package com.shujia.akka.actors

import akka.actor.{Actor, ActorRef}

class LiActor(wangYuJieRef: ActorRef) extends Actor {
  override def receive: Receive = {
    case "开打" => {
      println("李茂龙:我出招了，你小心")
      self ! "看招"
    }
    case "看招" => {
      println("李茂龙:看我闪电五连鞭...")
      wangYuJieRef ! "闪电五连鞭"
    }
    case "可乐炸弹" => {
        println("李茂龙:你好狠毒... 炸我一身  我不玩了 ...")
    }
  }
}
