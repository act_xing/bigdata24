package com.shujia.akka.sparkAkka.worker

import akka.actor.{Actor, ActorRef, ActorSelection, ActorSystem, Props}
import com.shujia.akka.serverAndClient.client.Client.{clientHost, clientPort}
import com.shujia.akka.sparkAkka.common.{RegisterWorker, WorkerHeartBeat, WorkerInfo}
import com.typesafe.config.{Config, ConfigFactory}

import java.util.concurrent.TimeUnit
import scala.concurrent.duration.{FiniteDuration, TimeUnit}


class SparkWorker(masterHost: String, masterPort: Int) extends Actor {
  var masterActorRef: ActorSelection = _

  override def preStart(): Unit = {
    // 通过服务端的url获取ActorRef引用信息
    masterActorRef = context.actorSelection(s"akka.tcp://Master@$masterHost:$masterPort/user/sparkMaster")

  }


  override def receive: Receive = {
    case "start" => {
      println("当前SparkWorker正在启动...")
      // 启动之后，需要和Spark进行通信，表示当前Worker已经启动
      masterActorRef ! RegisterWorker("1", 8, 16)
    }
    case WorkerInfo(id, cpu, mem) =>
      // 当work发现已经被Master接收了，那么就可以定期给Master去发送心跳
      println(s"${id}已经注册完成... 开始发送心跳")

      import context.dispatcher
      context.system.scheduler.schedule(new FiniteDuration(0, TimeUnit.SECONDS), new FiniteDuration(3, TimeUnit.SECONDS), self, "发送心跳")

    case "发送心跳" =>
      masterActorRef ! WorkerHeartBeat("1")
      println("当前发送了一次心跳...")
  }
}


object SparkWorker extends App {
  val (clientHost, clientPort, serverHost, serverPort) = ("127.0.0.1", 8888, "127.0.0.1", 9999)

  private val config: Config = ConfigFactory.parseString(
    s"""
       |akka.actor.provider="akka.remote.RemoteActorRefProvider"
       |akka.remote.netty.tcp.hostname=$clientHost
       |akka.remote.netty.tcp.port=$clientPort
       |""".stripMargin
  )

  private val workerActorSystem: ActorSystem = ActorSystem("worker", config)

  private val sparkWorkerRef: ActorRef = workerActorSystem.actorOf(Props(new SparkWorker(serverHost, serverPort)), "sparkWorker1")

  sparkWorkerRef ! "start"


}