package com.shujia.akka.sparkAkka.maser

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import com.shujia.akka.serverAndClient.server.Server.{host, port}
import com.shujia.akka.sparkAkka.common.{RegisterWorker, WorkerHeartBeat, WorkerInfo}
import com.typesafe.config.{Config, ConfigFactory}


import scala.collection.mutable

class SparkMaser extends Actor {
  // 将注册的Work信息保存到一个列表中
  private val workInfoMap: mutable.Map[String, WorkerInfo] = mutable.Map[String, WorkerInfo]()

  override def receive: Receive = {
    case "start" => {
      println("Spark集群中的Master启动了..")
      // 启动后，就可以接收从节点的注册
    }
    case RegisterWorker(id,cpu,memory) => {
      println(s"已经接收到${id}的注册信息...")
      val workerInfo: WorkerInfo = WorkerInfo(id = id, cpu = cpu, mem = memory)
      workInfoMap.put(id,workerInfo)
      println("已经将当前的服务器添加至列表中")
       // 当Master将对象添加至列表中后，可以返回消息
      sender() ! workerInfo
    }

    case WorkerHeartBeat(id) => {
      println(s"获取到${id}的心跳",workInfoMap.getOrElse(id,null))
    }


  }
}

object SparkMaser extends App {
  val host = "127.0.0.1"
  val port = 9999

  private val config: Config = ConfigFactory.parseString(
    s"""
       |akka.actor.provider="akka.remote.RemoteActorRefProvider"
       |akka.remote.netty.tcp.hostname=$host
       |akka.remote.netty.tcp.port=$port
       |""".stripMargin
  )

  private val masterActorSystem: ActorSystem = ActorSystem("Master", config)
  private val sparkMasterRef: ActorRef = masterActorSystem.actorOf(Props[SparkMaser], "sparkMaster")

  // 启动服务
  sparkMasterRef ! "start"

}