package com.shujia.akka.serverAndClient.server

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import com.shujia.akka.serverAndClient.comment.{ClientMessage, ServerMessage}
import com.typesafe.config.{Config, ConfigFactory}

import scala.io.StdIn

class Server extends Actor{
  override def receive: Receive = {
    case "start" =>
      println("数加报名服务器，开始工作了...")
    case ClientMessage(mes) =>
      println(mes)
      sender() ! ServerMessage("server:收到消息")
      val serverMes: String = StdIn.readLine()
      sender() ! ServerMessage("server:"+serverMes)

  }
}

object Server extends App{
  // 通过设置IP 和 PORT 来绑定当前服务期中的一个进程，那么客户端就可以通过 IP和PORT来发送消息
  val host = "127.0.0.1"
  val port = 9999

  private val config: Config = ConfigFactory.parseString(
      s"""
        |akka.actor.provider="akka.remote.RemoteActorRefProvider"
        |akka.remote.netty.tcp.hostname=$host
        |akka.remote.netty.tcp.port=$port
      |""".stripMargin
  )

  // 通过该Actor的工厂类可以创建对应的Actor对象
  val serverActorSystem: ActorSystem = ActorSystem("Server", config)

  private val serverObjRef: ActorRef = serverActorSystem.actorOf(Props[Server], "serverObj")

  serverObjRef ! "start"
}
