package com.shujia.akka.serverAndClient.comment

// 表示客户端发送的消息
case class ClientMessage(mes:String)

case class ServerMessage(mes:String)