package com.shujia.akka.serverAndClient.client

import akka.actor.{Actor, ActorRef, ActorSelection, ActorSystem, Props}
import com.shujia.akka.serverAndClient.comment.{ClientMessage, ServerMessage}
import com.shujia.akka.serverAndClient.server.Server.{host, port}
import com.typesafe.config.{Config, ConfigFactory}

import scala.io.StdIn

class Client(serverHost: String, serverPort: Int) extends Actor {
  var serverActorRef: ActorSelection = _

  override def preStart(): Unit = {
    // 通过服务端的url获取ActorRef引用信息
    serverActorRef = context.actorSelection(s"akka.tcp://Server@$serverHost:$serverPort/user/serverObj")

  }

  override def receive: Receive = {
    case "start" => println("客户端已经启动，可以咨询问题")
    case mes:String =>
    // 如果匹配到字符串消息，那么将消息发送给服务端
      serverActorRef ! ClientMessage("client:"+mes)
    case ServerMessage(mes) =>
      println(mes)
  }
}

object Client extends App {
  val (clientHost, clientPort, serverHost, serverPort) = ("127.0.0.1", 8888, "127.0.0.1", 9999)

  private val config: Config = ConfigFactory.parseString(
    s"""
       |akka.actor.provider="akka.remote.RemoteActorRefProvider"
       |akka.remote.netty.tcp.hostname=$clientHost
       |akka.remote.netty.tcp.port=$clientPort
       |""".stripMargin
  )

  private val clientActorSystem: ActorSystem = ActorSystem("client", config)

  private val clientObjRef: ActorRef = clientActorSystem.actorOf(Props(new Client(serverHost, serverPort)), "clientObj")

  // 启动服务
  clientObjRef ! "start"

  while (true){
    println("请输入你要咨询的问题：")
    val mes: String = StdIn.readLine()
    clientObjRef ! mes
  }


}