package com.shujia

object Code13Enum {
  def main(args: Array[String]): Unit = {
    println(Color.RED)
  }
}

// 枚举类
object Color extends Enumeration{
  val RED = Value("红色")
  val YELLOW = Value("黄色")
  val BLUE = Value("蓝色")
}