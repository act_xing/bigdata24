package com.shujia

class HelloWorldScala {
  // def 表示定义函数 main函数表示程序的入口  args 表示传入的参数数组  Unit 表示无返回值
//  def main(args: Array[String]): Unit = {
//    print("hello World Scala")
//  }
  // 对于scala中的并没有static关键字，如果想要运行静态代码，那么需要使用object

  def noStaticPrint(): Unit = {
    System.out.println("noStaticPrint function is running...")
  }

}

object HelloWorldScala{

  def printScala(): Unit = {
    System.out.println("print function is running...")
  }

  def main(args: Array[String]): Unit = {
    print("hello World Scala")
    printScala()

    // 类的对象：通过new的方式对类进行做实例化
    // 类对象：类被加载到JVM中的形式

//    new HelloWorldScala().noStaticPrint()

    // 对Java中的类进行调用
    new HelloWorldJava().noStaticPrint()
  }

}