package com.shujia

import scala.reflect.ClassTag.Nothing

object Code28Generic {

  def main(args: Array[String]): Unit = {
    /**
     * 1.泛型和类型的区别：
     *    1.所谓的类型是对外部数据的输入做约束
     *        val i:Int = "10"
     *    2. 泛型是对内部数据做约束
     *        val arr:Array[Int] = new Array[Int](6)
     *
     * 2. 泛型在某些场合下可以作为参数进行传递，并且泛型旨在编译期有效
     *
     * 3. 泛型最主要的目的是为了约束内部数据类型
     *
     * 4. 泛型和类型不是一个层次，泛型没有所谓的父子关系
     *
     * 5. 虽然没有继承关系，但是可以定义其边界
     */

    val scalaChildMessage: ScalaMessage[ScalaChild] = new ScalaMessage[ScalaChild]()
    val scalaParentMessage: ScalaMessage[ScalaParent] = new ScalaMessage[ScalaParent]()
    val scalaSonMessage: ScalaMessage[ScalaSon] = new ScalaMessage[ScalaSon]()

    val scalaProduce = new ScalaProduce[ScalaChild]()
    scalaProduce.produce(scalaChildMessage)
    scalaProduce.produce(scalaParentMessage)

    // [_ >: A] 表示只能传入当前 A 的父类以及当前类
//    scalaProduce.produce(scalaSonMessage)
    // [_ <: B] 表示只能传入当前 B 的子类及其当前类

    // -T 方式可以限制其泛型的边界 其中-表示 只能传递给子类或者当前类
    val value: ScalaMessage[ScalaParent] = new ScalaMessage[ScalaChild]()


  }
}
// Scala中定义泛型是以[] 确定的
class ScalaMessage[+T]{
  var data:ScalaParent = _

  // Scala中对于泛型取作为变量的类型 存在问题
//  val data2:T = Nothing;
}

class ScalaParent{

}

class ScalaChild extends ScalaParent{

}

class ScalaSon extends ScalaChild{

}

class ScalaProduce[A]{
  def produce(message:ScalaMessage[_ >: A]): Unit ={
    println("produce函数执行了...")
  }
}

class ScalaConsumer[B]{
  def consume(message:ScalaMessage[_ <: B]): Unit ={
    println("consume函数执行了...")
  }
}