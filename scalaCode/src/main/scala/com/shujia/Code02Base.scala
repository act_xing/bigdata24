package com.shujia

import java.io.{BufferedReader, FileReader}
import scala.io.Source

object Code02Base {

  def main(args: Array[String]): Unit = {
    // 单行注释
    // 单行注释
    // 单行注释

    /*
    * 多行注释
    * 多行注释
    * 多行注释
    * */

    /** 文档注释
     * 文档注释
     * 文档注释
     * 文档注释
     */

    println("打印");
    println("打印") // ; 可以用来分割两行代码 但是 一般情况下 不去使用 默认换行表示一行代码结束
    // 需要有缩进 格式化代码 方便查看


    // 定义变量
    /**
     * 变量命名规范：
     * 1.数字 字母 下划线
     * 2.数字不能作为变量名开头
     * 3.不能以关键字命名
     * 4.使用驼峰命名法
     */
    //    val helloWorldJava: HelloWorldJava = new HelloWorldJava();
    /**
     * 在Scala中定义变量不需声明类型；
     * 整体的变量是分为：变量和常量
     *
     */

    var i = 10 // 变量  当前变量定义时，会根据其值进行推断对应的变量类型
    val j = 10 // 常量
    //    j = 11 // 常量是不能重新赋值
    i = 100
    //    i = 1.0 //对于变量赋值时，不能改变其变量的类型

    var k: Int = 10 // 定义变量时，显示给定其类型

    /**
     * Scala中的数据类型：
     * 包含有Java中8中基本数据类型：Byte、Short、Int、Long、Float、Double、Boolean、Char 其父类为AnyVal
     * 同时 Scala还包括有引用类型 AnyRef
     */


    val byte: Byte = 12
    val short: Short = 20
    //    val short = 10
    val int: Int = 10
    val long: Long = 100L
    val double: Double = 1.234 // 给定一个小数点数值，默认为Double类型
    val float: Float = 1.234F // 对于浮点型数值，需要在末尾添加 F来进行标记
    val char: Char = 'a'
    val boolean1: Boolean = true
    val boolean2: Boolean = false

    // 类型转换
    //

    val i2: Int = 97
    val byte1: Byte = i2.toByte
    val long1: Long = i2.toLong // 显示转换类型
    val long2: Long = i2 // 低精度转换高精度 自动转换 隐式转换
    val float1: Float = i2.toFloat
    val double1: Double = i2.toDouble
    // 布尔类型不能和其他类型做转换

    println(i2.toChar) // 将Int类型转换成char类型，根据ASC码表

    // 定义字符串
    val str: String = "123" // Scala中的STRING类型来源于Java中的STRING，但是对其做了加强和改进
    println(str.toFloat) // Scala给STRING类型添加的方法，实际上是通过隐式转换的方式添加上去的
    println(str.toDouble) // 隐式转换就是动态给对象添加方法
    println(str.toInt)

    // 字符串常见操作

    val str2 = "abcd,EFgH,LwN"
    println(str2.toLowerCase)
    println(str2.toUpperCase)
    println(str2.substring(5, 9))

    // 字符串切分
    // Array[String] 表示Scala中的字符串数组 相当于Java中的String[]
    val splits: Array[String] = str2.split(",")
    println(splits) // [Ljava.lang.String;@19bb089b  => 类型及地址
    println(splits(1))

    for (elem <- splits) { // 通过for循环的方式对元素进行遍历
      println(elem)
    }

    // String类型在Scala中独有的方法
    splits.foreach(println) // Scala中对元素进行变量，并将变量的结果传给println进行打印，println作为参数变量进行传递
    println(str2.head) // 取头字符
    println(str2.tail) // 取除头以外的其他数据  bcd,EFgH,LwN
    println(str2.take(3)) //
    println(str2.length) //
    println(str2.size) //
    println(str2.sorted) //根据ASC码值进行排序
    println(str2.isEmpty)

    println("\" 这是一个转义操作")


    // 算术运算符
    println(1 + 1.1)
    println(10 - 3)
    println(10 * 3)
    println(10 / 3.0) // 对于除法 需要注意当前拿两个整数值相除得到的结果为整数，如果得到小数，那么除数或者被除数需要给定小数类型
    println(10 % 3)

    // 关系运算符
    println(3 > 4)
    println(3 < 4)
    println(3 >= 4)
    println(3 <= 4)
    println(3 != 4)
    println(3 == 4)

    // 对于字符来说 一般不做比较
    println("a" > "b") //false
    println("上" > "下") //false
    println("六" > "四") //false

    val st1 = "abc"
    val st2 = new String("abc")
    val st3: String = "abc"

    println(st1 == st2) // 在Scala中等值比较时拿具体的值进行比较，相当于 Java中的equals
    println(st1 == st3) //true

    println(st1.equals(st2)) //true

    println(st1.eq(st2)) // false  eq是比较两个对象的内存地址 相当于Java中的 ==

    // 位运算符
    println(7 & 4)
    println(7 | 4)
    println(7 ^ 4)


    val num1: Int = 10
    val num2: Int = 20
    val num3: Int = 30


    // 逻辑运算符
    println("+++++++++++++")
    println(true && false)
    println(true || false)
    println(true ^ false) //true 异或 只要不同则返回为True 相同则返回为False
    println(!false) // 取反操作
    println("==============")


    // println使用的是Java中的System.out.println 对其进行做了简化操作
    println("%d * %d = %d", num1, num2, num3)
    printf("%d * %d = %d \n", num1, num2, num3) // 格式化的第一种方式 printf

    println(s"$num1 * $num2 = $num3") // 格式化第二种方式 println + s
    val _num4: Int = 200
    println(s"$num1 * $num2 = ${_num4}") // 对于_开头的变量 需要使用{}对其进行修饰

    //
    val sql: String = "SELECT * FROM emp T1 JOIN dept T2 ON T1.deptNo = T2.deptNo"

    // 多行字符串
    val sqlStr: String =
      """SELECT
        |clazz
        |,count(*)
        |FROM student
        |GROUP BY clazz
        |""".stripMargin
    println(sqlStr)


    // if语句

    var age = 10 // 对于一个变量 如果只用来做判断，不重新复制，那么最好使用val
    if (age <= 10) {
      println("童年")
    }


    age = 20
    if (age <= 18) {
      println("未成年")
    } else {
      println("成年")
    }

    // 当第一个条件满足时，后续的判断条件不需要再执行..
    age = -10
    if (age <= 0 || age >= 200) {
      println("数据异常...")
    } else if (age <= 18) {
      println("未成年")
    } else {
      println("成年")
    }

    // 对于if判断可以将判断结果进行做返回 并且默认最后一行是其返回值，可以是一个数值，也可以是一个函数
    //    var if_res:String=>Unit =
    var if_res: Any = if (age <= 0 || age >= 200) {
      println("数据异常...")
      //      println
      0
    } else if (age <= 18) {
      println("未成年")
      //      println
      18
    } else {
      println("成年")
      //      println
      80
    }
    println(s"if结果:$if_res")
    //    splits.foreach(if_res)

    // 在Java中有Switch语句 对应在Scala中有match语句

    val value1: Int = 100

    val res: Any = value1 match {
      case 10 =>
        println("value值为10")
        println("value值为10") // 在Scala中对应多行代码并不需要使用{}来进行标记
        10.0 // 在Scala中没有break 并且对于有返回值的情况下 不需要通过return将数据返回，而默认最后一行的值最为其返回值
      case 20 =>
        println("value值为20")
        20.0
      case _ =>
        println("匹配到其他值")
        println
    }
    println(res) // 将匹配到的值进行打印


    // Scala中的数组

    val array: Array[String] = new Array[String](5)
    array(0) = "abc"
    array(1) = "def"
    array(2) = "gh"

    // for循环的方式一
    array.foreach(println) // foreach循环要求传入一个函数 并在源码中是通过调用while语句对传入的函数进行循环调用
    array.foreach(println(_)) // _ 表示一个占位 表示数组中取到的每一个值
    array.foreach(value => println(value)) // 将取到的每一个元素 保存在 value变量中，传给println函数

    // for循环方式二
    for (elem <- array) { // 将数组/集合 中的每一个元素取出，再执行对应的代码块 不需要给定类型 会自动进行推断
      println(elem)
    }

    for (elem <- 1 to 9) { // to 是左闭右闭区间
      println(elem)
    }

    for (elem <- 1 until 9) { // until给定的范围是 左闭右开的区间
      println(elem)
    }


    for (elem <- Range(1, 9)) { // Range给定的是一个左闭右开区间
      println(elem)
    }

    for (elem <- Range(1, 9, 2)) { // Range给定的是一个左闭右开区间 步长为2
      println(elem)
    }


    // while循环

    var cut: Int = 1
    var sum: Int = 1
    while (cut <= 10) {
      sum *= cut
      cut += 1
    }
    println("res:" + sum)

    // do while 是先执行 之后再做判断  而 while是先判断之后再执行
    cut = 1
    sum = 1
    do {
      sum *= cut
      cut += 1
    } while (cut <= 10)
    println("res:" + sum)


    // 读取数据
    // java 方式读取数据

    val reader: BufferedReader = new BufferedReader(new FileReader("data/student1000.txt"))
    var line: String = reader.readLine()
    //    while ((line = reader.readLine()) != null){  在Scala中不能以该方式读取数据
    //    }
    while (line != null) {
      println(line)
      line = reader.readLine() // 读取下一行数据，再进行循环判断
    }
    reader.close()
    println("=" * 30)

    // Scala 的方式
    Source
      .fromFile("data/student1000.txt")
      .getLines() // 如果不添加该函数，那么默认是一个字符一个字符进行读取，之后再做数据的打印 其返回的是一个迭代器Iterator
      .foreach(println)

//    val strings: Iterator[String] = Source
//      .fromFile("data/student1000.txt")
//      .getLines()


  }
}
