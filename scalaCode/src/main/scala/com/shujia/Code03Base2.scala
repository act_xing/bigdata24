package com.shujia

import com.mysql.jdbc.Driver

import java.io.{BufferedReader, FileInputStream, FileOutputStream}
import java.sql.{Connection, DriverManager, PreparedStatement, ResultSet}
import scala.io.{BufferedSource, Source}

object Code03Base2 {
  def main(args: Array[String]): Unit = {
    // 读取student数据
    val source: BufferedSource = Source.fromFile("data/student1000.txt")
    //调用Java的方式
    //    val reader: BufferedReader = source.bufferedReader()

    // Scala中的buffered方式
    //    val buffered: BufferedIterator[Char] = source.buffered
    //    while(buffered.hasNext){
    // buffered.next() 获取到的是单个的字符
    //      print(buffered.next())
    //    }

    source.close()


    // 写数据操作 => Java方式

    val stream: FileOutputStream = new FileOutputStream("data/read.txt")
    stream.write("hello Scala\n".getBytes())
    stream.write("hello world".getBytes())
    stream.flush()
    stream.close()


    // 操作MySQL  => Java

    DriverManager.registerDriver(new Driver())
    val mysqlCon: Connection = DriverManager.getConnection("jdbc:mysql://master:3306/student?useSSL=false&useUnicode=true&characterEncoding=utf-8", "root", "123456")

    val clazz:String = "文科一班"
    //    mysqlCon.prepareStatement(s"select * from student where clazz = '$clazz'")
    val statement: PreparedStatement = mysqlCon.prepareStatement(s"select * from student where clazz = '$clazz'")

    //   statement.setString(1,clazz) // 注意位置从1 开始替换？
    
    // 查询
    val set: ResultSet = statement.executeQuery()
    // 指针下移看是否有数据
    while(set.next()){
      val id: String = set.getString("id")
      val name: String = set.getString("name")
      println(s"$id,$name")
    }
    statement.close()
    mysqlCon.close()

  }
}
