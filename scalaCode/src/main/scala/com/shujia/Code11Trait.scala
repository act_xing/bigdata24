package com.shujia

object Code11Trait {
  def main(args: Array[String]): Unit = {

    /**
     *  在Java中会存在有接口 ,但是在Scala中并没有对应的接口概念，实际上采用的是trait来声明特征
     *   通过如下方式可以对某个对象中添加一个特质
     */
    val mysql = new Mysql() with Connect
    mysql.desc()

    // 需求：在不改变原先的类的情况下，增加MySQL对象的功能
    mysql.getConnect()

    mysql.execute()

    mysql.log()

  }
}
trait Connect{
  def getConnect(): Unit ={
    println("正在连接...")
  }
}

trait Executor{
  def execute(): Unit ={
    println("Mysql 可以执行SQL语句..")
  }
}


trait Logger{
  def log(): Unit ={
    println("会产生日志")
  }
}

// 特质之间也可以进行继承
trait MysqlLogger extends Logger {
  // 如果父特质中存在有 log 那么需要对其进行重写操作
  override def log(): Unit ={
    println("Mysql会产生日志")
  }
}


class DB extends Logger {

}

// 对于特质可以在类中进行做继承
// 当类中有继承父类，并且需要引入特质，那么可以使用 with语句进行连接
// 当有多个特质需要进行添加时，那么需要使用with 进行连接
// 当父类中的特质存在时，会直接被子类继承调用
class Mysql extends DB with Executor with MysqlLogger{
  def desc(): Unit ={
    println("当前是一个MySQL类")
  }

}


