package com.shujia

object Code21Implicit1 {

  def main(args: Array[String]): Unit = {

    /**
     * 隐式函数：
     *    在实际使用过程中，不需要显示调用该函数，直接在编译过程中将该函数添加到编译的文件中，不需要开发者手动指定
     *    识别过程：
     *        1. implicit修饰的函数中要求只有一个类型参数
     *        2.当在代码中发现有对应隐式转换函数的参数后，会默认对其添加函数调用
     *        3.当该对象使用时，发现需要的时隐式转换函数的返回值类型时，会自动调用
     *
     */
    val clazz: clz1 = clz1("10")
    getClz2(fun(clazz)) // 显示调用
    getClz2(clazz) // 隐式调用
//    getClz3(clazz)

    // 注意：对于默认的Scala中的基本数据类型，Scala内部实现了其隐式转换函数，所以重新定义时，会报错


  }

  // 通过添加 implicit 关键字 可以对当前的函数进行描述为一个隐式转换函数

  implicit def fun(clazz: clz1): clz2 = {
     clz2(clazz.num.toInt + 10)
  }

  // 当出现两个隐式转换函数 并且其传入值和返回值类型都一样，那么会报错
//  implicit def fun2(clazz: clz1): clz2 = {
//    clz2(clazz.num.toInt + 20)
//  }

  def getClz2(clazz:clz2): Unit ={
    println(clazz)
  }

  def getClz3(clazz:clz3): Unit ={
    println(clazz)
  }

}


case class clz1(num: String)

case class clz2(num: Int)

case class clz3(num:Long)