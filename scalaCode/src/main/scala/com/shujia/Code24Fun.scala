package com.shujia

object Code24Fun {

  def main(args: Array[String]): Unit = {
    // 惰性函数: 在变量前添加 lazy 修饰 表示当前的函数是一个惰性函数 其函数有返回值，但是该函数在lazy修饰后并不会立即执行
    //      而是在变量使用时才会执行对应的惰性函数

    /**
     *  res变量未被使用
        func已经被调用..
        res:100
     */

    lazy val res: Int = func("10")
    println("res变量未被使用")
    println(s"res:$res")

  }

  def func(str:String): Int ={
    println("func已经被调用..")
    str.toInt * 10
  }
}
