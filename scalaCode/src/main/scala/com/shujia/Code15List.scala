package com.shujia

object Code15List {

  def main(args: Array[String]): Unit = {


    /**
     * List列表
     * ① List中存储的元素一般情况下 会保持一致
     * ② List中对应的元素是可以重复的
     * ③ List是有序的，下标有序
     * ④ List是不可变的 一旦创建后，就不可以进行修改
     */
    val list: List[Int] = List[Int](1, 2, 3, 4, 4, 3, 2, 1)
    val list1: List[Any] = List[Any](1, 2, "3", "4", 4, 3, 2, 1)
    println(list)
    println(list1)

    // 通过下标进行取值
    println(list(2))

//        list(2)= 5

    // 常用方法
    println(list.head) //取头数据 下标为0
    println(list.tail) // 取除头以外的数据
    println(list.take(1)) // 取出指定前n个的数据
    println(list.max) // 取出最大值
    println(list.min) // 取出最小值
    println(list.sum) // 求和
    println(list.size) // 长度
    println(list.length) // 长度 元素个数
    println(list.sum / list.length) // 平均
    println(list.isEmpty) // 是否为空
    println(list.mkString(",")) // 转成字符串
    println(list.sorted) //排序
    println(list.reverse) // 翻转
    println(list.contains(4)) // 判断是否包含元素

    // List常用操作(函数) : map flatmap filter groupBy sort相关 foreach
    //  以上函数都是对数据进行做数据分析计算 遍历常用的函数

    /**
     * ①Map函数
     * 格式： 集合.map(函数)  返回类型为一个List集合
     * 作用：对函数中的每一个元素进行读取处理 处理逻辑是按照给定的函数进行计算  传入一条 返回一条数据
     */

    // 对每个元素进行求平方
    // def map[B, That](f: A => B)  其中 A => B 表示为函数的类型  A是List中的元素泛型 => Int ，B是任意的返回值类型

    // 方式一：
    def fun(x: Int): Int = {
      x * x
    }

    // 注意：map函数中的传入函数需要在该行前定义 才能调用
    val res: List[Int] = list.map(
      fun
    )
    res.foreach(println)

    // 方式二：匿名函数

    println(list.map(
      // 定义匿名函数
      (i: Int) => {
        i * i
      }
    ))

    println(list.map(
      // 定义匿名函数
      i => i * i
    ))


    /**
     * flatmap(扁平化操作)
     * 格式： flatmap((A)=>返回值为集合类型)
     * 作用：传入一个函数，将函数的返回的集合进行拆解，取出其中所有的元素，再将元素保存到大的集合中，并返回
     *
     *
     */

    val strList: List[String] = List[String]("hello world", "hello scala", "hello java")

    // 需求：对于上述的list中将每个单词进行切分，再以单词为元素 保存成List => 相当于 HIVE中的 explode(split())
    val splitList: List[Array[String]] = strList.map(
      (elem: String) => elem.split(" ")
    )
    println(splitList)


    /**
     * def flatMap[B, That](f: A => GenTraversableOnce[B])
     * flatMap定义时 其中传入参数函数 f 中 A表示List中每一个元素的类型
     * 、                             GenTraversableOnce[B] 是函数的返回值  GenTraversableOnce 类中的子类包含有 Iterable
     *
     * 效果：
     * List([hello, world], [hello, scala], [hello, java]) =>   List(hello, world, hello, scala, hello, java)
     */

    val flatMapResList: List[String] = strList
      .map(
        (elem: String) => elem.split(" "))
      // 由于 Array是 GenTraversableOnce 子类所以可以将对象直接返回
      .flatMap((array: Array[String]) => array)
    println(flatMapResList)

    // 简化上述流程
    println(strList.flatMap(
      (oneStr: String) => {
        oneStr.split(" ")
      }
    ))

    println(strList.flatMap(
      _.split(" ")
    ))


    /**
     * filter
     * 格式： filter(p: A => Boolean)
     * 作用：对数据进行做过滤操作，需要给定一个函数，该函数返回值为 True或者False 如果为True那么该行数据取出 如果为False那么过滤掉该数据
     *
     */

    // def filter(p: A => Boolean):
    // 过滤数值大于2
    println(list.filter(
      (i: Int) => {
        i > 2
      }
    ))


    /**
     * groupBy
     * 格式： groupBy[K](f: A => K)
     * 作用： 对数据进行做分组操作，需要传入一个函数  该函数取出List中每一个元素，之后再去指定以哪种形式（数据） 进行做分组 返回值为Map类型
     *
     */

    val flatMapRes: List[String] = strList.flatMap(
      _.split(" ")
    )

    // def groupBy[K](f: A => K) =》 A表示List中每一个元素类型 K表示任意类型
    val groupByRes: Map[String, List[String]] = flatMapRes.groupBy(
      (x: String) => x
    )

    println(groupByRes)

    /**
     * sort相关：
     * ① sorted 对List中所有的元素按照从小到大的顺序进行排列 并且不影响原先List集合
     * ② sortBy 按照指定的字段进行排序 同时可以对字段值进行处理
     * ③ sortWith 按照指定的规则进行排序
     */

    println(list.sorted)
    println(list)

    // ② sortBy
    val StuList: List[SortStu] = List[SortStu](
      SortStu("1", "zhangsan", 20),
      SortStu("21", "lisi", 20),
      SortStu("3", "wangwu", 18),
      SortStu("4", "zhaoliu", 24)
    )


    // (f: A => B)
    // 排序方式默认为升序排序
    val sortStuList: List[SortStu] = StuList.sortBy(
      // 如果要实现年龄的倒叙排序，那么在给定值前加 -
      (sortStu: SortStu) => -sortStu.age
    )
    // 倒叙排序
    //    println(sortStuList.reverse)

    // 简化写法
    println(StuList.sortBy(
      // 给定是一个字符串 那么默认按照字典序排序
      //      _.id
      // 将字符串转换成数值再去比较
      _.id.toInt
    ))


    /**
     * ③ sortWith
     * lt: (A, A) => Boolean 传入的参数列表是两个List中的元素值 要求返回的为 Boolean
     * 当返回值 第一个 参数 < 第二个参数 那么结果为升序排序 反之为降序排序
     *
     */


    println(StuList.sortWith(
      /**
       * 在函数体内部实现如下比较规则:
       * ① 按年龄进行升序排序
       * ② 如果年龄相同，那么比较其id 并且id按照降序排序
       */
      (a: SortStu, b: SortStu) => {
        if (a.age == b.age) {
          a.id.toInt > b.id.toInt
        } else {
          //
          a.age < b.age
        }
      }
    ))

    /**
     * foreach:
     * 格式：foreach[U](f: A => U)
     * 作用： 遍历每一个元素，并将元素传递给对应的函数，并且该函数没有返回值
     */

    list.foreach(println)
    //    list.foreach(println(_))
    //    list.foreach((x:Int) => println(x))

    list.foreach {
      x: Int => println(x * 10)
    }

    list.foreach(
      (x: Int) => println(x * 10)
    )



  }
}

case class SortStu(id: String, name: String, age: Int)