package com.shujia

object Code20function {
  def main(args: Array[String]): Unit = {
    println(fun("2", 3))

    //
    //    fun(a)
    val funVal: Int => Int = fun2("2")


    println(funVal(3))

  }


  def fun(a: String, b: Int): Int = {
    a.toInt * b
  }

  // 柯里化函数：在定义函数时，将函数的参数列表拆开，在函数调用时可以分次进行调用 可以给一部分参数，之后再将其函数保存成变量，之后再去调用
  def fun2(a: String)(b: Int): Int = {
    a.toInt * b
  }

}
