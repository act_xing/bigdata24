package com.shujia

import java.io.FileNotFoundException

object Code19Exception {
  def main(args: Array[String]): Unit = {
    /**
     * 异常捕获
     *    作用：在编写代码时，可能会发生一些预期错误，那么对于该类错误，可以在发生时对其进行做处理
     *    格式：
     *     try  {
     *        可能会发生异常的代码块
     *        可能会发生异常的代码块
     *     }catch { --注释：catch对发生异常类型进行判断
     *        case 异常的对象名(变量名): 异常类型 =>
     *          发生异常时的处理的代码块
     *
     *        case 异常的对象名(变量名): 异常类型 =>
     *          发生异常时的处理的代码块
     *
     *
     *     }finally {
     *        不管是否发生错误，都会执行的代码 ...
     *     }
     *
     */

    val map2: Map[String, Int] = Map[String, Int](("k1", 11), ("k2", 22))

    try {
      println(map2("k2"))
//      println(map2("k3"))
      println("=====") //如果之前出现错误，那么改行代码不会执行..
      throw new FileNotFoundException("XXX文件不存在") //手动抛出异常
    }catch {
      case e:NoSuchElementException =>
        println("当前代码块中map取值不存在")
        println(e.toString)
      case e:FileNotFoundException =>
        println("文件不存在")
        println(e.toString)
    }finally {
      println("不管是否发生错误，都会执行的代码 ...")
      println(map2)
    }






  }
}
