package com.shujia;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

// 需求：使用Java代码实现WordCount
public class Code18WordCount {
    public static void main(String[] args) throws IOException {

        HashMap<String, Integer> hashMap = new HashMap<>();

        // 1.读取数据
        BufferedReader bufferedReader = new BufferedReader(new FileReader("data/word.txt"));

        String line;
         while ((line = bufferedReader.readLine()) != null){
             String[] splits = line.split(" ");
             for (String word : splits) {
                 if (hashMap.containsKey(word)) {
                     hashMap.put(word,hashMap.get(word)+1);
                 }else{
                     hashMap.put(word,1);
                 }
             }

         }

        bufferedReader.close();
        for (String key : hashMap.keySet()) {
            System.out.println("key:"+key+" value:"+hashMap.get(key));
        }



    }
}
