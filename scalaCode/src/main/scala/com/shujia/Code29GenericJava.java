package com.shujia;

import java.util.ArrayList;
import java.util.List;

public class Code29GenericJava {
    public static void main(String[] args) {

        //2. 泛型在某些场合下可以作为参数进行传递，并且泛型旨在编译期有效
        TestGeneric<UserTest> userTestTest = new TestGeneric<>();
        UserTest userTest = userTestTest.t;

        // 3. 泛型最主要的目的是为了约束内部数据类型
        ArrayList arrayList = new ArrayList();
        arrayList.add(new OtherTest());
        ArrayList<UserTest> userTestList = arrayList;
        System.out.println(userTestList);

        // 下面会报错
//        for (UserTest test : userTestList) {
//            System.out.println(test);
//        }

        ArrayList<String> strings = new ArrayList<>();
        test(strings);

        // String 的父类是Object 但是在传入时会报错
        ArrayList<Object> objectArrayList = new ArrayList<>();
//        test(objectArrayList);

    }
    public static void test(ArrayList<String> arr){
        System.out.println(arr);
    }
}

class TestGeneric<T>{
    public T t;
}

class UserTest{

}

class OtherTest{}
