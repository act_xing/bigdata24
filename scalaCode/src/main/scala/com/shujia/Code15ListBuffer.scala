package com.shujia

import scala.::
import scala.collection.mutable.ListBuffer

object Code15ListBuffer {
  def main(args: Array[String]): Unit = {
    /**
     * 可变的List => ListBuffer
     */

    // 当前可变集合 都在scala.collection.mutable下
    val listBuffer: ListBuffer[Int] = ListBuffer[Int](1, 2, 3, 4)
    // 添加元素方式1：
    listBuffer.append(4, 5, 6)
    println(listBuffer)

    // 添加元素方式2：
    listBuffer.+=(7, 8, 9)
    listBuffer += 10
    println(listBuffer)

    // 对于 += 只能添加 元素 虽然给定值时没有提示错误
    val listBuffer2: ListBuffer[Int] = ListBuffer[Int](11, 13, 14)
//    listBuffer +=  listBuffer2
//    println(listBuffer)
    // 将两个ListBuffer合并生成新的ListBuffer
    val listBuffer3 =  listBuffer ++ listBuffer2
    println(listBuffer3)

    // 将另外一个ListBuffer添加到当前ListBuffer中
    listBuffer ++= listBuffer2
    println(listBuffer)

    // 添加元素方式3：
    listBuffer.insert(0,100)
    println(listBuffer)

    // 删除元素
    // 删除指定下标位置数据
//    listBuffer.remove(100)
    listBuffer.remove(0,2)
    println(listBuffer)

    listBuffer -= 10
    println(listBuffer)

    // 修改元素
    listBuffer(0) = 99
    listBuffer.update(1,88)
    println(listBuffer)

    // 不可变List的计算
    val list: List[Int] = List[Int](1, 2, 3)

    val list1: List[Int] = 4 :: list
    println(list1)

    val list2: List[Int] = 5 :: 7 :: list
    println(list2)

    println(list2.+:(8))

    // 合并两个List
    println(list1 ::: list2)

  }
}
