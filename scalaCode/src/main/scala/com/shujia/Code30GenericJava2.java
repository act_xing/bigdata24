package com.shujia;

import java.util.ArrayList;

public class Code30GenericJava2 {
    public static void main(String[] args) {
        ArrayList<ChildJava> objects = new ArrayList<>();

        // 泛型不存在有继承关系
//        ArrayList<ChildJava> personJavas = new ArrayList<PersonJava>();
    }
}

class PersonJava{}

class ChildJava extends PersonJava{}

class SonJava extends ChildJava{}