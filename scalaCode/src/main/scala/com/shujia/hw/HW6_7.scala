package com.shujia.hw

import org.junit.{Before, Test}

import java.lang.Math
import scala.io.{BufferedSource, Source}

class HW6_7 {
  case class Sco(id: String, courseId: String, score: Int)

  case class Stu(id: String, name: String, age: Int, gender: String, clazz: String)

  case class Subject(courseId: String, courseName: String, score: Int, jgScore: Int)

  var scoList: List[Sco] = _
  var stuList: List[Stu] = _
  var subList: List[Subject] = _

  // Before的作用是在执行Test之前执行函数初始化
  @Before
  def init(): Unit = {
    val scoreSource: BufferedSource = Source.fromFile("data/score.txt")
    val studentSource: BufferedSource = Source.fromFile("data/students.txt")
    val subjectSource: BufferedSource = Source.fromFile("data/subject.txt")

    scoList = scoreSource.getLines().toList.map {
      oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        Sco(splitRes(0), splitRes(1), splitRes(2).toInt)
    }

    stuList = studentSource.getLines().toList.map {
      oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        Stu(splitRes(0), splitRes(1), splitRes(2).toInt, splitRes(3), splitRes(4))
    }

    subList = subjectSource.getLines().toList.map {
      oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        Subject(splitRes(0), splitRes(1), splitRes(2).toInt, (splitRes(2).toInt * 0.6).toInt)
    }


    scoreSource.close()
    studentSource.close()
    subjectSource.close()

  }


  // 4、统计每个班级的前三名 [学号，姓名，班级，分数]
  @Test
  def hw6(): Unit = {
    // TODO:1、对学生总分进行统计
    val stuTotalScore: Map[String, Int] = scoList.groupBy {
      score: Sco => score.id
    }.map {
      // 从GROUPBy的结果中取出每个元素时，其为一个Tuple2 可以通过case 语句对其进行做匹配
      case (id: String, allScore: List[Sco]) =>
        // 相同学生ID的成绩进行累加
        (id, allScore.map(_.score).sum)
    }
    //  TODO: 2、总分和基本信息进行关联

    stuList
      .map {
        case stu: Stu =>
          val totalScore: Int = stuTotalScore.getOrElse(stu.id, 0)
          (stu, totalScore)
      }
      // TODO: 3、对班级进行分组
      .groupBy {
        // 对班级进行做分组
        case (stu: Stu, score: Int) => stu.clazz
        //        tuple:(Stu,Int) => tuple._1.clazz
        // case 可以给定判断 如果对于类型没有匹配上 那么将其他的数据 分为一组 编号为 1
        //        case _ => 1
      }
      // TODO: 4.对组内数据进行排序，再取前三
      .map {
        case (clazz: String, stuAndScore: List[(Stu, Int)]) =>
          val top3: List[(Stu, Int)] = stuAndScore.sortBy {
            case (stu: Stu, score: Int) =>
              -score
          }.take(3)
          (clazz, top3)
      }
      .flatMap {
        case (clazz: String, top3: List[(Stu, Int)]) => top3
      }
      .foreach(println)
  }

  // 5、统计偏科最严重的前100名学生  [学号，姓名，班级，科目，分数]
  @Test
  def hw7(): Unit = {
    /**
     * 偏科: 需要求出其方差，但是各科成绩对应的总分并不一致，所以需要消除科目总分带来的影响 可以使用归一化处理
     * 归一化： 拿各科考试成绩 / 科目总分 => 得到占总分的百分比，再对百分比进行做 方差处理
     */

    val stuMap: Map[String, Stu] = stuList.map {
      case stu: Stu => (stu.id, stu)
    }.toMap



    // TODO: 1.拿各科成绩和科目总分进行关联，关联后，各科成绩求百分比（归一化）
    val courseIDAndScore: Map[String, Int] = subList.map {
      case subject: Subject => (subject.courseId, subject.score)
    }.toMap

    val scorePre: List[(Sco, Double)] = scoList.map {
      case sco: Sco => {
        val totalScore: Int = courseIDAndScore.getOrElse(sco.courseId, 100)
        val preRes: Double = sco.score * 100 / totalScore.toDouble
        // 以学生成绩对象作为Key，百分比分数作为Value
        (sco, preRes)
      }
    }
    // TODO: 2.求方差 => 拿各科的百分比 算每个学生的平均百分比，
    val avgPreRes: Map[String, Double] = scorePre.groupBy {
      case (sco: Sco, preRes: Double) => sco.id
    }.map {
      case (stuId: String, list: List[(Sco, Double)]) => {
        // 对学生的百分比占比进行求和
        val stuPreResSum: Double = list.map {
          case (sco: Sco, preRes: Double) => preRes
        }.sum
        // 将学生ID作为Key  平均百分比作为Value 写出
        (stuId, stuPreResSum / list.size)
      }
    }
    //  TODO： 3.后再去拿各科成绩和百分比相减 求其平方 之后再去做累加 得到每个学生成绩的方差
    scorePre.map {
      case (sco: Sco, preRes: Double) => {
        val avgPre: Double = avgPreRes.getOrElse(sco.id, 100)
        (sco, (preRes - avgPre) * (preRes - avgPre))
      }
    }.groupBy{
      case (sco: Sco,pew:Double) => {
        sco.id
      }
    }.map{
      case (id:String,idList:List[(Sco,Double)]) => {
        (id, idList.map(_._2).sum)
      }
    }.toList
    // TODO: 4.将方差的结果进行排序，得到前100名学生
      .sortBy(-_._2).take(100).map{
      case (id:String,fc:Double) => {
        val joinRes: List[(String, String, String, String, Int)] = scoList.filter(_.id == id).map {
          case sco: Sco => {
            val stu: Stu = stuMap.getOrElse(sco.id, Stu(id, "未知", 0, "未知", "未知"))
            //              [学号，姓名，班级，科目，分数]
            (stu.id, stu.name, stu.clazz, sco.courseId, sco.score)
          }
        }
        joinRes
      }
    }.foreach(println)

  }


}
