package com.shujia.hw

import scala.io.Source

object HW1 {
  def main(args: Array[String]): Unit = {
    // 统计班级人数 [班级,人数]

    // 1.读取学生数据
    val list: List[String] = Source
      .fromFile("data/students.txt")
      .getLines()
      .toList

    // 分组结果是Map Key:班级信息  Value:相同班级中的所有学生
    val groupByRes: Map[String, List[(String, String, String, String, String)]] = list
      .map(
        oneLine => {
          val oneStudent: Array[String] = oneLine.split(",")
          // 取出每个学生的信息，将其打包成一个Tuple
          (oneStudent(0), oneStudent(1), oneStudent(2), oneStudent(3), oneStudent(4))
        }
      )
      // 2.对班级进行分组
      .groupBy(_._5)
    // 3.统计班级分组中的个数（一条数据代表一个人）

    // 匿名函数格式 () => {}
    val clazzAndNum: Map[String, Int] = groupByRes.map(
      (oneClazz: (String, List[(String, String, String, String, String)])) => {
        val stuNum: Int = oneClazz._2.size
        // 以班级名称作为Key 学生人数作为Value返回
        (oneClazz._1, stuNum)
      }
    )

    clazzAndNum.toList.sortBy(-_._2).foreach(println(_))

//    clazzAndNum.foreach(
//
//    )

  }
}
