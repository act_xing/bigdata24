package com.shujia.hw

import scala.io.{BufferedSource, Source}

object HW4 {

  def main(args: Array[String]): Unit = {
    //    统计总分大于年级平均分的学生 [学号，姓名，班级，总分]

    /**
     * 分析：
     *    1. 求学生总分
     *       2. 求平均分
     *       3. 拿总分和平均分进行比较过滤
     *       4. 拿结果数据和学生基本信息进行关联
     */
    val source: BufferedSource = Source
      .fromFile("data/score.txt")

    val studentScore: Map[String, Int] = source
      .getLines()
      .toList
      // Key:ID  Value:一个学生ID的所有成绩数据
      .map {
        oneScore: String =>
          val oneSc: Array[String] = oneScore.split(",")
          // 1500100001,1000001,98
          (oneSc(0), oneSc(1), oneSc(2))
      }
      // 2.按学生的id进行分组 之后再去统计
      .groupBy(_._1)
      //        tuple:(String,String,String) =>
      //          tuple._1
      // 3.对每组中的数据进行统计 对成绩进行做求和
      .map(
        (tuple: (String, List[(String, String, String)])) => {
          val totalScore: Int = tuple._2.map(_._3.toInt).sum
          (tuple._1, totalScore)
        }
      )


    val avgScore: Int = studentScore.map(_._2).sum / studentScore.size
    println("平均分：", avgScore)


    val studentSource: BufferedSource = Source.fromFile("data/students.txt")
    val studentInfo: Map[String, (String, String)] = getStudentInfo(studentSource).toMap


    val stringToInt: Map[String, Int] = studentScore
      .filter(
        _._2 > avgScore
      )
    stringToInt
      .map(
        (kv: (String, Int)) => {
          val nameAndClazz: (String, String) = studentInfo.getOrElse(kv._1, ("未知", "未知"))
          // [学号，姓名，班级，总分]
          (kv._1,nameAndClazz._1,nameAndClazz._2,kv._2)
        }
      ).foreach(println)

  }

  def getStudentInfo(studentSource: BufferedSource): List[(String, (String, String))] = {
    val tuples: List[(String, (String, String))] = studentSource
      .getLines()
      .toList
      .map {
        oneStudent => {
          val studentInfo: Array[String] = oneStudent.split(",")
          // 以学生的ID      姓名            班级
          (studentInfo(0), (studentInfo(1), studentInfo(4)))
        }
      }
    tuples
  }
}
