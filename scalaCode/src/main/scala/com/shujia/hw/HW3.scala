package com.shujia.hw

import scala.io.{BufferedSource, Source}

object HW3 {
  def main(args: Array[String]): Unit = {
    //    统计年级排名前十学生各科的分数 [学号,  姓名，班级，科目，分数]
    /**
     * 需求分析：
     * 要求按照各科成绩取出前10名的学生，并且需要和学生基本信息进行关联操作
     */

    // 1、读取数据
    val scoreSource: BufferedSource = Source
      .fromFile("data/score.txt")

    val list: List[String] = scoreSource
      .getLines()
      .toList
    // 2.包装数据 （样例类） --> 切分数据 --> 创建HWScore对象
    val top10Student: List[HWScore] = list
      .map(
        (oneScore: String) => {
          val splitRes: Array[String] = oneScore.split(",")
          // 将切分的结果包装成学生对象 并返回
          HWScore(splitRes(0), splitRes(1), splitRes(2).toInt)
        }
      )
      // 3.按照学生的courseID进行分组
      .groupBy(_.courseID)
      // 4.对分组后的组内数据进行处理
      .map {
        kv: (String, List[HWScore]) => {
          // 5.取出List中的数据，并对其进行排序，按照分数进行倒序排序
          val scores: List[HWScore] = kv._2
            .sortBy(-_.score)
            // 6.取出前10的数据
            .take(10)
          // 7.将前10的学生结果数据 List 直接返回
          scores
        }
      }
      // 取出集合List中的每一个元素
      .flatMap(
        (list: List[HWScore]) => {
          list
        }
      )
      .toList



    // 读取学生基本信息数据
    val studentSource: BufferedSource = Source
      .fromFile("data/students.txt")

    val idAndStudentInfo: Map[String, (String, String)] = studentSource
      .getLines()
      .toList
      .map {
        oneStudent => {
          val studentInfo: Array[String] = oneStudent.split(",")
          // 以学生的ID作为Key  班级和姓名作为Value  组合成一个Tuple2 为了返回之后形成Map集合
          (studentInfo(0), (studentInfo(1), studentInfo(4)))
        }
      }.toMap

    // 从  top10Student: List[HWScore] 列表中取出每一个学生 之后再从 idAndStudentInfo 中 取出每个学生ID对应的数据

    top10Student.map{
      studentTop10:HWScore => {
        val studentID: String = studentTop10.id
        // 根据学生ID从 idAndStudentInfo Map 中读取Value数据
        // 如果取不到 ，那么返回默认值 0
        val nameAndClazz: (String, String) = idAndStudentInfo.getOrElse(studentID, ("未知", "未知"))
        // [学号,  姓名，班级，科目，分数]
        (studentID,nameAndClazz._1,nameAndClazz._2,studentTop10.courseID,studentTop10.score)
      }
    }.foreach(println)

    studentSource.close()
    scoreSource.close()
  }
}

// 有实现其apply方法 所以在创建对象时 省略 new
case class HWScore(var id: String, var courseID: String, var score: Int)

