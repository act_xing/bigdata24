package com.shujia.hw

import org.junit.{Before, Test}

import scala.io.{BufferedSource, Source}

class HW4_5 {
  case class Sco(id: String, courseId: String, score: Int)

  case class Stu(id: String, name: String, age: Int, gender: String, clazz: String)

  case class Subject(courseId: String, courseName: String, score: Int, jgScore: Int)

  var scoList: List[Sco] = _
  var stuList: List[Stu] = _
  var subList: List[Subject] = _

  // Before的作用是在执行Test之前执行函数初始化
  @Before
  def init(): Unit = {
    val scoreSource: BufferedSource = Source.fromFile("data/score.txt")
    val studentSource: BufferedSource = Source.fromFile("data/students.txt")
    val subjectSource: BufferedSource = Source.fromFile("data/subject.txt")

    scoList = scoreSource.getLines().toList.map {
      oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        Sco(splitRes(0), splitRes(1), splitRes(2).toInt)
    }

    stuList = studentSource.getLines().toList.map {
      oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        Stu(splitRes(0), splitRes(1), splitRes(2).toInt, splitRes(3), splitRes(4))
    }

    subList = subjectSource.getLines().toList.map {
      oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        Subject(splitRes(0), splitRes(1), splitRes(2).toInt, (splitRes(2).toInt * 0.6).toInt)
    }


    scoreSource.close()
    studentSource.close()
    subjectSource.close()

  }


  @Test
  def test(): Unit = {
    println("junit 正在执行...")
  }

  @Test
  def hw4(): Unit = {
    val stringToScoes: Map[String, List[Sco]] = scoList
      .groupBy(_.id)

    val stuTotalScore: Map[String, Int] = stringToScoes.map {
      kv: (String, List[Sco]) => {
        // 学生ID作为Key  学生总分作为Value写出数据
        (kv._1, kv._2.map(_.score).sum)
      }
    }

    val avgScore: Int = stuTotalScore.map(_._2).sum / stuTotalScore.size

    stuList.map {
      stu: Stu => {
        // 从学生的总分中取出分数
        val score: Int = stuTotalScore.getOrElse(stu.id, 0)
        (stu, score)
      }
    }.filter(
      _._2 > avgScore
    ).foreach(println)
  }


  @Test
  // 统计每科都及格的学生 [学号，姓名，班级，科目，分数]
  def hw5(): Unit = {
    /**
     * 1.取出所有学生的各科成绩
     * 2.拿各科成绩和各科的及格线进行比较
     * 3.如果及格的科目为 6 那么表明每科都及格
     *
     */


    val courseIDANDjgScore: Map[String, Int] = subList.map {
      sub: Subject => (sub.courseId, sub.jgScore)
    }.toMap

    // 先遍历每个学生的成绩数据,取出成绩大于及格线
    scoList
      .filter(
        (score: Sco) => {
          val jgScore: Int = courseIDANDjgScore.getOrElse(score.courseId, 0)
          score.score >= jgScore
        }

      )
      // 对每个学生的ID进行分组，看过滤后每个学生及格的科目有几个
      .groupBy(
        _.id
      ).map(
      // Key 学生ID  Value: 相同学生ID的及格分数对象
      (kv:(String,List[Sco])) => {
        (kv._1,kv._2,kv._2.size)
      }
    ).filter(
      _._3 == 6
    ).foreach(println)


  }

}
