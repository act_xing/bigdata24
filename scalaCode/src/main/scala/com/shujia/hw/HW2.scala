package com.shujia.hw

import scala.io.{BufferedSource, Source}

object HW2 {
  def main(args: Array[String]): Unit = {
    /**
     * 链式编程
     */

    // 统计学生的总分 [学号,学生姓名,学生年龄,总分]
    // 读取数据
    val source: BufferedSource = Source
      .fromFile("data/score.txt")

    source
      .getLines()
      .toList
    // Key:ID  Value:一个学生ID的所有成绩数据
      .map {
        oneScore: String =>
          val oneSc: Array[String] = oneScore.split(",")
          // 1500100001,1000001,98
          (oneSc(0), oneSc(1), oneSc(2))
      }
      // 2.按学生的id进行分组 之后再去统计
      .groupBy(_._1)
    //        tuple:(String,String,String) =>
    //          tuple._1
    // 3.对每组中的数据进行统计 对成绩进行做求和
      .map(
        (tuple: (String, List[(String, String, String)])) => {
          val totalScore: Int = tuple._2.map(_._3.toInt).sum
          (tuple._1, totalScore)
        }
      )
      // 4.遍历结果
      .foreach(println)

    source.close()

  }
}
