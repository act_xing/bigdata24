package com.shujia

object Code22Implicit2 {

  def main(args: Array[String]): Unit = {
    // 隐式转换类
    // 显示调用
//    val mysql = new MysqlDB()
//    val connect = new MysqlConnect(mysql)
//    connect.getConnect()

    // 隐式转换

    val mysql = new MysqlDB()
    mysql.getConnect()

  }

  // 隐式转换类 需要在定义类时添加 implicit 描述,并且在默认构造函数中给定一个参数 之后在object中出现有对应参数类型的对象时，
  //    会自动对该对象进行包装 包装成隐式转换类的对象 获取其函数方法 再进行调用
  //  该过程实际上就是 隐式转换函数 隐式转换其默认构造函数  implicit this(mysql:MysqlDB)

  // 隐式转换类只能定义在object中（由其作用域决定的） 并且参数个数只有一个
  implicit class MysqlConnect(mysql:MysqlDB){
    def getConnect(): Unit ={
      println("获取到Mysql链接")
      mysql.desc()
    }
  }
}



class MysqlDB(){
  def desc(): Unit ={
    println("当前为一个Mysql对像")
  }
}