package com.shujia

object Code06Fun3 {
  def main(args: Array[String]): Unit = {
    /**
     * 函数的参数
     * 1.类/函数
     * 2.可变长参数
     * 3.默认值参数
     */

    func1("str1", "str2", "str3")
    func2("zhangsan","12","男")
    func3("zhangsan")
    func3("lisi",password = "123456")

  }

  def func1(strs: String*): Unit = {
    strs.foreach(println)
  }

  // 可变长参数需要在普通参数之后
  //  def func2(strs:String*,name:String): Unit ={
  //    strs.foreach(println)
  //  }

  def func2(name: String, strs: String*): Unit = {
    println("name:", name)
    strs.foreach(println)
  }


  // 默认值参数
  def func3(name:String, password:String = "000000"): Unit ={
    println(name)
    println(password)
  }

}
