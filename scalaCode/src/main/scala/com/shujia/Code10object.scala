package com.shujia

object Code10object {
  var name: String = "zhangsan"

  def main(args: Array[String]): Unit = {
    print(Code10object.name)
    fun1("10")
    Code10object.fun1("10")

    // 构造对象的三种方式
    val studentObject1 = new StudentObject("张三", 18)

    // 通过直接调用伴随对象的apply函数进行创建
    val studentObject2: StudentObject = StudentObject.apply("张三",18)

    // 隐式调用apply函数进行构建
    val studentObject3: StudentObject = StudentObject("张三", 18)

  }

  def fun1(str: String): Int = {
    str.toInt * 10
  }
}

// 伴生类
class StudentObject(name: String, age: Int) {

}

// 伴生对象
object StudentObject {

  // 对于对象进行初始化操作 相当于Java中的init函数
  def apply(name: String, age: Int): StudentObject = {
    var zhangsan = new StudentObject(name, age)
    zhangsan
  }
}

