package com.shujia

object Code07Fun4 {

  def main(args: Array[String]): Unit = {
    /**
     * 匿名函数
     * 匿名函数为函数的名称定义时没有明确指出 ,并且匿名函数的返回值需要自行推断
     * 匿名函数也时有函数类型的 需要找出其参数列表及返回值 ，可以直接传入函数中进行调用
     */


    // 简化过程
    func((str: String, i: Int) => str.toInt + i)

    // 将匿名函数赋值给一个变量进行保存 其变量类型 为函数的类型
    val funVal: (String, Int) => Int = (str: String, i: Int) => str.toInt + i

    // 定义了一个匿名函数 该函数被传给func2 在func2的内部对该函数进行了调用 并传入值20 value = 20  再对匿名函数的函数体进行执行 20 * 10 => 200
//    func2((value:Int) => value * 10)
    func2(value => value * 10)

    // 210  对于多个参数列表 其() 不可以省略
    func((str1,i) => str1.toInt * i + 10)

    func2(_ * 10)
//    func((_,i) => _.toInt * i + 10)



  }


  def func(f: (String, Int) => Int): Unit = {
    println(f("10", 20))
  }

  def func2(f: Int => Int): Unit = {
    println(f(20))
  }
}
