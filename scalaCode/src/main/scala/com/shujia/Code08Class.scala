package com.shujia

import scala.beans.BeanProperty

object Code08Class {
  def main(args: Array[String]): Unit = {
    /**
     * 类的定义：
     * ① Scala中定义类需要使用class关键字修饰
     * ② 类中定义一个属性，需要给定一个初始值 否则当前类会要求为一个抽象类
     * ③ 抽象类定义 abstract 修饰
     * ④ 当类的属性定义时 对于初始值可以使用 _ 来替代
     *
     * 类的格式：
     * [修饰符] class 类名 {}
     */

    // 构建Person对象
    val person: Person = new Person()
    person.running()
    println(person.age)
    println(person.name)

    // 当前person是val修饰的 是一个常量 对于常量一般不能做修改
    person.name = "lisi"
    // 对其中的属性修改后，生效 为什么？
    println(person.name)
    println(person) // com.shujia.Person@d7b1517 内存地址 当内存地址指向的对象其属性发生变化，并不会影响其对象的内存地址 所以person 可以使用val修饰

    // 对于类来说构建其对象是通过 new 类名() 来创建  其中类名()是类的构造函数

    val student = new Student("15001001001", "张怀远", 23)
    println(student._id) // 实际编译时是调用 public String _id()
    println(student._name)
    println(student._age)

    val student1: Student = new Student()
    println(student1._name)

    val student2: Student = new Student("15001001002", "陆奥")

    println(student2._name)

//    println(student2.get_name()) //

  }

}

class Person {
  var name: String = "zhangsan"
  //  val age:Int = _ // 0  对于 _ 占位修饰的属性 如果使用val修饰成一个常量 并没有任何含义 所以语法上并不能通过
  var age: Int = _

  def running(): Unit = {
    println("running is skill...")
  }
}

// (id:String,name:String,age:Int) 相当于构造函数的参数列表
// 该方式为默认的构造函数
class Student(id: String, name: String, age: Int) {
  // 在类中也可以对函数进行直接调用，并且是在类的对象构建时 执行
  println("当前类的对象正在被构建")

  val _id: String = id
//  @BeanProperty // 通过注解的方式添加get set 函数 注意被修饰的属性 需要是一个常量
  var _name: String = name
  val _age: Int = age


  def this() {
    this("1001", "张三", 18)
    println("辅助构造函数正在执行...")
  }

  def this(id: String, name: String) {
    this(id, name, 18)
    println("辅助构造函数2正在执行...")
  }


}