package com.shujia

class Code04Fun {
  def printHello(): Unit = {
    println("this function is running...")
  }
}

object Code04Fun {

  // main函数是一个特殊的函数，整个应用的入口 并且符合定义规则
  // def 函数定义的关键字 除了匿名函数以外 其他函数都需要使用该关键字进行修饰
  // main 方法名
  // args 参数名
  // Array[String] 参数的类型
  // Unit 返回值类型 相当于Java中Void关键字 没有返回值
  // {} 函数体
  def main(args: Array[String]): Unit = {
    // 在Scala中哪些地方可以定义函数？
    // ① 在class中可以定义一个函数 需要通过类的对象进行调用
    new Code04Fun().printHello()

    // ② object中定义方法 需要通过类对象进行调用
    Code04Fun.printHello2()

    // ③ 在函数内部定义 直接使用函数名 就可以调用
    def printHello3(): Unit = {
      println("this function is running...")
    }

    printHello3()


    fun2
    /**
     * 函数的省略
     *  ① 对于返回值，可以省略return 将具体的值放在函数体最后一行
     *  ② 对于只有一行代码 那么 {} 可以省略
     *  ③ 对于返回值类型可以自行推断，:类型 可以删除 但是 = 不能去除
     *  ④ 对于没有参数的函数 参数列表() 可以删除
     *  ⑤ 对于函数调用时 没有参数传入 那么() 也可以删除
     */

  }

  def printHello2(): Unit = {
    println("this function is running...")
  }

  // ① 既无参数，也无返回值
  def fun1 = println("既无参数，也无返回值")


  // ② 既无参数，有返回值
  def fun2() = {
    println("既无参数，有返回值")
    10
  }

  // ③ 有参数，没有返回值
  def fun3(str: String, i: Int): Unit = {
    println(s"str:$str,i:$i")
  }

  // ④ 有参，有返回值
  def fun4(str: String, i: Int): Int = {
    println(s"str:$str,i:$i")
    10
  }




}
