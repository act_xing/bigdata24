package com.shujia

object Code23Implicit3 {
  implicit val str:String = "这是一个隐式转换变量"

  def main(args: Array[String]): Unit = {
    /**
     * 隐式转换变量
     *   通过定义变量时，指定当前变量为一个隐式转换变量
     *   之后在 函数的参数列表中对于参数 也添加一个implicit修饰，那么在Object中调用该函数时，那么就不需要进行做传参
     *
      */

    hello(str)
    hello
  }

  def hello(implicit str:String ="hello world"): Unit ={
    println(str)
  }
}
