package com.shujia;

import javax.swing.*;

public class Code30GenericJava3 {

    public static void main(String[] args) {
        // 继承关系 SonJava  extend  ChildJava extend PersonJava
        ProduceJava2<ChildJava> childProduceJava2 = new ProduceJava2<>();
        childProduceJava2.produce(new MessageJava<ChildJava>());
        childProduceJava2.produce(new MessageJava<PersonJava>());
        // ? super A 表示当前MessageJava中给定的泛型是A的父类或者当前类
//        childProduceJava2.produce(new MessageJava<SonJava>());

        //  <? extends B> 表示当前 MessageJava中给定的泛型是B的子类或者当前类
        ConsumerJava2<ChildJava> childJavaConsumerJava2 = new ConsumerJava2<>();
        childJavaConsumerJava2.consumer(new MessageJava<ChildJava>());
        childJavaConsumerJava2.consumer(new MessageJava<SonJava>());
//        childJavaConsumerJava2.consumer(new MessageJava<PersonJava>());
    }
}

class MessageJava<T>{
    public T data;
}

class ProduceJava2<A> {
    public void produce(MessageJava<? super A> messageJava) {
        System.out.println("produce函数执行了...");
    }
}

class ConsumerJava2<B> {
    public void consumer(MessageJava<? extends B> messageJava) {
        System.out.println("produce函数执行了...");
    }
}