package com.shujia

import org.junit.{Before, Test}

import scala.io.{BufferedSource, Source}

class Code27Window {


  @Test
  def test(): Unit = {
    val list: List[Int] = List[Int](1, 2, 3, 4, 4, 5, 6, 7, 7, 8, 9, 11, 12, 13, 14, 15)

    // 用于开启一个窗口
    //    list.sliding(3).foreach(println)

    list.sliding(3, 2).foreach(println)


  }

  case class Sco(id: String, courseId: String, score: Int)

  case class Stu(id: String, name: String, age: Int, gender: String, clazz: String)

  case class Subject(courseId: String, courseName: String, score: Int, jgScore: Int)

  var scoList: List[Sco] = _
  var stuList: List[Stu] = _
  var subList: List[Subject] = _

  // Before的作用是在执行Test之前执行函数初始化
  @Before
  def init(): Unit = {
    val scoreSource: BufferedSource = Source.fromFile("data/score.txt")
    val studentSource: BufferedSource = Source.fromFile("data/students.txt")
    val subjectSource: BufferedSource = Source.fromFile("data/subject.txt")

    scoList = scoreSource.getLines().toList.map {
      oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        Sco(splitRes(0), splitRes(1), splitRes(2).toInt)
    }

    stuList = studentSource.getLines().toList.map {
      oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        Stu(splitRes(0), splitRes(1), splitRes(2).toInt, splitRes(3), splitRes(4))
    }

    subList = subjectSource.getLines().toList.map {
      oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        Subject(splitRes(0), splitRes(1), splitRes(2).toInt, (splitRes(2).toInt * 0.6).toInt)
    }


    scoreSource.close()
    studentSource.close()
    subjectSource.close()


  }


  // 案例：
  //  1.取出前一名和后一名学生成绩的分数差
  @Test
  def windowFun(): Unit = {

    val stuTotalScore: Map[String, Int] = scoList.groupBy {
      score: Sco => score.id
    }.map {
      // 从GROUPBy的结果中取出每个元素时，其为一个Tuple2 可以通过case 语句对其进行做匹配
      case (id: String, allScore: List[Sco]) =>
        // 相同学生ID的成绩进行累加
        (id, allScore.map(_.score).sum)
    }

    var row_num: Int = 0

    stuTotalScore
      .toList
      .sortBy(-_._2)
      .map {
        case (id: String, score: Int) => {
          row_num += 1
          (id,score,row_num)
        }
      }.foreach(println)



    //    val windowsRes: List[List[(Stu, Int)]] = stuList
    //      .map {
    //        case stu: Stu =>
    //          val totalScore: Int = stuTotalScore.getOrElse(stu.id, 0)
    //          (stu, totalScore)
    //      }.sortBy(-_._2)
    //      .sliding(3, 1).toList
    //
    //
    //    windowsRes.map{
    //      case oneWin:List[(Stu, Int)] => {
    //        val firstScore: Int = oneWin(0)._2  // 取出前一个学生的分数
    //        val nextScore: Int = oneWin(2)._2   // 取出后一个学生的分数
    //        (oneWin(1)._1, firstScore - nextScore)  // 返回中间学生的对象以及前后学生的分数差
    //      }
    //    }.foreach(println)
  }
}
