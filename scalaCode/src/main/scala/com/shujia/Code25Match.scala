package com.shujia

import scala.reflect.ClassTag.Null

object Code25Match extends App {
  /**
   * Scala中的模式匹配相当于Java中的Switch
   * 通过 match可以实现如下匹配：
   * 1、基本数据类型
   * 2、字符串
   * 3、枚举enum
   * 4、类型
   * 5、样例类的对象
   *
   *   注意：对于match 匹配到的代码中可以给定返回值
   */

  // 1、基本数据类型 => AnyVal

//  val value1: Int = 10
//  value1 match {
//    case 10 =>
//      println("value值为10")
//      println("value值为10") // 在Scala中对应多行代码并不需要使用{}来进行标记
//      10.0 // 在Scala中没有break 并且对于有返回值的情况下 不需要通过return将数据返回，而默认最后一行的值最为其返回值
//    case 20 =>
//      println("value值为20")
//      20.0
//    case _ =>
//      println("匹配到其他值")
//      println
//  }
  val i:Int = 10
  i match {
    case 20 =>
      println("value值为20")
    case 10 =>
      println("value值为10")
    case _ =>
      println("匹配到其他值")
  }

  // 2、字符串 => AnyRef

  val str:String = "scala"
  str match {
    case "java" => println("this is java")
    case "scala" => println("this is scala")
    case _ => println("匹配到其他值")
  }

  // 3、枚举enum
  val wednesday: Week.Value = Week.Wednesday
  wednesday match {
    case Week.Monday => println("上课")
    case Week.Tuesday => println("上课")
    case Week.Wednesday => println("自习")
    case Week.Thursday => println("上课")
    case Week.Friday => println("上课")
    case Week.Saturday => println("上课")
    case Week.Sunday => println("休息(自习)")
  }

  // 4、类型
  // Any是所有类的父类
  val value:Any = 100
  private val res: Long = value match {
    case v: String =>
      println("当前是一个String")
      v.toLong
    case v: Int =>
      println("当前是一个Int")
      v.toLong
    case _ =>
      println("未匹配到其类型..")
      10L
  }
  println(res)


  // 5、样例类的对象
  case class StuMatch(id:String,name:String,age:Int)

  val stuMatch: StuMatch = StuMatch("1001", "王玉洁", 20)

  stuMatch match {
    case StuMatch("1001", "王宇杰", 20) => {
      println("哦原来是王宇杰")
    }
    case StuMatch("1001", "王玉洁", 20) => {
      println("哦原来是玉洁姐")
    }
    case _ => {
      println("原来是小姐姐...")
    }
  }




}
object Week extends Enumeration{
  val Monday:Week.Value = Value("Mon.")
  val Tuesday:Week.Value = Value("Tue.")
  val Wednesday:Week.Value = Value("Wed.")
  val Thursday:Week.Value = Value("Thu.")
  val Friday:Week.Value = Value("Fir.")
  val Saturday:Week.Value = Value("Sat.")
  val Sunday:Week.Value = Value("Sun.")
}

