package com.shujia

import scala.collection.mutable

object Code18Set {
  def main(args: Array[String]): Unit = {

    /**
     * set集合
     * set集合中的元素是无序的，并且是不可重复的
     */

      // 不可变集合
    val set1: Set[Int] = Set[Int](1, 2, 3, 1, 2, 3, 4)
    println(set1)

    // 集合的运算
    val set2: Set[Int] = Set[Int](3, 4, 5, 6)


    println(set1 & set2) // 交集
    println(set1 | set2) // 并集
    println(set1.diff(set2)) // 差集  当前集合中的元素去除 交集的元素
    println(set2.diff(set1)) // 差集  当前集合中的元素去除 交集的元素

    // 可变集合

    val mutableSet: mutable.Set[Int] = mutable.Set[Int](1, 2, 3, 1, 2, 3, 4, 5)
    mutableSet.add(7)
    mutableSet.add(6)
    println(mutableSet)

    mutableSet += 8
    println(mutableSet)

    // 删除
    mutableSet -= 7
    mutableSet.remove(1)
    println(mutableSet)


    // 在mutableSet中依旧包含有常用的操作函数 map flatMap ...

    val mutableSet2: mutable.Set[Int] = mutable.Set[Int](9,10)

    // 将集合累加，并返回一个新的集合
    println( mutableSet2 ++ mutableSet)






  }
}
