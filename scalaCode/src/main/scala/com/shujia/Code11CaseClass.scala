package com.shujia

object Code11CaseClass {
  def main(args: Array[String]): Unit = {
    val teacher = new Teacher("老邢", "0001", "hadoop")
    println(teacher.name)
    println(teacher.id)


    val teacher1: Teacher = Teacher("笑哥", "0011", "flink")
    println(teacher1)  // case class 中对 Teacher 的toString函数进行了重写  =>  Teacher(笑哥,0011,flink)


    val student: Student = new Student()
    println(student) // => com.shujia.Student@5a39699c

  }
}


case class Teacher(var name: String, var id: String, var teacherObj: String)