package com.shujia

import scala.collection.immutable
import scala.collection.mutable.ArrayBuffer
object Code14Collection {
  def main(args: Array[String]): Unit = {

    /**
     * Scala的集合有三大类 ： ①序列seq  ② 集合 set ③ 映射 Map
     *    所有集合都扩展来自于 Iterable 所有的集合都是他的子实现
     *    并且Scala的集合提供了两个版本 可变 和 不可变
     *      可变集合包： scala.collection.mutable
     *      不可变集合包： scala.collection.immutable
     */


    /**
     * 数组：
     *    ① 不可变数组 -> 长度不可变
     */
    val arrays = new Array[Int](4)
    arrays(0) = 10
    arrays.update(1,20)

    // 遍历数据
    for (elem <- arrays) {
      println(elem)
    }
    arrays.foreach(println)

    // 将数组中的数据拼接成一个字符串
    println(arrays.mkString(","))


    /**
     * 数组：
     *    ② 可变数组 -> 长度可变
     */

    val arrayBuffer = new ArrayBuffer[Int]()

    // 参数类型加上 * 表示可变长
    arrayBuffer.append(1,2,3,4)
    println(arrayBuffer.mkString(","))
    arrayBuffer.update(1,10)
    arrayBuffer.foreach(println(_))


  }
}
