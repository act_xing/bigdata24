package com.shujia

object Code05Fun2 {

  def main(args: Array[String]): Unit = {
    val str2 = "abcd,EFgH,LwN"
    val splits: Array[String] = str2.split(",")

    // def foreach[U](f: A => U) 其中 A表示为一个泛型  U表示为Unit 为无返回值
    // println中函数参数列表为 x:Any  但是当前split.foreach 要求传入的是String 由于Any 是String的父类 所以可以直接使用
    // 传入参数可以为一个函数，但是函数是有类型的 那么类型怎么判断
    // 参数的类型和  参数列表中类型 个数 顺序 返回值类型 有关
    splits.foreach(println)
    splits.foreach(print)

    println()
    fun5(fun4)

    // fun7没有加() 表示作为变量参数传入 fun6有() 表示调用
    println(fun6(func7))

    val f: () => Int = func8(10)
    println(f())

    val res: Int = func9(10)
    println(res)

  }


  // 对于该函数来说，如何描述其类型
  // (String,Int) => Int  说明当前函数有两个参数 类型分别是String Int 其返回值为 Int
  def fun4(str: String, i: Int): Int = {
    println(s"str:$str,i:$i")
    10
  }

  // 该函数的作用是 接收一个函数 其类型为 (String,Int) => Int （注意：看参数的类型为参数名: 之后才是） 在函数体内部进行调用  并且没有返回值
  def fun5(f: (String, Int) => Int): Unit = {
    f("function", 10)
  }

  def fun6(f: () => Int): Int = {
    // 对于没有参数的函数 在函数内部调用时，需要加上()
    f()
  }

  // 参数名: () => Int
  def func7() = {
    println("func7被调用")
    10
  }

  // 函数类型 (Int) => (() => Int)
  def func8(i: Int): () => Int = {
    func7
  }

  // 查看函数的返回值类型为 : 右边 和 = 左边的内容
  // 该函数的类型 (Int) => Int
  def func9(i: Int) = {
    // 如果对于一个没有参数的函数作为最后一行 并且有返回值，那么默认会调用  直接查看自动类型推断就可以了
    func7
  }
}
