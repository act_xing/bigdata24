package com.shujia



object Code26NullAndNodeAndNothing {

  def main(args: Array[String]): Unit = {
    println(new TestNull().name)
    println(new TestNull().age)
    println(new TestNull().tag)


    val map: Map[String, Int] = Map[String, Int](("k1", 10), "k2" -> 20)
    println(map.get("k3"))  // None
    println(map.get("k2"))  // Some(20)

    map.getOrElse("k3",0)
    //通过match 实现getOrElse函数的逻辑
    val res: Int = map.get("k2") match {
      case None =>
        0
      case Some(v) =>
        v
    }
    println(res)

  }
}

class TestNull{
  var name:String = _
  var age:Int = _
  var tag:Char = _
}
