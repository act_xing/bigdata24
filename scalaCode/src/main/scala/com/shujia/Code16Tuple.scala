package com.shujia

object Code16Tuple {
  def main(args: Array[String]): Unit = {
    /**
     * Tuple元组
     *   ① 不可变的集合
     *   ② 元组构建时，需要指定Tuple的长度，并且在元组中可以存储不同类型的数据
     *   ③ Tuple最大长度为 22
     */

      // 注意：tuple的类型为 (元素类型...)
    val tuple: (Int, String) = Tuple2(1, "2")
    println(tuple._1)
    println(tuple._2)

    // 构建Tuple的第二种方式
    val tuple1: (Int, String, Int) = (1, "2", 3)
    println(tuple1)

    println(tuple1.x)

    val list = List(
      ("1001", "zhangsan", 24),
      ("1002", "lisi", 25),
      ("1003", "wangwu", 26)
    )

    // 取出所有年龄的数据，并将其保存为List
    val ageList: List[Int] = list.map(
      _._3
    )
    println(ageList)


  }

}
