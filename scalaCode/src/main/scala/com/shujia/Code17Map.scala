package com.shujia

import scala.io.Source
import scala.collection
import scala.collection.mutable

object Code17Map {

  def main(args: Array[String]): Unit = {

    /**
     * Map:
     * ①存储的元素为KV形式
     * ②每个元素的Key是不能重复的 并且相同Key会对值进行覆盖
     * ③ Map是无序的 没有下标  只能通过Key找映射的Value
     * ④ 适用于查询数据
     */


    // trait Map[A, +B] extends Iterable[(A, B)]
    // 定义Map  Java中Map是一个Key Value形式
    val map: Map[String, Int] = Map[String, Int](
      // elems:(String,Int)*  => Map中的元素类型为 Tuple2
      ("k1", 1), ("k2", 2)
      // 第二种元素给定方式
      , "k3" -> 3
    )

    // 取数据  1.通过get取值时，返回的是一个Option，如果Option有值，那么为Some 没有值为None
    // 通过该方式可以避免报错 .NoSuchElementException: key not found: k4
    val k4Value: Option[Int] = map.get("k4")
    println(k4Value)
    val k1Value: Option[Int] = map.get("k1")
    println(k1Value)


    // 2.通过getOrElse获取Map中的数据
    println(map.getOrElse("k1", -1))
    println(map.getOrElse("k4", -1))


    val map2: Map[String, Int] = Map[String, Int](("k1", 11), ("k1", 22))
    println(map2)

    // 以下取值方式不安全 不推荐使用
    println(map("k1"))
    //    println(map("k4"))

    // map函数

    val value1: Map[String, Int] = map + (("ky3", 3), ("ky4", 4))
    println(value1)

    val value2: Map[String, Int] = map ++ map2
    println(value2)



    // 对word.txt进行做词频统计

    // 1.读取数据
    // list中每一个元素是一行数据
    val list: List[String] = Source
      .fromFile("data/word.txt")
      .getLines()
      .toList

    val wordsArray: List[Array[String]] = list.map(
      _.split(" ")
    )

    // flatMap 是对集合中的数据进行扁平化 将集合数据取出，并对其保存到大的集合中
    // List[String] 保存了每一个单词
    val words: List[String] = wordsArray.flatMap(
      (words: Array[String]) => {
        words
      }
    )
    println(words)
    // 将相同的单词放到一组数据中
    val wordGroupBy: Map[String, List[String]] = words.groupBy(
      (word: String) => word
    )
    // 返回结果为Map类型 其中Key为单词 Value是相同单词的一组数据
    println(wordGroupBy)


    // Map集合的map函数 每次执行取出一个KV进行计算
    val wordCountRes: Map[String, Int] = wordGroupBy.map(
      // 每次取到的KV元素用 tuple2 变量保存，其中其类型为一个Tuple,并且其Tuple的类型为 (Tuple元素的类型)
      //       Tuple元素的类型: (Key的类型,Value的类型) => (String,List[String])
      (tuple2: (String, List[String])) => {
        // List(scala, scala, scala, scala, scala)
        // 取出其个数
        val num: Int = tuple2._2.size
        // 将单词作为Key，num作为Value 组成一个Tuple2 将结果返回出去
        (tuple2._1, num)
      }
    )

    wordCountRes.foreach(println)

    // 可变Map

    val mutableMap: mutable.Map[String, String] = mutable.Map(("k1", "v1"), ("k2", "v2"), ("k3", "v3"),"k4" -> "v4")

    // def put(key: A, value: B)
    mutableMap.put("k5","v5")
    println(mutableMap)

    mutableMap += Tuple2("k6","v6")
    mutableMap += (("k7","v7"))
    println(mutableMap)

    // 删除数据
    mutableMap -= "k7"
    mutableMap.remove("k6")
    println(mutableMap)

    // 修改数据
    mutableMap("k3") = "333"
    println(mutableMap)





  }
}
