package com.shujia.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.protocol.types.Field;

import java.util.ArrayList;
import java.util.Properties;

public class Demo3Consumer {
    public static void main(String[] args) {
        //1、创建消费者
        //指定kafka broker 列表
        Properties properties = new Properties();
        //指定broker列表
        properties.setProperty("bootstrap.servers", "master:9092,node1:9092,node2:9092");
        //指定反序列化方式
        properties.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        //设置消费者id
        properties.setProperty("group.id","shujia2");


        /*
         * earliest
         * 当各分区下有已提交的offset时，从提交的offset开始消费；无提交的offset时，从头开始消费
         * latest  默认
         * 当各分区下有已提交的offset时，从提交的offset开始消费；无提交的offset时，消费新产认值生的该分区下的数据
         * none
         * topic各分区都存在已提交的offset时，从offset后开始消费；只要有一个分区不存在已提交的offset，则抛出异常
         *
         */
        //读取数据的位置
        properties.setProperty("auto.offset.reset", "earliest");


        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties);

        //订阅topic
        ArrayList<String> topics = new ArrayList<>();
        topics.add("cars");
        consumer.subscribe(topics);


        while (true){
            //拉取数据,会返回一个迭代器
            ConsumerRecords<String, String> consumerRecords = consumer.poll(1000);

            //循环解析数据
            for (ConsumerRecord<String, String> consumerRecord : consumerRecords) {
                String topic = consumerRecord.topic();
                String key = consumerRecord.key();
                long offset = consumerRecord.offset();
                long timestamp = consumerRecord.timestamp();
                String value = consumerRecord.value();

                System.out.println(topic + "\t" + key + "\t" + offset + "\t" + timestamp + "\t" + value);
            }

        }

    }
}
