package com.shujia.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

public class Demo1Producer {
    public static void main(String[] args) {
        //1、创建生产者---建立网络连接

        Properties properties = new Properties();
        //指定kafka broker 列表
        properties.setProperty("bootstrap.servers", "master:9092,node1:9092,node2:9092");
        //指定kafka  key和value序列化方式
        properties.setProperty("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.setProperty("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        KafkaProducer<String, String> producer = new KafkaProducer<>(properties);


        //发送数据到kafka
        producer.send(new ProducerRecord<>("shujia", "java"));
        producer.send(new ProducerRecord<>("shujia", "spark"));

        //关闭消费者
        producer.close();
    }
}
