package com.shujia.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Properties;

public class Demo2CarToKafka {
    public static void main(String[] args) throws Exception {
        //1、创建生产者---建立网络连接

        Properties properties = new Properties();
        //指定kafka broker 列表
        properties.setProperty("bootstrap.servers", "master:9092,node1:9092,node2:9092");
        //指定kafka  key和value序列化方式
        properties.setProperty("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.setProperty("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        KafkaProducer<String, String> producer = new KafkaProducer<>(properties);

        //读取文件
        //创建字符流
        FileReader fileReader = new FileReader("flink/data/cars.json");

        //创建缓冲流
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;
        //循环读取数据
        while ((line = bufferedReader.readLine()) != null) {
            //将数据写入kafka
            producer.send(new ProducerRecord<>("cars", line));
        }

        producer.close();
    }
}
