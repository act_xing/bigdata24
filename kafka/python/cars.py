# -*- coding:utf-8 -*-
import json
import random
import time

# 定义卡扣编号列表、车牌号码列表、城市名列表、道路名列表、区县名列表
tollgate_list = ['110', '111', '112', '113', '114']
license_plate_list = ['川A', '鄂B', '湘D', '皖C', '京N']
city_list = ['北京市', '上海市', '广州市', '深圳市', '杭州市']
road_list = ['朝阳路', '人民路', '中山路', '解放路', '文化路']
district_list = ['东城区', '虹口区', '天河区', '南山区', '西湖区']

# 定义卡扣经纬度范围
tollgate_longitude_range = (116.2, 116.6)
tollgate_latitude_range = (39.8, 40.2)

# # 生成100条随机数据
data_list = []

with open("cars.json", mode="w") as file:
    ts = 1614614400
    for i in range(1000000):
        ts = ts + random.randint(1, 20)
        tollgate_id = random.choice(tollgate_list)
        license_plate = random.choice(license_plate_list) + str(random.randint(10000, 99999))
        timestamp = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(ts))
        longitude = round(random.uniform(*tollgate_longitude_range), 6)
        latitude = round(random.uniform(*tollgate_latitude_range), 6)
        speed = random.randint(60, 120)
        city = random.choice(city_list)
        road = random.choice(road_list)
        district = random.choice(district_list)

        m = {
            "tollgate_id": tollgate_id,
            "license_plate": license_plate,
            "timestamp": timestamp,
            "longitude": longitude,
            "latitude": latitude,
            "speed": speed,
            "city": city,
            "road": road,
            "district": district
        }
        dump = json.dumps(m, ensure_ascii=False)
        file.write(dump)
        file.write("\n")

        file.flush()

        time.sleep(0.04)
