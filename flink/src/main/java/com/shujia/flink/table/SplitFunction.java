package com.shujia.flink.table;

import org.apache.flink.table.functions.ScalarFunction;

/**
 * 自定义函数，方法名必须时eval
 */
public class SplitFunction extends ScalarFunction {

    public String[] eval(String line, String sep) {
        return line.split(sep);
    }
}
