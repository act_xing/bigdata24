package com.shujia.flink.table;

import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;

public class Demo1WordCount {
    public static void main(String[] args) {
        //1、创建flink的执行环境
        EnvironmentSettings settings = EnvironmentSettings
                .newInstance()
                //设置执行模式
                .inStreamingMode()
                //.inBatchMode()
                .build();

        TableEnvironment tEnv = TableEnvironment.create(settings);

        /*
         * 在流上定义表
         * 1、基于kafka数据创建表  -- source 表
         */

        tEnv.executeSql("CREATE TABLE words (\n" +
                "  `word` STRING\n" +
                ") WITH (\n" +
                "  'connector' = 'kafka',-- 数据源\n" +
                "  'topic' = 'words', -- topic\n" +
                "  'properties.bootstrap.servers' = 'master:9092,node1:9092,node2:9092', -- kafka节点\n" +
                "  'properties.group.id' = 'testGroup', -- 消费者组\n" +
                "  'scan.startup.mode' = 'latest-offset', -- 读取数据的位置\n" +
                "  'format' = 'csv' -- 数据格式\n" +
                ")");

        /*
         * 创建sink表
         */

        tEnv.executeSql("CREATE TABLE print_table (\n" +
                " word STRING,\n" +
                " num BIGINT\n" +
                ") WITH (\n" +
                " 'connector' = 'print'\n" +
                ")");

        /*
         * 执行连续查询，将结果保存到sink表中
         */
        tEnv.executeSql("insert into print_table\n" +
                "select \n" +
                "word,\n" +
                "count(1) as num\n" +
                "from \n" +
                "words\n" +
                "group by word");
    }
}
