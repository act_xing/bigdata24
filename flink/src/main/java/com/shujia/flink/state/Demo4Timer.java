package com.shujia.flink.state;

import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

public class Demo4Timer {
    public static void main(String[] args) throws Exception{
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStream<String> words = env.socketTextStream("master", 8888);

        KeyedStream<String, String> keyBys = words.keyBy(word -> word);

        keyBys.process(new KeyedProcessFunction<String, String, Object>() {
            @Override
            public void processElement(String value,
                                       KeyedProcessFunction<String, String, Object>.Context ctx,
                                       Collector<Object> out) throws Exception {


                //获取当前的处理时间
                long currentProcessingTime = ctx.timerService().currentProcessingTime();
                System.out.println("注册定时器");
                //注册定时器，在5秒之后触发
                ctx.timerService().registerProcessingTimeTimer(currentProcessingTime + 5000);
            }


            /**定时器时间到了的时候会触发执行
             *
             * @param timestamp 触发时间
             * @param ctx 上下文对象,可以活当前key,定时器对象
             * @param out 用于将数据发送到下游
             * @throws Exception
             */
            @Override
            public void onTimer(long timestamp,
                                KeyedProcessFunction<String, String, Object>.OnTimerContext ctx,
                                Collector<Object> out) throws Exception {
                System.out.println("定时器被粗发");

            }
        });

        env.execute();
    }
}
