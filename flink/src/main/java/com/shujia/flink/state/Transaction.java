package com.shujia.flink.state;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Transaction {
    private String id;
    private Double amount;
    private String dateStr;
}
