package com.shujia.flink.state;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;

import java.util.HashMap;

public class Demo1Process {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStream<String> lines = env.socketTextStream("master", 8888);

        /*
         * process算子
         * 1、在普通的DataStream后面使用，可以代替map,flatmap ,filter的功能
         * 2、在keyBy之后使用，可以操作flink底层的数据，时间。和状态
         *
         */

        DataStream<Tuple2<String, Integer>> words = lines
                .process(new ProcessFunction<String, Tuple2<String, Integer>>() {
                    /**
                     * processElement方法每一条数据执行一次，进入一条，可以输出任意条数据
                     * @param line 一行数据
                     * @param ctx 上下文对象,可以获取时间属性
                     * @param out 用于将数据发生到下游
                     */
                    @Override
                    public void processElement(String line,
                                               ProcessFunction<String, Tuple2<String, Integer>>.Context ctx,
                                               Collector<Tuple2<String, Integer>> out) {
                        for (String word : line.split(",")) {
                            out.collect(Tuple2.of(word, 1));
                        }
                    }
                });

        //分钟
        KeyedStream<Tuple2<String, Integer>, String> keyBys = words.keyBy(kv -> kv.f0);

        DataStream<Tuple2<String, Integer>> counts = keyBys
                .process(new KeyedProcessFunction<String, Tuple2<String, Integer>, Tuple2<String, Integer>>() {


                    //open方法每一个task执行一次，用于初始化
                    @Override
                    public void open(Configuration parameters)  {
                        System.out.println("open");
                    }

                    /**
                     * 在编写flink代码时，不能使用java中的数据结构（基本数据类型，HashMao，ArrayList,HashSet）来保存中间结果
                     */
                    //用于保存单词的数量
                    //同一个task中的数据共享同一个成员变量
                    //Integer count = 0;

                    //使用hashmap保存单词的数量，为每一个单词都保存一个数量
                    //hashmap数据在java内存中，不会被checkpoint持久化到hdfs中，任务失败重启会丢失
                    final HashMap<String, Integer> hashMap = new HashMap<>();

                    /**
                     * processElement方法每一行数据执行一次，可以输出多行
                     * @param kv 一行数据
                     * @param ctx 上下文对象
                     * @param out 用于将数据发生到下游
                     */
                    @Override
                    public void processElement(Tuple2<String, Integer> kv,
                                               KeyedProcessFunction<String, Tuple2<String, Integer>, Tuple2<String, Integer>>.Context ctx,
                                               Collector<Tuple2<String, Integer>> out) {

                        String word = kv.f0;

                        //从hashmap中获取单词的数量，第一次获取不到
                        Integer count = hashMap.getOrDefault(word, 0);

                        //统计单词数量
                        count += 1;

                        //将结果发送到下游
                        out.collect(Tuple2.of(word, count));

                        //将新的单词的数量保存到hashmap中
                        hashMap.put(word, count);
                    }
                });

        counts.print();

        env.execute();


    }
}
