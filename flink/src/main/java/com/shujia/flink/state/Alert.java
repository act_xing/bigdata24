package com.shujia.flink.state;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Alert {
    private String id;
    private Double minAmount;
    private Double maxAmount;
    private String minDateStr;
    private String maxDateStr;
}
