package com.shujia.flink.state;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

public class Demo3Fraud {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //1、从kafka中读取数据
        /*
         * 创建kafka source --- 无界流
         */
        KafkaSource<String> source = KafkaSource.<String>builder()
                //broker 列表
                .setBootstrapServers("master:9092,node1:9092,node2:9092")
                //指定topic
                .setTopics("transaction")
                //指定消费者组
                .setGroupId("my-group")
                //指定读取数据定的位置
                //.setStartingOffsets(OffsetsInitializer.earliest())
                //读取最新的数据
                .setStartingOffsets(OffsetsInitializer.latest())
                //指定反序列化方式
                .setValueOnlyDeserializer(new SimpleStringSchema())
                .build();

        //使用kafka source
        DataStream<String> lines = env
                .fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source");


        //2、解析数据
        DataStream<Transaction> transactions = lines.map(line -> {
            String[] split = line.split(",");
            String id = split[0];
            double amount = Double.parseDouble(split[1]);
            String dateStr = split[2];
            return new Transaction(id, amount, dateStr);
        });



        /*
         * 于一个账户，如果出现小于 $1 美元的交易后紧跟着一个大于 $500 的交易，就输出一个报警信息。
         */
        //安装账户id分组
        KeyedStream<Transaction, String> keyBys = transactions.keyBy(Transaction::getId);

        DataStream<Alert> alerts = keyBys
                .process(new KeyedProcessFunction<String, Transaction, Alert>() {

                    //用于保存前一条数据是否小于1的状态
                    ValueState<Boolean> flagState;

                    //用于保存上一条数据的状态
                    ValueState<Transaction> lastTransactionState;

                    @Override
                    public void open(Configuration parameters) {
                        RuntimeContext context = getRuntimeContext();
                        //创建状态描述对象
                        ValueStateDescriptor<Boolean> stateDescriptor = new ValueStateDescriptor<>("flag", Types.BOOLEAN);
                        //初始化状态
                        flagState = context.getState(stateDescriptor);


                        //创建状态描述对象
                        ValueStateDescriptor<Transaction> lastTransactionDesc = new ValueStateDescriptor<>("lastTransaction", Transaction.class);
                        //初始化状态
                        lastTransactionState = context.getState(lastTransactionDesc);
                    }

                    /**
                     * processElement方法每一行数据执行一次
                     * @param transaction 一行数据
                     * @param ctx 上下文对象
                     * @param out 用于将数据发送下游
                     */


                    @Override
                    public void processElement(Transaction transaction,
                                               KeyedProcessFunction<String, Transaction, Alert>.Context ctx,
                                               Collector<Alert> out) throws Exception {

                        //从状态中获取flag值
                        Boolean flag = flagState.value();
                        if (flag == null) {
                            flag = false;
                        }

                        if (flag) {
                            if (transaction.getAmount() > 500.0) {
                                //发出告警信息
                                System.out.println("发出告警信息");
                                //从状态中获取上一条数据
                                Transaction lastTransaction = lastTransactionState.value();
                                String id = transaction.getId();
                                Double minAmount = lastTransaction.getAmount();
                                Double maxAmount = transaction.getAmount();
                                String minDateStr = lastTransaction.getDateStr();
                                String maxDateStr = transaction.getDateStr();

                                Alert alert = new Alert(id, minAmount, maxAmount, minDateStr, maxDateStr);
                                //将数据发生到下游
                                out.collect(alert);
                            }
                            //g更新状态
                            flagState.update(false);
                            lastTransactionState.clear();
                        }
                        //判断交易金额是否小于1
                        if (transaction.getAmount() < 1.0) {
                            System.out.println("交易金额小于1");
                            //更新状态
                            flagState.update(true);
                            //将小于1的数据保存到状态中
                            lastTransactionState.update(transaction);
                        }
                    }
                });


        alerts.print();

        env.execute();


    }
}

