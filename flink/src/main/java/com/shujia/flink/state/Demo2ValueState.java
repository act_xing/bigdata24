package com.shujia.flink.state;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

public class Demo2ValueState {
    public static void main(String[] args) throws Exception {
        /*
         * valueState:单值状态，为每一个key在状态中保存一个值
         *
         */

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        /*
         * 创建kafka source --- 无界流
         */
        KafkaSource<String> source = KafkaSource.<String>builder()
                //broker 列表
                .setBootstrapServers("master:9092,node1:9092,node2:9092")
                //指定topic
                .setTopics("words")
                //指定消费者组
                .setGroupId("my-group")
                //指定读取数据定的位置
                //.setStartingOffsets(OffsetsInitializer.earliest())
                //读取最新的数据
                .setStartingOffsets(OffsetsInitializer.latest())
                //指定反序列化方式
                .setValueOnlyDeserializer(new SimpleStringSchema())
                .build();

        //使用kafka source
        DataStream<String> words = env
                .fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source");

        //安装单词分组
        KeyedStream<String, String> keyBys = words.keyBy(word -> word);

        DataStream<Tuple2<String, Integer>> counts = keyBys
                .process(new KeyedProcessFunction<String, String, Tuple2<String, Integer>>() {

                    /**
                     * 状态：之前的结果
                     * valueState: 单值状态，为每一个key保存一个值
                     * 使用flink提供的状态保存中间结果，不会丢失，因为状态会被checkpoint定时持久化到hdfs
                     */
                    ValueState<Integer> valueState;

                    //open方每一个task执行一次，用于初始化（初始化状态）
                    @Override
                    public void open(Configuration parameters) {
                        //获取flink运行环境对象,初始化状态需要使用上下文对象
                        RuntimeContext context = getRuntimeContext();
                        //状态描述对象
                        ValueStateDescriptor<Integer> valueStateDescriptor = new ValueStateDescriptor<>("count", Types.INT);
                        //初始化状态
                        valueState = context.getState(valueStateDescriptor);
                    }

                    /**
                     * processElement方法每一行数据执行一次，可以输出多行
                     * @param word 一行数据
                     * @param ctx 上下文对象
                     * @param out 用于将数据发送下游
                     */
                    @Override
                    public void processElement(String word,
                                               KeyedProcessFunction<String, String, Tuple2<String, Integer>>.Context ctx,
                                               Collector<Tuple2<String, Integer>> out) throws Exception {
                        System.out.println(valueState.value());

                        //从状态中获取之前的结果
                        //如果状态中没哟值，会返回null
                        Integer count = valueState.value();
                        //如果count等于null,需要给一个默认值
                        if (count == null) {
                            count = 0;
                        }
                        //累加计算单词的数量
                        count += 1;

                        //将计算结果发送到下游
                        out.collect(Tuple2.of(word, count));

                        //更新状态
                        valueState.update(count);
                    }
                });

        counts.print();

        env.execute();
    }
}
