package com.shujia.flink.tf;

import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.*;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.SlidingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;

public class Demo6Window {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        /*
         * 每隔10秒统计最近1分钟单词的数量  --- 滑动窗口
         */

        DataStream<String> words = env.socketTextStream("master", 8888);

        //转换成kv
        DataStream<Tuple2<String, Integer>> kvs = words
                .map(word -> Tuple2.of(word, 1), Types.TUPLE(Types.STRING, Types.INT));

        //分组
        KeyedStream<Tuple2<String, Integer>, String> keyBys = kvs.keyBy(kv -> kv.f0);

        //SlidingProcessingTimeWindows: 滑动的处理时间窗口
        //将相同的key分到同一个窗口中
        WindowedStream<Tuple2<String, Integer>, String, TimeWindow> windows = keyBys
                .window(SlidingProcessingTimeWindows.of(Time.minutes(1), Time.seconds(10)));

        //统计单词的数量
        DataStream<Tuple2<String, Integer>> counts = windows.sum(1);


        counts.print();

        env.execute();
    }
}
