package com.shujia.flink.tf;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.connector.file.src.FileSource;
import org.apache.flink.connector.file.src.reader.TextLineInputFormat;
import org.apache.flink.core.fs.Path;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Demo3Filter {
    public static void main(String[] args) throws Exception{
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();


        FileSource<String> fileSource = FileSource
                .forRecordStreamFormat(new TextLineInputFormat(), new Path("flink/data/students.txt"))
                .build();

        //读取文件
        DataStream<String> students = env.fromSource(fileSource, WatermarkStrategy.noWatermarks(), "fileSource");


        //取出文科一班的学生
        DataStream<String> filterStudent = students.filter(new FilterFunction<String>() {
            /**
             * filter方法，每一条数据执行一次，如果汉法返回true保留数据，返回false过滤数据
             * @param line 一行数据
             * @return 返回值
             */
            @Override
            public boolean filter(String line)  {
                String clazz = line.split(",")[4];
                return "文科一班".equals(clazz);
            }
        });


        //lambda表达式语法
        DataStream<String> lambdaStudent = students
                .filter(line -> "文科二班".equals(line.split(",")[4]));

        lambdaStudent.print();

        env.execute();

    }
}
