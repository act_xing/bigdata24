package com.shujia.flink.tf;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class KeyValue {
    private String word;
    private Integer num;
}