package com.shujia.flink.tf;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.connector.file.src.FileSource;
import org.apache.flink.connector.file.src.reader.TextLineInputFormat;
import org.apache.flink.core.fs.Path;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Demo4KeyBy {
    public static void main(String[] args) throws Exception{
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        FileSource<String> fileSource = FileSource
                .forRecordStreamFormat(new TextLineInputFormat(), new Path("flink/data/students.txt"))
                .build();

        //读取文件
        DataStream<String> lines = env.fromSource(fileSource, WatermarkStrategy.noWatermarks(), "fileSource");


        //转换成自定义类的对象
        DataStream<Student> students = lines.map(line -> {
            String[] split = line.split(",");
            Integer id = Integer.parseInt(split[0]);
            String name = split[1];
            Integer age = Integer.parseInt(split[2]);
            String gender = split[3];
            String clazz = split[4];
            return new Student(id, name, age, gender, clazz);
        });

        //keyBy: 将相同的key发送到同一个task中，keyBy一般需要和聚合函数一起使用
        //Student::getClazz lambda表达式的一种简写
        KeyedStream<Student, String> keyBy = students.keyBy(Student::getClazz);

        keyBy.print();

        env.execute();
    }
}
