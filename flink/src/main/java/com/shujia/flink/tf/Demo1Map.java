package com.shujia.flink.tf;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Demo1Map {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStreamSource<String> lines = env.socketTextStream("master", 8888);


        //使用匿名内部类的方式
        DataStream<Student> students = lines
                .map(new MapFunction<String, Student>() {
                    /**
                     * map汉法，每一条数据执行一次
                     * @param line 一行数据
                     * @return 返回一个新的数据
                     */
                    @Override
                    public Student map(String line) {
                        String[] split = line.split(",");
                        Integer id = Integer.parseInt(split[0]);
                        String name = split[1];
                        Integer age = Integer.parseInt(split[2]);
                        String gender = split[3];
                        String clazz = split[4];
                        return new Student(id, name, age, gender, clazz);
                    }
                });

        //使用lambda表达式的方式
        DataStream<Student> lambdaStudents = lines.map(line -> {
            String[] split = line.split(",");
            Integer id = Integer.parseInt(split[0]);
            String name = split[1];
            Integer age = Integer.parseInt(split[2]);
            String gender = split[3];
            String clazz = split[4];
            return new Student(id, name, age, gender, clazz);
        });

        lambdaStudents.print();


        env.execute();

    }

}

