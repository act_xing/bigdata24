package com.shujia.flink.tf;

import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
public class Demo2FlatMap {
    public static void main(String[] args)throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStreamSource<String> lines = env.socketTextStream("master", 8888);

        //flatMap: 一行转换成多行
        DataStream<String> words = lines.flatMap((line, out) -> {
            //切分数据，循环将数据发生到下游
            for (String word : line.split(",")) {
                out.collect(word);
            }
        }, Types.STRING);

        words.print();

        env.execute();
    }
}
