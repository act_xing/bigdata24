package com.shujia.flink.tf;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Demo5Reduce {
    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //读取数据
        DataStream<String> words = env.socketTextStream("master", 8888);

        //转换成kv格式
        DataStream<KeyValue> keyValues = words.map(word -> new KeyValue(word, 1));

        //安装单词分组
        KeyedStream<KeyValue, String> keyBys = keyValues.keyBy(KeyValue::getWord);

        //统计单词的数量
        DataStream<KeyValue> wordCount = keyBys.reduce(new ReduceFunction<KeyValue>() {
            /**
             * reduce方法，每一条数据执行一次，第一条数据不执行
             * @param kv1：之前的计算结果（状态）
             * @param kv2：新的单词
             */
            @Override
            public KeyValue reduce(KeyValue kv1, KeyValue kv2) throws Exception {
                System.out.println("kv1:" + kv1);
                System.out.println("kv2:" + kv2);
                String word = kv1.getWord();
                //计算新的单纯的数量
                int num = kv1.getNum() + kv2.getNum();
                return new KeyValue(word, num);
            }
        });


        //简写
        DataStream<KeyValue> lambdaCount = keyBys
                .reduce((kv1, kv2) -> new KeyValue(kv1.getWord(), kv1.getNum() + kv2.getNum()));


        lambdaCount.print();


        env.execute();

    }
}

