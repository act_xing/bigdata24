package com.shujia.flink.core;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

public class Demo1WordCount {
    public static void main(String[] args) throws Exception {
        //1、创建flink执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //设置任务的并行度
        env.setParallelism(2);

        //设置数据从上游发生到下游的延迟事件
        env.setBufferTimeout(200);


        //2、读取数据，构建DataStream
        //启动socket nc -lk 8888
        DataStream<String> lines = env.socketTextStream("master", 8888);

        //3、统计单词的数量
        //一行转成多行
        DataStream<String> words = lines.flatMap(new FlatMapFunction<String, String>() {
            /**
             * flatMap方法用于处理数据，每一条数据执行一次
             * @param line: 一行数据
             * @param out：用于将数据发生到下游，可以发生多条
             */
            @Override
            public void flatMap(String line, Collector<String> out) {
                //将一行数据切分成数组，循环发送下游
                String[] split = line.split(",");
                for (String word : split) {
                    //发送数据到下游
                    out.collect(word);
                }
            }
        });


        //转换成kv格式
        DataStream<Tuple2<String, Integer>> kvDataStream = words.map(new MapFunction<String, Tuple2<String, Integer>>() {
            /**
             * map每一条数据执行一次
             * @param word：一行数据
             * @return 新的数据
             */
            @Override
            public Tuple2<String, Integer> map(String word) {
                return Tuple2.of(word, 1);
            }
        });

        //按照单词分组
        //kv -> kv.f0  lambda表达式
        KeyedStream<Tuple2<String, Integer>, String> keyByDataStream = kvDataStream.keyBy(kv -> kv.f0);


        //统计单词的数量
        DataStream<Tuple2<String, Integer>> countDataStream = keyByDataStream.sum(1);

        //打印输出
        countDataStream.print();

        //启动flink
        env.execute();
    }
}
