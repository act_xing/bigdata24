package com.shujia.flink.core;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;

import java.time.Duration;

public class Demo7EventTime {
    public static void main(String[] args) throws Exception {

        /*
         * java,1691114102000
         * java,1691114101000
         * java,1691114105000
         * java,1691114103000
         * java,1691114108000
         * java,1691114109000
         * java,1691114110000
         * java,1691114115000
         *
         */
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        /*
         * 设置并行度
         * 当任务有多个并行度时，下游task接收到上游task发送的多个水位线时，会取最小值
         */
        env.setParallelism(1);

        DataStream<String> lines = env.socketTextStream("master", 8888);

        //解析数据
        DataStream<Tuple2<String, Long>> events = lines.map(line -> {
            String[] split = line.split(",");
            String word = split[0];
            long ts = Long.parseLong(split[1]);
            return Tuple2.of(word, ts);
        }, Types.TUPLE(Types.STRING, Types.LONG));

        /*
         * 需要告诉flink哪一个字段是时间字段
         */

        /*
         * 事件时间：数据中自带的一个时间字段，用事件时间进行计算可以反应数据真实发生的状态
         * 有数据从采集到传输过程中可能会乱序，所以使用事件时间可能会丢失数据，可以将水位线前移，解决数据丢失，
         */

        WatermarkStrategy<Tuple2<String, Long>> watermarkStrategy = WatermarkStrategy
                //java,1691114102000m,如果数据乱序可能还会丢失数据
                //设置水位线生成策略，水位线等于最新一条数据的时间戳，水位线只能增加
                //.<Tuple2<String, Long>>forMonotonousTimestamps()

                //数据之间存在最大固定延迟的时间戳分配器
                //水位线前移时间
                .<Tuple2<String, Long>>forBoundedOutOfOrderness(Duration.ofSeconds(5))
                //指定时间字段
                .withTimestampAssigner((event, ts) -> event.f1);

        //指定时间字段和水位线
        SingleOutputStreamOperator<Tuple2<String, Long>> ass = events
                .assignTimestampsAndWatermarks(watermarkStrategy);


        /**
         * 每隔5秒统计单纯的数量
         *
         * TumblingEventTimeWindows: 滚动的事件时间窗口
         */

        ass
                //转换成kv格式
                .map(event -> Tuple2.of(event.f0, 1), Types.TUPLE(Types.STRING, Types.INT))
                //安装单词分组
                .keyBy(kv -> kv.f0)
                //每个5秒一个窗口
                .window(TumblingEventTimeWindows.of(Time.seconds(5)))
                //统计单词的数量
                .sum(1)
                .print();


        env.execute();

    }
}
