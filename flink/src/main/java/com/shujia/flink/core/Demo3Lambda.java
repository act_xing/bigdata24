package com.shujia.flink.core;

import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Demo3Lambda {
    public static void main(String[] args) throws Exception {
        //1、创建flink执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //2、读取数据，构建DataStream
        //启动socket nc -lk 8888
        DataStream<String> lines = env.socketTextStream("master", 8888);

        /*
         * 使用lambda表达式需要增加返回的类型
         * Types.STRING
         * Types.TUPLE(Types.STRING, Types.INT)
         */

        //一行转换成多行
        DataStream<String> words = lines.flatMap((line, out) -> {
            //切分数据，循环将数据发生到下游
            for (String word : line.split(",")) {
                out.collect(word);
            }
        }, Types.STRING);

        //转换成kv
        DataStream<Tuple2<String, Integer>> kvDataStream = words.map(word -> Tuple2.of(word, 1), Types.TUPLE(Types.STRING, Types.INT));

        //分组
        KeyedStream<Tuple2<String, Integer>, String> keyByDataStream = kvDataStream.keyBy(kv -> kv.f0);

        DataStream<Tuple2<String, Integer>> countDataStream = keyByDataStream.sum(1);

        countDataStream.print();

        //启动flink
        env.execute();
    }
}
