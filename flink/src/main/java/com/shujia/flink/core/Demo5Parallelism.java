package com.shujia.flink.core;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

public class Demo5Parallelism {
    public static void main(String[] args) throws Exception{

        /*
         * 并行度
         * 1、并行度决定了flink任务的吞吐量（美妙处理的数据量）
         * 2、一个并行度需要一个slot
         *
         * 并行度设置方式
         *  1、每一个算子单独设置
         *  2、代码中设置env.setParallelism(2);
         *  3、提交任务设置 -p  （推荐使用）
         *  4、配置文件中设置默认并行度
         * 优先级从上往下降低
         *
         * 指定并行度策略 -- 取决于数据量
         * 1、非聚合计算  , 单并行吞吐量在10000-20000/s
         * 2、聚合计算（有状态，有shuffle），单并行吞吐量在5000-10000/s
         *
         */

        //1、创建flink执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();


        //2、读取数据，构建DataStream
        //启动socket nc -lk 8888
        DataStream<String> lines = env.socketTextStream("master", 8888);

        //3、统计单词的数量
        //一行转成多行
        DataStream<String> words = lines.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public void flatMap(String line, Collector<String> out) {
                //将一行数据切分成数组，循环发送下游
                String[] split = line.split(",");
                for (String word : split) {
                    //发送数据到下游
                    out.collect(word);
                }
            }
        }).setParallelism(2);

        //转换成kv格式
        DataStream<Tuple2<String, Integer>> kvDataStream = words.map(new MapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public Tuple2<String, Integer> map(String word) {
                return Tuple2.of(word, 1);
            }
        });

        //按照单词分组
        //kv -> kv.f0  lambda表达式
        KeyedStream<Tuple2<String, Integer>, String> keyByDataStream = kvDataStream.keyBy(kv -> kv.f0);


        //统计单词的数量
        DataStream<Tuple2<String, Integer>> countDataStream = keyByDataStream.sum(1);

        //打印输出
        countDataStream.print();

        //启动flink
        env.execute();
    }
}
