package com.shujia.flink.core;

import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.SlidingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;

public class Demo6ProcessTime {
    public static void main(String[] args) throws Exception {

        /*
         * 处理时间，数据被flink算子处理的时间，现实时间往后推移flink就会计算
         *
         */

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        /*
         * 每隔10秒统计单词的数量  ---滚动窗口
         */

        DataStream<String> words = env.socketTextStream("master", 8888);

        //转换成kv
        DataStream<Tuple2<String, Integer>> kvs = words
                .map(word -> Tuple2.of(word, 1), Types.TUPLE(Types.STRING, Types.INT));

        //分组
        KeyedStream<Tuple2<String, Integer>, String> keyBys = kvs.keyBy(kv -> kv.f0);

        //TumblingProcessingTimeWindows: 滚动的处理时间窗口
        //将相同的key分到同一个窗口中
        WindowedStream<Tuple2<String, Integer>, String, TimeWindow> windows = keyBys
                .window(TumblingProcessingTimeWindows.of(Time.seconds(10)));

        //统计单词的数量
        DataStream<Tuple2<String, Integer>> counts = windows.sum(1);

        counts.print();

        env.execute();
    }
}
