package com.shujia.flink.core;

import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

public class Demo2WordCountBatch {
    public static void main(String[] args) throws Exception{

        //1、创建flink的环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        /*
         * 修改flink的处理模式
         * BATCH： 批处理
         * 1、输出最终结果
         * 2、只能用于处理有界流，不能用于处理无界流
         * 3、批处理模式底层是mapreduce模型
         *
         * STREAMING： 流处理模式
         * 1、输出连续结果，每一条数据都会输出一个结果
         * 2、可以处理有界流，也可以处理无界流
         * 3、底层是持续流模型
         *
         *
         * flink流批统一
         * 1、同一个代码既能用于批处理，也能用于流处理
         */

        env.setRuntimeMode(RuntimeExecutionMode.BATCH);

        //2、读取文件 --- 有界流（有开始有结束）
        DataStream<String> lines = env.readTextFile("flink/data/words.txt");

        //3、统计单词的数量
        //一行转成多行
        DataStream<String> words = lines.flatMap(new FlatMapFunction<String, String>() {
            /**
             * flatMap方法用于处理数据，每一条数据执行一次
             * @param line: 一行数据
             * @param out：用于将数据发生到下游，可以发生多条
             */
            @Override
            public void flatMap(String line, Collector<String> out) {
                //将一行数据切分成数组，循环发送下游
                String[] split = line.split(",");
                for (String word : split) {
                    //发送数据到下游
                    out.collect(word);
                }
            }
        });

        //转换成kv格式
        DataStream<Tuple2<String, Integer>> kvDataStream = words.map(new MapFunction<String, Tuple2<String, Integer>>() {
            /**
             * map每一条数据执行一次
             * @param word：一行数据
             * @return 新的数据
             */
            @Override
            public Tuple2<String, Integer> map(String word) {
                return Tuple2.of(word, 1);
            }
        });

        //按照单词分组
        //kv -> kv.f0  lambda表达式
        KeyedStream<Tuple2<String, Integer>, String> keyByDataStream = kvDataStream.keyBy(kv -> kv.f0);


        //统计单词的数量
        DataStream<Tuple2<String, Integer>> countDataStream = keyByDataStream.sum(1);

        //打印输出
        countDataStream.print();


        env.execute();
    }
}
