package com.shujia.flink.window;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.EventTimeSessionWindows;
import org.apache.flink.streaming.api.windowing.time.Time;

import java.time.Duration;

public class Demo3EventTimeSessionWindow {
    public static void main(String[] args) throws Exception {

        /*
         * java,1691114102000
         * java,1691114101000
         * java,1691114105000
         * java,1691114103000
         * java,1691114108000
         * java,1691114115000
         * java,1691114120000
         */

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        DataStream<String> lines = env.socketTextStream("master", 8888);

        //解析数据
        DataStream<Event> events = lines.map(line -> {
            String[] split = line.split(",");
            String word = split[0];
            long ts = Long.parseLong(split[1]);
            return new Event(word, ts);
        });

        //指定时间字段和水位线生成策略
        DataStream<Event> assDataStream = events.assignTimestampsAndWatermarks(
                WatermarkStrategy
                        //水位线生成策略
                        .<Event>forBoundedOutOfOrderness(Duration.ofSeconds(5))
                        //事件字段
                        .withTimestampAssigner((event, ts) -> event.getTs())
        );



        /*
         * 会话窗口，一段时间没有数据开始计算前面的数据
         * 1、ProcessingTimeSessionWindows：处理时间的会话窗口
         * 2、EventTimeSessionWindows：事件时间的会话窗口
         */
        assDataStream
                //转换成kv
                .map(event -> Tuple2.of(event.getWord(), 1), Types.TUPLE(Types.STRING, Types.INT))
                .keyBy(kv -> kv.f0)
                .window(EventTimeSessionWindows.withGap(Time.seconds(5)))
                .sum(1)
                .print();

        env.execute();

    }
}
