package com.shujia.flink.window;

import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Demo4CountWindow {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStreamSource<String> lines = env.socketTextStream("master", 8888);

        DataStream<Tuple2<String, Integer>> kvDataStream = lines
                .map(word -> Tuple2.of(word, 1), Types.TUPLE(Types.STRING, Types.INT));

        /*
         * 统计窗口
         *  countWindow(10)： 滚动的统计窗口
         * .countWindow(10, 2): 滑动的统计窗口
         */
        kvDataStream
                .keyBy(kv -> kv.f0)
                //每隔两条数据计算最近10条数据
                .countWindow(10, 2)
                .sum(1)
                .print();

        env.execute();

    }
}
