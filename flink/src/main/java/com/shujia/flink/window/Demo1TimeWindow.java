package com.shujia.flink.window;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;

import java.time.Duration;

public class Demo1TimeWindow {
    public static void main(String[] args) throws Exception {

        /*
         * java,1691114102000
         * java,1691114101000
         * java,1691114105000
         * java,1691114103000
         * java,1691114108000
         * java,1691114109000
         * java,1691114110000
         * java,1691114115000
         */

        /*
         * 事件窗口
         * 1、SlidingEventTimeWindows：滑动的事件时间窗口
         * 2、SlidingProcessingTimeWindows：滑动的处理时间窗口
         * 3、TumblingEventTimeWindows：滚动的事件时间窗口
         * 4、TumblingProcessingTimeWindows：滚动的处理时间窗口
         *
         */

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        DataStream<String> lines = env.socketTextStream("master", 8888);

        //解析数据
        DataStream<Event> events = lines.map(line -> {
            String[] split = line.split(",");
            String word = split[0];
            long ts = Long.parseLong(split[1]);
            return new Event(word, ts);
        });

        //指定时间字段和水位线生成策略
        DataStream<Event> assDataStream = events.assignTimestampsAndWatermarks(
                WatermarkStrategy
                        //水位线生成策略
                        .<Event>forBoundedOutOfOrderness(Duration.ofSeconds(5))
                        //事件字段
                        .withTimestampAssigner((event, ts) -> event.getTs())
        );


        assDataStream
                //转换成kv
                .map(event -> Tuple2.of(event.getWord(), 1), Types.TUPLE(Types.STRING, Types.INT))
                .keyBy(kv -> kv.f0)
                //每隔4秒统计最近10秒的数据
                .window(SlidingEventTimeWindows.of(Time.seconds(10), Time.seconds(5)))
                .sum(1)
                .print();

        env.execute();

    }
}

@Data
@AllArgsConstructor
class Event {
    private String word;
    private Long ts;
}