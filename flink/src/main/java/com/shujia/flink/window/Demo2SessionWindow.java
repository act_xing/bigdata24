package com.shujia.flink.window;

import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.ProcessingTimeSessionWindows;
import org.apache.flink.streaming.api.windowing.time.Time;

public class Demo2SessionWindow {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStreamSource<String> lines = env.socketTextStream("master", 8888);

        DataStream<Tuple2<String, Integer>> kvDataStream = lines
                .map(word -> Tuple2.of(word, 1), Types.TUPLE(Types.STRING, Types.INT));


        /*
         * 会话窗口，一段时间没有数据开始计算前面的数据
         * 1、ProcessingTimeSessionWindows：处理时间的会话窗口
         * 2、EventTimeSessionWindows：事件时间的会话窗口
         */
        kvDataStream
                .keyBy(kv -> kv.f0)
                //5秒没有数据开始计算，。以key为单位的
                .window(ProcessingTimeSessionWindows.withGap(Time.seconds(5)))
                .sum(1)
                .print();


        env.execute();

    }
}
