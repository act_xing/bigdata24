package com.shujia.flink.window;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.annotation.JSONField;
import com.shujia.flink.util.DateUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.connector.file.src.FileSource;
import org.apache.flink.connector.file.src.reader.TextLineInputFormat;
import org.apache.flink.core.fs.Path;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.time.Duration;

/**
 * fastjson 用于解析json格式工具
 * SimpleDateFormat(DateUtil): 用于时间格式转换，可以将时间字符串转换成时间戳，将时间戳转换成时间字符串
 * window apply : 用于处理窗口内所有的数据，以迭代器的形式传递给apply
 *
 * 需求
 * 1、实时统计卡口的拥堵情况
 * 2、计算最近15分钟卡口的车流量和平均车速，每隔一分钟计算一次
 * 3、需要使用事件时间的滑动窗口
 *
 * 思路
 * 1、读取数据
 * 2、数据是json格式，解析数据，使用fastjson解析数据
 * 3、设置水位线和时间字段，需要将时间字符串转换成毫秒级别的时间戳
 * 4、安装道路分组
 * 5、划分窗口
 * 6、计算窗口内的平均车速和车流量
 */

public class Demo5Car {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //构建file source
        FileSource<String> fileSource = FileSource
                .forRecordStreamFormat(new TextLineInputFormat(), new Path("flink/data/cars.json"))
                .build();

        //使用file Source构建DataStream
        DataStream<String> linesDataStream = env
                .fromSource(fileSource, WatermarkStrategy.noWatermarks(), "fileSource");

        //使用fatjson工具解析数据
        DataStream<Car> cars = linesDataStream
                .map(line -> JSON.parseObject(line, Car.class));

        /*
         * 实时统计道路拥堵情况
         * 1、实时计算道路车流量和平均车速，计算最近15分钟，每隔1分钟计算一次
         */

        //设置时间字段和水位线
        DataStream<Car> assCars = cars.assignTimestampsAndWatermarks(
                WatermarkStrategy
                        //设置水位线生成策略
                        .<Car>forBoundedOutOfOrderness(Duration.ofSeconds(5))
                        //指定时间字段，时间字段必须是毫秒级别的时间戳
                        .withTimestampAssigner((car, tx) -> DateUtil.dateStrToTs(car.getTimestamp()))
        );

        //安装道路分组
        KeyedStream<Car, String> keyByCars = assCars.keyBy(Car::getRoad);

        //划分窗口
        WindowedStream<Car, String, TimeWindow> windowCars = keyByCars
                .window(SlidingEventTimeWindows.of(Time.minutes(15), Time.minutes(1)));

        /*
         * apply: 用于处理窗口内所有的数据，可以进行更灵活的计算
         *
         */
        DataStream<Result> results = windowCars
                .apply(new WindowFunction<Car, Result, String, TimeWindow>() {
                    /**
                     * apply方法每一个key的每个窗口执行一次
                     * @param road 分组的key
                     * @param window 窗口对象，可以获取窗口的开始和结束时间
                     * @param cars 窗口内所有的数据
                     * @param out 用于将计算结果发送到下游
                     */
                    @Override
                    public void apply(String road,
                                      TimeWindow window,
                                      Iterable<Car> cars,
                                      Collector<Result> out) {

                        //计算车流量和平均车速
                        long flow = 0L;

                        double sumSpeed = 0.0;

                        //循环累加计算
                        for (Car car : cars) {
                            flow++;
                            sumSpeed += car.getSpeed();
                        }

                        //计算平均车速
                        double avgSpeed = sumSpeed / flow;

                        //获取窗口开始和结束时间
                        long start = window.getStart();
                        String startStr = DateUtil.tsToDateStr(start);

                        long end = window.getEnd();
                        String endStr = DateUtil.tsToDateStr(end);

                        //将计算结果发送到下游
                        out.collect(new Result(road, flow, avgSpeed, startStr, endStr));
                    }
                });

        results.print();


        env.execute();

    }
}

@Data
@AllArgsConstructor
class Car {
    //当命名规范不一致时，可以手动指定json字段名
    @JSONField(name = "tollgate_id")
    private String tollgateId;

    @JSONField(name = "license_plate")
    private String licensePlate;

    private String timestamp;
    private Double longitude;
    private Double latitude;
    private Double speed;
    private String city;
    private String road;
    private String district;
}

@Data
@AllArgsConstructor
class Result {
    private String road;
    private long flow;
    private double avgSpeed;
    private String start;
    private String end;
}