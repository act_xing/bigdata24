package com.shujia.flink.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    /**
     * 将时间字符串转换成时间戳
     *
     * @param dateStr 时间字符串
     * @return 毫秒级别时间戳
     */
    public static long dateStrToTs(String dateStr) {
        //用于时间格式转换的类
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //将时间字符串转换成时间对象
        Date data = new Date();
        try {
            data = format.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //获取时间戳
        return data.getTime();
    }

    /**
     * 将时间戳转换成时间字符串
     *
     * @param ts 时间戳
     * @return 时间字符串
     */
    public static String tsToDateStr(long ts) {
        //用于时间格式转换的类
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(ts);
    }

    public static void main(String[] args) throws Exception {
        String dateStr = "2021-03-03 04:37:46";
        long ts = dateStrToTs(dateStr);
        System.out.println(ts);

        System.out.println(tsToDateStr(1691114101000L));
    }
}
