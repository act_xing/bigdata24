package com.shujia.flink.sink;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.connector.base.DeliveryGuarantee;
import org.apache.flink.connector.file.src.FileSource;
import org.apache.flink.connector.file.src.reader.TextLineInputFormat;
import org.apache.flink.connector.kafka.sink.KafkaRecordSerializationSchema;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.core.fs.Path;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Demo5KafkaSink {
    public static void main(String[] args) throws Exception{
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        FileSource<String> fileSource = FileSource
                .forRecordStreamFormat(new TextLineInputFormat(), new Path("flink/data/students.txt"))
                .build();

        //读取文件
        DataStreamSource<String> students = env
                .fromSource(fileSource, WatermarkStrategy.noWatermarks(), "file source");



        //将结果保存到奥kafka中
        KafkaSink<String> kafkaSink = KafkaSink.<String>builder()
                //指定broker列表
                .setBootstrapServers("master:9092,node1:9092,node2:9092")
                .setRecordSerializer(KafkaRecordSerializationSchema.builder()
                        //指定topic
                        .setTopic("students")
                        .setValueSerializationSchema(new SimpleStringSchema())
                        .build()
                )
                //设置数据处理的语义
                .setDeliverGuarantee(DeliveryGuarantee.AT_LEAST_ONCE)
                .build();

        //使用kafka sink
        students.sinkTo(kafkaSink);

        env.execute();

    }
}
