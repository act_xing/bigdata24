package com.shujia.flink.sink;

import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringEncoder;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.connector.file.sink.FileSink;
import org.apache.flink.connector.file.src.FileSource;
import org.apache.flink.connector.file.src.reader.TextLineInputFormat;
import org.apache.flink.core.fs.Path;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Demo1FIleSInkBatch {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //修改处理模式
        env.setRuntimeMode(RuntimeExecutionMode.BATCH);

        FileSource<String> fileSource = FileSource
                //指定数据格式和路径
                .forRecordStreamFormat(new TextLineInputFormat(), new Path("flink/data/students.txt"))
                .build();

        //使用file sourced读取文件
        DataStream<String> students = env
                .fromSource(fileSource, WatermarkStrategy.noWatermarks(), "fileSource");

        //解析数据，取出班级
        DataStream<Tuple2<String, Integer>> kvs = students.map(stu -> {
            //取出班级
            String clazz = stu.split(",")[4];
            return Tuple2.of(clazz, 1);
        }, Types.TUPLE(Types.STRING, Types.INT));

        //统计班级的人数
        DataStream<Tuple2<String, Integer>> clazzCounts = kvs
                .keyBy(kv -> kv.f0).sum(1);

        //整理数据格式，转换成字符串
        DataStream<String> results = clazzCounts.map(kv -> {
            String clazz = kv.f0;
            Integer num = kv.f1;
            return clazz + "," + num;
        });


        //创建file sink
        FileSink<String> fileSink = FileSink
                .forRowFormat(new Path("flink/data/clazz_num"), new SimpleStringEncoder<String>("UTF-8"))
                .build();

        //将结果保存到文件
        results.sinkTo(fileSink);

        env.execute();


    }
}
