package com.shujia.flink.sink;

import org.apache.flink.api.common.serialization.SimpleStringEncoder;
import org.apache.flink.configuration.MemorySize;
import org.apache.flink.connector.file.sink.FileSink;
import org.apache.flink.core.fs.Path;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.DefaultRollingPolicy;

import java.time.Duration;

public class Demo2FIleSInkStream {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //读取数据
        DataStream<String> lines = env.socketTextStream("master", 8888);


        //创建file sink
        FileSink<String> fileSink = FileSink
                .forRowFormat(new Path("flink/data/stream"), new SimpleStringEncoder<String>("UTF-8"))
                //指定滚动策略
                .withRollingPolicy(
                        DefaultRollingPolicy.builder()
                                //包含了至少10秒的数据量
                                .withRolloverInterval(Duration.ofSeconds(10))
                                //如果10秒没有新的数据也会滚动生成新的文件
                                .withInactivityInterval(Duration.ofSeconds(10))
                                //文件大小已经达到 1MB（写入最后一条记录之后）
                                .withMaxPartSize(MemorySize.ofMebiBytes(1))
                                .build())
                .build();


        //将数据保存到文件
        lines.sinkTo(fileSink);


        env.execute();

    }
}
