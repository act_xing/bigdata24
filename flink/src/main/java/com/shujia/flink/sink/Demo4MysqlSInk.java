package com.shujia.flink.sink;

import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class Demo4MysqlSInk {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStream<String> lines = env.socketTextStream("master", 8888);

        //一行转换成多行，返回kv
        DataStream<Tuple2<String, Integer>> kvs = lines
                .flatMap((line, out) -> {
                    for (String word : line.split(",")) {
                        out.collect(Tuple2.of(word, 1));
                    }
                }, Types.TUPLE(Types.STRING, Types.INT));

        //统计单词的数量
        DataStream<Tuple2<String, Integer>> wordCount = kvs
                .keyBy(kv -> kv.f0)
                .sum(1);

        //将计算结果保存到数据库中
        wordCount.addSink(new MysqlSink());

        env.execute();

    }
}

/**
 * RichSinkFunction比SinkFunction多了open和close方法，用于打开和关闭连接
 */
class MysqlSink extends RichSinkFunction<Tuple2<String, Integer>> {

    PreparedStatement stat;
    Connection con;


    /**
     * open方法每一个task执行一次，一般用于创建数据库连接（每一个task中创建一个数据库连接），初始化工作
     */
    @Override
    public void open(Configuration parameters) throws Exception {
        //1、加载驱动
        Class.forName("com.mysql.jdbc.Driver");
        long start = System.currentTimeMillis();
        //2、创建数据库连接
        con = DriverManager.getConnection("jdbc:mysql://master:3306/student?useSSL=false", "root", "123456");
        long end = System.currentTimeMillis();
        System.out.println(end - start);
        //3、编写插入数据sql
        //replace into 替换插入，如果存在就替换，如果不存在就插入，表需要有主键
        stat = con.prepareStatement("replace into word_count values(?,?)");
    }

    /**
     *close方法任务关闭时执行，用于回收资源
     */

    @Override
    public void close() throws Exception {
        stat.close();
        con.close();
    }

    /**
     * invoke: 每一条数据执行一次，如果将创建数据库连接放在invoke中，效率会很低
     */
    @Override
    public void invoke(Tuple2<String, Integer> kv, Context context) throws Exception {
        String word = kv.f0;
        Integer num = kv.f1;
        //j将数据保存到数据库
        //4、设置字段值
        stat.setString(1, word);
        stat.setInt(2, num);
        //5、执行插入
        stat.execute();
    }
}
