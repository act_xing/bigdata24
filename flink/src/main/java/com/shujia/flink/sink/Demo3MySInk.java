package com.shujia.flink.sink;

import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;

public class Demo3MySInk {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStreamSource<String> lines = env.socketTextStream("master", 8888);

        //使用自定义sink
        lines.addSink(new MySink());

        env.execute();

    }
}

/**
 * 自定义sink，实现SinkFunction接口
 */

class MySink implements SinkFunction<String> {
    /**
     * invoke每一条数据执行一次，用于sink数据
     *
     * @param line    一行数据
     * @param context flink上下文对象
     */
    @Override
    public void invoke(String line, Context context) {
        System.out.println(line);
    }
}
