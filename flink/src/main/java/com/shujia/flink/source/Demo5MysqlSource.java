package com.shujia.flink.source;

import lombok.*;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Demo5MysqlSource {
    public static void main(String[] args) throws Exception{
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //使用自定义source 读取mysql
        DataStream<Student> students = env.addSource(new MysqlSource());

        students.print();

        env.execute();

    }
}

/**
 * 自定义source读取mysql中的数据
 * 1、在run方法中读取mysql
 * 2、先加载驱动
 * 3、创建数据库连接
 * 4、查询数据
 * 5、将数据发生到下游
 */
class MysqlSource implements SourceFunction<Student> {

    @Override
    public void run(SourceContext<Student> ctx) throws Exception {
        //1、加载驱动
        Class.forName("com.mysql.jdbc.Driver");
        //2、创建数据库连接
        Connection con = DriverManager.getConnection("jdbc:mysql://master:3306/student?useSSL=false", "root", "123456");
        //3、查询数据
        PreparedStatement stat = con.prepareStatement("select id,name,age,gender,clazz from student");
        //z执行查询
        //ResultSet 中是查询到的数据
        ResultSet resultSet = stat.executeQuery();
        //循环解析数据
        while (resultSet.next()) {
            //通过字段名获取数据
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            int age = resultSet.getInt("age");
            String gender = resultSet.getString("gender");
            String clazz = resultSet.getString("clazz");

            //将数据发送到下游
            ctx.collect(new Student(id, name, age, gender, clazz));
        }
    }

    @Override
    public void cancel() {
    }
}


/**
 * lombok作用是，在代码编译时动态给类增加新的方法
 * AllArgsConstructor
 * NoArgsConstructor
 * Setter
 * Getter
 * ToString
 *
 */
@Data
@AllArgsConstructor
class Student {
    private Integer id;
    private String name;
    private Integer age;
    private String gender;
    private String clazz;
}
