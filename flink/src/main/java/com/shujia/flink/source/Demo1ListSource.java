package com.shujia.flink.source;

import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.ArrayList;

public class Demo1ListSource {
    public static void main(String[] args) throws Exception{
        //1、创建flink执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();


        ArrayList<String> list = new ArrayList<>();
        list.add("java");
        list.add("hive");
        list.add("spark");
        list.add("flink");
        list.add("java");

        //2、基于集合构建DataStream   -- 有界流
        DataStream<String> listDataStream = env.fromCollection(list);

        listDataStream.print();

        env.execute();
    }
}
