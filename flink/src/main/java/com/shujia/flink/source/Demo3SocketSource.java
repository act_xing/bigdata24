package com.shujia.flink.source;

import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SocketTextStreamFunction;

public class Demo3SocketSource {
    public static void main(String[] args) throws Exception{
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //flink官方提供的简化写法
        //env.socketTextStream()

        //增量一个source
        DataStream<String> socketDataStream = env
                .addSource(new SocketTextStreamFunction("master", 8888, "\n", 0));

        socketDataStream.print();


        env.execute();

    }
}
