package com.shujia.flink.source;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.connector.file.src.FileSource;
import org.apache.flink.connector.file.src.reader.TextLineInputFormat;
import org.apache.flink.core.fs.Path;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.time.Duration;

public class Demo2FileSource {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();


        //老版本的api方式 -- 有界流(淘汰)
        //DataStream<String> fileDataStream= env.readTextFile("flink/data/words.txt");
        //fileDataStream.print();

        /*
         * 基于文件构建DataStream
         *
         * 1、有界流
         * 只读取一次，每一个文件对于一个task，数据处理文件任务介绍
         *
         * 2、无界流
         * monitorContinuously(Duration.ofSeconds(5))
         * flink会定时扫描目录，读取目录下新的文件，一个文件读取一次
         *
         *
         * flink流批统一
         * 1、同一个代码既能用于批处理，也能用于流处理（计算引擎的流批统一）
         * 2、flink可以基于文件做流处理也能做批处理（数据源的流批统一）
         */

        //新版file source api
        FileSource<String> fileSource = FileSource
                //指定读取文件的格式和读取文件的路径
                .forRecordStreamFormat(new TextLineInputFormat(), new Path("flink/data/words"))
                //打开持续监控目录，需要设置间隔时间。（开启无界流读取文件）
                .monitorContinuously(Duration.ofSeconds(5))
                .build();

        //使用file source
        DataStreamSource<String> fileSourceDataStream = env
                .fromSource(fileSource, WatermarkStrategy.noWatermarks(), "fileSOurce");

        fileSourceDataStream.print();

        env.execute();
    }
}
