
package com.shujia.flink.source;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.annotation.JSONField;
import com.shujia.flink.util.DateUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.time.Duration;

public class Demo7KafkaCars {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        /*
         * 创建kafka source --- 无界流
         */
        KafkaSource<String> source = KafkaSource.<String>builder()
                //broker 列表
                .setBootstrapServers("master:9092,node1:9092,node2:9092")
                //指定topic
                .setTopics("cars")
                //指定消费者组
                .setGroupId("my-group")
                //指定读取数据定的位置
                //.setStartingOffsets(OffsetsInitializer.earliest())
                //读取最新的数据
                .setStartingOffsets(OffsetsInitializer.latest())
                //指定反序列化方式
                .setValueOnlyDeserializer(new SimpleStringSchema())
                .build();

        //使用kafka source
        DataStream<String> lines = env
                .fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source");

        //解析数据
        DataStream<Car> cars = lines
                .map(line -> JSON.parseObject(line, Car.class));


        //设置事件时间和水位线
        DataStream<Car> assCars = cars.assignTimestampsAndWatermarks(
                WatermarkStrategy
                        .<Car>forBoundedOutOfOrderness(Duration.ofSeconds(5))
                        .withTimestampAssigner((car, ts) -> DateUtil.dateStrToTs(car.getTimestamp()))
        );


        //安装道路分组
        KeyedStream<Car, String> keyByCars = assCars.keyBy(Car::getRoad);

        //划分窗口
        WindowedStream<Car, String, TimeWindow> windowCars = keyByCars
                .window(SlidingEventTimeWindows.of(Time.minutes(15), Time.minutes(1)));

        /*
         * apply: 用于处理窗口内所有的数据，可以进行更灵活的计算
         *
         */
        DataStream<Result> results = windowCars
                .apply(new WindowFunction<Car, Result, String, TimeWindow>() {
                    /**
                     * apply方法每一个key的每个窗口执行一次
                     * @param road 分组的key
                     * @param window 窗口对象，可以获取窗口的开始和结束时间
                     * @param cars 窗口内所有的数据
                     * @param out 用于将计算结果发送到下游
                     */
                    @Override
                    public void apply(String road,
                                      TimeWindow window,
                                      Iterable<Car> cars,
                                      Collector<Result> out) {

                        //计算车流量和平均车速
                        long flow = 0L;

                        double sumSpeed = 0.0;

                        //循环累加计算
                        for (Car car : cars) {
                            flow++;
                            sumSpeed += car.getSpeed();
                        }

                        //计算平均车速
                        double avgSpeed = sumSpeed / flow;

                        //获取窗口开始和结束时间
                        long start = window.getStart();
                        String startStr = DateUtil.tsToDateStr(start);

                        long end = window.getEnd();
                        String endStr = DateUtil.tsToDateStr(end);

                        //将计算结果发送到下游
                        out.collect(new Result(road, flow, avgSpeed, startStr, endStr));
                    }
                });

        //将计算结果实时保存到mysql中

        results.addSink(new RichSinkFunction<Result>() {
            Connection con;
            PreparedStatement stat;

            //创建数据库连接
            @Override
            public void open(Configuration parameters) throws Exception {
                Class.forName("com.mysql.jdbc.Driver");
                con = DriverManager.getConnection("jdbc:mysql://master:3306/student?useSSL=false&useUnicode=true&characterEncoding=UTF-8", "root", "123456");
                stat = con.prepareStatement("replace into road_flow_and_speed values(?,?,?,?,?)");

            }

            //关闭连接
            @Override
            public void close() throws Exception {
                stat.close();
                con.close();
            }

            //写数据
            @Override
            public void invoke(Result result, Context context) throws Exception {
                stat.setString(1, result.getRoad());
                stat.setLong(2, result.getFlow());
                stat.setDouble(3, result.getAvgSpeed());
                stat.setString(4, result.getStart());
                stat.setString(5, result.getEnd());

                stat.execute();
            }
        });

        env.execute();


    }
}


@Data
@AllArgsConstructor
class Car {
    //当命名规范不一致时，可以手动指定json字段名
    @JSONField(name = "tollgate_id")
    private String tollgateId;

    @JSONField(name = "license_plate")
    private String licensePlate;

    private String timestamp;
    private Double longitude;
    private Double latitude;
    private Double speed;
    private String city;
    private String road;
    private String district;
}

@Data
@AllArgsConstructor
class Result {
    private String road;
    private long flow;
    private double avgSpeed;
    private String start;
    private String end;
}


