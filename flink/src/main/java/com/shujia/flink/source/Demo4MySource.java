package com.shujia.flink.source;

import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;

public class Demo4MySource {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //使用自定义source
        DataStream<Integer> myDataStream = env.addSource(new MySource());

        myDataStream.print();

        env.execute();
    }
}

/**
 * 自定义source,实现SourceFunction接口
 */
class MySource implements SourceFunction<Integer> {

    /**
     * run方法在任务启动的时候执行一次，用于读取数据，将数据发送到下游
     *
     * @param ctx：用于将数据发送到下游
     */
    @Override
    public void run(SourceContext<Integer> ctx) throws Exception {
       while (true){
           ctx.collect(100);

           Thread.sleep(1000);
       }
    }

    //用于任务在取消的时候回收资源
    @Override
    public void cancel() {
    }
}