package com.shujia.flink.source;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Demo6KafkaSource {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        /*
         * 创建kafka source --- 无界流
         */
        KafkaSource<String> source = KafkaSource.<String>builder()
                //broker 列表
                .setBootstrapServers("master:9092,node1:9092,node2:9092")
                //指定topic
                .setTopics("cars")
                //指定消费者组
                .setGroupId("my-group")
                //指定读取数据定的位置
                .setStartingOffsets(OffsetsInitializer.earliest())
                //读取最新的数据
                //.setStartingOffsets(OffsetsInitializer.latest())
                //指定反序列化方式
                .setValueOnlyDeserializer(new SimpleStringSchema())
                .build();

        //使用kafka source
        DataStream<String> cars = env
                .fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source");

        cars.print();


        env.execute();


    }
}




