package com.shujia.flink.checkpoint;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.connector.base.DeliveryGuarantee;
import org.apache.flink.connector.kafka.sink.KafkaRecordSerializationSchema;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.Properties;

public class Demo2ExactlyOnce {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        /*
         * 创建kafka source --- 无界流
         */
        KafkaSource<String> source = KafkaSource.<String>builder()
                //broker 列表
                .setBootstrapServers("master:9092,node1:9092,node2:9092")
                //指定topic
                .setTopics("exactly_words")
                //指定消费者组
                .setGroupId("my-group")
                //指定读取数据定的位置
                //.setStartingOffsets(OffsetsInitializer.earliest())
                //读取最新的数据
                .setStartingOffsets(OffsetsInitializer.latest())
                //指定反序列化方式
                .setValueOnlyDeserializer(new SimpleStringSchema())
                .build();

        //使用kafka source
        DataStream<String> words = env
                .fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source");

        DataStream<String> filterS = words.filter(word -> !"bigdata".equals(word));


        //将结果保存到kafka
        //将结果保存到奥kafka中
        Properties properties = new Properties();
        //指定事务超时时间，必须要小于15分钟
        properties.setProperty("transaction.timeout.ms", "600000");

        KafkaSink<String> kafkaSink = KafkaSink.<String>builder()
                //指定broker列表
                .setBootstrapServers("master:9092,node1:9092,node2:9092")
                //指定生产者额外的配置
                .setKafkaProducerConfig(properties)
                .setRecordSerializer(KafkaRecordSerializationSchema.builder()
                        //指定topic
                        .setTopic("filter_words")
                        .setValueSerializationSchema(new SimpleStringSchema())
                        .build()
                )
                //设置数据处理的语义
                .setDeliverGuarantee(DeliveryGuarantee.EXACTLY_ONCE)
                .build();

        filterS.sinkTo(kafkaSink);

        env.execute();
    }
}
