package com.shujia.flink.checkpoint;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Demo1Checkpoint {
    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        /*
         * 开启checkpoint。指定间隔时间
         */
/*
        env.enableCheckpointing(5000);

        //指定保存checkpoint路径
        env.getCheckpointConfig().setCheckpointStorage(new Path("hdfs://master:9000/flink/checkpoint"));

        //任务被手动取消时保留最新的一次checkpoint
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(
                CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
*/

        //创建kafka Source
        KafkaSource<String> source = KafkaSource.<String>builder()
                //broker 列表
                .setBootstrapServers("master:9092,node1:9092,node2:9092")
                //指定topic
                .setTopics("words")
                //指定消费者组
                .setGroupId("my-group")
                //指定读取数据定的位置
                .setStartingOffsets(OffsetsInitializer.earliest())
                //读取最新的数据
                //.setStartingOffsets(OffsetsInitializer.latest())
                //指定反序列化方式
                .setValueOnlyDeserializer(new SimpleStringSchema())
                .build();

        //使用kafka source
        DataStream<String> words = env
                .fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source");

        //统计单词的数量
        words.map(word -> Tuple2.of(word, 1), Types.TUPLE(Types.STRING, Types.INT))
                .keyBy(kv -> kv.f0)
                .sum(1)
                .print();


        env.execute();
    }
}
