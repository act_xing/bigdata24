package com.shujia.test;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;

public class Demo1FastJson {
    public static void main(String[] args) {
        String jsonStr = "{\"tollgate_id\": \"113\", \"license_plate\": \"湘D80357\", \"timestamp\": \"2021-03-02 00:00:18\", \"longitude\": 116.278578, \"latitude\": 40.164708, \"speed\": 91, \"city\": \"杭州市\", \"road\": \"朝阳路\", \"district\": \"天河区\"}";

        /**
         * 使用fastJson解析json字符串
         */

        //将json字符串转换成json对象，转换成之后就可以通过key获取value了
        JSONObject jsonObject = JSON.parseObject(jsonStr);

        //通过key获取value
        String license_plate = jsonObject.getString("license_plate");
        System.out.println(license_plate);

        Integer speed = jsonObject.getInteger("speed");
        System.out.println(speed);


        //将json字符串转换成java自定义类的对象
        Car car = JSON.parseObject(jsonStr, Car.class);
        System.out.println(car.getCity());
        System.out.println(car.getDistrict());


        //类的对象
        //Car car1 = new Car();

        //类对象：java class文件在内存中的一种抽象
        //反射：可以基于类对象创建类的对象
        //Class<Car> carClass = Car.class;

    }
}

@Data
@AllArgsConstructor
class Car {
    private String tollgate_id;
    private String license_plate;
    private String timestamp;
    private Double longitude;
    private Double latitude;
    private Double speed;
    private String city;
    private String road;
    private String district;
}