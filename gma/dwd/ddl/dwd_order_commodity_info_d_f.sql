drop table if exists dwd.dwd_order_commodity_info_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS dwd.dwd_order_commodity_info_d_f(
  id INT COMMENT "编号",
  order_id INT COMMENT "订单编号",
  commodity_id INT COMMENT "商品编号",
  create_time STRING COMMENT "创建时间",
  update_time STRING COMMENT "更新时间",
  commodity_num INT COMMENT "购买商品数量",
  commodity_price DECIMAL(10,2) COMMENT "购买价格"
)
partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/dwd/dwd_order_commodity_info_d_f';