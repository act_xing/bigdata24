drop table if exists dwd.dwd_shop_order_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS dwd.dwd_shop_order_d_f(
    id INT comment '编号',
    shop_id INT comment '店铺编号',
    order_id INT comment '订单编号',
    start_time STRING comment '开始时间',
    done_time STRING comment '结束时间'
)partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/dwd/dwd_shop_order_d_f/';


