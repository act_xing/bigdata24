drop table if exists dwd.dwd_delivery_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS dwd.dwd_delivery_d_f(
    id INT comment '快递编号',
    delivery_no STRING comment '快递类型',
    order_id INT comment '订单编号',
    shop_id INT comment '店铺编号',
    postman INT comment '快递员',
    pick_time STRING comment '取件时间',
    arrive_time STRING comment '送达时间',
    member_id INT comment '用户编号',
    member_addr_id INT comment '用户地址编号'
)partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/dwd/dwd_delivery_d_f/';


