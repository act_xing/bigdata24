drop table if exists dwd.dwd_order_info_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS dwd.dwd_order_info_d_f(
  order_id INT COMMENT "订单编号",
  member_id INT COMMENT "用户编号",
  shop_id INT COMMENT "店铺编号",
  order_status INT COMMENT "订单状态：1,进行中 2 已完成 3 已取消",
  create_time STRING COMMENT "创建时间",
  update_time STRING COMMENT "更新时间",
  origin_price DOUBLE COMMENT "订单价格",
  pay_price DOUBLE COMMENT "支付价格"
)
partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/dwd/dwd_order_info_d_f';