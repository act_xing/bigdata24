drop table if exists dwd.dwd_coupon_member_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS dwd.dwd_coupon_member_d_f(
    id INT comment '编号',
    coupon_id INT comment '优惠卷编号',
    member_id INT comment '用户编号',
    coupon_channel INT comment '优惠卷来源：1 用户购买 2 公司发放',
    create_time STRING comment '创建时间',
    update_time STRING comment '更新时间'
)partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/dwd/dwd_coupon_member_d_f/';


