drop table if exists dwd.dwd_feedback_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS dwd.dwd_feedback_d_f(
    id INT comment '编号',
    member_id INT comment '用户编号',
    create_user_id INT comment '创建用户编号',
    feedback_content STRING comment '反馈类容',
    feedback_type INT comment '1 破损 2 缺货 3 错货 4 投诉'
)partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/dwd/dwd_feedback_d_f/';


