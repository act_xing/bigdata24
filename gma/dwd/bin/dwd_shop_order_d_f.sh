ds=$1

# 初始化环境变量
source /etc/profile

# 切换到脚本当前的路径，避免相对路径出错
shell_home="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ${shell_home}

# Spark任务通常会以OnYarn的模式运行
# OnYarn：client、cluster
# 如果直接以spark-sql -f的方式启动 不能使用cluster方式运行
spark-sql \
--master local[*] \
--conf spark.sql.shuffle.partitions=1 \
--num-executors 1 \
--executor-cores 2 \
--executor-memory 1G \
-f ../dql/dwd_shop_order_d_f.sql -d ds=${ds}