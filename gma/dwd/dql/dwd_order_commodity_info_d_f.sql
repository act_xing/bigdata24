insert overwrite table dwd.dwd_order_commodity_info_d_f partition(ds=${ds})
select  id
        ,order_id
        ,commodity_id
        ,create_time
        ,update_time
        ,commodity_num
        ,commodity_price
from ods.ods_t_order_commodity_d
where ds = '${ds}';