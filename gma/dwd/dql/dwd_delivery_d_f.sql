insert overwrite table dwd.dwd_delivery_d_f partition(ds='${ds}')
select
id,
delivery_no,
order_id,
shop_id,
postman,
pick_time,
arrive_time,
member_id,
member_addr_id
from
ods.ods_t_delivery_d
where ds='${ds}';