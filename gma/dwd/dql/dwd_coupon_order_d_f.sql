insert overwrite table dwd.dwd_coupon_order_d_f partition(ds='${ds}')

select
id,
coupon_id,
member_id,
order_id,
create_time
from
ods.ods_t_coupon_order_d
where ds='${ds}';