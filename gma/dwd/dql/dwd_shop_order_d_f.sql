
-- 基于ods中的订单表构建订单事实表
insert overwrite table  dwd.dwd_shop_order_d_f partition(ds='${ds}')
select
    id,
    shop_id,
    order_id,
    start_time,
    done_time
from
ods.ods_t_shop_order_d -- 需要将表的权限授权给dwd用户
where ds='${ds}';