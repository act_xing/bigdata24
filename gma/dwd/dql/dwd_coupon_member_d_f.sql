insert overwrite table dwd.dwd_coupon_member_d_f partition(ds='${ds}')
select
id,
coupon_id,
member_id,
coupon_channel,
create_time,
update_time
from
ods.ods_t_coupon_member_d
where ds='${ds}';