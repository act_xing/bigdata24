insert overwrite table dwd.dwd_feedback_d_f partition(ds='${ds}')
select
id,
member_id,
create_user_id,
feedback_content,
feedback_type
from
ods.ods_t_feedback_d
where ds='${ds}';