insert overwrite table dwd.dwd_order_info_d_f partition(ds=${ds})
select  order_id
        ,member_id
        ,shop_id
        ,order_status
        ,create_time
        ,update_time
        ,origin_price
        ,pay_price
from ods.ods_t_order_d
where ds = '${ds}';