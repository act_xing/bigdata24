insert overwrite table ods.{hive_table_name} partition(ds='${ds}')
select  {fields}
from (
  select  *
  ,row_number() over (partition by t1.{pk} order by t1.{time_field} desc) as rn
  from (
    select  *
    from ods.{hive_table_name}
    where ds = '${yesterday}'
    union all
    select  *
    from ods.{hive_table_name_delta}
    where ds = '${ds}'
  ) t1
) tt1 where tt1.rn = 1;