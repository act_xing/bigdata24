# /bin/bash
ds=$1

# 初始化环境变量
source /etc/profile

# 切换到脚本当前的路径，避免相对路径出错
shell_home="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ${shell_home}

# 1、删除已有的目录 实现overwrite写入的方式
hdfs dfs -rmr -skipTrash /daas/shujia/ods/{hive_table_name}/ds=${ds}

# 2、创建目录，datax不会自动创建目录
hdfs dfs -mkdir -p /daas/shujia/ods/{hive_table_name}/ds=${ds}

# 3、启动datax采集脚本
datax.py -p"-Dds=${ds}" ../datax/{hive_table_name}.json

# 4、增加表的分区
spark-sql -e "alter table ods.{hive_table_name} add if not exists partition(ds=${ds})"