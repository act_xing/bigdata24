drop table if exists ods.{hive_table_name};
CREATE EXTERNAL TABLE IF NOT EXISTS ods.{hive_table_name}(
{fields}
)
partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/ods/{hive_table_name}';