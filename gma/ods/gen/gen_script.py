import os
import re


def read_template_file(path):
    with open(path, mode='r', encoding='utf8') as f:
        return "".join(f.readlines())


def write_file(path, s):
    with open(path, mode='w', encoding='utf8',newline="\n") as f:
        f.write(s)


types_mapping = {
    "int": "INT"
    , "varchar": "STRING"
    , "datetime": "STRING"
    , "decimal": "DOUBLE"
    , "tinytext": "STRING"
    , "text": "STRING"
}

table_update_time_field = {
    "t_commodity": "update_time",
    "t_commodity_cate": "update_time",
    "t_coupon": "update_time",
    "t_coupon_member": "update_time",
    "t_coupon_order": "create_time",
    "t_delivery": "arrive_time",
    "t_member": "update_time",
    "t_member_addr": "update_time",
    "t_order": "update_time",
    "t_order_commodity": "update_time",
    "t_shop": "update_time",
    "t_shop_order": "done_time",
    "t_user": "update_time"
}


def transform_mysql_type_to_hive_type(mysql_type_list):
    hive_type_list = []
    for mysql_type in mysql_type_list:
        hive_type_list.append(types_mapping[mysql_type.split("(")[0]])
    return hive_type_list


if __name__ == '__main__':
    for file_name in os.listdir("../mysql_ddl"):
        # 打开并加载文件
        with open(f"../mysql_ddl/{file_name}", mode='r', encoding='utf8') as f:
            ddl_str = "".join(f.readlines())
            # 需要提取什么：
            # 表名、字段名、字段类型、主键
            mysql_table_name = re.search("(`.*?`)", ddl_str).group(1).replace("`", "")
            # re.S 表示匹配代换行符\n的字符串
            field_name_list = [filed_str.strip().split(" ")[0] for filed_str in
                               re.search("\((.*)PRIMARY KEY", ddl_str, re.S).group(1).replace("`", "").split(",\n")[
                               :-1]]
            field_type_list = [filed_str.strip().split(" ")[1] for filed_str in
                               re.search("\((.*)PRIMARY KEY", ddl_str, re.S).group(1).replace("`", "").split(",\n")[
                               :-1]]
            pk = re.search("PRIMARY KEY \(`(.*?)`\)", ddl_str, re.S).group(1)

            # 基于表名生成hive中的表名
            hive_table_name = f"ods_{mysql_table_name}_d"
            hive_table_name_delta = f"ods_{mysql_table_name}_d_delta"

            # 加载模版，生成对应的脚本
            ###################################生成全量数据采集shell脚本###################################
            sh_datax_template = read_template_file("template/sh_datax_template.sh") \
                .replace("{hive_table_name}", hive_table_name)
            write_file(f"../bin/{hive_table_name}.sh", sh_datax_template)

            ###################################生成全量数据采集DataX Json配置文件###################################
            hive_field_type = transform_mysql_type_to_hive_type(field_type_list)
            tmp_list = []
            for name, type in zip(field_name_list, hive_field_type):
                tmp_list.append('{"name": "%s","type": "%s"}' % (name, type))
            datax_template = read_template_file("template/datax_template.json") \
                .replace("{fields}", ",".join([f'"{field}"' for field in field_name_list])) \
                .replace("{mysql_table_name}", mysql_table_name) \
                .replace("{hive_table_name}", hive_table_name) \
                .replace("{fields_with_types}", ",".join(tmp_list))
            write_file(f"../datax/{hive_table_name}.json", datax_template)
            ###################################生成全量表的建表语句###################################
            fields_tmp_list = []
            for name, type in zip(field_name_list, hive_field_type):
                fields_tmp_list.append(f"\t`{name}` {type}")
            ods_template = read_template_file("template/ods_template.sql") \
                .replace("{hive_table_name}", hive_table_name) \
                .replace("{fields}", ",\n".join(fields_tmp_list))
            write_file(f"../ddl/{hive_table_name}.sql", ods_template)
            # 跳过无时间字段的表，即不需要做增量
            if mysql_table_name in table_update_time_field:
                ###################################生成增量数据采集shell脚本###################################
                sh_datax_template_delta = read_template_file("template/sh_datax_template_delta.sh") \
                    .replace("{hive_table_name_delta}", hive_table_name_delta) \
                    .replace("{hive_table_name}", hive_table_name)
                write_file(f"../bin_delta/{hive_table_name_delta}.sh", sh_datax_template_delta)
                ###################################生成增量数据采集DataX Json配置文件###################################
                datax_template_delta = read_template_file("template/datax_template_delta.json") \
                    .replace("{fields}", ",".join([f'"{field}"' for field in field_name_list])) \
                    .replace("{time_field}", table_update_time_field[mysql_table_name]) \
                    .replace("{mysql_table_name}", mysql_table_name) \
                    .replace("{hive_table_name}", hive_table_name_delta) \
                    .replace("{fields_with_types}", ",".join(tmp_list))
                write_file(f"../datax_delta/{hive_table_name_delta}.json", datax_template_delta)
                ###################################生成增量表的建表语句###################################
                fields_tmp_list_delta = []
                for name, type in zip(field_name_list, hive_field_type):
                    fields_tmp_list_delta.append(f"\t`{name}` {type}")
                ods_template_delta = read_template_file("template/ods_template.sql") \
                    .replace("{hive_table_name}", hive_table_name_delta) \
                    .replace("{fields}", ",\n".join(fields_tmp_list_delta))
                write_file(f"../ddl_delta/{hive_table_name_delta}.sql", ods_template_delta)
                ###################################生成合并数据的SQL任务###################################
                merge_template = read_template_file("template/merge_template.sql") \
                    .replace("{hive_table_name}", hive_table_name) \
                    .replace("{fields}", ",".join(field_name_list)) \
                    .replace("{pk}", pk) \
                    .replace("{time_field}", table_update_time_field[mysql_table_name]) \
                    .replace("{hive_table_name_delta}", hive_table_name_delta)
                write_file(f"../dql/merge_{hive_table_name}.sql", merge_template)
