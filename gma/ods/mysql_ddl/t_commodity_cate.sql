CREATE TABLE `t_commodity_cate` (
  `id` int(11) NOT NULL,
  `cate_name` varchar(50) DEFAULT NULL,
  `cate_parent_id` int(11) DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `status` int(4) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;