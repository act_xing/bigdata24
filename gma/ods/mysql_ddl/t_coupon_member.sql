CREATE TABLE `t_coupon_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `coupon_channel` int(4) DEFAULT NULL COMMENT '1 用户购买 2 公司发放',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=801 DEFAULT CHARSET=utf8mb4;
