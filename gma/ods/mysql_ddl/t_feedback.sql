CREATE TABLE `t_feedback` (
  `id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `feedback_content` tinytext,
  `feedback_type` int(4) DEFAULT NULL COMMENT '1 破损 2 缺货 3 错货 4 投诉',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;