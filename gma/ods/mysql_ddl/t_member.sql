CREATE TABLE `t_member` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address_default_id` int(11) DEFAULT NULL,
  `member_channel` int(4) DEFAULT NULL COMMENT '1 IOS 2 android 3 微信小程序 4 微信公众号 5 h5',
  `mp_open_id` varchar(32) DEFAULT NULL COMMENT '微信公众号openId',
  `status` int(4) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
