CREATE TABLE `t_order` (
  `order_id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `origin_price` decimal(10,2) DEFAULT NULL,
  `pay_price` decimal(10,2) DEFAULT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `shop_name` varchar(50) DEFAULT NULL,
  `order_status` int(4) DEFAULT NULL COMMENT '1,进行中 2 已完成 3 已取消',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
