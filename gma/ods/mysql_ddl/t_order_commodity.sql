CREATE TABLE `t_order_commodity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `commodity_id` int(11) DEFAULT NULL,
  `commodity_name` varchar(50) DEFAULT NULL,
  `commodity_num` int(4) DEFAULT NULL,
  `commodity_price` decimal(10,2) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8mb4;