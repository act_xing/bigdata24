CREATE TABLE `t_commodity` (
  `id` int(11) NOT NULL,
  `commodity_name` varchar(50) DEFAULT NULL,
  `commodity_price` decimal(10,2) DEFAULT NULL,
  `commodity_cate_one` int(11) DEFAULT NULL,
  `commodity_cate_two` int(11) DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `status` int(4) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;