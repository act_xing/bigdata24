CREATE TABLE `t_coupon` (
  `id` int(11) NOT NULL,
  `coupon_name` varchar(50) DEFAULT NULL,
  `coupon_price` decimal(10,2) DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
