CREATE TABLE `t_coupon_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL COMMENT '商品订单ID',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=562 DEFAULT CHARSET=utf8mb4;
