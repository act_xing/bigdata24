CREATE TABLE `t_delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_no` varchar(64) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `postman` int(11) DEFAULT NULL,
  `pick_time` datetime DEFAULT NULL,
  `arrive_time` datetime DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `member_addr_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=801 DEFAULT CHARSET=utf8mb4;