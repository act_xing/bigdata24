drop table if exists gma_ads.ads_pi_rw_reg_d_f;
CREATE TABLE IF NOT EXISTS gma_ads.ads_pi_rw_reg_d_f(
  create_date VARCHAR(255) COMMENT "日期",
  reg_num INT COMMENT "当日注册人数",
  last_reg_num INT COMMENT "上周期注册人数",
  primary key(create_date)
);