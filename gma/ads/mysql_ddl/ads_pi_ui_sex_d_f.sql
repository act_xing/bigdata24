drop table if exists gma_ads.ads_pi_ui_sex_d_f;
CREATE TABLE IF NOT EXISTS gma_ads.ads_pi_ui_sex_d_f(
  sex VARCHAR(255) COMMENT "性别",
  cnt INT COMMENT "人数",
  primary key (sex)
);