drop table if exists gma_ads.ads_pi_wc_d_f;
CREATE TABLE IF NOT EXISTS gma_ads.ads_pi_wc_d_f(
  commodity_name VARCHAR(255) COMMENT "商品名称",
  cnt INT COMMENT "数量",
  primary key (commodity_name)
);