drop table if exists gma_ads.ads_pi_pd_d_f;
CREATE TABLE IF NOT EXISTS gma_ads.ads_pi_pd_d_f(
  create_date VARCHAR(255) COMMENT "创建日期",
  reg_num INT COMMENT "每日新增注册人数",
  sum_reg_num INT COMMENT "累计注册人数",
  sum_order_num INT COMMENT "每日新增订单数",
  sum_order_price DOUBLE COMMENT "累计订单金额",
  primary key(create_date)
);