drop table if exists gma_ads.ads_pi_ui_channel_d_f;
CREATE TABLE IF NOT EXISTS gma_ads.ads_pi_ui_channel_d_f(
  channel VARCHAR(255) COMMENT "渠道",
  cnt INT COMMENT "人数",
  primary key(channel)
);