insert overwrite table ads.ads_pi_ui_hot_d_f partition(ds=${ds})
-- 已注册
select  '已注册' as status
        ,count(*) as cnt
from dim.dim_pub_user_info_d_f
where ds = '20230727'
union all
-- 已下单
select  '已下单' as status
        ,count(distinct member_id) as cnt
from dwd.dwd_order_info_d_f
where ds = '20230727'
union all
-- 已复购
select  '已复购' as status
        ,count(*)
from (
  select  member_id
  ,count(1) as cnt
  from dwd.dwd_order_info_d_f
  where ds = '20230727'
  group by member_id
  having cnt >= 2
) t1
union all
-- 已领券
select  '已领券' as status
        ,count(distinct member_id) as cnt
from dwd.dwd_coupon_member_d_f
where ds = '20230727'
union all
-- 已完善
select  '已完善' as status
        ,count(*) as cnt
from dim.dim_pub_user_info_d_f
where ds = '20230727'
and mp_open_id is not null
and mp_open_id != ''
;