insert overwrite table ads.ads_pi_ui_channel_d_f partition(ds=${ds})
select  case when member_channel = '1' then 'IOS'
             when member_channel = '2' then 'Android'
             when member_channel = '3' then '微信小程序'
             when member_channel = '4' then '微信公众号'
             when member_channel = '5' then 'H5'
             end as channel
        ,count(distinct id) as cnt
from dim.dim_pub_user_info_d_f
where ds = '${ds}'
group by member_channel
;