insert overwrite table ads.ads_pi_rw_reg_d_f partition(ds=${ds})
select  t1.create_date
        ,t1.cnt as reg_num
        ,t1.last_reg_num
from (
  select  date_format(create_time,'yyyy-MM-dd') as create_date
          ,count(*) as cnt
          ,lag(count(*),7) over (order by date_format(create_time,'yyyy-MM-dd')) as last_reg_num
  from dim.dim_pub_user_info_d_f
  where ds = '20230727'
  --and create_time > date_sub(current_date,14)
  and create_time > date_sub('2022-11-30',13)
  group by date_format(create_time,'yyyy-MM-dd')
) t1 where t1.last_reg_num is not null
;