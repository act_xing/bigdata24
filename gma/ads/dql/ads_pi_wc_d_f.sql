insert overwrite table ads.ads_pi_wc_d_f partition(ds=${ds})
select  t2.commodity_name
        ,count(*) as cnt
from (
  select  commodity_id
  from dwd.dwd_order_commodity_info_d_f
  where ds = '${ds}'
) t1 left join
(
  select  id
  ,commodity_name
  from dim.dim_pub_commodity_info_d_f
  where ds = '${ds}'
) t2 on t1.commodity_id = t2.id
group by t2.commodity_name
;