insert overwrite table ads.ads_pi_ui_sex_d_f partition(ds=${ds})
select  case when sex = '1' then '男性'
             when sex = '2' then '女性'
        else '未知'
        end as sex
        ,count(distinct id) as cnt
from dim.dim_pub_user_info_d_f
where ds = '${ds}'
group by sex
;