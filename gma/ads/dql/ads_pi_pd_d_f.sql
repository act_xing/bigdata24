insert overwrite table ads.ads_pi_pd_d_f partition(ds=${ds})
select  t1.create_date
        ,t1.reg_num
        ,t1.sum_reg_num
        ,t2.sum_order_num
        ,t2.sum_order_price/1000
from (
  select  date_format(create_time,'yyyy-MM-dd') as create_date
          ,count(*) as reg_num
          ,sum(count(*)) over (order by date_format(create_time,'yyyy-MM-dd')) as sum_reg_num
  from dim.dim_pub_user_info_d_f
  where ds = '${ds}'
  group by date_format(create_time,'yyyy-MM-dd')
) t1 join (
  select  date_format(create_time,'yyyy-MM-dd') as create_date
          ,sum(count(*)) over (order by date_format(create_time,'yyyy-MM-dd')) as sum_order_num
          ,sum(sum(pay_price)) over (order by date_format(create_time,'yyyy-MM-dd')) as sum_order_price
  from dwd.dwd_order_info_d_f
  where ds = '${ds}'
  group by date_format(create_time,'yyyy-MM-dd')
) t2 on t1.create_date = t2.create_date
--where t1.create_date > date_sub(current_date,7)
where t1.create_date > date_sub('2022-11-30',7)
;