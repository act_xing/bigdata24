drop table if exists ads.ads_pi_ui_channel_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS ads.ads_pi_ui_channel_d_f(
  channel STRING COMMENT "渠道",
  cnt INT COMMENT "人数"
)
partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/ads/ads_pi_ui_channel_d_f';