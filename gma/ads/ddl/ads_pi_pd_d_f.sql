drop table if exists ads.ads_pi_pd_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS ads.ads_pi_pd_d_f(
  create_date STRING COMMENT "创建日期",
  reg_num INT COMMENT "每日新增注册人数",
  sum_reg_num INT COMMENT "累计注册人数",
  sum_order_num INT COMMENT "每日新增订单数",
  sum_order_price DOUBLE COMMENT "累计订单金额"
)
partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/ads/ads_pi_pd_d_f';