drop table if exists ads.ads_pi_wc_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS ads.ads_pi_wc_d_f(
  commodity_name STRING COMMENT "商品名称",
  cnt INT COMMENT "数量"
)
partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/ads/ads_pi_wc_d_f';