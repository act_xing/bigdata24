drop table if exists ads.ads_pi_ui_sex_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS ads.ads_pi_ui_sex_d_f(
  sex STRING COMMENT "性别",
  cnt INT COMMENT "人数"
)
partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/ads/ads_pi_ui_sex_d_f';