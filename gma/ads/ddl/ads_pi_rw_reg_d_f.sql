drop table if exists ads.ads_pi_rw_reg_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS ads.ads_pi_rw_reg_d_f(
  create_date STRING COMMENT "日期",
  reg_num INT COMMENT "当日注册人数",
  last_reg_num INT COMMENT "上周期注册人数"
)
partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/ads/ads_pi_rw_reg_d_f';