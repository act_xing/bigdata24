package com.shujia.utils

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.{HBaseConfiguration, TableName}
import org.apache.hadoop.hbase.client.{Connection, ConnectionFactory, Put, Table}
import org.apache.spark.sql.DataFrame

object HBaseUtils {
  def getConnection:Connection = {
    // 建立HBase连接
    val conf: Configuration = HBaseConfiguration.create()
    conf.set("hbase.zookeeper.quorum", "master:2181,node1:2181,node2:2181")
    val conn: Connection = ConnectionFactory.createConnection(conf)

    conn
  }

  def saveDFToHBase(df:DataFrame,fieldName:String):Unit = {
    df
      .foreachPartition(iter => {
        // 首先得在HBase中先把表创建好，create 'userprofile','info'
        // 获取表对象
        val userProfile: Table = getConnection.getTable(TableName.valueOf("userprofile"))
        iter.foreach(row => {
          val id: Int = row.getAs[Int]("id")
          val field: String = row.getAs[String](fieldName)
          val put: Put = new Put(String.valueOf(id).getBytes())
          put.addColumn("info".getBytes(), fieldName.getBytes(), field.getBytes())
          userProfile.put(put)
        })
      })
  }

}
