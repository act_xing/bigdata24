package com.shujia.utils

import org.apache.spark.internal.Logging
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.slf4j.Logger

object SparkRunner {
  def getSparkSession: SparkSession = {
    // 初始化SparkSession
    val spark: SparkSession = SparkSession
      .builder()
      //      .master("local")
      .appName(this.getClass.getSimpleName.replace("$", ""))
      .enableHiveSupport()
      .getOrCreate()

    spark
  }

}

abstract class SparkRunner extends Logging {
  var fieldName: String = _
  var ds: String = _

  val logger: Logger = log

  def main(args: Array[String]): Unit = {
    if (args.isEmpty) {
      logger.error("请指定分区日期！")
      return
    }
    ds = args.head

    // 1、构建SparkSession
    val spark: SparkSession = SparkRunner.getSparkSession

    // 2、提取用户标签 构建DF
    val resDF: DataFrame = run(spark)

    // 3、将DF保存到HBase
    HBaseUtils.saveDFToHBase(resDF, fieldName)
  }

  def run(spark: SparkSession): DataFrame

}
