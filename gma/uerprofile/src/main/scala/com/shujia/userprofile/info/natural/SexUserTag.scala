package com.shujia.userprofile.info.natural

import com.shujia.utils.{HBaseUtils, SparkRunner}
import org.apache.spark.sql.{DataFrame, SparkSession}

object SexUserTag extends SparkRunner {
  fieldName = "sex"

  override def run(spark: SparkSession): DataFrame = {
    import spark.implicits._
    import org.apache.spark.sql.functions._
    spark
      .table("dim.dim_pub_user_info_d_f")
      .where($"ds" === ds)
      .select($"id"
        , when($"sex" === "1", "男")
          .when($"sex" === "2", "女")
          .otherwise("未知") as "sex"
      )
  }
}
