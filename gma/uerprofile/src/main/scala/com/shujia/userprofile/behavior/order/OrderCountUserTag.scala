package com.shujia.userprofile.behavior.order

import com.shujia.utils.SparkRunner
import org.apache.spark.sql.{DataFrame, SparkSession}

object OrderCountUserTag extends SparkRunner{
  fieldName = "orderCount"

  override def run(spark: SparkSession): DataFrame = {

    import spark.implicits._
    import org.apache.spark.sql.functions._

    spark
      .table("dwd.dwd_order_info_d_f")
      .where($"ds"===ds)
      .groupBy($"member_id" as "id")
      .agg(countDistinct($"order_id").cast("String") as "orderCount")
  }
}
