package com.shujia.userprofile.info.social

import com.shujia.utils.{HBaseUtils, SparkRunner}
import org.apache.spark.sql.{DataFrame, SparkSession}

object AddressUserTag extends SparkRunner{
  fieldName = "address"

  override def run(spark: SparkSession): DataFrame = {
    import spark.implicits._
    // 加载用户维表
    spark
      .table("dim.dim_pub_user_addr_d_f")
      .where($"ds" === ds)
      .select($"id"
        ,$"address"
      )
  }
}
