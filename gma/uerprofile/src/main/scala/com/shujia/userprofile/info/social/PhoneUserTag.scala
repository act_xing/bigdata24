package com.shujia.userprofile.info.social

import com.shujia.utils.{HBaseUtils, SparkRunner}
import org.apache.spark.sql.{DataFrame, SparkSession}

object PhoneUserTag extends SparkRunner {
  fieldName = "phone"

  override def run(spark: SparkSession): DataFrame = {
    import spark.implicits._
    // 加载用户维表
    spark
      .table("dim.dim_pub_user_info_d_f")
      .where($"ds" === ds)
      .select($"id"
        , $"phone"
      )

  }
}
