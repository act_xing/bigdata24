package com.shujia.userprofile.info.platform

import com.shujia.utils.SparkRunner
import org.apache.spark.sql.{DataFrame, SparkSession}

object MemberIDUserTag extends SparkRunner {
  fieldName = "member_id"

  override def run(spark: SparkSession): DataFrame = {
    import spark.implicits._
    import org.apache.spark.sql.functions._
    spark
      .table("dim.dim_pub_user_addr_d_f")
      .where($"ds" === ds)
      .select(
        $"id"
        , md5($"member_id".cast("String")) as "member_id"
      )

  }
}
