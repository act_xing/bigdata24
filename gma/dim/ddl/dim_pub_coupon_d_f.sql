drop table if exists dim.dim_pub_coupon_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS dim.dim_pub_coupon_d_f(
    id INT comment '编号',
    coupon_name STRING comment '优惠卷名',
    coupon_price decimal(10,2) comment '优惠卷价格',
    create_user_id INT comment '创建用户编号',
    create_time STRING comment '创建时间'
)partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/dim/dim_pub_coupon_d_f/';
