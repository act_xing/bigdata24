drop table if exists dim.dim_pub_shop_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS dim.dim_pub_shop_d_f(
    id INT comment '店铺编号',
    shop_name STRING comment '店铺名',
    city_id INT comment '城市编号',
    city_name STRING comment '城市名',
    area_id INT comment '地区编号',
    area_name STRING comment '地区名',
    charge_user STRING comment '用户',
    create_time STRING comment '创建时间',
    update_time STRING comment '更新时间'
)partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/dim/dim_pub_shop_d_f/';
