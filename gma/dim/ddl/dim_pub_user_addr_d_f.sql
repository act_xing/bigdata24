drop table if exists dim.dim_pub_user_addr_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS dim.dim_pub_user_addr_d_f(
  id INT COMMENT "地址编号",
  member_id INT COMMENT "用户编号",
  contact_person STRING COMMENT "联系人",
  contact_phone STRING COMMENT "联系电话",
  address STRING COMMENT "地址",
  create_time STRING COMMENT "创建时间",
  update_time STRING COMMENT "更新时间"
)
partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/dim/dim_pub_user_addr_d_f';