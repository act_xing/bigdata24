drop table if exists dim.dim_pub_commodity_info_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS dim.dim_pub_commodity_info_d_f(
  id INT COMMENT "商品编号",
  commodity_name STRING COMMENT "商品名称",
  commodity_price DECIMAL(10,2) COMMENT "商品价格",
  commodity_cate_name STRING COMMENT "商品类别",
  create_user_id INT COMMENT "创建用户",
  status INT COMMENT "商品状态",
  create_time STRING COMMENT "创建时间",
  update_time STRING COMMENT "更新时间"
)
partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/dim/dim_pub_commodity_info_d_f';