drop table if exists dim.dim_pub_user_info_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS dim.dim_pub_user_info_d_f(
  id INT COMMENT "用户编号",
  name STRING COMMENT "用户名",
  password STRING COMMENT "用户密码",
  sex STRING COMMENT "性别",
  phone STRING COMMENT "手机号",
  address_default_id INT COMMENT "地址编号",
  member_channel INT COMMENT "用户来源：1 IOS 2 android 3 微信小程序 4 微信公众号 5 h5",
  mp_open_id STRING COMMENT "微信公众号openId",
  status INT COMMENT "用户状态",
  create_time STRING COMMENT "用户创建时间",
  update_time STRING COMMENT "用户更新时间"
)
partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/dim/dim_pub_user_info_d_f';