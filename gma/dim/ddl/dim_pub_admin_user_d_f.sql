drop table if exists dim.dim_pub_admin_user_d_f;
CREATE EXTERNAL TABLE IF NOT EXISTS dim.dim_pub_admin_user_d_f(
  id INT COMMENT "编号",
  user_name STRING COMMENT "用户名",
  user_password STRING COMMENT "用户密码",
  user_phone STRING COMMENT "用户手机号",
  create_time STRING COMMENT "创建时间",
  update_time STRING COMMENT "更新时间"
)
partitioned by (ds STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
location '/daas/shujia/dim/dim_pub_admin_user_d_f';