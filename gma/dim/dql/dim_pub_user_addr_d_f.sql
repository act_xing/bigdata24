insert overwrite table dim.dim_pub_user_addr_d_f partition(ds=${ds})
select  id
        ,member_id
        ,regexp_replace(contact_person,'(.).*(.)','$1***$2') as contact_person
        ,regexp_replace(contact_phone,'(\\d{3}).*(\\d{2})','$1******$2') as contact_phone
        ,regexp_replace(address,'\\d','*') as address
        ,create_time
        ,update_time
from ods.ods_t_member_addr_d
where ds=${ds};