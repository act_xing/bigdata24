insert overwrite table dim.dim_pub_user_info_d_f partition(ds=${ds})
select  id
        ,regexp_replace(name,'(.).*(.)','$1***$2') as name
        ,md5(password) as password
        ,sex
        ,regexp_replace(phone,'(\\d{3}).*(\\d{2})','$1******$2') as phone
        ,address_default_id
        ,member_channel
        ,mp_open_id
        ,status
        ,create_time
        ,update_time
from ods.ods_t_member_d
where ds=${ds};