insert overwrite table dim.dim_pub_admin_user_d_f partition(ds=${ds})
select  id
        ,regexp_replace(user_name,'(.).*(.)','$1***$2') as user_name
        ,md5(user_password) as user_password
        ,regexp_replace(user_phone,'(\\d{3}).*(\\d{2})','$1******$2') as user_phone
        ,create_time
        ,update_time
from ods.ods_t_user_d
where ds=${ds};