
insert overwrite table dim.dim_pub_coupon_d_f partition(ds = '${ds}')
select
id,
coupon_name,
coupon_price,
create_user_id,
create_time
from
ods.ods_t_coupon_d
where ds='${ds}';