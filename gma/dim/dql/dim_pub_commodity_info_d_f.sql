insert overwrite table dim.dim_pub_commodity_info_d_f partition(ds=${ds})
select  t1.id
        ,t1.commodity_name
        ,t1.commodity_price
        ,t2.cate_name as commodity_cate_name
        ,t1.create_user_id
        ,t1.status
        ,t1.create_time
        ,t1.update_time
from (
  select  id
          ,commodity_name
          ,commodity_price
          ,commodity_cate_two
          ,create_user_id
          ,status
          ,create_time
          ,update_time
  from ods.ods_t_commodity_d
  where ds = ${ds}
) t1 left join (
  select  id
  ,cate_name
  from ods.ods_t_commodity_cate_d
  where ds = ${ds}
) t2 on t1.commodity_cate_two = t2.id
