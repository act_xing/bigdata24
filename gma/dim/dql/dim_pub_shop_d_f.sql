

insert overwrite table dim.dim_pub_shop_d_f partition(ds='${ds}')
select
    id,
    shop_name,
    city_id,
    city_name,
    area_id,
    area_name,
    charge_user,
    create_time,
    update_time
from
    ods.ods_t_shop_d
where ds='${ds}';