CREATE TABLE hive_catalog.lake_dws.dws_ck_order_wide(
    order_id BIGINT comment '订单ID (主键)',
    passenger_id BIGINT comment '乘客ID (外键，关联乘客表)',
    driver_id BIGINT comment '司机ID (外键，关联司机表)',
    start_time TIMESTAMP(3) comment '开始时间',
    end_time TIMESTAMP(3) comment '结束时间',
    pickup_location STRING comment '起始地点',
    dropoff_location STRING comment '目的地点',
    activity_id BIGINT comment '活动ID',
    city STRING comment '城市',
    prize_id BIGINT comment '奖品ID',
    status BIGINT comment '订单状态',
    estimated_amount decimal(10, 2) comment '预估金额',
    actual_amount decimal(10, 2) comment '实际支付金额',
    take_time DOUBLE comment '乘车用时',
    money decimal(164) comment '券金额',
    rating INT comment '评分',
    `comment` STRING comment '评价评论',
    activity_name STRING comment '活动名称',
    activity_type STRING comment '活动类型 BUBBLE：冒泡',
    complaint_type STRING comment '投诉类型',
    description STRING comment '投诉描述',
    prize_name STRING comment '奖品名称',
    prize_type STRING comment '奖品类型',
    PRIMARY KEY (order_id) NOT ENFORCED
)WITH (
    'connector' = 'clickhouse',
    'url' = 'clickhouse://master:8123',
    'database-name' = 'default',
    'table-name' = 'dws_ck_order_wide',
    'sink.batch-size' = '50000',
    'sink.flush-interval' = '1000',
    'sink.max-retries' = '3'
);


-- clickhouse建表语句
create table dws_ck_order_wide (
    order_id Int64 comment '订单ID (主键)',
    passenger_id Int64 comment '乘客ID (外键，关联乘客表)',
    driver_id Int64 comment '司机ID (外键，关联司机表)',
    start_time DateTime comment '开始时间',
    end_time DateTime comment '结束时间',
    pickup_location String comment '起始地点',
    dropoff_location String comment '目的地点',
    activity_id Int64 comment '活动ID',
    city String comment '城市',
    prize_id Int64 comment '奖品ID',
    `status` Int32 comment '订单状态',
    estimated_amount Decimal(10, 2) comment '预估金额',
    actual_amount Decimal(10, 2) comment '实际支付金额',
    take_time DOUBLE comment '乘车用时',
    money Decimal(10,2) comment '券金额',
    rating Int32 comment '评分',
    comment String comment '评价评论',
    activity_name String comment '活动名称',
    activity_type String comment '活动类型 BUBBLE：冒泡',
    complaint_type String comment '投诉类型',
    description String comment '投诉描述',
    prize_name String comment '奖品名称',
    prize_type String comment '奖品类型',
    PRIMARY KEY(order_id)
)ENGINE = MergeTree()
;