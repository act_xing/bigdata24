
-- flink sql 问题和优化
--1、状态不断增加，导致内存不够，taskmanager丢失  --- 解决增加taskmanager的内存或者增加并行度  -tm  -p
--2、常规关联方式flink会将两个表的数据一直保存至状态中，可以合理设计状态有效期避免状态无限增大
--3、lookup 管理性能差，在flink中增加缓存
--4、数据写入ck性能差，可以增加写入批次大小

set 'table.exec.state.ttl' = '1min';

insert into hive_catalog.lake_dws.dws_ck_order_wide /*+options('sink.batch-size' = '100000') */
select
    d.order_id,
    d.passenger_id,
    d.driver_id,
    d.end_time as start_time,
    d.start_time as end_time,
    d.pickup_location,
    d.dropoff_location,
    h.activity_id,
    d.city,
    h.prize_id,
    d.status,
    d.estimated_amount,
    d.actual_amount,
    cast(TIMESTAMPDIFF(SECOND,end_time,start_time) / 60.0 as double) as  take_time,
    h.money,
    g.rating,
    g.`comment`,
    h.activity_name,
    h.activity_type,
    f.complaint_type,
    f.description,
    h.prize_name,
    h.prize_type
from
    hive_catalog.lake_dwd.dwd_dl_order as d -- 订单表
left join
    hive_catalog.lake_dwd.dwd_dl_complaint as f -- 投诉表
on
    d.order_id=f.order_id
left join
    hive_catalog.lake_dwd.dwd_dl_review as g -- 评价表
on
    d.order_id=g.order_id
left join
    (select
        a.record_id,
        a.order_id,
        a.activity_id,
        a.prize_id,
        a.money,
        b.info.name as activity_name,
        b.info.type as activity_type,
        c.info.name as prize_name,
        c.info.type as prize_type
    from
    (
        select * , PROCTIME() as proctime
        from hive_catalog.lake_dwd.dwd_dl_record -- 活动流水记录表
    ) as a
    join
        hive_catalog.lake_dim.dim_hbase_pub_activity /*+options('lookup.cache.max-rows' ='100','lookup.cache.ttl'='10min') */  FOR SYSTEM_TIME AS OF a.proctime   as b  --活动维度表
    on
        a.activity_id=b.activity_id
    join
        hive_catalog.lake_dim.dim_hbase_pub_prize  /*+options('lookup.cache.max-rows' ='232','lookup.cache.ttl'='10min') */ FOR SYSTEM_TIME AS OF a.proctime   as c  -- 奖品维度表
    on
        a.prize_id=c.prize_id
    ) h
on
    d.order_id=h.order_id;

