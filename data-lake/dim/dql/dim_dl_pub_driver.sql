
EXECUTE STATEMENT SET
BEGIN

-- 保存到hudi
insert into hive_catalog.lake_dim.dim_dl_pub_driver
select
driver_id,
MD5(CONCAT(name,'shujia')) as name,
MD5(CONCAT(phone,'shujia')) as phone,
MD5(CONCAT(email,'shujia')) as email,
MD5(CONCAT(license_number,'shujia')) as license_number
from
hive_catalog.lake_ods.ods_dl_driver

;

--保存到hbase
insert into hive_catalog.lake_dim.dim_hbase_pub_driver

select
    driver_id,
    ROW(
     name,
     phone,
     email,
     license_number
    ) as info
from (
    select
        driver_id,
        MD5(CONCAT(name,'shujia')) as name,
        MD5(CONCAT(phone,'shujia')) as phone,
        MD5(CONCAT(email,'shujia')) as email,
        MD5(CONCAT(license_number,'shujia')) as license_number
    from
        hive_catalog.lake_ods.ods_dl_driver
);

END;