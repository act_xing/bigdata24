

EXECUTE STATEMENT SET
BEGIN

-- 将为表保存到数据湖中
insert into hive_catalog.lake_dim.dim_dl_pub_activity
select
    activity_id,
    cid,
    name,
    type,
    start_time,
    end_time,
    is_default,
    status,
    is_delete,
    creator_id,
    md5(concat(nick_name,'shujia')) as nick_name,
    update_user_id,
    md5(concat(update_nick_name,'shujia')) as update_nick_name,
    gmt_create,
    gmt_modified
from
    hive_catalog.lake_ods.ods_dl_activity;


-- 将维表保存到hbase中
insert into hive_catalog.lake_dim.dim_hbase_pub_activity
select
 activity_id,
 ROW(cid,
     name,
     type,
     start_time,
     end_time,
     is_default,
     status,
     is_delete,
     creator_id,
     nick_name,
     update_user_id,
     update_nick_name,
     gmt_create,
     gmt_modified) as info
from (
    select
        activity_id,
        cid,
        name,
        type,
        start_time,
        end_time,
        is_default,
        status,
        is_delete,
        creator_id,
        md5(concat(nick_name,'shujia')) as nick_name,
        update_user_id,
        md5(concat(update_nick_name,'shujia')) as update_nick_name,
        gmt_create,
        gmt_modified
    from
        hive_catalog.lake_ods.ods_dl_activity
)
;

END;