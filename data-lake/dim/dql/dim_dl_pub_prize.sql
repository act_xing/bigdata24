EXECUTE STATEMENT SET
BEGIN

insert into hive_catalog.lake_dim.dim_dl_pub_prize
select
prize_id,
cid,
name,
type,
`module`,
status,
is_delete
from
hive_catalog.lake_ods.ods_dl_prize;


insert into hive_catalog.lake_dim.dim_hbase_pub_prize
select
prize_id,
ROW(cid,
    name,
    type,
    `module`,
    status,
    is_delete
) as info
from
hive_catalog.lake_ods.ods_dl_prize;

END;
