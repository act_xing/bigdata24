

EXECUTE STATEMENT SET
BEGIN

insert into hive_catalog.lake_dim.dim_dl_pub_passenger
select
    passenger_id,
    md5(concat(name,'shujia')) as name,
    md5(concat(phone,'shujia')) as phone,
    md5(concat(email,'shujia')) as email,
    md5(concat(address,'shujia')) as address
from  hive_catalog.lake_ods.ods_dl_passenger;


insert into hive_catalog.lake_dim.dim_hbase_pub_passenger
select
passenger_id,
ROW(name,
    phone,
    email,
    address
) as info
from (
    select
        passenger_id,
        md5(concat(name,'shujia')) as name,
        md5(concat(phone,'shujia')) as phone,
        md5(concat(email,'shujia')) as email,
        md5(concat(address,'shujia')) as address
    from  hive_catalog.lake_ods.ods_dl_passenger
)
;
END;