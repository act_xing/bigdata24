CREATE TABLE hive_catalog.lake_dim.dim_hbase_pub_passenger(
    passenger_id BIGINT comment '乘客ID (主键)',
    info ROW(
        name STRING ,
        phone STRING ,
        email STRING ,
        address STRING
    ),
    PRIMARY KEY (passenger_id) NOT ENFORCED
)WITH (
 'connector' = 'hbase-1.4',
 'table-name' = 'dim_hbase_pub_passenger', -- 需要在hbase中手动创建表 create 'dim_hbase_pub_passenger','info'
 'zookeeper.quorum' = 'master:2181,node1:2181,node2:2181'
);