CREATE TABLE hive_catalog.lake_dim.dim_dl_pub_passenger(
    passenger_id BIGINT comment '乘客ID (主键)',
    name STRING comment '乘客姓名',
    phone STRING comment '乘客电话号码',
    email STRING comment '乘客电子邮件',
    address STRING comment '乘客地址',
    PRIMARY KEY (passenger_id) NOT ENFORCED
)WITH (
  'connector' = 'hudi',
  'path' = 'hdfs://master:9000/data/lake/dim/dim_dl_pub_passenger',
  'read.streaming.enabled' = 'true',
  'read.start-commit' = '20210316134557',
  'read.streaming.check-interval' = '4'
);