CREATE TABLE hive_catalog.lake_dim.dim_hbase_pub_driver(
    driver_id BIGINT comment '司机ID (主键)',
    info ROW<
      name STRING ,
      phone STRING ,
      email STRING,
      license_number STRING
    >,
    PRIMARY KEY (driver_id) NOT ENFORCED
)WITH (
 'connector' = 'hbase-1.4',
 'table-name' = 'dim_hbase_pub_driver', -- 需要在hbase中手动创建表 create 'dim_hbase_pub_driver','info'
 'zookeeper.quorum' = 'master:2181,node1:2181,node2:2181'
);