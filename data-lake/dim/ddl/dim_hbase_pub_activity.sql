CREATE TABLE hive_catalog.lake_dim.dim_hbase_pub_activity(
    activity_id BIGINT comment '主键',

    info ROW<cid STRING ,
             name STRING ,
             type STRING ,
             start_time TIMESTAMP(3) ,
             end_time TIMESTAMP(3) ,
             is_default INT ,
             status INT ,
             is_delete INT ,
             creator_id STRING ,
             nick_name STRING ,
             update_user_id STRING ,
             update_nick_name STRING ,
             gmt_create TIMESTAMP(3) ,
             gmt_modified TIMESTAMP(3)>,

    PRIMARY KEY (activity_id) NOT ENFORCED
)WITH (
 'connector' = 'hbase-1.4',
 'table-name' = 'dim_hbase_pub_activity', -- 需要在hbase中手动创建表 (create 'dim_hbase_pub_activity','info')
 'zookeeper.quorum' = 'master:2181,node1:2181,node2:2181'
);