CREATE TABLE hive_catalog.lake_dim.dim_dl_pub_activity(
    activity_id BIGINT comment '主键',
    cid STRING comment '企业ID',
    name STRING comment '活动名称',
    type STRING comment '活动类型 BUBBLE：冒泡',
    start_time TIMESTAMP(3) comment '开始时间',
    end_time TIMESTAMP(3) comment '结束时间',
    is_default INT comment '',
    status INT comment '活动状态 0-草稿、1-待启用',
    is_delete INT comment '是否删除 0-未删除、1-删除',
    creator_id STRING comment '创建者id',
    nick_name STRING comment '创建者名称',
    update_user_id STRING comment '最后修改人id',
    update_nick_name STRING comment '最后修改人名称',
    gmt_create TIMESTAMP(3) comment '创建时间',
    gmt_modified TIMESTAMP(3) comment '更新时间',
    PRIMARY KEY (activity_id) NOT ENFORCED
)WITH (
  'connector' = 'hudi',
  'path' = 'hdfs://master:9000/data/lake/dim/dim_dl_pub_activity',
  'read.streaming.enabled' = 'true',
  'read.start-commit' = '20210316134557',
  'read.streaming.check-interval' = '4'
);