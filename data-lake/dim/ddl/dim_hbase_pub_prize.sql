CREATE TABLE hive_catalog.lake_dim.dim_hbase_pub_prize(
    prize_id BIGINT comment '奖品ID',
    info ROW<
        cid STRING ,
        name STRING,
        type STRING ,
        `module` BIGINT ,
        status BIGINT ,
        is_delete BIGINT
    >,
    PRIMARY KEY (prize_id) NOT ENFORCED
)WITH (
 'connector' = 'hbase-1.4',
 'table-name' = 'dim_hbase_pub_prize', -- 需要在hbase中手动创建表 create 'dim_hbase_pub_prize','info'
 'zookeeper.quorum' = 'master:2181,node1:2181,node2:2181'
);