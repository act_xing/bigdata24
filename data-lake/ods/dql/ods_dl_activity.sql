

-- mysql cdc表。用于数据采集，可以不保存元数据
CREATE TABLE ods_activity_cdc(
    activity_id BIGINT comment '主键',
    cid STRING comment '企业ID',
    name STRING comment '活动名称',
    type STRING comment '活动类型 BUBBLE：冒泡',
    start_time TIMESTAMP(3) comment '开始时间',
    end_time TIMESTAMP(3) comment '结束时间',
    is_default INT comment '',
    status INT comment '活动状态 0-草稿、1-待启用',
    is_delete INT comment '是否删除 0-未删除、1-删除',
    creator_id STRING comment '创建者id',
    nick_name STRING comment '创建者名称',
    update_user_id STRING comment '最后修改人id',
    update_nick_name STRING comment '最后修改人名称',
    gmt_create TIMESTAMP(3) comment '创建时间',
    gmt_modified TIMESTAMP(3) comment '更新时间',
    PRIMARY KEY (activity_id) NOT ENFORCED
) WITH (
    'connector' = 'mysql-cdc',
    'hostname' = 'master',
    'port' = '3306',
    'username' = 'root',
    'password' = '123456',
    'database-name' = 'trip',
    'table-name' = 'activity'
);

-- 实时从mysql中采集数据。将数据实时保存到数据湖中
insert into hive_catalog.lake_ods.ods_dl_activity
select * from ods_activity_cdc;