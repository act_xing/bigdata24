

-- mysql cdc表。用于数据采集，可以不保存元数据
CREATE TABLE ods_prize_cdc(
    prize_id BIGINT comment '奖品ID',
    cid STRING comment '企业ID',
    name STRING comment '奖品名称',
    type STRING comment '奖品类型',
    `module` BIGINT comment '奖品发放方式 0：智能 1手动',
    status BIGINT comment '奖品状态 0-停用、1-启用',
    is_delete BIGINT comment '时候删除',
    PRIMARY KEY (prize_id) NOT ENFORCED
) WITH (
    'connector' = 'mysql-cdc',
    'hostname' = 'master',
    'port' = '3306',
    'username' = 'root',
    'password' = '123456',
    'database-name' = 'trip',
    'table-name' = 'prize'
);

-- 实时从mysql中采集数据。将数据实时保存到数据湖中
insert into hive_catalog.lake_ods.ods_dl_prize
select * from ods_prize_cdc;