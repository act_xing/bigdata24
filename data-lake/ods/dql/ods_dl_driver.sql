

-- mysql cdc表。用于数据采集，可以不保存元数据
CREATE TABLE ods_driver_cdc(
    driver_id BIGINT comment '司机ID (主键)',
    name STRING comment '司机姓名',
    phone STRING comment '司机电话号码',
    email STRING comment '司机电子邮件',
    license_number STRING comment '司机驾驶执照号码',
    PRIMARY KEY (driver_id) NOT ENFORCED
) WITH (
    'connector' = 'mysql-cdc',
    'hostname' = 'master',
    'port' = '3306',
    'username' = 'root',
    'password' = '123456',
    'database-name' = 'trip',
    'table-name' = 'driver'
);


-- 实时从mysql中采集数据。将数据实时保存到数据湖中
insert into hive_catalog.lake_ods.ods_dl_driver
select * from ods_driver_cdc;