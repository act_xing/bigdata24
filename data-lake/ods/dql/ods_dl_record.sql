
-- mysql cdc表。用于数据采集，可以不保存元数据
CREATE TABLE ods_record_cdc(
    record_id BIGINT comment '流水编号',
    passenger_id BIGINT comment '乘客ID',
    activity_id BIGINT comment '活动ID',
    prize_id BIGINT comment '券ID',
    order_id BIGINT comment '订单ID',
    money decimal(164) comment '券金额',
    record_status BIGINT comment '券状态',
    is_delete BIGINT comment '时候删除',
    gmt_create TIMESTAMP(3) comment '创建时间',
    gmt_modified TIMESTAMP(3) comment '更新时间',
    PRIMARY KEY (record_id) NOT ENFORCED
) WITH (
    'connector' = 'mysql-cdc',
    'hostname' = 'master',
    'port' = '3306',
    'username' = 'root',
    'password' = '123456',
    'database-name' = 'trip',
    'table-name' = 'record'
);

-- 实时从mysql中采集数据。将数据实时保存到数据湖中
insert into hive_catalog.lake_ods.ods_dl_record
select * from ods_record_cdc;