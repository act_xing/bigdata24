
-- mysql cdc表。用于数据采集，可以不保存元数据
CREATE TABLE ods_passenger_cdc(
    passenger_id BIGINT comment '乘客ID (主键)',
    name STRING comment '乘客姓名',
    phone STRING comment '乘客电话号码',
    email STRING comment '乘客电子邮件',
    address STRING comment '乘客地址',
    PRIMARY KEY (passenger_id) NOT ENFORCED
) WITH (
    'connector' = 'mysql-cdc',
    'hostname' = 'master',
    'port' = '3306',
    'username' = 'root',
    'password' = '123456',
    'database-name' = 'trip',
    'table-name' = 'passenger'
);


-- 实时从mysql中采集数据。将数据实时保存到数据湖中
insert into hive_catalog.lake_ods.ods_dl_passenger
select * from ods_passenger_cdc;