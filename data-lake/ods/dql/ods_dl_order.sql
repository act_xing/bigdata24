

-- mysql cdc表。用于数据采集，可以不保存元数据
CREATE TABLE ods_order_cdc(
    order_id BIGINT comment '订单ID (主键)',
    passenger_id BIGINT comment '乘客ID (外键，关联乘客表)',
    driver_id BIGINT comment '司机ID (外键，关联司机表)',
    city STRING comment '城市',
    pickup_location STRING comment '起始地点',
    dropoff_location STRING comment '目的地点',
    estimated_amount decimal(10, 2) comment '预估金额',
    actual_amount decimal(10, 2) comment '实际支付金额',
    status BIGINT comment '订单状态',
    start_time TIMESTAMP(3) comment '开始时间',
    end_time TIMESTAMP(3) comment '结束时间',
    PRIMARY KEY (order_id) NOT ENFORCED
) WITH (
    'connector' = 'mysql-cdc',
    'hostname' = 'master',
    'port' = '3306',
    'username' = 'root',
    'password' = '123456',
    'database-name' = 'trip',
    'table-name' = 'order'
);


-- 实时从mysql中采集数据。将数据实时保存到数据湖中
insert into hive_catalog.lake_ods.ods_dl_order
select * from ods_order_cdc;