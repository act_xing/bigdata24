

-- mysql cdc表。用于数据采集，可以不保存元数据
CREATE TABLE ods_complaint_cdc(
    complaint_id BIGINT comment '投诉ID (主键)',
    order_id BIGINT comment '订单ID (外键，关联订单表)',
    passenger_id BIGINT comment '乘客ID (外键，关联乘客表)',
    driver_id BIGINT comment '司机ID (外键，关联司机表)',
    complaint_type STRING comment '投诉类型',
    description STRING comment '投诉描述',
    status BIGINT comment '投诉状态',
    `timestamp` TIMESTAMP(3) comment '投诉时间',
    PRIMARY KEY (complaint_id) NOT ENFORCED
) WITH (
    'connector' = 'mysql-cdc',
    'hostname' = 'master',
    'port' = '3306',
    'username' = 'root',
    'password' = '123456',
    'database-name' = 'trip',
    'table-name' = 'complaint'
);


-- 实时从mysql中采集数据。将数据实时保存到数据湖中
insert into hive_catalog.lake_ods.ods_dl_complaint
select * from ods_complaint_cdc;