

-- mysql cdc表。用于数据采集，可以不保存元数据
CREATE TABLE ods_review_cdc(
    review_id BIGINT comment '评价ID (主键)',
    order_id BIGINT comment '订单ID (外键，关联订单表)',
    passenger_id BIGINT comment '乘客ID (外键，关联乘客表)',
    driver_id BIGINT comment '司机ID (外键，关联司机表)',
    rating INT comment '评分',
    `comment` STRING comment '评价评论',
    `timestamp` TIMESTAMP(3) comment '评价时间',
    PRIMARY KEY (review_id) NOT ENFORCED
) WITH (
    'connector' = 'mysql-cdc',
    'hostname' = 'master',
    'port' = '3306',
    'username' = 'root',
    'password' = '123456',
    'database-name' = 'trip',
    'table-name' = 'review'
);


-- 实时从mysql中采集数据。将数据实时保存到数据湖中
insert into hive_catalog.lake_ods.ods_dl_review
select * from ods_review_cdc;