CREATE TABLE hive_catalog.lake_ods.ods_dl_review(
    review_id BIGINT comment '评价ID (主键)',
    order_id BIGINT comment '订单ID (外键，关联订单表)',
    passenger_id BIGINT comment '乘客ID (外键，关联乘客表)',
    driver_id BIGINT comment '司机ID (外键，关联司机表)',
    rating INT comment '评分',
    `comment` STRING comment '评价评论',
    `timestamp` TIMESTAMP(3) comment '评价时间',
    PRIMARY KEY (review_id) NOT ENFORCED
)WITH (
  'connector' = 'hudi',
  'path' = 'hdfs://master:9000/data/lake/ods/ods_dl_review',
  'read.streaming.enabled' = 'true',
  'read.start-commit' = '20210316134557',
  'read.streaming.check-interval' = '4'
);