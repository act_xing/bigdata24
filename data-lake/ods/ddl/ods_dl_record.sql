CREATE TABLE hive_catalog.lake_ods.ods_dl_record(
    record_id BIGINT comment '流水编号',
    passenger_id BIGINT comment '乘客ID',
    activity_id BIGINT comment '活动ID',
    prize_id BIGINT comment '券ID',
    order_id BIGINT comment '订单ID',
    money decimal(164) comment '券金额',
    record_status BIGINT comment '券状态',
    is_delete BIGINT comment '时候删除',
    gmt_create TIMESTAMP(3) comment '创建时间',
    gmt_modified TIMESTAMP(3) comment '更新时间',
    PRIMARY KEY (record_id) NOT ENFORCED
)WITH (
  'connector' = 'hudi',
  'path' = 'hdfs://master:9000/data/lake/ods/ods_dl_record',
  'read.streaming.enabled' = 'true',
  'read.start-commit' = '20210316134557',
  'read.streaming.check-interval' = '4'
);