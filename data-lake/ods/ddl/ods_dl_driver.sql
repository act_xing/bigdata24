CREATE TABLE hive_catalog.lake_ods.ods_dl_driver(
    driver_id BIGINT comment '司机ID (主键)',
    name STRING comment '司机姓名',
    phone STRING comment '司机电话号码',
    email STRING comment '司机电子邮件',
    license_number STRING comment '司机驾驶执照号码',
    PRIMARY KEY (driver_id) NOT ENFORCED
)WITH (
  'connector' = 'hudi',
  'path' = 'hdfs://master:9000/data/lake/ods/ods_dl_driver',
  'read.streaming.enabled' = 'true',
  'read.start-commit' = '20210316134557',
  'read.streaming.check-interval' = '4'
);