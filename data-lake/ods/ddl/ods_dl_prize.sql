CREATE TABLE hive_catalog.lake_ods.ods_dl_prize(
    prize_id BIGINT comment '奖品ID',
    cid STRING comment '企业ID',
    name STRING comment '奖品名称',
    type STRING comment '奖品类型',
    `module` BIGINT comment '奖品发放方式 0：智能 1手动',
    status BIGINT comment '奖品状态 0-停用、1-启用',
    is_delete BIGINT comment '时候删除',
    PRIMARY KEY (prize_id) NOT ENFORCED
)WITH (
  'connector' = 'hudi',
  'path' = 'hdfs://master:9000/data/lake/ods/ods_dl_prize',
  'read.streaming.enabled' = 'true',
  'read.start-commit' = '20210316134557',
  'read.streaming.check-interval' = '4'
);