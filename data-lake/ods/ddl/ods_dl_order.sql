CREATE TABLE hive_catalog.lake_ods.ods_dl_order(
    order_id BIGINT comment '订单ID (主键)',
    passenger_id BIGINT comment '乘客ID (外键，关联乘客表)',
    driver_id BIGINT comment '司机ID (外键，关联司机表)',
    city STRING comment '城市',
    pickup_location STRING comment '起始地点',
    dropoff_location STRING comment '目的地点',
    estimated_amount decimal(10, 2) comment '预估金额',
    actual_amount decimal(10, 2) comment '实际支付金额',
    status BIGINT comment '订单状态',
    start_time TIMESTAMP(3) comment '开始时间',
    end_time TIMESTAMP(3) comment '结束时间',
    PRIMARY KEY (order_id) NOT ENFORCED
)WITH (
  'connector' = 'hudi',
  'path' = 'hdfs://master:9000/data/lake/ods/ods_dl_order',
  'read.streaming.enabled' = 'true',
  'read.start-commit' = '20210316134557',
  'read.streaming.check-interval' = '4'
);