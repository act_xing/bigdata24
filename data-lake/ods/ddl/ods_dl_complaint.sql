CREATE TABLE hive_catalog.lake_ods.ods_dl_complaint(
    complaint_id BIGINT comment '投诉ID (主键)',
    order_id BIGINT comment '订单ID (外键，关联订单表)',
    passenger_id BIGINT comment '乘客ID (外键，关联乘客表)',
    driver_id BIGINT comment '司机ID (外键，关联司机表)',
    complaint_type STRING comment '投诉类型',
    description STRING comment '投诉描述',
    status BIGINT comment '投诉状态',
    `timestamp` TIMESTAMP(3) comment '投诉时间',
    PRIMARY KEY (complaint_id) NOT ENFORCED
)WITH (
  'connector' = 'hudi',
  'path' = 'hdfs://master:9000/data/lake/ods/ods_dl_complaint',
  'read.streaming.enabled' = 'true',
  'read.start-commit' = '20210316134557',
  'read.streaming.check-interval' = '4'
);

