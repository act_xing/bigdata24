package com.shujia.flink;

import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.catalog.Catalog;
import org.apache.flink.table.catalog.hive.HiveCatalog;

public class FlinkSqlRun {
    public static void main(String[] args) {
        //1、创建flink的执行环境
        EnvironmentSettings settings = EnvironmentSettings
                .newInstance()
                //设置执行模式
                .inStreamingMode()
                //.inBatchMode()
                .build();

        TableEnvironment tEnv = TableEnvironment.create(settings);

        // 创建hive catalog
        Catalog catalog = new HiveCatalog("hive_catalog", null, "/usr/local/soft/hive-1.2.1/conf");
        //注册到flink 环境中
        tEnv.registerCatalog("hive_catalog", catalog);

        //从参数中获取sql字符串
        String sqlStr = args[0];

        System.out.println("====================正在执行的sql语句=======================");
        System.out.println(sqlStr);
        System.out.println("====================正在执行的sql语句=======================");

        if (sqlStr.contains("STATEMENT")) {
            tEnv.executeSql(sqlStr);
        } else {
            //安装分号将sql字符串切分成一个 一个sql
            String[] sqls = sqlStr.split(";");

            //循环执行sql
            for (String sql : sqls) {
                if (!"".equals(sql.trim())) {
                    tEnv.executeSql(sql);
                }
            }
        }
    }
}
