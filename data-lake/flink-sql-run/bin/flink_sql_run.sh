
username=root
password=123456
database=flink
checkpoint_base=/flink/checkpoint/

# 需要执行的sql文件的路径
sql_path=$1

# 获取文件名,以文件名作为任务名称
job_name=`basename $sql_path`

# 从sql文件中获取sql
sql_str=`cat ${sql_path}`


# flink任务运行的模式
run_mode=$2

# yarn session集群id
app_id=$3

echo "${run_mode}"

if [ "${run_mode}" = "yarn-session" ]; then
  echo "使用yarn-session提交任务"
  FLINK_RUN="flink run -t yarn-session  -Dyarn.application.id=${app_id} "
else
  echo "使用yarn-per-job提交任务"
  FLINK_RUN="flink run -t yarn-per-job "
fi

# 从数据库中任务编号，如果数据库中有，说明不是第一次提交，那就需要基于checkpoint地址重启任务
old_job_id=`mysql -u${username} -p${password} -e "select job_id from ${database}.flink_job_id where job_name='${job_name}'" -N -s`


# 切换到脚本当前的路径，避免相对路径出错
shell_home="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ${shell_home}

if [ "${old_job_id}" = "" ] ;then
  # 任务id不存在第一提交
  echo "=================任务id不存在第一提交======================="
  # 执行sql
  exec_log=`${FLINK_RUN}  -c com.shujia.flink.FlinkSqlRun flink-sql-run-1.0.jar "${sql_str}"`
  # 打印执行日志
  echo "${exec_log}"
else
  echo "=================任务重启======================="
  # 如果id不为空，需要基于checkpoint恢复任务

  # 获取checkpoint路径
  checkpoint_path=`hdfs dfs -ls ${checkpoint_base}${old_job_id} | grep chk | awk '{print $8}'`

  # 执行sql
  exec_log=`${FLINK_RUN} \
  -s hdfs://master:9000/${checkpoint_path}  \
  -c com.shujia.flink.FlinkSqlRun flink-sql-run-1.0.jar "${sql_str}"`
  # 打印执行日志
  echo "${exec_log}"
fi


# 获取任务编号
new_job_id=`echo "${exec_log}" | grep JobID | awk '{print $7}'`

echo "${new_job_id}"

# 将任务编号保存到数据
mysql -u${username} -p${password} -e "replace into ${database}.flink_job_id values('${job_name}','${new_job_id}')"