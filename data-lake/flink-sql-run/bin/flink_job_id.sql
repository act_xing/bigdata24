
DROP TABLE IF EXISTS `flink_job_id`;
CREATE TABLE `flink_job_id` (
  `job_name` varchar(255) NOT NULL,
  `job_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`job_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
