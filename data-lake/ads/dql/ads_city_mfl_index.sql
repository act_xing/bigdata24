-- 订单状态 0 冒泡 1 呼叫 2 司机接单 3 乘客上车 4 载客 5 订单完成 6 订单支付，7 订单评价 ，8 司机取消，9 乘客取消
-- 冒发率：呼叫预估成功转化为呼叫的比率，即成功预估并发起呼叫的比率。

-- 可以将指标定义成视图给到bi
create view ads_city_mfl_index as
select
city,
toYYYYMMDD(start_time) as `day`,
sum(case when status >= 1 then 1.0 else 0.0 end)/count(1) as p
from
dws_ck_order_wide
group by
city,
toYYYYMMDD(start_time)




-- 实时统计每个活动的冒发率
select
activity_name,
round(sum(case when status >= 1 then 1.0 else 0.0 end)/count(1),3) as p
from
dws_ck_order_wide
where activity_name != ''
group by
activity_name


-- 计算呼叫率

select
city,
activity_name,
round(sum(case when status >= 2 then 1.0 else 0.0 end)/sum(case when status >= 1 then 1.0 else 0.0 end),3) as p
from
dws_ck_order_wide
where activity_name != ''
group by
city,
activity_name

--- 3.补贴率：出行公司发放的补贴券与实际支付金额之间的比率，用于衡量补贴的使用情况。


select
city,
toYYYYMMDD(start_time),
sum(money)/sum(actual_amount) as p
from
dws_ck_order_wide
where activity_name != ''
group by
city,
toYYYYMMDD(start_time);


-- 4.ASP（预估平均流水）：预估金额的平均值，表示每个呼叫的平均预估金额。

select
city,
activity_name,
toYYYYMMDD(start_time),
avg(estimated_amount),
sum(case when status >= 1 then 1.0 else 0.0 end)/count(1) as p
from
dws_ck_order_wide
where activity_name != ''
group by
city,
activity_name,
toYYYYMMDD(start_time)
