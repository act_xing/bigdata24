CREATE TABLE hive_catalog.lake_ads.ads_city_mfl_index (
    city STRING,
    `day` STRING,
    p DECIMAL(10,4),
    PRIMARY KEY (city,`day`) NOT ENFORCED -- flink会按照主键，对数据库中的数据进行更新
) WITH (
    'connector' = 'jdbc',
    'url' = 'jdbc:mysql://master:3306/trip_ads?useSSL=false&useUnicode=true&characterEncoding=UTF-8',
    'table-name' = 'ads_city_mfl_index', -- 需要手动创建
    'username' ='root',
    'password' ='123456'
);


CREATE TABLE ads_city_mfl_index (
    city varchar(10),
    `day` varchar(10),
    p DECIMAL(10,4),
    PRIMARY KEY (city,`day`)
) ;