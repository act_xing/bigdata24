package com.shujia.core;

import org.apache.spark.rdd.RDD
import org.apache.spark.util.LongAccumulator
import org.apache.spark.{SparkConf, SparkContext}

object Code34PageRank1 {

  def main(args: Array[String]): Unit = {

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")
    val sc = new SparkContext(sparkConf)

    val pageRankRDD: RDD[String] = sc.textFile("data/pageRank.txt")

    // 对文本关系进行切分处理
    val SourceRDD: RDD[(String, List[String])] = pageRankRDD.map {
      // C->A,B 获取到每一行数据 对其进行切分
      case line: String =>
        val splitRes: Array[String] = line.split("->")
        val currentPage: String = splitRes(0)
        val nextPages: List[String] = splitRes(1).split(",").toList
        (currentPage, nextPages)
    }
    var prPageRank: RDD[(String, List[String], Double)] = SourceRDD.map {
      // 当前页面  投票的页面列表  初识权重值 1.0
      // currentPage => C nextPages => [A,B]
      case (currentPage, nextPages) => (currentPage, nextPages, 1.0)
    }

    val stop = 10
    var cnt = 0

    while (cnt < stop) {

      val pageRankRes: RDD[(String, Double)] = prPageRank
        // 将初识值 1 平均分配给 nextPages 列表中的每一个页面
        .map {
          case (currentPage, nextPages, pr) =>
            val pageNum: Int = nextPages.size
            var avgPr = pr / pageNum
            // 计算得到平均权重后，将平均权重赋予给 要被投票的每一个页面 [A,B]
            val avgPrNextPage: List[(String, Double)] = nextPages.map {
              case page => (page, avgPr)
            }
            // 返回的是一个投票的列表 该列表为 [(A,0.5),(B,0.5)]
            avgPrNextPage
        }
        // 1.由于RDD中每一个数据avgPrNextPage 的类型是List 所以需要对其进行扁平化 将tuple取出
        .flatMap {
          case avgPrNextPage: List[(String, Double)] => avgPrNextPage
        }
        // 2.每个页面获得得分后，需要再对页面进行做分组统计得到最终页面的总分
        .reduceByKey(_ + _)
      // 得到第一轮计算的结果
      pageRankRes.foreach(println)
      // 将第一轮计算结果作为下一轮计算的开始数值

      // 将当前分值和SourceRDD的关系进行关联整合成
      val joinRes: RDD[(String, (Double, List[String]))] = pageRankRes.join(SourceRDD)
      // 对关联后的结果进行调整

      prPageRank = joinRes.map {
        case (currentPage, (pr, nextPages)) => (currentPage, nextPages, pr)
      }
      cnt += 1
      println(s"第${cnt}得到的结果")
    }

    /**
     * 第10轮结果
     * (B,1.3330078125)
     * (A,0.7607421875)
     * (C,1.5244140625)
     * (D,0.3818359375)
     * 第9轮结果
     * (B,1.333984375)
     * (A,0.763671875)
     * (C,1.521484375)
     * (D,0.380859375)
     *
     *
     */

  }
}
