package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code27Partition {
  def main(args: Array[String]): Unit = {


    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")
    sparkConf.set("spark.default.parallelism", "2")


    val sc = new SparkContext(sparkConf)

    /**
     * 1. 默认分区数 是根据读取文件的数量以及文件的大小决定的 受minPartitions影响
     */

    //    val stuRDD: RDD[String] = sc.textFile("data/words",minPartitions = 4)
    //    println(stuRDD.getNumPartitions)

    /**
     * 2. 设置默认值 spark.default.parallelism
     */

    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")
    println(stuRDD.getNumPartitions)

    /**
     * 3. 默认情况下，子RDD中的分区数等于父RDD中的分区数
     */

    val stuInfo: RDD[(String, (String, String, String, String))] = stuRDD.map {
      case oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        (splitRes(0), (splitRes(1), splitRes(2), splitRes(3), splitRes(4)))
    }
    println("map：", stuInfo.getNumPartitions)

    //    stuInfo.take(10)

    /**
     * 4.对应有Shuffle过程的算子，可以重新设置其RDD的数量
     */
    //    val groupByClazzRDD: RDD[(String, Iterable[(String, (String, String, String, String))])] = stuInfo.groupBy(
    //      _._2._4, numPartitions = 3
    //    )
    //    println("分区数：",groupByClazzRDD.getNumPartitions)

    //    groupByClazzRDD.take(2)


    /**
     * 5.手动设置分区数量
     * 方式1:通过repartition对当前分区进行重新设置，其过程会产生Shuffle
     * 可以动态增加对应的分区数量  也可以减少对应的分区数
     * 对应源码中实际上调用的也是coalesce coalesce(numPartitions, shuffle = true)
     *
     * 方式2：通过 coalesce 对当前的分区数进行调整 默认不会产生Shuffle过程
     * coalesce 默认情况下 不能增加分区数 可以实现减小分区数  该过程中没有产生Shuffle
     * coalesce 可以通过调整shuffle参数来实现增加分区数
     *
     * 注意：对于减小分区来说，尽量不去使用repartition 而使用 coalesce
     *
     */


    val repartitionRDD: RDD[(String, (String, String, String, String))] = stuInfo.repartition(1)
    repartitionRDD.take(10)

    println("repartitoin:", repartitionRDD.getNumPartitions)

    //    val coalesceRDD: RDD[(String, (String, String, String, String))] = stuInfo.coalesce(
    //      4
    //      ,shuffle = true
    //    )
    //    println("coalesceRDD:", coalesceRDD.getNumPartitions)

    //    coalesceRDD.take(10)


    /**
     * 分区数的优先级：
     * 默认分区数(读取文件) <  spark.default.parallelism < 手动设置(repartition,coalesce / groupBy等Shuffle类算子)
     */


    while (true) {}
  }
}
