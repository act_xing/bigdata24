package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code18Join {
  def main(args: Array[String]): Unit = {

    /**
     * 关联操作需要父RDD是一个KV形式的数据类型
     * Join:操作是一个内连接 当Key都存在时，结果才会返回，如果其中RDDKey不存在，那么结果不返回
     * leftOuterJoin: 左外连接  会保留左边RDD中的所有数据 当右边的RDD中未取到值时返回的是一个None 可以通过match对类型进行判断做对应的操作
     *
     */
    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val rdd: RDD[(String, Int)] = sc.parallelize(
      List(
        ("a", 1), ("c", 2), ("b", 3)
      )
    )

    val rdd2: RDD[(String, String)] = sc.parallelize(
      List(
        ("a", "a1"), ("c", "c1")
      )
    )

    rdd.join(rdd2).foreach(println)
    /*
关联结果
(a,(1,a1))
(b,(3,b1))
(c,(2,c1))
     */

    rdd.leftOuterJoin(rdd2).foreach(println)

    /**
     *  (a,(1,Some(a1)))
     *  (b,(3,None))
     *  (c,(2,Some(c1)))
     */

  }
}
