package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code22MapValues {
  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")
    val sc = new SparkContext(sparkConf)

    /**
     * mapValues:转换算子 是对RDD中所有的Value数据做函数计算
     */
    val rdd: RDD[(String, Int)] = sc.parallelize(
      List(("k1", 12), ("k2", 20), ("k3", 10))
    )

    rdd.mapValues {
      case value: Int => {
        value + value
      }
    }.foreach(println)

  }
}
