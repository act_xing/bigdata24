package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable.ListBuffer

object Code20coGroup {
  def main(args: Array[String]): Unit = {

    /**
     * coGroup:转换算子，需要作用在KV格式的RDD上 对相同的Key进行做全连接，将关联得到的结果保存在一个集合中
     */

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val rdd: RDD[(String, Int)] = sc.parallelize(
      List(
        ("a", 1), ("c", 2), ("b", 3)
      )
    )

    val rdd2: RDD[(String, String)] = sc.parallelize(
      List(
        ("a", "a1"), ("c", "c1"), ("c", "c2"), ("c", "c3")
      )
    )

    val value: RDD[(String, (Iterable[Int], Iterable[String]))] = rdd.cogroup(rdd2)
    value.map {
      case (key: String, (ite1: Iterable[Int], ite2: Iterable[String])) => {
        val tuples: ListBuffer[(String, Int, String)] = ListBuffer[(String, Int, String)]()
        for (elem1 <- ite1) {
          for (elem2 <- ite2) {
            tuples.append((key, elem1, elem2))
          }
        }
        tuples
      }
    }.flatMap {
      case list: ListBuffer[(String, Int, String)] => list
    }
      .foreach(println)
  }
}
