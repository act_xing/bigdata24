package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.io.Source

object Code21MapJoin {

  def main(args: Array[String]): Unit = {
    /**
     * MapJoin: 在SPark中并没有Mapjoin的算子，而 MapJoin 的作用是将一个大表和小表进行关联
     */

    // 在SPark中如何实现MapJOIN
    /**
     * 在SPark应用程序中 所有的RDD操作都是在Executor中执行的 所有其他非RDD的操作都是在Driver执行的
     */


    // 需求：通过Scala读取成绩数据，统计总分，之后再关联学生基本信息数据

    // 非分布式的应用程序，在Driver端执行的
    val stuScore: Map[String, Int] = Source
      .fromFile("data/score.txt")
      .getLines()
      .toList
      .map {
        case oneLine =>
          val splitRes = oneLine.split(",")
          (splitRes(0), splitRes(2))
      }.groupBy(_._1).map {
      case (id, list) => (id, list.map(_._2.toInt).sum)
    }

    stuScore.foreach(println)

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    /**
     * RDD中的计算逻辑是在Task中执行的 ，而执行过程中需要获取 stuScore Map集合中的数据，
     * 所以Driver会将当前集合分发到每个Task中进行缓存，从而避免了Shuffle过程
     */
    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")

    stuRDD.map {
      case oneLine =>
        val splitRes = oneLine.split(",")
        val totalScore: Int = stuScore.getOrElse(splitRes(0), 0)

        (splitRes(0), (splitRes(1), splitRes(2), splitRes(3), splitRes(4), totalScore))
    }.foreach(println)


    while (true) {

    }


  }
}
