package com.shujia.core

import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code31BroadCast2 {
  def main(args: Array[String]): Unit = {

    /**
     * 以MapJoin为例使用 BroadCast
     *
     */


    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")
    sparkConf.set("spark.default.parallelism", "2")
    val sc = new SparkContext(sparkConf)

    val scoRDD: RDD[String] = sc.textFile("data/score.txt")

    val groupByRes: RDD[(String, Iterable[(String, String)])] = scoRDD.map {
      case oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        (splitRes(0), (splitRes(1), splitRes(2)))
    }.groupByKey()

    val scoreInfo: RDD[(String, Int)] = groupByRes.map {
      case (id, ite) => {
        (id, ite.map(_._2.toInt).sum)
      }
    }

    // 将所有的RDD数据收集到Driver端，之后再将其进行广播变量


    val broadCast: Broadcast[Map[String, Int]] = sc.broadcast(scoreInfo.collect().toMap)

    // 在每个Executor中对应有一个stuScore 而不是每个Task中存在
    //


    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")
    println("分区数：", stuRDD.getNumPartitions)


    stuRDD.map {
      case oneLine =>
        val splitRes = oneLine.split(",")
        // 先将RDD中的Map集合去除
        //        val totalScore: Int = stuScore.getOrElse(splitRes(0), 0)

        // 通过.value获取广播变量的集合
        val totalScore: Int = broadCast.value.getOrElse(splitRes(0), 0)

        // 不能通过一个RDD在另外一个RDD内部进行调用，因为RDD不能进行做序列操作
        //        val totalScore: Int = scoreInfo.collect().toMap.getOrElse(splitRes(0), 0)


        (splitRes(0), (splitRes(1), splitRes(2), splitRes(3), splitRes(4), totalScore))
    }.foreach(println)

    while (true) {}
  }
}
