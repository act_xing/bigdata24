package com.shujia.core
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
object Code14SortBy {

  def main(args: Array[String]): Unit = {
    /**
     * SortBy:转换算子，可以对数据进行做排序操作，排序时，需要指定其排序规则
     * 如果存在多级排序，那么用于排序的数据可以为一个tuple 在Tuple中按位置进行依次排序
     *
     * SortByKey 根据KEY值进行排序操作，如果要多级排序，那么要求KEY是一个Tuple类型 按位置顺序依次排序
     *
     */

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")

    stuRDD.map {
      case oneStu: String =>
        val splitRes: Array[String] = oneStu.split(",")
        (splitRes(0), splitRes(1), splitRes(2), splitRes(3), splitRes(4))
    }
      .sortBy {
        case (id: String, name: String, age: String, gender: String, clazz: String) => {
          // 注意顺序
          (-age.toInt, id)
        }
      }
      .foreach(println)

    //    stuRDD.map{
    //      case oneStu:String =>
    //        val splitRes: Array[String] = oneStu.split(",")
    //        (splitRes(2),(splitRes(0),splitRes(1),splitRes(3),splitRes(4)))
    //    }
    //      .sortByKey(false).foreach(println)
  }
}
