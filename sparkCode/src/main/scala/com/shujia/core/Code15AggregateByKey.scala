package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code15AggregateByKey {
  def main(args: Array[String]): Unit = {

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")

    stuRDD.map {
      case oneStu: String =>
        val splitRes: Array[String] = oneStu.split(",")
        (splitRes(4), 1)
    }.reduceByKey {
      case (sumNum: Int, num: Int) => {
        sumNum + num
      }
    }.foreach(println)


    /**
     * def aggregateByKey[U: ClassTag](zeroValue: U)(seqOp: (U, V) => U,
     * combOp: (U, U) => U): RDD[(K, U)]
     *
     * zeroValue表示0值
     * seqOp: 表示区间内计算规则  相当于MapReduce中的Combine，在Map端进行Reduce端的计算逻辑
     * combOp: 表示区间外计算规则  相当于Reduce的计算逻辑
     *
     */
    stuRDD.map {
      case oneStu: String =>
        val splitRes: Array[String] = oneStu.split(",")
        (splitRes(4), 1)
    }.aggregateByKey(
      0
    )(
      (sumNum: Int, num: Int) => {
        sumNum + num
      },
      (sumNumPartition1: Int, sumNumPartition2: Int) => {
        sumNumPartition1 + sumNumPartition2
      }
    ).foreach(println)


  }
}
