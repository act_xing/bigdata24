package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}

object Code28Cache extends App {

  /**
   * 对于每个RDD中 如果存在有对一个父RDD存在有依赖关系，那么默认情况下 不会对其RDD的数据进行缓存，而是会重新计算
   * 如下过程中：textFile => map 会被执行3次
   *
   * 方式1：
   * cache 默认是将数据进行缓存到内存当中 persist(StorageLevel.MEMORY_ONLY)
   *
   * 方式2：
   * persist函数 该函数中可以对缓存的级别进行设置  StorageLevel中共有12中缓存方式 分别包括：内存 磁盘 序列化 副本 四个方面
   * 注意：
   * 对缓存的数据可以通过 unpersist 对其进行清除缓存  手动清除缓存有助于节约资源
   * 对于SPark中的缓存方式，如果当前应用程序结束，那么默认会对其缓存进行清空
   */


  val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")
  sparkConf.set("spark.default.parallelism", "2")

  val sc = new SparkContext(sparkConf)

  val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")

  val stuInfo: RDD[(String, (String, String, String, String))] = stuRDD.map {
    case oneLine: String =>
      //      println("当前map执行了1次") //不做任何操作的情况下 map执行了3000次
      val splitRes: Array[String] = oneLine.split(",")
      (splitRes(0), (splitRes(1), splitRes(2), splitRes(3), splitRes(4)))
  }

  // 对重复使用的RDD进行做cache缓存，当对数据进行缓存之后 整个 textFile及map 只执行一次流程
  //  stuInfo.cache()

  // MEMORY_AND_DISK 表示将数据缓存到内存和磁盘 当内存不足时，可以将溢出的数据写入至磁盘当中
  //  stuInfo.persist(StorageLevel.MEMORY_AND_DISK)

  // MEMORY_ONLY_2 表示数据存储在内存当中，并保存有2个副本数
  stuInfo.persist(StorageLevel.MEMORY_ONLY_2)

  //默认缓存方式为MEMORY_ONLY
  //  stuInfo.persist()

  // 1.统计各班级的人数
  private val clazzRDD: RDD[(String, Int)] = stuInfo.groupBy(_._2._4).map {
    case (clazz, list) =>
      (clazz, list.size)
  }


  // 2.统计各年龄段的人数
  private val ageRDD: RDD[(String, Int)] = stuInfo.groupBy(_._2._2).map {
    case (age, list) => (age, list.size)
  }

  // 3.统计性别人数
  private val genderRDD: RDD[(String, Int)] = stuInfo.groupBy(_._2._3).map {
    case (gender, list) => (gender, list.size)
  }

  private val unionRDD: RDD[(String, Int)] = genderRDD.union(ageRDD).union(clazzRDD)

  //  stuInfo.unpersist()
  unionRDD.collect().toList.foreach(println)


  //    .foreach(println)



  while (true) {}

}
