package com.shujia.core

import com.mysql.jdbc.Driver
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import java.sql.{Connection, DriverManager, PreparedStatement, ResultSet}

object Code24MysqlConOnSpark {
  def main(args: Array[String]): Unit = {

    /**
     * 连接MySQL将MySQL中的学生表数据和当前学生的成绩数据进行关联
     */

    // 注册驱动
    DriverManager.registerDriver(new Driver())

    //获取连接对象

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")
    val sc = new SparkContext(sparkConf)
    val scoRDD: RDD[String] = sc.textFile("data/score.txt", 2)

    scoRDD.map {
      case oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        (splitRes(0), (splitRes(1), splitRes(2)))

        val mysqlCon: Connection = DriverManager.getConnection(
          "jdbc:mysql://master:3306/student?useSSL=false&useUnicode=true&characterEncoding=utf8"
          , "root"
          , "123456"
        )
        val statement: PreparedStatement = mysqlCon.prepareStatement("select * from student")
        val resultSet: ResultSet = statement.executeQuery()
        var tuple: (Int, String, Int, String, String, String, String) = (0, "", 0, "", "", "", "")
        while (resultSet.next()) {
          val id: Int = resultSet.getInt("id")
          val name: String = resultSet.getString("name")
          val age: Int = resultSet.getInt("age")
          val gender: String = resultSet.getString("gender")
          val clazz: String = resultSet.getString("clazz")

          if (splitRes(0) == id.toString) {
            tuple = (id, name, age, gender, clazz, splitRes(1), splitRes(2))
          }
        }
        statement.close()
        mysqlCon.close()
        tuple
    }.foreach(println)


    scoRDD.mapPartitions {
      case iter => {

        // 对于每个分区中的迭代器创建一个MySQL连接
        val mysqlCon: Connection = DriverManager.getConnection(
          "jdbc:mysql://master:3306/student?useSSL=false&useUnicode=true&characterEncoding=utf8"
          , "root"
          , "123456"
        )
        val resultSet: ResultSet = mysqlCon.prepareStatement("select * from student").executeQuery()

        import scala.collection.mutable
        val stuMap: mutable.Map[String, (String, String, String, String)] = mutable.Map[String, (String, String, String, String)]()

        while (resultSet.next()) {
          val id: Int = resultSet.getInt("id")
          val name: String = resultSet.getString("name")
          val age: Int = resultSet.getInt("age")
          val gender: String = resultSet.getString("gender")
          val clazz: String = resultSet.getString("clazz")

          // 将结果保存到Map中
          stuMap.put(id.toString, (name, age.toString, gender, clazz))
        }

        // iter 中保存了当前分区中的所有学生成绩数据 iter是Scala中的一个集合
        // map 是Scala中的map函数不是RDD算子
        iter.map {
          case oneLine: String => {
            val splitRes: Array[String] = oneLine.split(",")
            val id: String = splitRes(0)
            // 从MySQL中获取对应ID的学生数据
            val matchRes: (String, String, String, String, String, String, String) = stuMap.get(id) match {
              case Some((name, age, gender, clazz)) => {
                (id, name, age, gender, clazz, splitRes(1), splitRes(2))
              }
            }
            matchRes
          }
        }
        //        iter.map 返回的结果依旧是一个迭代器，不需要再去做其他操作
      }
    }.foreach(println)

  }
}
