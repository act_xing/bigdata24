
package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.util.LongAccumulator
import org.apache.spark.{SparkConf, SparkContext}

import scala.util.Random

object Code33Pi {

  def main(args: Array[String]): Unit = {
    // 计算pi的值
    // 通过推导公式可以得到 π = 4 ( 落在圆内的随机点数 / 正方形的随机点数 )

    // 获取一个随机点数 由于坐标系图中表示 随机点在 (x,y)在([-1,1],[-1,1])

    // 由于随机值默认在0-1之间 那么需要扩展到 -1,1之间
    //    println(Random.nextDouble())
    //    println(Random.nextDouble() * 2 -1)

    // 通过该方式可以得到一个随机坐标点 在[-1,1]之间
    println((Random.nextDouble() * 2 - 1, Random.nextDouble() * 2 - 1))

    // 通过Spark产生更多的随机点
    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")
    val sc = new SparkContext(sparkConf)

    val taskNum = args.head.toInt
    val pointNum = 100 * 10000

    // 需要构建RDD，并且在RDD内部产生随机点
    val numRDD: RDD[Int] = sc.parallelize(
      1 to pointNum * taskNum, numSlices = taskNum
    )

    val inNum: Int = numRDD
      .map {
        // 根据生成的序列，产生对应的随机坐标
        case num: Int => (Random.nextDouble() * 2 - 1, Random.nextDouble() * 2 - 1)
      }
      //计算随机坐标是否落在圆内
      .map {
        case (x, y) =>
          if ((x * x + y * y) <= 1) {
            1
          } else {
            0
          }
      }.reduce(_ + _)

    println(inNum)

    println((inNum / (pointNum * taskNum).toDouble) * 4)


  }
}
