package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code25Action2 {

  def main(args: Array[String]): Unit = {

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")
    val sc = new SparkContext(sparkConf)
    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt", 2)


    /**
     * take:行动算子，从RDD的结果中取出固定数量的数据，将其结果保存成一个Array数组，该数组是在Driver端中存在，
     * 所以在Take执行时，会从Executor中取出对应的数据通过网络IO返回给Driver
     *
     * 如何判断代码是否在Driver端中执行？
     * 代码不在RDD中定义的 都会在Driver端中执行
     *
     */
    val takeRes: Array[(String, String, String, String, String)] = stuRDD.map {
      case oneLine =>
        val splitRes = oneLine.split(",")
        (splitRes(0), splitRes(1), splitRes(2), splitRes(3), splitRes(4))
    }.sortBy {
      case (id, name, age, gender, clazz) => (clazz, id.toInt)
    }.take(10)

    println(takeRes.toList)

    /**
     * 需求：
     * 关联学生的基本信息和总分数据，并求出每个班级中的前3名，将前3名数据获取到Driver端
     */

  }
}
