package com.shujia.core
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
/**
 * Spark ON Yarn 集群模式提交命令
 * spark-submit --class com.shujia.Code02SparkSubmit --master yarn-cluster --executor-memory 512m --num-executors 2 --executor-cores 1  sparkCode-1.0-SNAPSHOT.jar
 *
 * 当数据写出到HDFS中时，对应 --num-executors 2 那么HDFS中会产生两个执行结果
 *
 */
object Code02SparkSubmit {
  def main(args: Array[String]): Unit = {

    // 创建操作对象需要conf配置类的对象
    val conf = new SparkConf()
    // 设置当前任务的名称
    conf.setAppName("TestSubmit")
    // 设置运行模式
    //    conf.setMaster("local")  // 提交任务到Yarn中执行时，不需要添加setMaster 可以在命令参数中指定


    // 构建Spark的操作对象 常用变量名称为 sc
    val sc: SparkContext = new SparkContext(conf)

    // 做wordCount需求
    // 1. 读取文件 需要指定HDFS中的路径
    val words: RDD[String] = sc.textFile("/input/words.txt")


    val countRes: RDD[(String, Int)] = words.flatMap(_.split(" "))
      .map((_, 1))
      .groupBy(_._1)
      .map {
        case (word: String, num: Iterable[(String, Int)]) => {
          println("当前map函数执行了一次...")
          (word, num.size)
        }
      }
    //      .foreach(println)  // 每一个foreach都会产生一个job

    countRes.saveAsTextFile("/output/sparkCount")
  }
}
