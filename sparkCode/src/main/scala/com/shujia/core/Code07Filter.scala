package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code07Filter {
  def main(args: Array[String]): Unit = {

    /**
     * Filter: 转换算子 可以实现对RDD读取的数据进行过滤
     * 需要传入一个函数f 该函数的返回值类型为 布尔类型
     * 如果返回值为 True 那么保留数据
     * 如果返回值为 False 那么过滤数据
     *
     */

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")

    // stu 数据过滤 文科一班的所有学生

    //    stuRDD.filter(_.contains("文科一班")).foreach(println)

    stuRDD.map {
      case oneline: String =>
        val splitRes: Array[String] = oneline.split(",")
        (splitRes(0), splitRes(1), splitRes(2), splitRes(3), splitRes(4))
    }.filter(_._5 == "文科一班").foreach(println)

  }
}
