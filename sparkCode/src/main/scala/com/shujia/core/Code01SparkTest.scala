package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code01SparkTest {
  def main(args: Array[String]): Unit = {

    // 创建操作对象需要conf配置类的对象
    val conf = new SparkConf()
    // 设置当前任务的名称
    conf.setAppName("TestSpark")
    // 设置运行模式
    conf.setMaster("local")


    // 构建Spark的操作对象 常用变量名称为 sc
    val sc: SparkContext = new SparkContext(conf)

    // 做wordCount需求
    // 1. 读取文件

    val words: RDD[String] = sc.textFile("data/word.txt")


    words.flatMap(_.split(" "))
      .map((_, 1))
      .groupBy(_._1)
      .map {
        case (word: String, num: Iterable[(String, Int)]) => {
          println("当前map函数执行了一次...")
          (word, num.size)
        }
      }
      .foreach(println) // 每一个foreach都会产生一个job
    // 如果job不提交 那么在当前代码中的函数是不会执行的

    //    words.flatMap(_.split(" "))
    //      .map((_,1))
    //      .groupBy(_._1)
    //      .map{
    //        case (word:String,num:Iterable[(String,Int)]) => {
    //          (word,num.size)
    //        }
    //      }.foreach(println)

    // 要求win中有HADOOP的执行环境 winUtil.exe

    //    while (true){
    //
    //    }

  }
}
