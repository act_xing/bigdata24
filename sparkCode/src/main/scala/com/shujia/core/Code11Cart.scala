package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code11Cart {
  def main(args: Array[String]): Unit = {
    /**
     * cartesian:转换算子，可以对两个RDD产生笛卡尔积
     */

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val intList: List[Int] = List[Int](1, 2, 3, 4, 5)
    val stringList: List[String] = List[String]("a", "b", "c")

    val intRdd: RDD[Int] = sc.parallelize(intList)
    val stringRdd: RDD[String] = sc.parallelize(stringList)


    intRdd.cartesian(stringRdd).foreach(println)


  }
}
