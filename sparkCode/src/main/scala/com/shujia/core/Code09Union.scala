package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code09Union {
  def main(args: Array[String]): Unit = {
    /**
     * Union:转换算子，类似于SQL中的 UNION ALL 可以将两个RDD进行拼接
     * Union操作会对两个RDD中的分区数据进行合并放置一个RDD中，但默认并不会改变之前父RDD中的分区数，
     * 属于窄依赖不会产生Shuffle过程
     *
     */

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")

    val sampleRdd1: RDD[String] = stuRDD.sample(
      true,
      0.01,
      10
    )

    println("sampleRdd1.getNumPartitions:", sampleRdd1.getNumPartitions)
    val sampleRdd2: RDD[String] = stuRDD.sample(
      true,
      0.01,
      10
    )
    println("sampleRdd1.getNumPartitions:", sampleRdd2.getNumPartitions)
    val unionRes: RDD[String] = sampleRdd1.union(sampleRdd2)

    println("unionRes.getNumPartitions:", unionRes.getNumPartitions)

    unionRes
      .foreach(println)


  }
}
