package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
object Code06FlatMap {
  def main(args: Array[String]): Unit = {
    /**
     * FlatMap: 转换算子，需要传入一个函数 f
     * 从源码中可以看出 当前参数是一个函数，并且该函数的返回值类型是 TraversableOnce的子类 TraversableOnce[+A] = scala.collection.TraversableOnce[A]
     *
     * 作用：
     * 对返回值中的数据进行做扁平化操作
     *
     * 传入一条数据，返回多条数据
     *
     */


    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val fileRDD: RDD[String] = sc.textFile("data/words")

    fileRDD.flatMap(_.split(" ")).foreach(println)

  }
}
