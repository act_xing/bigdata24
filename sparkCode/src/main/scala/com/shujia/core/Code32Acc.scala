package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.util.LongAccumulator
import org.apache.spark.{SparkConf, SparkContext}

object Code32Acc {

  def main(args: Array[String]): Unit = {

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")
    sparkConf.set("spark.default.parallelism", "2")
    val sc = new SparkContext(sparkConf)

    // 对于全校人数进行做统计

    // sc.DoubleAccumulator 和 collectAccumulator 及 longAccumulator
    val stuNum: LongAccumulator = sc.longAccumulator("stuNum")

    var num = 0

    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")


    stuRDD.map {

      case oneStudent =>
        //        num += 1
        //        println("获取到当前人数：" + num)

        stuNum.add(1L)
        val splitRes: Array[String] = oneStudent.split(",")
        (splitRes(0), (splitRes(1), splitRes(2), splitRes(3), splitRes(4)))
    }
      // 由于take中只去了10条数据，那么整个RDD在做数据计算时，由于一条数据会执行整个血统中的所有计算逻辑
      //    当执行到第10条后，发现并不需要继续执行，那么整个流程结束
      .foreach(println)

    // 当Driver端定义一个变量时，在RDD内部使用，会将该变量发生给每一个Task进行使用，但是Task累加完之后，
    //   并不会将数据返回给Driver端  因为没有使用对应的算子，如task collect 等 可以对RDD中的数据进行拉取到Driver端

    println("最终统计人数：" + stuNum.value)

  }
}
