package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code13ReduceByKey {
  def main(args: Array[String]): Unit = {

    /**
     * ReduceByKey : 转换算子，要求父RDD中的数据类型是Tuple2,可以根据KEY进行分组，不需要再指定分组的依据
     *
     */

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")

    // 统计班级中的人数
    // 对于该操作返回的类型是 tuple5 不是tuple2类型，所以没有Key-Value概念
    //    val value: RDD[(String, String, String, String, String)] = stuRDD.map {
    //      case oneLine: String =>
    //        val splitRes: Array[String] = oneLine.split(",")
    //        (splitRes(0), splitRes(1), splitRes(2), splitRes(3), splitRes(4))
    //    }

    stuRDD
      .map {
        case oneLine: String =>
          val splitRes: Array[String] = oneLine.split(",")
          // 以班级作为Key 1作为Value
          (splitRes(4), 1)
      }
      //      .reduceByKey(
      //        _ + _
      //      ).foreach(println)
      .filter {
        case (clazz: String, num: Int) => clazz == "理科一班"
      }
      .reduceByKey {
        /*
        通过一下打印结果可以看出每次num1保存的是计算的统计结果值，num2是父RDD中对应的Value值 1
        并且num1一开始对应的值是从Value中取到的第一个值
(num1:,1)
(num2:,1)
(num1:,2)
(num2:,1)
(num1:,3)
(num2:,1)
         */
        case (sumNum: Int, value: Int) => {

          //          println("num1:",num1)
          //          println("num2:",num2)
          sumNum + value
        }
      }.foreach(println)


  }
}
