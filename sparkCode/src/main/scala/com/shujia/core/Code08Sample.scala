package com.shujia.core
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
object Code08Sample {
  def main(args: Array[String]): Unit = {

    /**
     * Sample:转换算子，对数据进行做采样 查看数据分布情况
     *
     *  def sample(
     *  withReplacement: Boolean,  -- 是否有放回  false表示不放回
     *  fraction: Double, -- 概率数  注意： 最终取到的结果 和 整体数量 * 概率值 相差不大 但并不一定相同
     *  seed: Long = Utils.random.nextLong)
     *
     */

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")

    val sampleRdd: RDD[String] = stuRDD.sample(
      true,
      0.01
    )

    sampleRdd.foreach(println)


  }
}
