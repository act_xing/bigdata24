package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code17AggregateByKey {

  def main(args: Array[String]): Unit = {

    // 有一批数据 需要再分区内做累计 分区间做最大值

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val rdd: RDD[(String, Int)] = sc.parallelize(
      List(
        ("a", 1), ("a", 2), ("b", 3)
        , ("b", 4), ("c", 5), ("c", 6)
      ), 2
    )
    println(rdd.getNumPartitions)

    rdd.foreach(println)

    rdd.aggregateByKey(0)(
      (sum: Int, value: Int) => {
        sum + value
      }
      , (sumPartition1: Int, sumPartition2: Int) => {
        math.max(sumPartition2, sumPartition1)
      }
    ).foreach(println)

  }

}
