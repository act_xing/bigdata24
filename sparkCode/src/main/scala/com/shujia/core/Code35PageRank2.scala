package com.shujia.core;

import org.apache.spark.rdd.RDD
import org.apache.spark.util.LongAccumulator
import org.apache.spark.{SparkConf, SparkContext}

object Code35PageRank2 {
  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")
    val sc = new SparkContext(sparkConf)

    val pageRankRDD: RDD[String] = sc.textFile("data/pageRank.txt")

    // 对文本关系进行切分处理
    val SourceRDD: RDD[(String, List[String])] = pageRankRDD.map {
      // C->A,B 获取到每一行数据 对其进行切分
      case line: String =>
        val splitRes: Array[String] = line.split("->")
        val currentPage: String = splitRes(0)
        val nextPages: List[String] = splitRes(1).split(",").toList
        (currentPage, nextPages)
    }
    var prPageRank: RDD[(String, List[String], Double)] = SourceRDD.map {
      // 当前页面  投票的页面列表  初识权重值 1.0
      // currentPage => C nextPages => [A,B]
      case (currentPage, nextPages) => (currentPage, nextPages, 1.0)
    }

    /**
     * 现在对页面的算法进行修正： 添加阻尼系数 q = 0.85  N 表示页面总数  L表示投票给下游的 页面数量
     * 计算分值的公式为  (1-q) / N + q * 求和(pr/L)
     *
     * 当上一次得到的结果和下一次得到的结果之间每个页面分值差总和小于 收敛系数 p = 0.0001 那么循环退出，得到最终的排名
     */
    val q = 0.85
    val N: Long = prPageRank.count()
    val p = 0.001
    var flag = true

    while (flag) {
      // 循环计算每次统计的结果

      val pageRankRes: RDD[(String, Double)] = prPageRank
        // 将初识值 1 平均分配给 nextPages 列表中的每一个页面
        .map {
          case (currentPage, nextPages, pr) =>
            val avgPage: Double = pr / nextPages.size
            val avgNextPages: List[(String, Double)] = nextPages.map {
              // 对每个页面的赋予平均得分
              case nextPage: String => (nextPage, avgPage)
            }
            avgNextPages
        }
        .flatMap {
          case avgNextPages: List[(String, Double)] => avgNextPages
        }
        // 对每个页面的得分进行分组统计
        .reduceByKey(_ + _)
        .map {
          // 修正后的计算逻辑
          case (page: String, pr: Double) => {
            //  (1-q) / N + q * 求和(pr/L)
            (page, (1 - q) / N + q * pr)
          }
        }

      // 在进行下一轮之前需要比较前后两次得分统计的结果
      val sumRes: Double = prPageRank
        .map {
          case (currentPage, nextPages, pr) => (currentPage, pr)
        }
        .join(pageRankRes)
        .map {
          // 对每个网页前后两次得到的PR值进行比较差额
          case (currentPage, (pr1, pr2)) => (currentPage, math.abs(pr1 - pr2))
        }
        // 对当前所有页面的PR差额进行统计其均值
        .map(_._2).sum()

      // 当平均差额 小于 阈值 那么认为前后两次计算得到的pageRank结果变化不大，那么退出循环得到最终的结果
      if (sumRes / N < p) {
        flag = false
        println("得到最终的结果为：")
        pageRankRes.foreach(println)
      }


      // 将RDD进行关联 准备进行下一次循环
      val joinRes: RDD[(String, (List[String], Double))] = SourceRDD.join(pageRankRes)

      // 得到下一轮的RDD
      prPageRank = joinRes.map {
        case (currentPage, (nextPages, pr)) => (currentPage, nextPages, pr)
      }

    }


  }
}
