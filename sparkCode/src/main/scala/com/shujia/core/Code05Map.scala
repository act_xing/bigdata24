package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
object Code05Map {
  def main(args: Array[String]): Unit = {
    /**
     * Spark中的算子可以分为两类：
     * 1.转换算子  RDD和RDD之间的转换
     * 2.行动算子  每个算子会触发一次Job
     * 转换算子 它是一个懒执行的 只有当存在有行动算子存在时，才会执行
     *
     * 如何从形式上区分？
     * 对于算子的返回值类型如果不是一个RDD那么就表示为一个行动算子
     */


    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val list: List[Int] = List[Int](1, 2, 3, 4, 5, 6)

    val listRDD: RDD[Int] = sc.parallelize(list)

    listRDD.map {
      case i: Int => {
        println("获取到一条数据...")
        i
      }
    }.foreach(println)


  }
}
