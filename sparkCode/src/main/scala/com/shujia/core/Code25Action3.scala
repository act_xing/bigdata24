package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code25Action3 {

  def main(args: Array[String]): Unit = {

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")
    val sc = new SparkContext(sparkConf)
    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt", 2)

    /**
     * collect:行动算子，将所有executor中所有的数据保存到 Driver 端中的Array中
     * collect 算子在开发过程中慎用 容易导致Driver端中内存不足
     *
     */
    val clazzRes: Array[(String, String, String, String, String)] = stuRDD.map {
      case oneLine =>
        val splitRes = oneLine.split(",")
        (splitRes(0), splitRes(1), splitRes(2), splitRes(3), splitRes(4))
    }.filter(
      _._5 == "文科一班"
    ).collect()
    //    clazzRes.toList.foreach(println)


    /**
     * count:行动算子，可以统计当前executor中一共有多少条数据
     */
    // 需求：统计某个班中学生的数量
    val num: Long = stuRDD.map {
      case oneLine =>
        val splitRes = oneLine.split(",")
        (splitRes(0), splitRes(1), splitRes(2), splitRes(3), splitRes(4))
    }.filter(
      _._5 == "文科一班"
    ).count()

    //    println(num)

    /**
     * reduce:Action算子 和 ReduceByKey的计算逻辑一致，但是返回的结果是单个值
     *
     */

    val tuple: (String, Int) = stuRDD.map {
      case oneLine =>
        val splitRes = oneLine.split(",")
        // 班级 年龄
        (splitRes(4), splitRes(2).toInt)
    }.filter(
      _._1 == "文科一班"
    ).reduce {
      case ((clazz, age), (valueClazz, valueAge)) => {
        (clazz, age + valueAge)
      }
    }
    println(tuple)


    /**
     * reduceByKeyLocally:Action算子 = reduceByKey + collect + toMap
     *
     */

    val clazzSumAge: collection.Map[String, Int] = stuRDD.map {
      case oneLine =>
        val splitRes = oneLine.split(",")
        // 班级 年龄
        (splitRes(4), splitRes(2).toInt)
    }.reduceByKeyLocally {
      case (sumAge, valueAge) => {
        sumAge + valueAge
      }
    }
    //    clazzSumAge.foreach(println)


    val clazzSumAge2: collection.Map[String, Int] = stuRDD.map {
      case oneLine =>
        val splitRes = oneLine.split(",")
        // 班级 年龄
        (splitRes(4), splitRes(2).toInt)
    }.reduceByKey {
      case (sumAge, valueAge) => {
        sumAge + valueAge
      }
    }.collect().toMap

    clazzSumAge2.foreach(println)

    /**
     * lookup:Action算子 根据Key取出指定Key对应的Value数据 并将其拉取到Driver端保存为一个集合
     */

    val list: List[Int] = stuRDD.map {
      case oneLine =>
        val splitRes = oneLine.split(",")
        // 班级 年龄
        (splitRes(4), splitRes(2).toInt)
    }.lookup("文科一班").toList
    println(list)


  }
}
