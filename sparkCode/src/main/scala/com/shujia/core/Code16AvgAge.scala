package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code16AvgAge {
  def main(args: Array[String]): Unit = {
    /**
     * groupBy/ groupByKey、reduceByKey、aggregateByKey 之间的比较
     *
     * groupByKye：没有预聚合操作，会将所有相同Key的数据发送到下游的分区中，再对数据进行做累加操作 网络IO较多 速度慢
     * groupBy:相比groupByKye可以指定分组的规则，可操作性较强，但是相比其他执行效率低
     * reduceByKey: 存在有预聚合操作，在Shuffle之前会在分区内进行数据的逻辑计算，之后将计算结果进行Shuffle再到下游的分区中再做聚合操作
     * aggregateByKey: 可以定义初始值，分区内计算规则和分区间计算规则 存在有预聚合操作，计算效率高 reduceByKey就是一个特殊的aggregateByKey
     *
     */


    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")

    // 各班级 平均年龄 =>  班级中所有年龄累加 / 班级人数
    stuRDD.map {
      case oneStu: String =>
        val splitRes: Array[String] = oneStu.split(",")
        (splitRes(4), (splitRes(2).toInt, 1))
    }.reduceByKey {
      case (sumTup: (Int, Int), valueTup: (Int, Int)) => {
        val ageSum: Int = sumTup._1 + valueTup._1
        val stuSum: Int = sumTup._2 + valueTup._2
        (ageSum, stuSum)
      }
    }.map {
      case (clazz: String, (ageSum: Int, stuSum: Int)) => (clazz, ageSum / stuSum.toDouble)
    }.foreach(println)


    stuRDD.map {
      case oneStu: String =>
        val splitRes: Array[String] = oneStu.split(",")
        //          value为一个Tuple 其中第一个值为年龄 第二个值为 人数1
        (splitRes(4), (splitRes(2).toInt, 1))
    }.aggregateByKey(
      // 初始值
      (0, 0)
    )(
      // 第一个匿名函数：在分区间内进行做数据计算
      (sumTup: (Int, Int), valueTup: (Int, Int)) => {
        val ageSum = sumTup._1 + valueTup._1
        val stuSum = sumTup._2 + valueTup._2
        // 将结果返回作为下一次计算的第一个参数列表值
        (ageSum, stuSum)
      }
      ,
      // 第二个匿名函数: 在分区间做数据计算
      (sumTup: (Int, Int), valueTup: (Int, Int)) => {
        val ageSum = sumTup._1 + valueTup._1
        val stuSum = sumTup._2 + valueTup._2
        // 将结果返回作为下一次计算的第一个参数列表值
        (ageSum, stuSum)
      }
    ).map {
      case (clazz: String, (ageSum: Int, stuSum: Int)) => (clazz, ageSum / stuSum.toDouble)
    }.foreach(println)


  }
}
