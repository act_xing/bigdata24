package com.shujia.core
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.io.Source
object Code30BroadCast {
  def main(args: Array[String]): Unit = {

    /**
     * 以MapJoin为例使用 BroadCast
     *
     * BroadCast 应用场景：
     * 对Driver端的变量进行做广播时，可以调用  调用的前提条件一般为 Executor数量远远小于 Task对应的数量
     *
     *
     */

    // 读取学生的成绩数据，并对每个学生的分数统计总分
    val stuScore: Map[String, Int] = Source
      .fromFile("data/score.txt")
      .getLines()
      .toList
      .map {
        case oneLine =>
          val splitRes = oneLine.split(",")
          (splitRes(0), splitRes(2))
      }.groupBy(_._1).map {
      case (id, list) => (id, list.map(_._2.toInt).sum)
    }

    //    stuScore.foreach(println)

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")
    sparkConf.set("spark.default.parallelism", "2")
    val sc = new SparkContext(sparkConf)

    // 在每个Executor中对应有一个stuScore 而不是每个Task中存在
    val broadCast: Broadcast[Map[String, Int]] = sc.broadcast(stuScore)


    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")
    println("分区数：", stuRDD.getNumPartitions)


    stuRDD.map {
      case oneLine =>
        val splitRes = oneLine.split(",")
        // 先将RDD中的Map集合去除
        //        val totalScore: Int = stuScore.getOrElse(splitRes(0), 0)

        // 通过.value获取广播变量的集合
        val totalScore: Int = broadCast.value.getOrElse(splitRes(0), 0)

        (splitRes(0), (splitRes(1), splitRes(2), splitRes(3), splitRes(4), totalScore))
    }.foreach(println)

    while (true) {}
  }
}
