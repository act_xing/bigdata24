package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code12GroupBy {

  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")

    // 统计班级中的人数
    stuRDD.map {
      case oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        (splitRes(0), splitRes(1), splitRes(2), splitRes(3), splitRes(4))
    }
      .groupBy(_._5)
      .map {
        case (clazz: String, value: Iterable[(String, String, String, String, String)]) => {
          (clazz, value.size)
        }
      }.foreach(println)

    // 班级中的平均年龄

    stuRDD.map {
      case oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        (splitRes(0), splitRes(1), splitRes(2), splitRes(3), splitRes(4))
    }
      .groupBy(_._5)
      .map {
        case (clazz: String, value: Iterable[(String, String, String, String, String)]) => {
          // 对于map算子中的函数操作 可以使用scala中的函数进行计算
          (clazz, value.map(_._3.toInt).sum / value.size.toDouble)
        }
      }.foreach(println)


  }
}
