package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code26TopN {
  def main(args: Array[String]): Unit = {

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")

    val scoRDD: RDD[String] = sc.textFile("data/score.txt")

    val groupByRes: RDD[(String, Iterable[(String, String)])] = scoRDD.map {
      case oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        (splitRes(0), (splitRes(1), splitRes(2)))
    }.groupByKey()

    val scoreInfo: RDD[(String, Int)] = groupByRes.map {
      case (id, ite) => {
        (id, ite.map(_._2.toInt).sum)
      }
    }


    val stuInfo: RDD[(String, (String, String, String, String))] = stuRDD.map {
      case oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        (splitRes(0), (splitRes(1), splitRes(2), splitRes(3), splitRes(4)))
    }

    //    val joinRes: RDD[(String, ((String, String, String, String), (String, String)))] = stuInfo.join(scoreRDD)

    // Option 中存在有两种类型 Some 和 None Some表示有关联到数据 None表示没有关联到数据
    val joinRes: RDD[(String, ((String, String, String, String), Option[Int]))] = stuInfo.leftOuterJoin(scoreInfo)


    val joinMapRes: RDD[(String, String, String, String, String, Int)] = joinRes.map {
      case (id: String, ((name, age, gender, clazz), scoreOp)) =>
        scoreOp match {
          case Some(score) =>
            (id, name, age, gender, clazz, score)
          case None =>
            (id, name, age, gender, clazz, 0)
        }
    }
    // 对名次进行操作
    // 需要对班级进行分组操作

    val clazzGroupBy: RDD[(String, Iterable[(String, String, String, String, String, Int)])] = joinMapRes.groupBy(
      _._5
    )

    val scorePmRdd: RDD[List[(String, String, String, String, String, Int, Int)]] = clazzGroupBy.map {
      // 对相同班级中的数据进行组内排名
      case (clazz, ite) =>
        var pm = 0
        val list: List[(String, String, String, String, String, Int, Int)] = ite.toList.sortBy(-_._6).map {
          case (id, name, age, gender, clazz, score) =>
            pm += 1
            (id, name, age, gender, clazz, score, pm)
        }
        list.take(3)
    }


    val flatMap: RDD[(String, String, String, String, String, Int, Int)] = scorePmRdd.flatMap {
      case list => list
    }

    flatMap.collect().toList.foreach(println)

    //    val pmResList: List[(String, String, String, String, String, Int, Int)] = flatMap
    //      .filter(
    //        _._7 <= 3
    //      ).collect().toList
    //
    //
    //    pmResList.foreach(println)


  }
}
