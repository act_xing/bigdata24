package com.shujia.core
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.SparkConf

import scala.io.Source

/**
 *
 *
 */
object Code03RDDTrait {
  def main(args: Array[String]): Unit = {

    // 创建操作对象需要conf配置类的对象
    val conf = new SparkConf()
    // 设置当前任务的名称
    conf.setAppName("TestSubmit")
    // 设置运行模式
    conf.setMaster("local[4]")

    /**
     *
     * RDD的五大特性：
     *    1. A list of partitions 由一系列分区组成 分区就是一个并发
     *       分区数或并发数，其默认值受哪些因素影响
     *       1.修改读取文件的数量  可以修改分区数
     *       FileInputFormat => 可以获取数据的切片 => 1个切片对应一个分区 => 切片数是由文件数和文件大小决定的
     *       2.设定最小分区数 （当前任务运行时最小的分区数）
     *       3.在Spark提交时设置executor的数量
     *    2. A function for computing each split 函数是作用在每一个分区上的
     *       当数据被切分成多个切片时，每个切片默认对应一个分区，每个分区中的数据都会执行当前stage中的每一个函数
     *
     * 3. A list of dependencies on other RDDs RDD之间是有一系列的依赖关系
     * 3.1一系列的依赖关系，形成了数据的处理逻辑（DAG有向无环图）
     * 3.2 并不是所有的算子都会返回一个RDD 能返回RDD的算子 称为 转换算子 返回值为RDD 不能返回RDD的称为行动算子
     * 行动算子的源码中对当前的Job进行了提交，所以可以形成一个Job
     *
     * 4. Optionally, a Partitioner for key-value RDDs 可选项，分区器是作用在KV形式的RDD上
     * KV形式RDD表示为RDD中存储数据的类型为 tuple2(key,value)
     *
     * 5.Optionally, a list of preferred locations to compute each split on Spark会给每个执行任务提供最佳的计算位置  移动计算不移动数据
     * 如果当前数据存储在HDFS中的node1节点，那么Spark在计算时，会将executor分配到node1节点上运行
     *
     */

    val sc: SparkContext = new SparkContext(conf)


    /**
     * RDD的描述
     * ① 通过读取文件可以返回一个 RDD 该RDD是一个抽象类
     * ② RDD中的泛型对应是该RDD中操作数据的数据类型
     * ③ RDD中并不存储具体的数据，存储是数据处理的逻辑 （DAG有向无环图）
     * ④ RDD中封装的处理逻辑不可变，如果需要变更，那么需要产生新的RDD
     *
     * RDD=> RDD又称为弹性分布式数据集
     * 弹性：
     * 1.可以存储数据到内存，也可以存储到磁盘
     * 2.具有一定的容错性，数据发生错误，会自动重试
     * 3.数据可以按需求进行做分片(Split)
     * 分布式: 数据存储在不同节点上，计算也可以在不同节点上
     *
     * RDD中的算子是针对每一个数据做整个数据逻辑的处理
     * Scala中的函数是针对所有数据执行每个函数
     *
     *
     */
    //
    //

    val list: List[String] = Source
      .fromFile("data/words/word.txt")
      .getLines().toList

    list
      .flatMap(_.split(" "))
      .map((_, 1))
      .groupBy(_._1)

      /*
当前Scala中map函数执行了一次...
当前Scala中map函数执行了一次...
当前Scala中map函数执行了一次...
当前Scala中map函数执行了一次...
(scala,5)
(world,2)
(java,2)
(hello,3)
       */
      .map {
        case (word: String, num: Iterable[(String, Int)]) => {
          println("当前Scala中map函数执行了一次...")
          (word, num.size)
        }
      }
      .foreach(println)

    /**
     * def textFile(
     * path: String,
     * minPartitions: Int = defaultMinPartitions => 默认值参数  defaultMinPartitions  => 默认值为 1
     * 最小分区数为 1
     * )
     */
    val words: RDD[String] = sc.textFile("data/words", minPartitions = 4)
    println(words.getNumPartitions)

    val flatMapRDD: RDD[String] = words.flatMap(_.split(" "))
    println(flatMapRDD.getNumPartitions)
    val mapRdd: RDD[(String, Int)] = flatMapRDD.map((_, 1))
    println(mapRdd.getNumPartitions)
    val groupByRDD: RDD[(String, Iterable[(String, Int)])] = mapRdd.groupBy(_._1)
    println(groupByRDD.getNumPartitions)
    val resRDD: RDD[(String, Int)] = groupByRDD

      /*
当前Spark中map函数执行了一次...
(scala,5)
当前Spark中map函数执行了一次...
(hello,3)
当前Spark中map函数执行了一次...
(java,2)
当前Spark中map函数执行了一次...
(world,2)
       */
      .map {
        case (word: String, num: Iterable[(String, Int)]) => {
          println("当前Spark中map函数执行了一次...")
          (word, num.size)
        }
      }
    resRDD.foreach(println) // 每一个foreach都会产生一个job
    resRDD.foreach(println)
    //
    // 在stage1中对数据进行写出操作，输出的文件是由三个，并且并发数是有3个，表示一个并发对应一个结果文件
    //    resRDD.saveAsTextFile("output/sparkCount")
    while (true) {}
  }
}
