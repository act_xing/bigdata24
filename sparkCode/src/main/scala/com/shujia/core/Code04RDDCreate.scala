package com.shujia.core
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
object Code04RDDCreate {

  def main(args: Array[String]): Unit = {

    /**
     * 构建RDD的方式：
     * 1.通过读取文件来获取RDD
     * 2.通过集合构建RDD
     * *3.通过RDD来返回RDD
     */
    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    // 通过读取文件获取RDD
    val fileRDD: RDD[String] = sc.textFile("data/words")
    fileRDD.foreach(println)

    val list: List[Int] = List[Int](1, 2, 3, 4, 5, 6)

    val listRDD: RDD[Int] = sc.parallelize(list)
    listRDD.foreach(println)


  }
}
