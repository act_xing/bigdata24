package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code10distinct {
  def main(args: Array[String]): Unit = {
    /**
     * distinct:转换算子，可以对算子处理的数据进行做去重操作 去重过程中会产生Shuffle过程 所以会切分成多个Stage 属于宽依赖
     *
     */

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")

    val sampleRdd1: RDD[String] = stuRDD.sample(
      true,
      0.01,
      10
    )

    println("sampleRdd1.getNumPartitions:", sampleRdd1.getNumPartitions)
    val sampleRdd2: RDD[String] = stuRDD.sample(
      true,
      0.01,
      10
    )
    println("sampleRdd1.getNumPartitions:", sampleRdd2.getNumPartitions)
    val unionRes: RDD[String] = sampleRdd1.union(sampleRdd2)

    println("unionRes.getNumPartitions:", unionRes.getNumPartitions)


    // 宽依赖
    val distinctRes: RDD[String] = unionRes
      .distinct(1)

    println("distinctRes.getNumPartitions:", distinctRes.getNumPartitions)

    distinctRes
      .foreach(println)
    while (true) {

    }


  }
}
