package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code13GroupByKey {
  def main(args: Array[String]): Unit = {

    /**
     * GROUPBYKEY : 转换算子，要求父RDD中的数据类型是Tuple2,可以根据KEY进行分组，不需要再指定分组的依据
     *
     */

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")

    // 统计班级中的人数
    // 对于该操作返回的类型是 tuple5 不是tuple2类型，所以没有Key-Value概念
    //    val value: RDD[(String, String, String, String, String)] = stuRDD.map {
    //      case oneLine: String =>
    //        val splitRes: Array[String] = oneLine.split(",")
    //        (splitRes(0), splitRes(1), splitRes(2), splitRes(3), splitRes(4))
    //    }

    val groupByKeyRes: RDD[(String, Iterable[(String, String, String, String)])] = stuRDD
      .map {
        case oneLine: String =>
          val splitRes: Array[String] = oneLine.split(",")
          // 以班级作为Key 其他信息做为Value
          (splitRes(4), (splitRes(1), splitRes(2), splitRes(3), splitRes(0)))
      }
      .groupByKey()
    groupByKeyRes
      .map {
        case (clazz: String, value: Iterable[(String, String, String, String)]) => {
          (clazz, value.size)
        }
      }.foreach(println)


  }
}
