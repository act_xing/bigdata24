package com.shujia.core

import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
object Code29CheckPoint {

  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    /**
     * checkpoint:会对RDD构建检查点，会单独形成一个job,在该JOB中会将之前checkpoint的父RDD流程重新执行一次
     * 一般情况下，如果要对数据进行checkpoint会在之前对其进行做cache缓存，将父RDD中的数据缓存后，在checkpoint中不需要再重复计算
     *
     */

    val sc = new SparkContext(sparkConf)

    // 如果是local模式那么会存储到本地文件系统中，如果是集群模式standalone 和 sparkOnYarn 那么会将数据持久化到HDFS磁盘中
    sc.setCheckpointDir("checkpoint/data")

    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")

    val stuInfo: RDD[(String, (String, String, String, String))] = stuRDD.map {
      case oneLine: String =>
        println("当前map执行了1次") //不做任何操作的情况下 map执行了4000次
        val splitRes: Array[String] = oneLine.split(",")
        (splitRes(0), (splitRes(1), splitRes(2), splitRes(3), splitRes(4)))
    }

    // 1.统计各班级的人数
    val clazzRDD: RDD[(String, Int)] = stuInfo.groupBy(_._2._4).map {
      case (clazz, list) =>
        (clazz, list.size)
    }

    // 将数据保存到磁盘当中 需要给定一个磁盘路径
    // checkpoint
    stuInfo.cache()
    stuInfo.checkpoint()

    // 2.统计各年龄段的人数
    val ageRDD: RDD[(String, Int)] = stuInfo.groupBy(_._2._2).map {
      case (age, list) => (age, list.size)
    }

    // 3.统计性别人数
    val genderRDD: RDD[(String, Int)] = stuInfo.groupBy(_._2._3).map {
      case (gender, list) => (gender, list.size)
    }

    val unionRDD: RDD[(String, Int)] = genderRDD.union(ageRDD).union(clazzRDD)

    //  stuInfo.unpersist()
    unionRDD.collect().toList.foreach(println)


    while (true) {}
  }
}
