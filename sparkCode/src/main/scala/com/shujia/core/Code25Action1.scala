package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import java.sql.{Connection, DriverManager, PreparedStatement}

object Code25Action1 {

  def main(args: Array[String]): Unit = {

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")
    val sc = new SparkContext(sparkConf)
    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt", 2)

    /**
     * foreach:行动算子，将所有的数据依次进行遍历  没有任何返回值
     */
    //    stuRDD.map{
    //      case oneLine =>
    //        val splitRes = oneLine.split(",")
    //        (splitRes(0),(splitRes(1),splitRes(2),splitRes(3),splitRes(4)))
    //    }.foreach(println)
    //
    //    println("当前分区数:",stuRDD.getNumPartitions)

    /**
     * foreachPartition:行动算子，将一个分区中的所有数据进行遍历 一般来说经常在数据写出时调用
     */
    stuRDD.map {
      case oneLine =>
        val splitRes = oneLine.split(",")
        (splitRes(0), splitRes(1), splitRes(2), splitRes(3), splitRes(4))
    }.foreachPartition {
      // 要求将所有的数据写入至MySQL中  => 和 mapPartition 类似
      case iter: Iterator[(String, String, String, String, String)] =>

        val mysqlCon: Connection = DriverManager.getConnection(
          "jdbc:mysql://master:3306/student?useSSL=false&useUnicode=true&characterEncoding=utf8"
          , "root"
          , "123456"
        )
        println("有一个MySQL连接创建成功....")
        val statement: PreparedStatement = mysqlCon.prepareStatement("insert into student (id,name,age,gender,clazz) values (?,?,?,?,?)")

        // 通过foreach函数对数据进行遍历操作，将遍历到的数据写入至MySQL中
        iter.foreach {
          case (id, name, age, gender, clazz) => {
            // 获取每条数据后，将数据填充至SQL中
            statement.setInt(1, id.toInt)
            statement.setString(2, name)
            statement.setInt(3, age.toInt)
            statement.setString(4, gender)
            statement.setString(5, clazz)

            /**
             * 执行SQL语句
             * 每一条数据执行一次SQL，运算量较大，在实际开发过程中不推荐使用
             */
            //            val exeNum: Int = statement.executeUpdate()
            //            println(s"有${exeNum}条数据插入成功")

            // 将一条SQL语句添加到缓存中 可以批量执行
            statement.addBatch()
          }
        }
        // 执行一批SQL语句
        val exeRes: Array[Int] = statement.executeBatch()
        println(s"有${exeRes.size}条数据插入成功")

        /**
         * 批量执行SQL语句除了使用 executeBatch 方式还可以使用 字符串拼接
         * insert into student (id,name,age,gender,clazz) values
         * (1500100001,'施笑槐'	,22	,'女',	'文科六班'),
         * (1500100002,'吕金鹏'	,24	,'男',	'文科六班')
         *
         * 该方式在MySQL中比executeBatch效率更高
         */

        // 关闭连接
        statement.close()
        mysqlCon.close()

    }


  }
}
