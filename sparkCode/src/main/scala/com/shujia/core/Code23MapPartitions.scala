package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code23MapPartitions {

  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")
    val sc = new SparkContext(sparkConf)

    val rdd: RDD[(String, Int)] = sc.parallelize(
      List(
        ("a", 1), ("a", 2), ("b", 3)
        , ("b", 4), ("c", 5), ("c", 6)
      ), 2
    )

    /**
     * mapPartition:转换算子，将当前分区中的所有数据打包成一个 iterator,
     * 再对当前的iterator进行逻辑计算，之后再返回一个 iterator
     * 和Map不同的是，该算子对一个分区内所有的数据进行一次调用
     */
    rdd.mapPartitions {
      case iter =>
        val list: List[(String, Int)] = iter.toList
        val res: Map[String, Int] = list.groupBy(_._1).map {
          case (key, list) => (key, list.map(_._2).sum)
        }
        res.toIterator
    }

      .foreach(println)

  }
}
