package com.shujia.core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Code19JoinStudent {
  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")

    // 统计班级中的人数
    val stuInfo: RDD[(String, (String, String, String, String))] = stuRDD.map {
      case oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        (splitRes(0), (splitRes(1), splitRes(2), splitRes(3), splitRes(4)))
    }

    val scoRDD: RDD[String] = sc.textFile("data/score.txt")
    val scoreRDD: RDD[(String, (String, String))] = scoRDD.map {
      case oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        (splitRes(0), (splitRes(1), splitRes(2)))
    }.filter(_._1 != "1500100980")

    // (1500100980,((霍谷槐,22,女,理科一班),Some((1000001,33))))
    //    stuInfo.leftOuterJoin(scoreRDD).foreach(println)
    stuInfo.leftOuterJoin(scoreRDD).map {
      case (id: String, ((name: String, age: String, gender: String, clazz: String), op: Option[(String, String)])) => {
        // match 模式匹配是有返回值的
        op match {
          case Some((obj_id: String, score: String)) =>
            // match中最后一行的数据就是返回值
            (id, name, age, gender, clazz, obj_id, score)
          case None => {
            (id, name, age, gender, clazz, "未知", 0)
          }
        }
      }
    }.foreach(println)

    while (true) {}

  }
}
