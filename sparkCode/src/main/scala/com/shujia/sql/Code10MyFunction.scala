package com.shujia.sql

import org.apache.spark.sql.expressions.{UserDefinedFunction, Window}
import org.apache.spark.sql.{DataFrame, SparkSession}

import java.text.{DateFormat, FieldPosition, ParsePosition, SimpleDateFormat}
import java.util.Date

object Code10MyFunction {
  def main(args: Array[String]): Unit = {

    // 对于连续登录

    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .config("spark.sql.shuffle.partitions", 2)
      .getOrCreate()

    val dataDf: DataFrame = spark
      .read
      .format("csv")
      .option("sep", ",")
      .schema("id String,datestr String,amount Double")
      .load("data/continue.txt")
    import org.apache.spark.sql.functions._
    // 使用列表达式
    import spark.implicits._

    dataDf.show()

    // 针对DSL: 不能在Spark.sql中使用
    // 格式 val 函数名 = udf(匿名函数)
    val myDateSub: UserDefinedFunction = udf(
      (rq:String,days:Int) => {
        // 创建对应的时间格式
        val dateFormat = new SimpleDateFormat("yyyy-MM-dd")
        // 对字符串进行解析获取日期对象 再取得时间戳
        val date: Date = dateFormat.parse(rq)
        dateFormat.format(date.getTime - days * 24 * 3600 * 1000)
      }
    )


    val rnResDf: DataFrame = dataDf
      .groupBy($"id", $"datestr")
      .agg(sum($"amount") as "day_amount")
      // 对数据进行按ID分区，按日期进行排序 得到其排名 之后再去将 日期减去排名 得到结果一致 那么说明这段时间连续
      .withColumn("rn", row_number() over Window.partitionBy("id").orderBy("datestr"))

    rnResDf
      // 对于date_sub函数来说 不能传入一个列进行计算，只能传入一个固定的Int类型天数
      // 对于date_sub函数不能使用，所以只能自定义函数
      .select($"id", $"datestr", $"day_amount", myDateSub($"datestr", $"rn") as "flagDate")
//      .show()


    rnResDf.createOrReplaceTempView("rnResTbl")


    // 通过Spark.sql方式注册自定义函数
    spark.udf.register(
      "myDateSub",(rq:String,days:Int) => {
        // 创建对应的时间格式
        val dateFormat = new SimpleDateFormat("yyyy-MM-dd")
        // 对字符串进行解析获取日期对象 再取得时间戳
        val date: Date = dateFormat.parse(rq)
        dateFormat.format(date.getTime - days * 24 * 3600 * 1000)
      }
    )

    spark.sql(
      """
        |
        |SELECT
        |id
        |,datestr
        |,day_amount
        |,myDateSub(datestr, rn) as flagDate
        |FROM rnResTbl
        |
        |""".stripMargin).show()

//    println(test("2019-02-15",1))





  }



  def test (rq:String,days:Int) = {
      // 创建对应的时间格式
      val dateFormat = new SimpleDateFormat("yyyy-MM-dd")
      // 对字符串进行解析获取日期对象 再取得时间戳
      val date: Date = dateFormat.parse(rq)

      dateFormat.format(date.getTime - days * 24 * 3600 * 1000)
  }
}
