package com.shujia.sql

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD


object Code01RDDPerson {

  def main(args: Array[String]): Unit = {

    val sparkConf: SparkConf = new SparkConf().setAppName("createRDD").setMaster("local")

    val sc = new SparkContext(sparkConf)

    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")

    // 统计班级中的人数
    val studentRDD: RDD[StudentRDD2] = stuRDD.map {
      case oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        // 将切分后的数据包装成学生对象

        new StudentRDD2(splitRes(0), splitRes(1), splitRes(2).toInt, splitRes(3), splitRes(4))
    }


    studentRDD.groupBy {
      case stu: StudentRDD2 => stu.clazz
    }.map {
      case (clazz, stuList) => (clazz, stuList.size)
    }.foreach(println)


  }

}

// 如果RDD需要使用一个自定义的类，那么需要对当前类进行做序列化操作
class StudentRDD(id: String, name: String, age: Int, gender: String, clazz: String) extends Serializable {
  var _id: String = id;
  var _name: String = name
  var _age: Int = age
  var _gender: String = gender
  var _clazz: String = clazz
}

// 对于样例类来说默认实现了序列化操作
case class StudentRDD2(id: String, name: String, age: Int, gender: String, clazz: String)