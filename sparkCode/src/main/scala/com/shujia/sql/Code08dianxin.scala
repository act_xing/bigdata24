package com.shujia.sql

import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.{Column, DataFrame, SparkSession}

object Code08dianxin {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .config("spark.sql.shuffle.partitions", 2)
      .getOrCreate()

    val dataDf: DataFrame = spark
      .read
      .format("csv")
      .option("sep", ",")
      // 853101,2010,100200,25002,19440,20550,14990,17227,40990,28778,19088,29889,10990,20990
      .schema("mdn String, grid_id String, city_id String, county_id String, duration Int, start_time String, end_time String, pt String")
      .load("data/dianxin.csv")

    dataDf.show(truncate = false)

    // 1.统计每座城市停留时间超过两小时的游客人数
    /**
     * 1.对每个人在每个城市对停留时间进行累计 之后再去做数据的过滤
     */

    import org.apache.spark.sql.functions._
    // 使用列表达式
    import spark.implicits._


    dataDf
      .groupBy($"mdn",$"city_id")
      .agg(sum($"duration") as "sum_duration")
      // 过滤时间大于2小时
      .where($"sum_duration" >= 2 * 60)
      // 对省 城市 进行分组 求游客人数
      .groupBy($"city_id")
      .agg(count("*") as "cun_num")
      .show()



    // 2、统计每座城市停留时间最久的前三个区县
    //      [city_id, county_id, duration, rn]

    /**
     * 1.对城市 区县 累计其duration 再对停留时间进行做排名
     */

    dataDf
      .groupBy($"city_id",$"county_id")
      .agg(sum($"duration") as "sum_duration")
      .withColumn("rn",row_number() over Window.partitionBy($"city_id").orderBy($"sum_duration".desc))
      // isNotNull 表示对空值进行过滤 注意需要使用.isNotNull
      .where($"rn" <= 3 and $"city_id".isNotNull)
      .show()


    /**
     * 3、统计每座城市游客人数最多的前三个区县
     */


    dataDf
      .groupBy($"mdn",$"city_id",$"county_id")
      .agg(sum($"duration") as "sum_duration")
      // 过滤时间大于2小时
      .where($"sum_duration" >= 2 * 60)
      // 对城市 进行分组 求游客人数
      .groupBy($"city_id",$"county_id")
      .agg(count("*") as "cun_num")
      .withColumn("rn",row_number() over Window.partitionBy($"city_id").orderBy($"cun_num".desc))
      .where($"rn" <= 3)
      .show()


    /**
     * 4、统计每个用户在同一个区县内相邻两条位置记录之间的时间间隔
     *      [mdn, county_id, start_time, end_time, interval]
     */

    dataDf
      .where($"city_id".isNotNull)
      .withColumn("last_end_time", lag($"end_time",1) over Window.partitionBy($"mdn",$"city_id",$"county_id").orderBy($"end_time"))
      // 对于last_end_time 和 start_time 进行相减 需要对其进行格式化操作 将时间转换成时间戳格式
      .withColumn("interval",unix_timestamp($"start_time","yyyyMMddHHmmss") - unix_timestamp($"last_end_time","yyyyMMddHHmmss"))
      .select("mdn", "county_id", "start_time", "end_time", "interval")
      .show()


    /**
     * 5、统计每个网格内停留时间降序排名，最长、最短以及平均停留时间
     *    [grid_id, duration, rn, max_duration, min_duration, avg_duration]
     */

    dataDf
      .where($"city_id".isNotNull)
      .select("grid_id", "duration")
      .withColumn("rn",row_number() over Window.partitionBy("grid_id").orderBy($"duration".desc))
      // 对于窗口中如果聚合函数添加了 orderBy 那么表示累计 进行求和 或最大值等等
      .withColumn("max_duration",max($"duration") over Window.partitionBy("grid_id"))
      .withColumn("min_duration",min($"duration") over Window.partitionBy("grid_id"))
      .withColumn("avg_duration",avg($"duration") over Window.partitionBy("grid_id"))
      .orderBy($"grid_id",$"rn")
      .show()




  }
}
