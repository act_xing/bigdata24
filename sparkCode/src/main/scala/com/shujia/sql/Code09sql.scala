package com.shujia.sql

import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.{DataFrame, SparkSession}

object Code09sql {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .config("spark.sql.shuffle.partitions", 2)
      .getOrCreate()

    val dataDf: DataFrame = spark
      .read
      .format("csv")
      .option("sep", ",")
      // 853101,2010,100200,25002,19440,20550,14990,17227,40990,28778,19088,29889,10990,20990
      .schema("a Int,b String,c Int")
      .load("data/sql0704.txt")
    import org.apache.spark.sql.functions._
    // 使用列表达式
    import spark.implicits._


    dataDf.show()

    // 方式一：
    /**
|2014|  A|  3|    C|    A|
|2014|  B|  1|    C|    A|
|2014|  C|  2|    C|    A|

先按 年分组 => 对 max_b 和 b 进行判断  如果相等 那么返回 c 如果不相等 那么返回 0

|2014|  A|  3|    C|    A| 0
|2014|  B|  1|    C|    A| 0
|2014|  C|  2|    C|    A| 2
之后再对上述新增列求最大值
另外一列和 上述逻辑一致

     */

    dataDf
      .withColumn("max_b",max($"b") over Window.partitionBy($"a"))
      .withColumn("min_b",min($"b") over Window.partitionBy($"a"))
      .groupBy($"a")
      .agg(max(when($"min_b" === $"b",$"c").otherwise(0)) as "min_c",max(when($"max_b" === $"b",$"c").otherwise(0)) as "max_c")
      .show()


    /**
     * 方式二：
     *    先对 a 进行分组  再求出 b的最小值和最大值 之后逻辑和上述一样
     *
     */

    val groupByDf: DataFrame = dataDf
      .groupBy($"a")
      .agg(min($"b") as "min_b", max($"b") as "max_b")

    groupByDf
      .join(dataDf,"a")
      .groupBy($"a")
      .agg(max(when($"min_b" === $"b",$"c").otherwise(0)) as "min_c",max(when($"max_b" === $"b",$"c").otherwise(0)) as "max_c")
      .show()





    val dataDf0708: DataFrame = spark
      .read
      .format("csv")
      .option("sep", ",")
      // 853101,2010,100200,25002,19440,20550,14990,17227,40990,28778,19088,29889,10990,20990
      .schema("date_id String,is_work Int")
      .load("data/sql0708.txt")

    dataDf0708.show()

    /**
     * 分析：
     *    1.对日期进行标记属于第几周
     *    2.对每周内的数据进行累计求和
     */

    dataDf0708
      .withColumn("week_flag",weekofyear($"date_id"))
      .withColumn("week_to_work",sum($"is_work") over Window.partitionBy($"week_flag").orderBy($"date_id"))
      .show()



    val dataDf0707: DataFrame = spark
      .read
      .format("csv")
      .option("sep", ",")
      // 853101,2010,100200,25002,19440,20550,14990,17227,40990,28778,19088,29889,10990,20990
      .schema("id String,rq String,num Int,stulen Int")
      .load("data/sql0707.txt")

    dataDf0707.show()

    /**
     * 1.先对stulen进行判断，如果为5 那么对 当前日期 加上 月份 返回为 一个数组
     *
     * 2.对数组进行一行转多行
     */

    dataDf0707
      .withColumn("inSchArr",when($"stulen" === 5,array($"rq"
        ,add_months($"rq",1)
        ,add_months($"rq",2)
        ,add_months($"rq",3)
        ,add_months($"rq",4)
      )).when($"stulen" === 6,array($"rq"
        ,add_months($"rq",1)
        ,add_months($"rq",2)
        ,add_months($"rq",3)
        ,add_months($"rq",4)
        ,add_months($"rq",5)
      )))
      .select(
        $"id",$"num",$"stulen",explode($"inSchArr") as "inSchMon"
      )
      .groupBy($"inSchMon")
      .agg(sum($"num"))
      .show()


    /**
     * 补全数据
     */
    val dataDf07010: DataFrame = spark
      .read
      .format("csv")
      .option("sep", ",")
      // 853101,2010,100200,25002,19440,20550,14990,17227,40990,28778,19088,29889,10990,20990
      .schema("id String,a String,b String,c String")
      .load("data/sql0710.txt")

    dataDf07010.createOrReplaceTempView("tab0710")



    /**
     * 思路：
     *    1.先根据id进行排序开窗，之后在对列进行累计求个数 注意count中不能加 * 需要给定 列名（添加列名后，对NULL值不做统计）
     *    2.基于统计好的数据 对 相同的计数分为一组 ，之后再对组内求对应列的第一个值
     */
    spark.sql(
      """
        |
        |
        |SELECT
        |id
        |,a
        |,b
        |,c
        |,count(a) over(order by id desc) as cnt_a
        |,count(b) over(order by id desc) as cnt_b
        |,count(c) over(order by id desc) as cnt_c
        |FROM tab0710
        |
        |""".stripMargin).createOrReplaceTempView("mid_tab0710")


    // 注意顺序
    spark.sql(
      """
        |
        |
        |SELECT
        |id
        |,first_value(a) over(partition by cnt_a order by id desc) as a_res
        |,first_value(b) over(partition by cnt_b order by id desc) as b_res
        |,first_value(c) over(partition by cnt_c order by id desc) as c_res
        |FROM mid_tab0710
        |
        |""".stripMargin).show()

    // 自己通过DSL语言实现

//    dataDf07010.show()

    dataDf07010.select($"id",last($"a",true) over Window.orderBy($"id")).show()

  }
}
