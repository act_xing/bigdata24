package com.shujia.sql

import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.sum
import org.apache.spark.sql.{DataFrame, SparkSession}

object Code05Function {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .config("spark.sql.shuffle.partitions", 2)
      .getOrCreate()

    val stuDf: DataFrame = spark
      .read // 读取数据时 选择read
      .format("csv") // 指定读取数据的数据源 csv、json、parquet、orc、jdbc ...
      .option("sep", ",") // 指定配置参数 sep 表示列的分隔符
      .schema("id String,name String,age Int,gender String,clazz String") // 指定文本信息的数据结构
      .load("data/student1000.txt")
      .cache()


    val scoreDf: DataFrame = spark
      .read
      .format("csv")
      .option("sep", ",") // 指定配置参数 sep 表示列的分隔符
      .schema("id String,objId String,score Int") // 指定文本信息的数据结构
      .load("data/score.txt") // 指定路径 读取到的数据自动保存为一个DF


    /**
     * 聚合函数：
     * max、min、sum、avg、count、collect_list、collect_set
     *
     * 聚合函数可以跟groupby 分组后再去直接调用对应的函数 但是 也可以通过agg来使用
     * 如果使用agg 那么需要传入一个表达式 表达式中调用对应的函数 该函数需要手动导入
     * import org.apache.spark.sql.functions._
     *
     */
    scoreDf
      .groupBy("id")
      .sum()
      .withColumnRenamed("sum(score)", "totalScore")
      .show()

    // 导入表达式中的函数
    import org.apache.spark.sql.functions._
    // 使用列表达式
    import spark.implicits._

    val totalScoreDF: DataFrame = scoreDf
      .groupBy("id")
      .agg(sum($"score") as "totalScore")

    val joinRes: DataFrame = totalScoreDF
      .withColumnRenamed("id", "sid")
      .join(stuDf, $"id" === $"sid")
      .cache()

    joinRes
      .groupBy($"clazz")
      .agg(max("totalScore"))
      .show()


    joinRes
      .groupBy($"clazz")
      .agg(min("totalScore"))
      .show()

    joinRes
      .groupBy($"clazz")
      .agg(avg("totalScore"))
      .show()


    joinRes
      .groupBy($"clazz")
      // 对于多列拼接时，可以使用concat ,但是concat中需要给定多个column 对于分隔符可以使用 expr
      .agg(collect_set(concat($"id", expr("'|'"), $"name")) as "concat_res")
      // truncate=false可以展示折叠内容
//      .show(truncate = false)


    /**
     * 数值函数：abs、round、ceil、floor、sin、cos、tan、log
     * abs：取绝对值
     * round: 取四舍五入
     * ceil：向上取整数
     * floor:向下取整数
     *
     */

    /**
     * 字符串函数：concat、concat_ws、md5、split、substring、trim、to_json
     * concat:字符串拼接
     * concat_ws：以固定分隔符进行拼接列数据
     * md5: 表示对数据进行md5函数加密
     * split：切分
     * substring：截取
     * trim：数据前后去除空格 用于数据清洗操作
     * to_json：转换Json
     */

    joinRes
      .groupBy($"clazz")
      // 对于多列拼接时，可以使用concat ,但是concat中需要给定多个column 对于分隔符可以使用 expr
      .agg(collect_set(concat_ws("|",$"id",$"name") as "concat_res"))
      // truncate=false可以展示折叠内容
//      .show(truncate = false)


    // md5函数可以对数据进行加密操作，相同的数据得到的加密结果是一致的 一般来说是用于对数据进行脱敏操作
    stuDf
      .select(md5($"id") as "md5_id",$"name",$"clazz")
      .join(totalScoreDF.select(md5($"id") as "md5_sid",$"totalScore" ),$"md5_id" === $"md5_sid")
//      .show(truncate = false)

    /**
     * 日期函数：
     * from_unixtime、unix_timestamp、to_char、datediff、date_format
     *
     */


    /**
     * 窗口函数：
        聚合函数类：max、min、sum、avg、count
        排名类：row_number、rank、dense_rank
        取值类：lag、lead、first_value、last_value
     */

    /**
     * 取出班级排名前三的学生
     */

    joinRes
      // 在SELECT中可以使用 * 取出所有字段，之后再通过 row_number 对数据进行排名 需要通过 over函数开启一个窗口
      //       在over中需要给定一个WindowSpec的对象，该对象可以通过Window下的函数获取  partitionBy 表示对数据进行分区 orderBy 表示对数据进行排序
      //          并且期返回的结果也是一个WindowSpec 如果要倒序排序，那么需要再 列表达式中的列后给定一个 .desc
      .select($"*",row_number() over( Window.partitionBy($"clazz").orderBy($"totalScore".desc)) as "pm")
      .where($"pm" <= 3)
      .show()


    joinRes
      .withColumn("pm",dense_rank() over Window.partitionBy($"clazz",$"age").orderBy($"totalScore".desc))
      .where($"pm" <= 3)
      .show()

    joinRes
      .withColumn("lag_score",lag($"totalScore",1,0) over Window.partitionBy($"clazz",$"age").orderBy($"totalScore".desc))
      .show()


    /**
     * 条件函数：when（scala中if是关键字，故无if）、coalesce、nanvl
     *
     * when:相当于SQL中的case when
     * coalesce: 给定多个列 返回第一个不为空的列信息
     * nanvl: 对Null值进行替换
     *
     */

    joinRes
      // when表示对数据进行判断，结果为true得到期返回值
      // otherwise 表示上述都不满足 那么
      .withColumn("flag",when($"totalScore" > 450,"优秀").when($"totalScore" > 300,"中等").otherwise("良好"))
      .sample(0.1)
      .show(10)



  }
}
