package com.shujia.sql

import org.apache.spark.sql.{Column, DataFrame, SparkSession}

object Code07Burk {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .config("spark.sql.shuffle.partitions", 2)
      .getOrCreate()

    val dataDf: DataFrame = spark
      .read
      .format("csv")
      .option("sep", ",")
      // 853101,2010,100200,25002,19440,20550,14990,17227,40990,28778,19088,29889,10990,20990
      .schema("id String,year_ String,tsl01 Int,tsl02 Int,tsl03 Int,tsl04 Int,tsl05 Int,tsl06 Int,tsl07 Int,tsl08 Int,tsl09 Int,tsl10 Int,tsl11 Int,tsl12 Int")
      .load("data/burks.txt")

    // 需求：将每个月的金额由一行行转成多行 => 行转列
    // explode 需要传入一个集合


    import org.apache.spark.sql.functions._
    // 使用列表达式
    import spark.implicits._





    dataDf
      .select($"id",$"year_",posexplode(array("tsl01", "tsl02", "tsl03", "tsl04", "tsl05", "tsl06", "tsl07", "tsl08", "tsl09", "tsl10", "tsl11", "tsl12")) as Array("mon","num"))
      .select($"id",$"year_",$"mon"+1 as "mon",$"num")
      .show()

    // 用SQL方式完成
    /**
     * createTempView : 创建普通的临时表  如果存在有同名的临时表会报错
     * createGlobalTempView 创建全局的临时表 (在多个SparkSession之间)
     * createOrReplaceTempView  创建一个可以替换的临时表 如果表存在那么直接替换  最常用
     * createOrReplaceGlobalTempView 创建一个全局可以替换的临时表
     */

    dataDf.createOrReplaceTempView("burks")

    spark.sql(
      """
        |
        |SELECT
        |id
        |,year_
        |,view.mon
        |,view.num
        |FROM burks lateral view posexplode(array(tsl01, tsl02, tsl03, tsl04, tsl05, tsl06, tsl07, tsl08, tsl09, tsl10, tsl11, tsl12)) view as mon,num
        |""".stripMargin
    ).show()

    /**
     * 方式三：
     */

    val col: Column = map(
      expr("1"), $"tsl01",
      expr("2"), $"tsl02",
      expr("3"), $"tsl03",
      expr("4"), $"tsl04",
      expr("5"), $"tsl05",
      expr("6"), $"tsl06",
      expr("7"), $"tsl07",
      expr("8"), $"tsl08",
      expr("9"), $"tsl09",
      expr("10"), $"tsl10",
      expr("11"), $"tsl11",
      expr("12"), $"tsl12"
    )

    dataDf.select($"id",$"year_",explode(col) as (Array("mon","num"))).show()



    //    dataDf.show()
  }
}
