package com.shujia.sql

import org.apache.spark.sql.{DataFrame, SparkSession}

object Code06WordCount {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .config("spark.sql.shuffle.partitions", 2)
      .getOrCreate()


    val wordDF: DataFrame = spark
      .read
      .format("csv")
      .schema("word String")
      .load("data/words/word.txt")

    wordDF.show()

//    spark.conf.set("spark.sql.shuffle.partitions", 2)
//    set spark.sql.shuffle.partitions=2;


    import org.apache.spark.sql.functions._
    // 使用列表达式
    import spark.implicits._

    wordDF
      .select(split($"word"," ") as "split_word")
      .select(explode($"split_word") as "words")
      .groupBy($"words")
      .count()
//      .show()

    // posexplode函数 可以获取当前数组中的下标位置以及下标对应的数据，并将其返回为两列数据
    wordDF
      .select(split($"word"," ") as "split_word")

      .select(posexplode($"split_word"))
      .show()

  }
}
