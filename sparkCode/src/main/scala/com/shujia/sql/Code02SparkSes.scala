package com.shujia.sql

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, SparkSession}

object Code02SparkSes {

  def main(args: Array[String]): Unit = {

    // 要获取SparkSession的操作对象 先构建builder对象
//    val builder: SparkSession.Builder = SparkSession.builder()
//    // 再通过builder对象的getOrCreate函数获取SparkSession对象
//    val session: SparkSession = builder
//      .appName("sparkSession")
//      .master("local")
//      .getOrCreate()

    // SparkSession对象 一般命名为 spark
    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .getOrCreate()

    // SparkSession中包含了有其他的API
    val sc: SparkContext = spark.sparkContext



    val stuDF: DataFrame = spark
      .read
      // 读取textFile文本数据可以使用csv
      .format("csv")
      // 给定分隔符
      .option("sep", ",")
      .schema("id String,name String,age Int,gender String,clazz String")
      .load("data/student1000.txt")

    // 展示读取到的数据 一般来说用于测试 查看数据
//    stuDF.show(100)


    val stuRDD: RDD[String] = sc.textFile("data/student1000.txt")
    val studentRDD: RDD[StudentRDD2] = stuRDD.map {
      case oneLine: String =>
        val splitRes: Array[String] = oneLine.split(",")
        // 将切分后的数据包装成学生对象

        new StudentRDD2(splitRes(0), splitRes(1), splitRes(2).toInt, splitRes(3), splitRes(4))
    }

    // 通过隐式转换的方式将RDD 添加toDF函数
    import spark.implicits._
    studentRDD.toDF().show(100)


    // 1.选取部分字段进行数据展示
    stuDF.select("id","name","age").show()

    // 2.对name列进行做字符串的切分 取出姓氏
    // 通过隐式转换的方式 获取sql中所包含的一些函数
    import org.apache.spark.sql.functions._
    // 由于substring中对应列类型需要的是column类型，所以需要将字符串转换成 column形式
    // 由于$() 函数可以传入一个任意类型的变量将其转换成 ColumnName class ColumnName(name: String) extends Column(name)
    // 由于select函数中的类型 要么为Column* 表示传入的列都为Column的子类 要么为String* 表示传入的都是字符串
    stuDF.select($"id",substring($"name",1,1) as "subName").show()

    // DF中对于DSL语法可以不遵循SQL的执行顺序
    stuDF
      .select($"id",substring($"name",1,1) as "subName")
      .where("subName = '施'")
      .show()

    stuDF
      .where($"name" like "施%") // 该方式比上述WHERE查询方式更好 可以检查语法是否正确
      .select($"id",substring($"name",1,1) as "subName")
      .show()

    // 将读取到的数据创建成一个临时表，可以对应查询数据
    stuDF.createTempView("student")

    // sql查询数据时需要获取其表名称
    // 通过给定一个SQL语句查询对应的数据 获取其结果
    spark.sql(
      """
        |select
        |*
        |from student
        |where clazz like '理科一班'
        |""".stripMargin).show()

  }
}
