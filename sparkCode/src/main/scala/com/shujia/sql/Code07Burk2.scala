package com.shujia.sql

import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.{Column, DataFrame, SparkSession, expressions}

object Code07Burk2 {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .config("spark.sql.shuffle.partitions", 2)
      .getOrCreate()

    val dataDf: DataFrame = spark
      .read
      .format("csv")
      .option("sep", ",")
      // 853101,2010,100200,25002,19440,20550,14990,17227,40990,28778,19088,29889,10990,20990
      .schema("id String,year_ String,tsl01 Int,tsl02 Int,tsl03 Int,tsl04 Int,tsl05 Int,tsl06 Int,tsl07 Int,tsl08 Int,tsl09 Int,tsl10 Int,tsl11 Int,tsl12 Int")
      .load("data/burks.txt")

    // 需求：将每个月的金额由一行行转成多行 => 行转列
    // explode 需要传入一个集合


    import org.apache.spark.sql.functions._
    // 使用列表达式
    import spark.implicits._

    val col: Column = map(
      expr("1"), $"tsl01",
      expr("2"), $"tsl02",
      expr("3"), $"tsl03",
      expr("4"), $"tsl04",
      expr("5"), $"tsl05",
      expr("6"), $"tsl06",
      expr("7"), $"tsl07",
      expr("8"), $"tsl08",
      expr("9"), $"tsl09",
      expr("10"), $"tsl10",
      expr("11"), $"tsl11",
      expr("12"), $"tsl12"
    )

    dataDf
      .select($"id",$"year_",explode(col) as  Array("mon","num"))
      .withColumn("sum_num",sum($"num") over Window.partitionBy($"id",$"year_").orderBy($"mon")
      )
      .show()


    // 统计每个公司当月比上年同期增长率
//    |846271| 2012|  1|100212| 100212|
//    |846271| 2013|  1|100214| 100214|  100212

    dataDf
      .select($"id",$"year_",explode(col) as Array("mon","num"))
      .withColumn("last_num",lag($"num",1,1) over Window.partitionBy($"id").orderBy($"mon",$"year_"))
      // (当月收入-去年当月收入)/去年当月收入 = 当月收入/去年当月收入 - 1
      .withColumn("tb",ceil(($"num"-$"last_num")/$"last_num" * 100))
      .show()

    //    dataDf.show()
  }
}
