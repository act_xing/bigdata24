package com.shujia.sql

import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.{DataFrame, SparkSession}

object Code14Shebao {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .config("spark.sql.shuffle.partitions", 2)
      .getOrCreate()

    import org.apache.spark.sql.functions._
    // 使用列表达式
    import spark.implicits._

    val shebaoDf: DataFrame = spark
      .read
      .format("csv")
      .option("sep", ",")
      .schema("id String,item String,sdate String")
      .load("data/shebao.txt")

    shebaoDf.show()

    shebaoDf
      // 1.取出对应的日期
      .select($"id",$"item",date_format($"sdate","yyyy-MM-dd") as "s_date")
      // 2.取出每个人在上一个月对应的公司
      .withColumn("last_item",lag($"item",1) over Window.partitionBy($"id").orderBy($"s_date"))
      // 3.对上一个工作和下一个工作不一样的标记为 1 没有换工作的标记为 0
      .withColumn("flag",when($"last_item" === $"item",0).otherwise(1))
      // 4. 对标记信息进做累加和
      .withColumn("clazz",sum($"flag") over Window.partitionBy($"id").orderBy($"s_date"))
      // 5. 按照id 公司 clazz 进行分组
      .groupBy($"id",$"item",$"clazz")
      .agg(min($"s_date") as "start_time",max($"s_date")as "end_time")
      .show()

  }
}
