package com.shujia.sql

import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object Code03Source {
  def main(args: Array[String]): Unit = {

    val spark: SparkSession = SparkSession
      .builder()
      .appName("Source") // 在Yarn中执行时，查看到当前应用的名称
      .master("local") // 本地执行给定local即可  如果是standalone模式 http://master:7077  yarn中执行时 需要通过sparkSubmit进行任务的提交 指定提交参数中 --master 为  yarn-cluster / yarn-client
      .getOrCreate()


    val stuDf: DataFrame = spark
      .read // 读取数据时 选择read
      .format("csv") // 指定读取数据的数据源 csv、json、parquet、orc、jdbc ...
      .option("sep", ",") // 指定配置参数 sep 表示列的分隔符
      .schema("id String,name String,age Int,gender String,clazz String") // 指定文本信息的数据结构
      .load("data/student1000.txt") // 指定路径 读取到的数据自动保存为一个DF

    //    stuDf.show(20)  // 查看前20条数据
    // 如果查询的列非常多，或者当前列中数据长度非常长，那么Spark默认会展示部分数据，如果要展示所有数据
    //    stuDf.show(truncate = true) // 展示所有数据 但是不能展示所有行数据

    stuDf.printSchema() // 打印数据结构

    // 保存数据
    stuDf
      .write // 表示写出数据
      .format("csv")
      .option("sep", "|")
      .mode(SaveMode.Overwrite) //  SaveMode中 Overwrite 表示覆盖写入  Append表示追加
      .save("data/sparkSave/csvRes")


    // 将数据写出成json格式
    /**
     * json格式：
     * ① Key为一个字符串
     * ② Value 可以为字符串 数值类型可以不同添加 双引号  可以为一个数组 []
     * ③ 数组内可以给定多个json
     * ④ 每个JSON可以表示为一个对象
     */
    stuDf
      .write // 表示写出数据
      .format("json")
      .mode(SaveMode.Overwrite) //  SaveMode中 Overwrite 表示覆盖写入  Append表示追加
//      .save("data/sparkSave/jsonRes")


    // 压缩格式
    stuDf
      .write // 表示写出数据
      .format("parquet")
      .mode(SaveMode.Overwrite) //  SaveMode中 Overwrite 表示覆盖写入  Append表示追加
      .save("data/sparkSave/parquetRes")


    // 将数据保存成一个ORC格式
    stuDf
      .write // 表示写出数据
      .format("orc")
      .mode(SaveMode.Overwrite) //  SaveMode中 Overwrite 表示覆盖写入  Append表示追加
      .save("data/sparkSave/orcRes")


    val jsonDf: DataFrame =
      spark
        .read
        .format("json")  // 对于JSON文件中已经包含了有其数据结构，那么不需要再去给定
        .load("data/sparkSave/jsonRes")
//    jsonDf.show(10)


    val orcDf: DataFrame =
      spark
        .read
        .format("orc")  // 对于JSON文件中已经包含了有其数据结构，那么不需要再去给定
        .load("data/sparkSave/orcRes") //对特定的压缩格式要求其文件为对应的压缩文件
//    orcDf.show(10)


    val parquetDf: DataFrame =
      spark
        .read
        .format("parquet")  // 对于JSON文件中已经包含了有其数据结构，那么不需要再去给定
        .load("data/sparkSave/parquetRes")
//    parquetDf.show(10)


    // 5.jdbc
    val mysqlDf: DataFrame = spark
      .read
      .format("jdbc") // 表示读取符合jdbc协议的数据源
      .option("url", "jdbc:mysql://master:3306/student?useSSL=false&useUnicode=true&characterEncoding=utf8") // 表示参数配置
      .option("user", "root")
      .option("password", "123456")
      .option("dbtable", "student")
      .load()
    mysqlDf.show(10)

    // 写入数据到MySQL中

    mysqlDf
      .write
      .format("jdbc")
      .option("url", "jdbc:mysql://master:3306/student?useSSL=false&useUnicode=true&characterEncoding=utf8") // 表示参数配置
      .option("user", "root")
      .option("password", "123456")
      .option("dbtable", "student_jdbc")
      .option("truncate","true") // 表示当保存的方式为Overwrite时，会先清空表，之后再去覆盖写入 不会修改对应表的结构
      .mode(SaveMode.Overwrite)  // Overwrite 在写入数据时会自动创建对应的表  如果表存在的情况下，会先删除 后创建再写入数据
      .save()


  }
}
