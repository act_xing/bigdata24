package com.shujia.sql

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Dataset, SparkSession}

object Code12DSAndDfAndRDD {
  def main(args: Array[String]): Unit = {

    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .config("spark.sql.shuffle.partitions", 2)
      .enableHiveSupport()
      .getOrCreate()

    import spark.implicits._

    // Dataset中存储的是对象 但是可以使用DF中的一些函数 也可以使用RDD中的部分算子
    val ds: Dataset[Person1] = List(Person1("zhangsan", 20),Person1("zhangsan2", 18)).toDS()

    ds.select($"name").show()

    ds.map{
      case person1: Person1 => person1.name
    }.show()

    ds.show()



    // Dataset 转换成RDD

    val rdd: RDD[Person1] = ds.rdd

    rdd.map{
      case person1: Person1 => person1.name
    }.foreach(println)

    // Dataset 转换成DF

    ds.toDF("dfName","dfAge").show()


      // DF 转换成DataSet
    val dsVehPass: Dataset[VehPass] = spark.sql(
      """
        |select sbbh,rq,cast(num as int) as num from learn6.veh_pass
        |""".stripMargin).as[VehPass]


    dsVehPass.show()


  }
}
case class Person1(name:String,age:Long)
case class VehPass(sbbh:String,rq:String,num:Int)
