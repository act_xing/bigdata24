package com.shujia.sql

import org.apache.spark.sql.{DataFrame, SparkSession}

object Code04DSL {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .appName("dsl")
      .master("local")
      .config("spark.sql.shuffle.partitions",2) // 表示设置Shuffle过程中对应的分区数 -> Task数量
      .getOrCreate()

    val stuDf: DataFrame = spark
      .read // 读取数据时 选择read
      .format("csv") // 指定读取数据的数据源 csv、json、parquet、orc、jdbc ...
      .option("sep", ",") // 指定配置参数 sep 表示列的分隔符
      .schema("id String,name String,age Int,gender String,clazz String") // 指定文本信息的数据结构
      .load("data/student1000.txt") // 指定路径 读取到的数据自动保存为一个DF
      .cache()


    stuDf
      .where("clazz like '理科%'") // 直接传入一个字符串表达式
      .show()


    // 在编写DSL语句时，需要导入下面两个内容
    import spark.implicits._  // 导入spark中的隐式转换 可以使用 $ 将字符串列名称转换成 column的对象 column 对象中包含有一些 关键字函数
    import org.apache.spark.sql.functions._

    stuDf
      .where($"clazz" like "文科%")  // like 属于一个函数,所以需要对该函数进行导入 对于其.号可以省略
      .limit(10) // 表示限定10条数据 SQL中limit(a,b) a表示限定其行数 b表示分页（从哪条数据开始）
      .show()

    /**
     * 字符串表达式：运行时才能发现错误
     * 列表达式（推荐）：可自动检查语法错误并提示  注意：列表达式是将字符串 通过$转换成 列对象，再调用列对象的函数 执行计算逻辑
     * 使用列表达式时需要先导入隐式转换   import spark.implicits._  其中spark是指当前SparkSession的对象名
     */


    /**
     * DSL具体的语法：
select：
同SQL中的select操作，指定需要返回的列或使用函数对列进行处理及取别名操作
where：
类似SQL中的where操作，用于过滤，也可使用filter，还可用于分组之后的过滤
groupBy：
按照指定的字段进行分组，分组后通常需要跟agg操作并结合聚合函数进行处理
orderBy：
同SQL中的order by操作，按照指定的列进行排序，默认升序，可通过desc表明降序

limit：
类似SQL中的limit操作，指定返回数据的条数，但不能指定位置返回
union：
类似SQL中的union all操作，默认不进行去重，若需去重可再接distinct操作
withColumnRenamed：对列进行重命名
withColumn：接收两个参数，给当前DataFrame增加一列
colName:String 增加列的列名
col:Column 需要传入一个列表达式，即处理逻辑

     *
     * 回顾：
     * SQL中常见操作
     *
     * SELECT
     * 字段
     * ，函数（字段）
     * FROM table_name
     * WHERE 过滤
     * group by 分组
     * having 分组后过滤
     * order by 排序
     * limit 限制条数
     *
     *
     * DSL:通过列表达式或字符串表达式 随意组合上述SQL操作中的每一个步骤
     *
     * 表关联:
     *  join  leftJoin  rightJoin
     *
     * 函数：
     *    字符串函数
     *    数值函数
     *    条件函数
     *    窗口函数
     *    日期函数
     *
     */

    /**
     * GROUPBY: 对指定列进行分组操作
     *    GROUPBY过程会产生Shuffle Spark中Shuffle默认的分区数会达到200  如果要设置默认Shuffle的分区数可以对其设置如下：
     *      config("spark.sql.shuffle.partitions",2)
     */

    // 对数据进行分组统计班级各年龄段人数
    stuDf.groupBy($"clazz",$"age").agg(count("*") as "cnt").show()

    // 将班级人数大于90的进行过滤
    stuDf
      .groupBy("clazz")
      .agg(count("*") as "cnt")
      .where($"cnt" > 90)
      .show()

    /**
     * withColumn: 添加列操作
     *    需要给定两个参数，第一个参数是列名称 第二个参数是 column 如果给定的是固定值，可以通过expr给定
     *
     * expr:表示给定一个SQL的表达式 如果给定是一个字符串 需要通过\'\'将其进行转换
     *
     */

    stuDf
      .groupBy("clazz")
      .agg(count("*") as "cnt")
      .where($"cnt" > 90)
      .withColumn("flag",expr("'more 90'"))
      .show()


    /**
     * 将大于90的和小于90进行拼接
     * union:对两个DF进行拼接，不会对数据进行去重操作
     *    如果需要去重，那么要通过distinct进行操作
     *
     */

      // 由于 agg统计结果被重复使用,那么可以将统计的结果进行缓存
      //      cache是将数据缓存到内存 persist是可以指定缓存策略

      val cacheDf: DataFrame = stuDf
        .groupBy("clazz")
        .agg(count("*") as "cnt")
        .cache()

    val lessDf: DataFrame = cacheDf
      .where($"cnt" < 90)
      .withColumn("flag", expr("'less 90'"))

    val moreDf: DataFrame = cacheDf
      .where($"cnt" > 90)
      .withColumn("flag", expr("'more 90'"))


    lessDf
      .union(moreDf)
      .union(moreDf)
      .distinct() // 对数据进行做去重操作
      .show()

    /**
     * orderBy:
     *    对数据进行排序操作
     */

    cacheDf
      .orderBy($"cnt" desc,$"clazz" desc) // 排序默认以升序方式排序 如果要倒序排序，那么需要在列后添加 desc操作
      .show()


    /**
     * withColumnRenamed:对列进行重命名操作
     *    注意：重命名时，不需要给定列表达式
     */
    cacheDf
      .withColumnRenamed("cnt","newCnt")
      .show()

    /**
     * Join:
     *
     */

    val scoreDf: DataFrame = spark
      .read
      .format("csv")
      .option("sep", ",") // 指定配置参数 sep 表示列的分隔符
      .schema("id String,objId String,score Int") // 指定文本信息的数据结构
      .load("data/score.txt") // 指定路径 读取到的数据自动保存为一个DF


    // 统计学生的总分，再关联学生基本信息
    val totalScoreDf: DataFrame = scoreDf
      .groupBy($"id")
      .agg(sum($"score") as "totalScore")
      .cache()

    println("++"*30)
    totalScoreDf.join(stuDf,"id").show()  // 由于两张表中的关联字段一致，所以可以使用单个字段，但是通常来说，不使用该方式

    totalScoreDf
      .withColumnRenamed("id","sid")
      // 在列表达式中需要使用 === 表达式来表示等于 等于号两边的字段名称顺序随意给定
      .join(stuDf,$"sid" === $"id").show()

    totalScoreDf
      .where($"totalScore" > 450)
      .withColumnRenamed("id","sid")
      // Type of join to perform. Default inner. Must be one of: inner, cross, outer, full, full_outer, left, left_outer, right, right_outer, left_semi, left_anti.
      .join(stuDf,$"sid" === $"id",joinType = "right")
      .orderBy($"age")
      .show(100)


    totalScoreDf
      .withColumnRenamed("id","sid")
      // Type of join to perform. Default inner. Must be one of: inner, cross, outer, full, full_outer, left, left_outer, right, right_outer, left_semi, left_anti.
      .join(stuDf.where($"age" > 23),$"sid" === $"id",joinType = "left")
      .orderBy($"totalScore")
      .show(100)


    // fullJoin表示全连接
    // sample表示从df中进行数据采样 第一个参数表示 是否放回 第二个表示抽取的比率 第三个表示种子
    //               其中种子表示对每次取到的一批结果数据进行标记 当种子一样时，同时其他信息一样，那么得到的结果一样
    //       一般情况下是用于查看整个表中的数据分布情况
    totalScoreDf
      .where($"totalScore" > 450)
      .withColumnRenamed("id","sid")
      // Type of join to perform. Default inner. Must be one of: inner, cross, outer, full, full_outer, left, left_outer, right, right_outer, left_semi, left_anti.
      .join(stuDf.where($"age" > 23),$"sid" === $"id",joinType = "full")
//      .orderBy($"clazz".desc,$"totalScore")
      .sample(false,fraction = 0.01,10)
      .show(100)

  }
}
