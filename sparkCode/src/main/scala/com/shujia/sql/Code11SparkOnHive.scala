package com.shujia.sql

import org.apache.spark.sql.SparkSession

object Code11SparkOnHive {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .config("spark.sql.shuffle.partitions", 2)
      .enableHiveSupport() // 开启HIVE支持
      .getOrCreate()

    spark.sql(
      """
        |show databases
        |""".stripMargin).show()

    // spark sql 中可以保存当前所在数据库位置
    spark.sql(
      """
        |use learn6
        |""".stripMargin)

    spark.sql(
      """
        |
        |select * from veh_pass
        |
        |""".stripMargin).show()
  }
}
