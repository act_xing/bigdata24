package com.shujia.ml

import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object Demo04Image {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .config("spark.sql.shuffle.partitions", 2)
      .getOrCreate()

    import spark.implicits._

    val imageDf: DataFrame = spark
      .read
      .format("image") //对于图片数据可以指定其format类型为image进行数据的加载
      .load("C:\\Users\\Lenovo-1\\Desktop\\train")

//    imageDf.show() // 直接展示数据，查看不了全部信息，需要对其进行取结构数据
    imageDf.printSchema()

    val imageDfRes: DataFrame = imageDf
      .select($"image.origin" as "path", $"image.data" as "data")
      // 经过查看data数据后，需要对字节数据转换成Int，再去做数据处理 并且对于路径数据需要提取出文件名
      .map(
        row => {
          val strings: Array[String] = row.getString(0).split("/")
          val fileName: String = strings.reverse.head
          // 取到的数据为字节数组 需要将其转换成一个List
          val dataByte: List[Byte] = row.getAs[Array[Byte]]("data").toList
          val dataList: List[Double] = dataByte.map {
            data =>
              if (data.toInt >= 0) {
                0.0
              } else {
                1.0
              }
          }

          // 需要将List转换成稠密向量
          (fileName, Vectors.dense(dataList.toArray))
        }
        // 数据列名称必须为features名称
      ).toDF("fileName", "features")


    // 需要再加载标签数据

    val labelDf: DataFrame = spark
      .read
      .format("csv")
      .option("sep", " ")
      .schema("fileName String,label Double")
      .load("data/ml/image_res.txt")

    labelDf.show()


    labelDf
      // 关联特征数据
      .join(imageDfRes,"fileName")
      // 取出标签和特征数据 之后写出成libsvm格式 和人体指标数据格式一致
      .select($"label",$"features")
      .write
      .format("libsvm")
      .mode(SaveMode.Overwrite)
      .save("data/ml/imageSvm")




  }
}
