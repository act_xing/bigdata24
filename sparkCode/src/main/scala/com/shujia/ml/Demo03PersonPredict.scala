package com.shujia.ml

import org.apache.spark.ml.classification.LogisticRegressionModel
import org.apache.spark.ml.linalg
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, SparkSession}

object Demo03PersonPredict {

  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .config("spark.sql.shuffle.partitions", 2)
      .getOrCreate()

    // 加载对应的模型
    val model: LogisticRegressionModel = LogisticRegressionModel.load("data/model/lr_person")

    // 1:5.2 2:4.5 3:3.3 4:141.3 5:85.1 6:71.3 7:73
    val predictRes: Double = model.predict(Vectors.dense(5.6, 5.5, 3.8, 141.3, 85.1, 71.3, 73))
    // 预测结果
    println(predictRes)


    val rddV: RDD[linalg.Vector] = spark.sparkContext.parallelize(List(
      Vectors.dense(5.6, 5.5, 3.8, 141.3, 85.1, 71.3, 73),
      Vectors.dense(5.6, 5.5, 2.8, 121.3, 85.1, 71.3, 73),
      Vectors.dense(5.6, 3.5, 2.8, 131.3, 85.1, 71.3, 73),
      Vectors.dense(5.6, 2.5, 1.8, 111.3, 85.1, 71.3, 73),
      Vectors.dense(5.6, 1.5, 3.8, 101.3, 85.1, 71.3, 73)
    ))

    // 使用列表达式
    import spark.implicits._
    val dataFrame: DataFrame = rddV.map(
      value => Tuple1(value)
    ).toDF("features")


    val predictRes2: DataFrame = model.transform(dataFrame)
    predictRes2.show()

  }
}
