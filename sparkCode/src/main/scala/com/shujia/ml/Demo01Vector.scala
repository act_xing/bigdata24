package com.shujia.ml

import org.apache.spark.mllib.linalg
import org.apache.spark.mllib.linalg.{SparseVector, Vectors}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, SparkSession}

object Demo01Vector {
  def main(args: Array[String]): Unit = {

    /**
     * Spark机器学习中用于保存数据的变量通常为一个向量
     *    向量有两种表示形式：
     *       1.稠密向量  对于所有特征数据都需要对其进行展示     1  2 130  0  2
     *       2.稀疏向量  可以用下标位置来保存其对应的数据 对于一些0值可以不保存 [0,1,2,3,4,5] [1,2,130,0,2]
     *
     *    假设现有一批数据 其特征总数有200个 但是只有部分特征有实际数据 大部分数据其特征值为 0
     *        稠密向量: [1,2,3,4,0,0,0,0,0,0,0,0,...,5,6]
     *        稀疏向量: [0,1,2,3,199,200],[1,2,3,4,5,6]
     *
     * 假设影响房价因素：
     *    1. 朝向 东 0 南 1 西 2 北 3 ...
     *    2. 楼层  高 2 中 1 低 0
     *    3. 房屋面积  130 120 100 ...
     *    4. 是否二次销售  0 新房  1 二手  2 三手
     *    5. 房龄  1年 1 2年 2 ....
     *
     * 将其影响因素作为数据中的列  并称为其为 特征
     *
     * SparkMlLib中有两个API
     *    一套是基于DataFrame的API: ml
     *    一套是基于RDD的API：mllib
     *
     */

    // 定义稠密向量
    val denseV1: linalg.Vector = Vectors.dense(1, 2, 130, 0, 2)
    val denseV2: linalg.Vector = Vectors.dense(1, 2, 3, 4, 0, 0, 0, 0, 0, 6, 7, 8)

    // 将稠密向量转换成稀疏向量
    val sparseV1: SparseVector = denseV1.toSparse
    // (12,[0,1,2,3,9,10,11],[1.0,2.0,3.0,4.0,6.0,7.0,8.0])  第一个值为整体的长度 第二个值数组 表示有值的下标 第三个值数组 对应下标的值
    val sparseV2: SparseVector = denseV2.toSparse
    println(denseV1)
    println(sparseV1)
    println(denseV2)
    println(sparseV2)

    // 手动创建稀疏向量
    val denseV3: linalg.Vector = Vectors.sparse(10, Array(3, 4, 7), Array(5, 6, 8))
    println(denseV3)
    println(denseV3.toDense)


    // 加载特征数据
    //  人体指标数据称为libsvm  简称为SVM格式数据
    //  SVM格式数据:包含有其特征值位置:特征值 标签信息（是否健康）
    //  SparkMLlib做机器学习计算 通常数据分为两个部分： 标签：Label 特征：Feature
    //     基于有标签的机器学习算法 称为有监督学习  没有标签的称为无监督学习

    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .config("spark.sql.shuffle.partitions", 2)
      .getOrCreate()

    // 加载SVM数据方式一：
    // LabeledPoint 表示带有标签的向量
    val pointData: RDD[LabeledPoint] = MLUtils.loadLibSVMFile(spark.sparkContext, "data/ml/人体指标.txt", 7)

    // 获取数据并对其进行打印
    pointData.foreach{
      case point:LabeledPoint =>
      println("标签数据：",point.label)
      println("特征数据：",point.features.toDense)
    }


    // 加载SVM数据方式二：

    val dataFrame: DataFrame = spark
      .read
      .format("libsvm")
      .option("numFeatures", 7)
      .load("data/ml/人体指标.txt")

    dataFrame.printSchema()
    dataFrame.show(truncate = false)


  }
}
