package com.shujia.ml

import org.apache.spark.ml.clustering.{KMeans, KMeansModel}
import org.apache.spark.ml.linalg
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, SparkSession}

object Demo07KMeans {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .config("spark.sql.shuffle.partitions", 2)
      .getOrCreate()

    import spark.implicits._

    val dataDf: DataFrame = spark
      .read
      .format("csv")
      .option("sep", " ")
      .schema("feature String")
      .load("data/ml/kmeans.txt")
      .rdd
      .map {
        case row =>
          val data: String = row.getAs[String]("feature")
          // 由于做数据计算时，需要将Df中feature变成一个稠密向量 并且向量中的数据类型是Double
          val dataArr: Array[Double] = data.split(",").toList.map(value => value.toDouble).toArray
          Tuple1(Vectors.dense(dataArr))
      }
      .toDF("features")


    // 获取模型，再对模型进行训练

    val kMeans: KMeans = new KMeans().setK(2).setSeed(1L)

    // 对于聚类算法，直接对其进行fit做训练得到训练结果
    // 由于训练数据是没有标签的，属于一个无监督的机器学习算法，那么就不需要存在训练集 对结果进行验证
    val model: KMeansModel = kMeans.fit(dataDf)

    // 将训练的结果
    model.transform(dataDf).show()

  }
}
