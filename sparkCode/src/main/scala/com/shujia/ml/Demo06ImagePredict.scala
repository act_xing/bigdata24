package com.shujia.ml

import org.apache.spark.ml.classification.LogisticRegressionModel
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.sql.{DataFrame, SparkSession}

object Demo06ImagePredict {
  def main(args: Array[String]): Unit = {

    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .config("spark.sql.shuffle.partitions", 2)
      .getOrCreate()

    val model: LogisticRegressionModel = LogisticRegressionModel.load("data/model/lr_image")
    import spark.implicits._

    val imageDf: DataFrame = spark
      .read
      .format("image") //对于图片数据可以指定其format类型为image进行数据的加载
      .load("C:\\Users\\Lenovo-1\\Desktop\\predict_image")


    imageDf.printSchema()

    val imageDfRes: DataFrame = imageDf
      .select($"image.origin" as "path", $"image.data" as "data")
      // 经过查看data数据后，需要对字节数据转换成Int，再去做数据处理 并且对于路径数据需要提取出文件名
      .map(
        row => {
          val strings: Array[String] = row.getString(0).split("/")
          val fileName: String = strings.reverse.head
          // 取到的数据为字节数组 需要将其转换成一个List
          val dataByte: List[Byte] = row.getAs[Array[Byte]]("data").toList
          val dataList: List[Double] = dataByte.map {
            data =>
              if (data.toInt >= 0) {
                0.0
              } else {
                1.0
              }
          }

          // 需要将List转换成稠密向量
          (fileName, Vectors.dense(dataList.toArray))
        }
        // 数据列名称必须为features名称
      ).toDF("fileName", "features")

    imageDfRes.show(truncate = false)

    model.transform(imageDfRes).show(truncate = false)

  }
}
