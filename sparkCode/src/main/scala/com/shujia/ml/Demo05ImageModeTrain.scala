package com.shujia.ml

import org.apache.spark.ml.classification.{LogisticRegression, LogisticRegressionModel}
import org.apache.spark.sql.{DataFrame, SparkSession}

object Demo05ImageModeTrain {
  def main(args: Array[String]): Unit = {
    /**
     * 机器学习一般步骤：
     *    1.加载数据
     *    2.对其进行做数据特征工程，选出其中的训练数据以及测试数据
     *    3.基于现有的数据及业务要求选择合适机器学习算法模型
     *          基于人体指标数据，对7个指标进行测量后 预测该人员是否健康
     *          是否有标签
     *              ->是标签
     *                  -> 有监督学习
     *                        -> 看标签值是否为连续的
     *                            -> 连续 回归问题
     *                            -> 离散的  分类问题  而当前指标数据标签是离散值 为分类问题
     *
     *             ->无标签
     *                -> 聚类问题
     *      由于当前人体指标数据中 标签值只有两个 那么对应是一个二分类问题 对于一个二分类问题 可以选择使用逻辑回归算法
     *    4. 训练模型
     *
     *    5. 测试数据带入模型中 得到模型的评估结果
     *
     *    6.如果模型通过评估，那么需要对该模型进行保存
     *
     *    7.对给定的值进行预测
     *
     */


    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local")
      .config("spark.sql.shuffle.partitions", 2)
      .getOrCreate()

    // 1.加载数据
    val imageDataDf: DataFrame = spark
      .read
      .format("libsvm")
      .option("numFeatures", 28*28)
      .load("data/ml/imageSvm")

    // 2.对其进行做数据特征工程，选出其中的训练数据以及测试数据
    val randomSplitArr: Array[DataFrame] = imageDataDf.randomSplit(Array(0.8, 0.2))

    val transDs: DataFrame = randomSplitArr(0)
    val testDs: DataFrame = randomSplitArr(1)

    // 3.基于现有的数据及业务要求选择合适机器学习算法模型
    //  由于现在对数据进行做多分类的计算，那么需要对参数进行调整
    val mlr = new LogisticRegression()
      .setMaxIter(10)
      .setFitIntercept(true)
      .setFamily("multinomial")

    //   得到训练的模型结果， 对于位置参数进行求解
    val model: LogisticRegressionModel = mlr.fit(transDs)

    //    5. 测试数据带入模型中 得到模型的评估结果

    val testResDf: DataFrame = model.transform(testDs)
    // 通过比较模型结果，可以判断该模型效果可以
    testResDf.show(truncate = false)

    import org.apache.spark.sql.functions._
    // 使用列表达式
    import spark.implicits._
    // 评估模型结果
    println("模型的评估结果：", testResDf.where($"label"=== $"prediction").count().toDouble / testResDf.count())

    model.save("data/model/lr_image")

  }
}
