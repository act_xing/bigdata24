package com.shujia.stream

import org.apache.spark.sql.functions.{count, explode, split}
import org.apache.spark.sql.streaming.OutputMode
import org.apache.spark.sql.{DataFrame, SparkSession}

object Demo09SparkCreateStream {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local[2]")
      .config("spark.sql.shuffle.partitions", 2)
      .getOrCreate()

    val dStreamDf: DataFrame = spark
      .readStream // 从流中读取数据
      .option("host", "master")
      .option("port", 8888)
      .format("socket") // 设置协议为 socket
      .load()

    import spark.implicits._
    import org.apache.spark.sql.functions._

    val quereDf: DataFrame = dStreamDf
      .select(explode(split($"value", " ")) as "word")
      .groupBy($"word")
      .agg(count("*") as "num")


    quereDf
      .writeStream
      /**
       * OutputMode中存在有三种模式：
       *  Append： 只能用于没有聚合的查询
       *  Complete： 可以用于有聚合的查询
       *  Update： 可以用于有更新的数据
       */
      .outputMode(OutputMode.Complete())
      .format("console") // 将数据写出到控制台
//    with streaming sources must be executed with writeStream.start();;
      .start()
      // 等待停止
      .awaitTermination()


  }
}
