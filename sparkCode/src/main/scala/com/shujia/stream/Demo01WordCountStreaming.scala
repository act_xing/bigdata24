package com.shujia.stream

import org.apache.spark.streaming.dstream.ReceiverInputDStream
import org.apache.spark.streaming.{Durations, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}



object Demo01WordCountStreaming {

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()

    conf.setAppName("sparkStreaming")
    conf.setMaster("local[*]")

    val sc = new SparkContext(conf)

    // batchDuration: Duration
    val ssc: StreamingContext = new StreamingContext(sc, Durations.seconds(5))

    // 从端口取数据
    // nc -lk 8888 在Linux中启动socket服务
    val line: ReceiverInputDStream[String] = ssc.socketTextStream("master", 8888)
    line.print()

    ssc.start()
    ssc.awaitTermination()
    ssc.stop()


  }
}
