package com.shujia.stream

import org.apache.spark.broadcast.Broadcast
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.streaming.{Durations, StreamingContext}
import org.apache.spark.streaming.dstream.ReceiverInputDStream

import java.sql.{Connection, DriverManager, PreparedStatement, ResultSet}
import scala.collection.mutable.ListBuffer

object Demo08JCBK {

  def main(args: Array[String]): Unit = {
    /**
     * 需求：
     * 现在有一批黑名单人员，需要通过 SparkStream 实时从电信数据中 对黑名单人员进行排查，如果出现黑名单人员，那么直接显示出来
     *
     * 黑名单人员保存在MySQL中  电信数据
     */

    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local[2]")
      .getOrCreate()

    val ssc = new StreamingContext(spark.sparkContext, Durations.seconds(5))

    val lineDs: ReceiverInputDStream[String] = ssc.socketTextStream("master", 7777)


    import spark.implicits._
    import org.apache.spark.sql.functions._


    /**
     * 步骤：
     * 1.需要从MySQL中读取数据
     * 2.从端口中接收数据，并将两者进行匹配，如果相同，那么对其进行打印
     *
     * 问题：
     *  如果将MySQL连接放在流处理过程之外，那么对于MySQL查询数据整体只会查询一次，对于后添加的数据不能进行更新操作
     *      所以需要将MySQL的链接放在处理过程内部 需要放在foreachRDD当中 对每个RDD遍历时，可以创建对应的MySQL连接
     *
     * 当ListBuffer在RDD以外创建时，那么改变量在Driver端中，当执行RDD内部的任务时，每个Task获取其中一份备份
     *    如果查询的数据量较大，那么会非常消耗资源，如何对其进行优化 => 使用广播变量
     *
     *
     *
     */




    lineDs.foreachRDD(
      // rdd中包含有一批数据
      rdd => {

        val mdnList: ListBuffer[String] = ListBuffer()
        val mysqlCon: Connection = DriverManager.getConnection(
          "jdbc:mysql://master:3306/bigdata22?useSSL=false&useUnicode=true&characterEncoding=utf8"
          , "root"
          , "123456"
        )
        val statement: PreparedStatement = mysqlCon.prepareStatement("select * from black_mdn")
        val resultSet: ResultSet = statement.executeQuery()
        println("获取到一条MySQL连接...")
        while (resultSet.next()) {
          val mdn: String = resultSet.getString("mdn")
          mdnList.append(mdn)
        }
        statement.close()
        mysqlCon.close()

        // 添加完数据后，对该变量进行广播 将其发送到每一个Executor中
        val listBroadCast: Broadcast[ListBuffer[String]] = spark.sparkContext.broadcast(mdnList)

        rdd.foreach {
          case oneLine: String => {
            val splitRes: Array[String] = oneLine.split(",")
            val mdn: String = splitRes(0)
            val exeListBuffer: ListBuffer[String] = listBroadCast.value
//            println(exeListBuffer)
            if (exeListBuffer.contains(mdn)) {
              println(splitRes(0),splitRes(1),splitRes(2),splitRes(3))
            }
          }
        }
      }
    )

    // 开始启动应用程序
    ssc.start()
    // 等待停止 会将当前程序阻塞 不会关闭
    ssc.awaitTermination()
    ssc.stop()
  }
}
