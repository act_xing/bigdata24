package com.shujia.stream

import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{Durations, StreamingContext}

object Demo05DStream2RDD {
  def main(args: Array[String]): Unit = {
    val conf: SparkConf = new SparkConf().setAppName("wordCount")
      // local 只能获取单个的进程 只有一个cpu  可以替换成local[*](所有空闲的cpu 都可以使用) local[2] 表示选择其中两个
      .setMaster("local[2]")
    val ssc = new StreamingContext(new SparkContext(conf), Durations.seconds(5))

    val lineDs: ReceiverInputDStream[String] = ssc.socketTextStream("master", 7777)

    // transform的作用是将一批转换成RDD再去通过RDD对数据进行计算，再将RDD进行返回出去

    val transFormRes: DStream[(String, Int)] = lineDs.transform(
      rdd => {
        val resRDD: RDD[(String, Int)] = rdd
          .flatMap {
            case line: String => {
              line.split(" ")
            }
          }
          .groupBy {
            case word: String => word
          }.map {
          case (word: String, ite: Iterable[String]) =>
            (word, ite.size)
        }
        // 对数据计算后，再将RDD返回出去 注意 在过程中不能存在有Action算子
        resRDD
      }
    )
    // 通过调用DStream的print函数将数据进行打印操作
    transFormRes.print()
    
    // 开始启动应用程序
    ssc.start()
    // 等待停止 会将当前程序阻塞 不会关闭
    ssc.awaitTermination()
    ssc.stop()
  }
}
