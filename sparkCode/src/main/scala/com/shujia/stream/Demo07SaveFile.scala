package com.shujia.stream

import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.streaming.{Durations, StreamingContext}

object Demo07SaveFile {
  def main(args: Array[String]): Unit = {
//    val conf: SparkConf = new SparkConf().setAppName("wordCount")
//      // local 只能获取单个的进程 只有一个cpu  可以替换成local[*](所有空闲的cpu 都可以使用) local[2] 表示选择其中两个
//      .setMaster("local[2]")
    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local[2]")
      .config("spark.sql.shuffle.partitions", 2)
      .getOrCreate()

    val ssc = new StreamingContext(spark.sparkContext, Durations.seconds(5))

    val lineDs: ReceiverInputDStream[String] = ssc.socketTextStream("master", 7777)


    import org.apache.spark.sql.functions._
    import spark.implicits._

    /**
     * transform 需要有一个RDD的返回
     */
    val dStream: DStream[Row] = lineDs.transform(
      rdd => {
        val splitDf: DataFrame = rdd
          .toDF()
          .select(explode(split($"value", " ")) as "word")

        splitDf
          .groupBy($"word")
          .agg(count("*") as "num")
          .rdd // 将统计好的结果 转换成RDD再返回
        //        rdd.saveAsTextFile("path")
      }
    )

    dStream.saveAsTextFiles("data/streaming")

    // 开始启动应用程序
    ssc.start()
    // 等待停止 会将当前程序阻塞 不会关闭
    ssc.awaitTermination()
    ssc.stop()
  }
}
