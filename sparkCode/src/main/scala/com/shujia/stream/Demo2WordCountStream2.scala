package com.shujia.stream

import org.apache.spark.streaming.dstream.ReceiverInputDStream
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{Durations, StreamingContext}

object Demo2WordCountStream2 {
  def main(args: Array[String]): Unit = {

    /**
     * def this(sparkContext: SparkContext, batchDuration: Duration)  构建 ssc需要给定SparkContext 和间隔的时间
     * 间隔时间给定 5秒 表示5秒形成一批数据
     */
    val conf: SparkConf = new SparkConf().setAppName("wordCount")
      // local 只能获取单个的进程 只有一个cpu  可以替换成local[*](所有空闲的cpu 都可以使用) local[2] 表示选择其中两个
      .setMaster("local[2]")
    val ssc = new StreamingContext(new SparkContext(conf), Durations.seconds(5))

    /**
     * 监听某个端口  存储等级可以使用默认方式  内存磁盘序列化副本为2 存储
     * socketTextStream(
     * hostname: String,
     * port: Int,
     * storageLevel: StorageLevel = StorageLevel.MEMORY_AND_DISK_SER_2
     *
     * ReceiverInputDStream:表示存储的具体一行数据 包装成一个 DStream
     */
    val lineDs: ReceiverInputDStream[String] = ssc.socketTextStream("master", 7777)
    lineDs
      .flatMap {
        case line: String => {
          line.split(" ")
        }
      }
      .map {
        case word: String => (word, 1)
      }
      .reduceByKey(_ + _)
      .print()

    // 开始启动应用程序
    ssc.start()
    // 等待停止 会将当前程序阻塞 不会关闭
    ssc.awaitTermination()
    ssc.stop()
  }
}
