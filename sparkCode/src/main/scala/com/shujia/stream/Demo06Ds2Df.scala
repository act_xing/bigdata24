package com.shujia.stream

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{Durations, StreamingContext}
import org.apache.spark.streaming.dstream.ReceiverInputDStream

object Demo06Ds2Df {
  def main(args: Array[String]): Unit = {
//    val conf: SparkConf = new SparkConf().setAppName("wordCount")
//      // local 只能获取单个的进程 只有一个cpu  可以替换成local[*](所有空闲的cpu 都可以使用) local[2] 表示选择其中两个
//      .setMaster("local[2]")
    val spark: SparkSession = SparkSession
      .builder()
      .appName("sparkSession")
      .master("local[2]")
      .getOrCreate()

    val ssc = new StreamingContext(spark.sparkContext, Durations.seconds(5))

    val lineDs: ReceiverInputDStream[String] = ssc.socketTextStream("master", 7777)


    import spark.implicits._
    import org.apache.spark.sql.functions._

    /**
     * DStream转换成DF 需要通过 foreachRDD/transform 将其转换成RDD 再通过RDD 转换成DF
     *    但是RDD转换成DF 需要导入Spark的隐式转换 所以构建ssc过程中需要先构建SparkSession对象
     */
    lineDs.foreachRDD(
      rdd => {
        val splitDf: DataFrame = rdd
          .toDF()
          .select(explode(split($"value", " ")) as "word")
        splitDf
          .groupBy($"word")
          .agg(count("*") as "num")
          .show()

        splitDf.createOrReplaceTempView("wordTbl")
        spark.sql(
          """
            |SELECT
            |word
            |,count(*) as new_num
            |FROM wordTbl
            |group by word
            |""".stripMargin).show()

      }
    )

    // 开始启动应用程序
    ssc.start()
    // 等待停止 会将当前程序阻塞 不会关闭
    ssc.awaitTermination()
    ssc.stop()
  }
}
