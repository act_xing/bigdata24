package com.shujia.stream

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{Durations, StreamingContext}
import org.apache.spark.streaming.dstream.ReceiverInputDStream

object Demo04Window {
  def main(args: Array[String]): Unit = {

    /**
     * 需求：
     * 每5秒执行一次，处理的数据是最近10秒内的
     */

    val conf: SparkConf = new SparkConf().setAppName("wordCount")
      // local 只能获取单个的进程 只有一个cpu  可以替换成local[*](所有空闲的cpu 都可以使用) local[2] 表示选择其中两个
      .setMaster("local[2]")
    val ssc = new StreamingContext(new SparkContext(conf), Durations.seconds(5))
    ssc.checkpoint("data/ssc/checkpoint2")

    val lineDs: ReceiverInputDStream[String] = ssc.socketTextStream("master", 7777)

    lineDs
      .flatMap {
        case line: String => {
          line.split(" ")
        }
      }
      .map {
        case word: String => (word, 1)
      }

      /**
       * def reduceByKeyAndWindow(
       * reduceFunc: (V, V) => V,
       * windowDuration: Duration, 表示当前窗口的时间区间
       * slideDuration: Duration 表示步长间隔
       * )
       *
       * 为了提高计算的效率：
       *    可以对最近一次步长的时间间隔内的数据进行统计，再去减去上一次统计的结果中第一个步长时间间隔内的数据
       *
    def reduceByKeyAndWindow(
      reduceFunc: (V, V) => V,
      invReduceFunc: (V, V) => V, 该参数表示减去的过程
      windowDuration: Duration,
      slideDuration: Duration = self.slideDuration,
      numPartitions: Int = ssc.sc.defaultParallelism,
      filterFunc: ((K, V)) => Boolean = null
    ): DStream[(K, V)] = ssc.withScope {
       *
       *   需要设置 checkpoint
       *   requirement failed: The checkpoint directory has not been set
       *
       */
      .reduceByKeyAndWindow(
        (num1: Int, num2: Int) => num1 + num2,
        (num1: Int, num2: Int) => num1 - num2,
        Durations.seconds(15), Durations.seconds(5)
      ).print()

    ssc.start()
    // 等待停止 会将当前程序阻塞 不会关闭
    ssc.awaitTermination()
    ssc.stop()
  }
}
