package com.shujia.stream

import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.streaming.{Durations, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}

object Demo3UpdateStata {
  def main(args: Array[String]): Unit = {

    /**
     * def this(sparkContext: SparkContext, batchDuration: Duration)  构建 ssc需要给定SparkContext 和间隔的时间
     * 间隔时间给定 5秒 表示5秒形成一批数据
     */
    val conf: SparkConf = new SparkConf().setAppName("wordCount")
      // local 只能获取单个的进程 只有一个cpu  可以替换成local[*](所有空闲的cpu 都可以使用) local[2] 表示选择其中两个
      .setMaster("local[2]")
    val ssc = new StreamingContext(new SparkContext(conf), Durations.seconds(5))

    ssc.checkpoint("data/ssc/checkpoint")

    val lineDs: ReceiverInputDStream[String] = ssc.socketTextStream("master", 7777)
    val mapResDStream: DStream[(String, Int)] = lineDs
      .flatMap {
        case line: String => {
          line.split(" ")
        }
      }
      .map {
        case word: String => (word, 1)
      }

    /**
     * updateStateByKey 针对每个Key去维护其Value对应的值 对其进行不断的累加 或者其他操作
     *
     * def updateStateByKey[S: ClassTag](
     * updateFunc: (Seq[V], Option[S]) => Option[S]
     * )
     * 其中seq表示获取到的单词的个数的集合
     * op表示上一次历史统计的结果
     *
     * 例如：
     * 第一个5s:
     * 结果计算后得到结果： (hello,2)
     * 第二个5s(map算子)：
     * (hello,1),(hello,1),(spark,1)
     * updateStateByKey=> 对上述相同的Key进行更新状态
     * (hello,1),(hello,1)  => 将 1,1 传入seq 将上一次的结果  (hello,2) 的Value 赋值给 op => 经过函数计算得到 4 返回出去
     * (spark,1) => 将 1 传入seq  将上一次的结果 没有 => op中为None => 经过函数计算得到 1 返回出去
     */
    def updateFun(seq: Seq[Int], op: Option[Int]): Option[Int] = {

      // 根据Some 或 None进行判断
      op match {
        case Some(x) =>
          Option(seq.toList.sum + x)
        case None =>
          Option(seq.toList.sum)
      }

      //      seq.toList.sum + op
    }

    /**
     * requirement failed: The checkpoint directory has not been set
     *  表示updateStateByKey算子需要设置 checkpoint目录 防止程序宕机 历史记录丢失
     */
    mapResDStream
      .updateStateByKey(updateFun)
      .print()

    // 开始启动应用程序
    ssc.start()
    // 等待停止 会将当前程序阻塞 不会关闭
    ssc.awaitTermination()
    ssc.stop()
  }
}
