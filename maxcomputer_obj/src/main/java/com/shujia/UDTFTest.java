package com.shujia;

import com.aliyun.odps.udf.ExecutionContext;
import com.aliyun.odps.udf.UDFException;
import com.aliyun.odps.udf.UDTF;
import com.aliyun.odps.udf.annotation.Resolve;

/**
 *  第1小时消耗 2 度电   2 度电 * 0.5元 得出 第1小时电费为 1元
 *   id    第1小时  第2小时  第3小时 第4小时
 *   1001   2    3       1          5
 *
 *   id    hour consumer
 *   1001   1    1.0
 *   1001   2    1.5
 *   1001   3    0.5
 *   1001   4    2.5
 *
 *
 */
// TODO define input and output types, e.g. "string,string->string,bigint".
@Resolve({"*->bigint,double"})
public class UDTFTest extends UDTF {

//    @Override
//    public void setup(ExecutionContext ctx) throws UDFException {
//
//    }

    @Override
    public void process(Object[] args) throws UDFException {
        // args =>  2    3       1          5
        // 1. 遍历每一列数据
        for (int i = 0; i < args.length; i++) {
            long hour = (long) (i+1);
            // 2.乘以每度电 0.5 元
            double consume =  (long)args[i] * 0.5;
            // 3.组合第n个小时 和 其电费
            forward(hour,consume);
        }
    }

//    @Override
//    public void close() throws UDFException {
//
//    }

}