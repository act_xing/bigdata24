package com.shujia;

import com.aliyun.odps.udf.UDF;
import com.aliyun.odps.udf.annotation.Resolve;

//@Resolve("string->bigint")
public class UDFTest extends UDF {

    public String evaluate(String str){
        return Integer.toString(str.length());
    }

}
